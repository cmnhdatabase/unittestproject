﻿




CREATE VIEW [dbo].[vwMarketRegionCountry]
AS

SELECT mkt.MarketId
	 , mkt.MarketName
	 , mkt.LegacyMarketId
	 , mkt.Active
	 , rgn.RegionId
	 , rgn.RegionName
	 , co.CountryId
	 , co.CountryName
	 , co.Abbreviation
	 , co.PhonePrefix
	 , cu.CurrencyType
	 , cu.CurrencyTypeId
  FROM [dbo].[Markets] AS mkt
	INNER JOIN [dbo].[Regions] AS rgn ON mkt.RegionId = rgn.RegionId
	INNER JOIN [dbo].[Countries] AS co ON mkt.CountryId = co.CountryId
	INNER JOIN [dbo].[Currencies] AS cu ON co.CurrencyTypeId = cu.CurrencyTypeId





