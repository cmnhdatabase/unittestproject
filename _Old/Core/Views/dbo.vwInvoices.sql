﻿
CREATE VIEW dbo.vwInvoices
AS
	SELECT	InvoiceId ,
			InvoiceYear ,
			InvoiceQuarter ,
			InvoiceDate
	FROM	Core.dbo.Invoices;
