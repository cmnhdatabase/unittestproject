﻿
CREATE VIEW [dbo].[vwDMTopIncreases]
AS

SELECT    c.CampaignName , PledgeDate, p.Amount
          FROM      DanceMarathons dm
                    INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
                    INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                    LEFT JOIN PledgeData p ON cd.CampaignDetailsId = p.CampaignDetailsId



