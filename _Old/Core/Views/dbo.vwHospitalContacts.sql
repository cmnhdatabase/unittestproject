﻿
CREATE VIEW [dbo].[vwHospitalContacts]
AS
SELECT DISTINCT 
                         h.HospitalId, h.Address1, h.Address2, 'Hospitals' AS Category, '2' AS CategoryId, h.City, h.HospitalName AS Company, ISNULL(h.Address1, '') + ' ' + ISNULL(h.Address2, '') + ' ' + ISNULL(h.City, '') 
                         + ' ' + ISNULL(p.Abbreviation, '') + ' ' + ISNULL(h.PostalCode, '') AS CompleteAddress, c.CountryId, c.CountryName, fax.Phone AS Fax, '' AS FirstName, h.HospitalName AS FullName, '' AS LastName, 
                         business.Phone, h.PostalCode, p.Abbreviation AS Province, r.RegionName AS SubCategory, sc.SubcategoryId, m.MarketName AS Title, 
                         'https://cdn.cmnhospitals.org/MissionControl/Apps/Contacts/ProfileImages/Hospital.png' AS URI, h.Website AS Email
FROM            dbo.Hospitals AS h LEFT OUTER JOIN
                         dbo.Markets AS m ON m.MarketId = h.MarketId LEFT OUTER JOIN
                         dbo.Regions AS r ON r.RegionId = m.RegionId LEFT OUTER JOIN
                         Contacts.dbo.Subcategories AS sc ON sc.Description = r.RegionName LEFT OUTER JOIN
                         dbo.Provinces AS p ON p.ProvinceId = h.ProvinceId LEFT OUTER JOIN
                         dbo.Countries AS c ON c.CountryId = h.CountryId LEFT OUTER JOIN
                         dbo.HospitalPhoneNumbers AS hp ON hp.HospitalId = h.HospitalId LEFT OUTER JOIN
                         dbo.PhoneNumbers AS fax ON hp.PhoneId = fax.PhoneId AND fax.PhoneTypeId = 2 LEFT OUTER JOIN
                         dbo.PhoneNumbers AS business ON hp.PhoneId = business.PhoneId AND business.PhoneTypeId = 1 LEFT OUTER JOIN
                         dbo.Images AS i ON CAST(h.HospitalId AS varchar(50)) = i.Value AND i.TypeId = 3
WHERE        (h.Active = 1)



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[10] 2[28] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "h"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 283
               Bottom = 136
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "r"
            Begin Extent = 
               Top = 6
               Left = 494
               Bottom = 102
               Right = 664
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sc"
            Begin Extent = 
               Top = 6
               Left = 702
               Bottom = 119
               Right = 872
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 910
               Bottom = 136
               Right = 1080
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 102
               Left = 494
               Bottom = 232
               Right = 667
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hp"
            Begin Extent = 
               Top = 120
               Left = 705
               Bottom = 216
               Right = 875
            End
            DisplayFlags = 280
            TopColumn = 0
         End
 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwHospitalContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        Begin Table = "fax"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "business"
            Begin Extent = 
               Top = 138
               Left = 246
               Bottom = 268
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 188
               Left = 931
               Bottom = 318
               Right = 1101
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwHospitalContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwHospitalContacts';

