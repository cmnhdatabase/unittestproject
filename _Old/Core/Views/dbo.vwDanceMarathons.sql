﻿
-- ==========================================================================================
-- = Update views
-- ==========================================================================================

CREATE VIEW [dbo].[vwDanceMarathons]
AS
	SELECT [DanceMarathonId], [CampaignDetailsId], [SchoolId], [TotalHours], [Shifts], [AnticipatedParticipants],
		[TotalParticipants], [ContactFirstName], [ContactLastName], [ContactEmail], [GrossTotal],
		[ToteBoardImagePath], [EventId], [Note], [SubmittedBy], [SubmittedDate], [ModifiedBy], [ModifiedDate],
		[Active], [ManagerId], [RegistrationUrl], [DonationUrl], [HasMiniMarathons], [MiniMarathonCount],
		[MiniMarathonContribution], [DanceMarathonTypeId], [Nickname]
	FROM [dbo].[DanceMarathons];
