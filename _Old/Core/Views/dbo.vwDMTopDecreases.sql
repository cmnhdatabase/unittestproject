﻿

CREATE VIEW [dbo].[vwDMTopDecreases]
AS

SELECT    c.CampaignName , PledgeDate, p.Amount, ct.CampaignTypeId
          FROM      DanceMarathons dm
                    INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
                    INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                    INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                    LEFT JOIN PledgeData p ON cd.CampaignDetailsId = p.CampaignDetailsId




