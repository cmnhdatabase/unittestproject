﻿

Create VIEW [dbo].[vwLocationAreasDetails]
AS

WITH CTE
AS ( 
SELECT *
FROM(
SELECT l.LocationId, AreaType, Name
FROM  dbo.Locations l
LEFT OUTER JOIN dbo.LocationAreas la ON l.LocationId = la.LocationId
LEFT OUTER JOIN dbo.Areas a ON a.AreaId = la.AreaId
LEFT OUTER JOIN dbo.AreaTypes at ON at.AreaTypeId = a.AreaTypeId
) t
PIVOT (MAX(Name) FOR AreaType IN (Region, Division, Zone, District)) AS test
)

SELECT l.*, Region, Division, Zone, District
FROM dbo.Locations l 
INNER JOIN CTE ON CTE.LocationId = l.LocationId
--WHERE PartnerId =123
--WHERE 
--AND M.marketid = 1
--AND b.Foundation = 1
--AND ((DonationDate <= DATEADD(year, -1, GETDATE()) AND FundraisingYear = 2014-1) OR FundraisingYear = 2014)



