﻿


CREATE VIEW [dbo].[vwRTDResults]
AS
SELECT  fe.Name ,
        m.MarketName ,
        ct.campaigntype ,
        d.amount ,
        l.LocationName ,
        l.LocationNumber ,
        b.BatchNumber ,
        l.Address1 ,
        l.City ,
        p.Abbreviation ,
        l.PostalCode ,
        d.Comment,
		fe.FundraisingEntityId,
		c.CampaignTypeId
FROM    Disbursements d
        INNER JOIN batches b ON d.batchID = b.batchid
        INNER JOIN Markets m ON d.MarketId = m.MarketId
        LEFT JOIN Locations l ON d.LocationId = l.LocationId
        INNER JOIN FundraisingEntities fe ON d.FundraisingEntityId = fe.FundraisingEntityId
        INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
        INNER JOIN campaigns c ON cd.campaignId = c.campaignId
        INNER JOIN campaignTypes ct ON c.campaignTypeId = ct.campaignTypeid
        LEFT JOIN Provinces p ON l.ProvinceId = p.ProvinceId




