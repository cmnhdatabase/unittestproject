﻿



-- =============================================
-- Author:        Darrin Olsen
-- Create date:   2015-09-25
-- =============================================


CREATE  VIEW [dbo].[vwFundraisingDetailsReport]
AS
    SELECT  d.FundraisingYear AS CampaignYear ,
            RTRIM(cu.CurrencyType) AS CurrencyType ,
            RTRIM(ISNULL(pp.SponsorId , '')) AS SponsorID ,
            ISNULL(pp.Name , '') AS SponsorName ,
            '' AS LocationID ,
            M.MarketName AS MarketName ,
            M.MarketName AS ContractHolderID ,
            M.MarketName AS MarketID ,
            ct.CampaignType AS EventID ,
            d.Amount ,
            RTRIM(d.Comment) AS Comment ,
            ISNULL(sub.Division , '') AS Division ,
            ISNULL(sub.Region , '') AS Region ,
            ISNULL(sub.District , '') AS District ,
            ISNULL(sub.Zone , '') AS Zone ,
            ISNULL(l.LocationName , '') AS LocationName ,
            ISNULL(l.Address1 , '') AS LocationAddress1 ,
            ISNULL(l.Address2 , '') AS LocationAddress2 ,
            ISNULL(l.City , '') AS LocationCity ,
            ISNULL(pr.Province , '') AS LocationState ,
            ISNULL(pc.PostalCode , '') AS LocationZipCode ,
            ISNULL(c.CountryName , '') AS LocationCountry ,
            RTRIM(cd.CampaignDetailName) AS EventName ,
            ISNULL(b.BatchNumber , '') AS BatchNumber ,
            d.DirectToHospital AS DirectToHospitalFlag ,
            dp.DisbursementYear ,
            dp.DisbursementQuarter ,
            dd.DisbursementDate ,
            rt.RecordType ,
            d.DonationDate
    FROM    Disbursements d
            INNER JOIN RecordTypes rt ON rt.RecordTypeId = d.RecordTypeId
            INNER JOIN Markets M ON M.MarketId = d.MarketId
            INNER JOIN DisbursementDates dd ON dd.DisbursementDateId = d.DisbursementDateId
            INNER JOIN DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
            INNER JOIN Currencies cu ON cu.CurrencyTypeId = d.CurrencyTypeId
            INNER JOIN dbo.FundraisingEntities pp ON pp.FundraisingEntityId = d.FundraisingEntityId
            INNER JOIN (
                         SELECT l.LocationId ,
                                MAX(CASE WHEN a.AreaTypeId = 1 THEN a.Name
                                    END) AS Region ,
                                MAX(CASE WHEN a.AreaTypeId = 2 THEN a.Name
                                    END) AS Division ,
                                MAX(CASE WHEN a.AreaTypeId = 3 THEN a.Name
                                    END) AS Zone ,
                                MAX(CASE WHEN a.AreaTypeId = 4 THEN a.Name
                                    END) AS District ,
                                MAX(CASE WHEN a.AreaTypeId = 5 THEN a.Name
                                    END) AS Facility
                         FROM   dbo.Locations l
                                JOIN dbo.LocationAreas la ON la.LocationId = l.LocationId
                                JOIN dbo.Areas a ON a.AreaId = la.AreaId
                         GROUP BY l.LocationId
                       ) sub ON sub.LocationId = d.LocationId
            LEFT OUTER JOIN CampaignDetails cd ON cd.CampaignDetailsId = d.CampaignDetailsId
            LEFT OUTER JOIN Campaigns ca ON ca.CampaignId = cd.CampaignId
            LEFT OUTER JOIN CampaignTypes ct ON ct.CampaignTypeId = ca.CampaignTypeId
            LEFT OUTER JOIN Locations l ON l.FundraisingEntityId = d.FundraisingEntityId
                                           AND l.LocationId = d.LocationId
            LEFT OUTER JOIN PostalCodes pc ON pc.PostalCode = l.PostalCode
            LEFT OUTER JOIN Provinces pr ON pr.ProvinceId = pc.ProvinceId
            LEFT OUTER JOIN Countries c ON c.CountryId = pr.CountryId
            LEFT OUTER JOIN Batches b ON b.BatchId = d.BatchId
    WHERE   rt.RecordType = 'D'
    UNION ALL
    SELECT  Pd.FundraisingYear AS CampaignYear ,
            cu.CurrencyType ,
            RTRIM(ISNULL(pp.SponsorId , '')) AS SponsorID ,
            ISNULL(pp.Name , '') AS SponsorName ,
            '' AS LocationID ,
            M.MarketName AS MarketName ,
            M.MarketName AS ContractHolderID ,
            M.MarketName AS MarketID ,
            ct.CampaignType AS EventID ,
            Pd.Amount ,
            '' AS Comment ,
            '' AS Division ,
            '' AS Region ,
            '' AS District ,
            '' AS Zone ,
            '' AS LocationName ,
            '' AS LocationAddress1 ,
            '' AS LocationAddress2 ,
            '' AS LocationCity ,
            '' AS LocationState ,
            '' AS LocationZipCode ,
            '' AS LocationCountry ,
            '' AS EventName ,
            '' AS BatchNumber ,
            CASE WHEN pt.PledgeType = 'HospitalRadiothonAnnouncedTotal' THEN 1
                 ELSE 0
            END AS DirectToHospitalFlag ,
            '' AS DisbursementYear ,
            '' AS DisbursementQuarter ,
            '' AS DisbursementDate ,
            'D' AS RecordType ,
            Pd.PledgeDate                     -- pd.DonationDate       
    FROM    PledgeData Pd
            INNER JOIN PledgeTypes pt ON pt.PledgeTypeId = Pd.PledgeTypeId
            INNER JOIN Markets M ON M.MarketId = Pd.MarketId
            INNER JOIN Currencies cu ON cu.CurrencyTypeId = Pd.CurrencyTypeId
            INNER JOIN dbo.FundraisingEntities pp ON pp.FundraisingEntityId = Pd.FundraisingEntityId
            LEFT OUTER JOIN CampaignDetails cd ON cd.CampaignDetailsId = Pd.CampaignDetailsId
            LEFT OUTER JOIN Campaigns ca ON ca.CampaignId = cd.CampaignId
            LEFT OUTER JOIN CampaignTypes ct ON ct.CampaignTypeId = ca.CampaignTypeId;



