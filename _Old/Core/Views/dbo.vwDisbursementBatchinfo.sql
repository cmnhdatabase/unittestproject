﻿


CREATE VIEW [dbo].[vwDisbursementBatchinfo]
AS

SELECT        dbd.BatchTypeId, dbd.BatchType, dbd.Abbreviation, dbd.BatchId, dbd.BatchNumber, dbd.Reconciled, dbd.DateReconciled, dbd.Foundation, dbd.RecordTypeId, dbd.RecordType, dbd.DisbursementId, dbd.MarketId, 
                         dbd.SubMarketId, dbd.DirectToHospital, dbd.FundraisingYear, dbd.CurrencyTypeId, dbd.CurrencyType, CASE WHEN FundraisingCategoryId = 0 THEN (dbd.Amount * - 1) ELSE dbd.Amount END AS Amount, 
                         dbd.CampaignDetailsId, dbd.LocationId, dbd.DateReceived, dbd.DateRecorded, dbd.DonationDate, dbd.FundTypeId, dbd.FundType, dbd.CampaignPeriod, dbd.UploadId, dbd.Comment, dbd.DisbursementDate, 
                         dbd.DisbursementYear, dbd.DisbursementQuarter, dfe.FundraisingEntityId, dfe.FundraisingCategoryId
FROM            dbo.vwDisbursementBatchDetails AS dbd LEFT OUTER JOIN
                         dbo.vwDisbursements AS dfe ON dfe.DisbursementId = dbd.DisbursementId


