﻿
CREATE VIEW [dbo].[vwLanguages]
AS
	SELECT [LanguageId], [Language], [Abbreviation]
	FROM [dbo].[Languages]


