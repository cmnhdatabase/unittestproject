﻿
CREATE VIEW [dbo].[vwRegions]
AS
	SELECT [RegionId], [RegionName]
	FROM [dbo].[Regions]
	WHERE [RegionId] > 0


