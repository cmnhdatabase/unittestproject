﻿
CREATE VIEW [dbo].[vwRadiothons]
AS
	SELECT [RadiothonId], [CampaignDetailsId], [City], [OnAirHours], [OnAirMinutes], [LanguageId],
		[PhoneBank], [Note], [SubmittedBy], [SubmittedDate], [ModifiedBy], [ModifiedDate], [Active],
		[EventId], [RadiothonContactEmailSent], [ToteBoardImagePath]
	FROM [dbo].[Radiothons];
