﻿


CREATE VIEW [dbo].[vwReMaxOffice]
AS
SELECT
OfficeIDName 
, LEFT(OfficeIDName, 4) AS RemaxRegion
, OfficeID
, OfficeName
, r.RegionName
, o.Address1
, o.Address2
, o.City
, StateProvince
, ZipPostalCode
, Country
, Phone
, o.Active
, m.MarketId
, m.MarketName
, FundraisingYear
, d.DonationDate
, Amount
, cd.CampaignDetailName
,h.hospitalName
,d.FundraisingEntityId

FROM core.dbo.Disbursements d 
INNER JOIN core.dbo.Locations l ON l.LocationId = d.LocationId
INNER JOIN MiracleSystem.dbo.Office o ON o.OfficeIDName = l.LocationNumber
INNER JOIN core.dbo.Markets m ON m.MarketId = o.MarketID
INNER JOIN core.dbo.Regions r ON r.RegionId = m.RegionId
INNER JOIN core.dbo.CampaignDetails cd ON cd.CampaignDetailsId = d.CampaignDetailsId
INNER JOIN core.dbo.Hospitals h ON h.MarketId = o.MarketID




