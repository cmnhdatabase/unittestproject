﻿
/****** Object:  View [dbo].[vwRadiothonRadioStations] ******/
CREATE VIEW [dbo].[vwRadiothonRadioStations]
AS
	SELECT [RadiothonId], [RadioStationId], [PrimaryRadioStation]
	FROM [dbo].[RadiothonRadioStations];


