﻿
CREATE VIEW [dbo].[vwFundraisingEntities]
AS
  SELECT [FundraisingEntityId], [Name], [FriendlyName], [DisplayName], [YearStarted], [FundraisingCategoryId], [Website], 
    [Active], [SponsorId], [CountryId]
  FROM [dbo].[FundraisingEntities];
