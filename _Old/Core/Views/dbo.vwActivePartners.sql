﻿
/**********************************************************************************************/
/* Gets a list of all Active Fundraising Entities with a FundraisingCategoryId of 1 (Partner) */
/* This view is used by Cmnh.Data.Core to get a full list of Partner Information.             */
/* View was created because there was not a partner only view that previously existed.        */
/**********************************************************************************************/
CREATE VIEW [vwActivePartners]
AS
SELECT fe.[FundraisingEntityId], fe.[Name], fe.[FriendlyName], fe.[DisplayName], fe.[YearStarted],
       c.[CountryId], c.[Abbreviation] AS 'Country', fe.[Website], fc.[FundraisingCategory], fe.[SalesForceId], fe.[SponsorId], fe.[Active]
FROM [dbo].[FundraisingEntities] fe
INNER JOIN [dbo].[FundraisingCategories] fc ON fc.[FundraisingCategoryId] = fe.[FundraisingCategoryId]
INNER JOIN [dbo].[Countries] c ON c.[CountryId] = fe.[CountryId]
WHERE fe.[FundraisingCategoryId] = 1
AND fe.[Active] = 1;
