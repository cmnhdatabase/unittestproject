﻿

CREATE VIEW [dbo].[vwRTDLookup] AS
SELECT r.RTDLookupId ,
       r.RTDLookup ,
       rt.RTDLookupTypeId ,
       rt.RTDLookupType
FROM dbo.RTDLookup r INNER JOIN dbo.RTDLookupType rt ON r.RTDLookupTypeId = rt.RTDLookupTypeId
