﻿CREATE VIEW [dbo].[vwMarkets]
AS
	SELECT [MarketId], [MarketName], [CountryId], [RegionId], [Active]
	FROM [dbo].[Markets]
	WHERE [Active] = 1


