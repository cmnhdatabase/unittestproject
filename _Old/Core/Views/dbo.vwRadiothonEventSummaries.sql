﻿


CREATE VIEW [dbo].[vwRadiothonEventSummaries]
AS
SELECT        c.[CampaignId], c.[CampaignName], cd.[CampaignDetailsId], cd.[CampaignDetailName], cd.[StartDate], cd.[EndDate], r.[RadiothonId], r.[City], r.[OnAirHours], r.[OnAirMinutes], r.[LanguageId], r.[PhoneBank], r.[Note], 
                         r.[SubmittedBy], r.[SubmittedDate], r.[ModifiedBy], r.[ModifiedDate], r.[Active], r.[EventId], l.[Language], rs.[Director] AS [DirectorId], co.[FirstName] AS [DirectorFirstName], co.[LastName] AS [DirectorLastName],
                             (SELECT        SUM([Amount])
                               FROM            [dbo].[PledgeData] pd
                               WHERE        pd.[CampaignDetailsId] = cd.[CampaignDetailsId] AND pd.[PledgeTypeId] = 4) AS [GoalAmount],
                             (SELECT        SUM([Amount])
                               FROM            [dbo].[PledgeData] pd
                               WHERE        pd.[CampaignDetailsId] = cd.[CampaignDetailsId] AND pd.[PledgeTypeId] = 3) AS [AnnouncedTotal],
                             (SELECT        [CallLetters] + ' ' + ISNULL([Owner], '')
                               FROM            [dbo].[RadiothonRadioStations] rss INNER JOIN
                                                         [dbo].[RadioStations] rs ON rss.[RadioStationId] = rs.[RadioStationId]
                               WHERE        rss.[RadiothonId] = r.[RadiothonId] AND rs.[Active] = 1 FOR XML PATH('')) AS [CallLettersAndOwners]
FROM            [dbo].[Campaigns] c INNER JOIN
                         [dbo].[CampaignDetails] cd ON c.[CampaignId] = cd.[CampaignId] INNER JOIN
                         [dbo].[Radiothons] r ON cd.[CampaignDetailsId] = r.[CampaignDetailsId] INNER JOIN
                         [dbo].[Languages] l ON r.[LanguageId] = l.[LanguageId] LEFT OUTER JOIN
                             (SELECT        [RadiothonId], [RadioStationId]
                               FROM            [dbo].[RadiothonRadioStations]
                               WHERE        [PrimaryRadioStation] = 1) rss ON r.[RadiothonId] = rss.[RadiothonId] LEFT OUTER JOIN
                             (SELECT        [RadioStationId], [Director]
                               FROM            [dbo].[RadioStations]
                               WHERE        [Active] = 1) rs ON rss.[RadioStationId] = rs.[RadioStationId] LEFT OUTER JOIN
                             (SELECT        [ContactId], [FirstName], [LastName]
                               FROM            [Contacts].[dbo].[Contacts]
                               WHERE        [Active] = 1) co ON rs.[Director] = co.[ContactId]
WHERE        r.[Active] = 1 AND cd.[StartDate] IS NOT NULL AND cd.[EndDate] IS NOT NULL;


