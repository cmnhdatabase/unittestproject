﻿
/**********************************************************************************************/
/* Gets a list of all active Location Areas.  This view was created to add additional fields  */
/* that didn't exist in the existing vwLocationAreasDetails.  This is also a more performant  */
/* method because it filters by only Active locations                                         */
/**********************************************************************************************/
CREATE VIEW [vwActiveLocationAreas]
AS
SELECT l.[LocationId], l.[FundraisingEntityId], l.[LocationName], l.[LocationNumber], l.[Address1], l.[Address2], l.[City], 
	   l.[ProvinceId], pr.[Abbreviation] AS 'Province', l.[PostalCode], l.[CountryId], c.[Abbreviation] AS 'Country', 
	   pt.[PropertyTypeId], pt.[PropertyType], l.[Latitude], l.[Longitude], p.[Region], p.[Division], p.[Zone], p.[District]
FROM [dbo].[Locations] l 
INNER JOIN [dbo].[Provinces] pr ON pr.[ProvinceId] = l.[ProvinceId]
INNER JOIN [dbo].[Countries] c ON c.[CountryId] = l.[CountryId]
INNER JOIN [dbo].[PropertyTypes] pt ON pt.[PropertyTypeId] = l.[PropertyTypeId]
INNER JOIN 
(
	SELECT [LocationId], [Region], [Division], [Zone], [District]
	FROM(
		SELECT l.[LocationId], aty.[AreaType], a.[Name]
		FROM  dbo.[Locations] l
		LEFT OUTER JOIN [dbo].[LocationAreas] la ON l.[LocationId] = la.[LocationId]
		LEFT OUTER JOIN [dbo].[Areas] a ON a.[AreaId] = la.[AreaId]
		LEFT OUTER JOIN [dbo].[AreaTypes] aty ON aty.[AreaTypeId] = a.[AreaTypeId]
	) t
	PIVOT (MAX([Name]) FOR [AreaType] IN (Region, Division, Zone, District)) AS p
) p ON p.[LocationId] = l.[LocationId]
WHERE l.[Active] = 1;
