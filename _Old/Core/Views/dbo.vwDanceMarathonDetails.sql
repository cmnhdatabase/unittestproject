﻿
/**********************************************************************************************/
/* Gets a list of all Dance Marathons.  This view was created because there was no existing   */
/* view that gave this level of detail for a Dance Marathon.                                  */
/**********************************************************************************************/
CREATE VIEW [vwDanceMarathonDetails]
AS
SELECT d.[DanceMarathonId], d.[TotalHours], d.[Shifts], d.[AnticipatedParticipants], d.[TotalParticipants], 
	   d.[ContactFirstName], d.[ContactLastName], d.[ContactEmail], d.[GrossTotal], d.[ToteBoardImagePath], 
	   d.[Note], d.[RegistrationUrl], d.[DonationUrl], d.[HasMiniMarathons], d.[MiniMarathonCount], 
	   d.[MiniMarathonContribution], d.[DanceMarathonTypeId], dt.[DanceMarathonType], d.[Nickname], 
	   d.[ManagerId], d.[SubmittedBy], d.[SubmittedDate], d.[ModifiedBy], d.[ModifiedDate], 
	   d.[Active] AS [DanceMarathonActive], d.[CampaignDetailsId], cd.[CampaignDetailName], cd.[StartDate], 
	   cd.[EndDate], cd.[CampaignId], c.[CampaignName], d.[SchoolId], s.[SchoolName], 
	   s.[Address1] AS [SchoolAddress1], s.[Address2] AS [SchoolAddress2], s.[City] AS [SchoolCity], 
	   s.[ProvinceId] AS [SchoolProvinceId], p1.[Province] AS [SchoolProvince], 
	   s.[PostalCode] AS [SchoolPostalCode], s.[CountryId] AS [SchoolCountryId], 
	   ct1.[Abbreviation] AS [SchoolCountry], s.[Active] AS [SchoolActive], h.[HospitalId], h.[HospitalName], 
	   h.[Address1] AS [HospitalAddress1], h.[Address2] AS [HospitalAddress2], h.[City] AS [HospitalCity], 
	   m.[MarketId], m.[MarketName], h.[ProvinceId] AS [HospitalProvinceId], 
	   p2.[Abbreviation] AS [HosptialProvince], h.[CountryId] AS [HospitalCountryId], 
	   ct1.[Abbreviation] AS [HospitalCountry], h.[Active] AS [HospitalActive]
FROM [dbo].[DanceMarathons] d
INNER JOIN [dbo].[DanceMarathonTypes] dt ON dt.[DanceMarathonTypeId] = d.[DanceMarathonTypeId]
INNER JOIN [dbo].[CampaignDetails] cd ON cd.[CampaignDetailsId] = d.[CampaignDetailsId]
INNER JOIN [dbo].[Campaigns] c ON c.[CampaignId] = cd.[CampaignId]
LEFT OUTER JOIN [dbo].[Schools] s ON s.[SchoolId] = d.[SchoolId]
LEFT OUTER JOIN [dbo].[DanceMarathonHospitals] dh ON dh.[DanceMarathonId] = d.[DanceMarathonId]
LEFT OUTER JOIN [dbo].[Hospitals] h ON h.[HospitalId] = dh.[HospitalId]
LEFT OUTER JOIN [dbo].[Markets] m ON m.[MarketId] = h.[MarketId]
LEFT OUTER JOIN [dbo].[Provinces] p1 ON p1.[ProvinceId] = s.[ProvinceId]
LEFT OUTER JOIN [dbo].[Provinces] p2 ON p2.[ProvinceId] = h.[ProvinceId]
LEFT OUTER JOIN [dbo].[Countries] ct1 ON ct1.[CountryId] = s.[CountryId]
LEFT OUTER JOIN [dbo].[Countries] ct2 ON ct2.[CountryId] = h.[CountryId]
