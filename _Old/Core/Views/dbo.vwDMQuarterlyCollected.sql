﻿

CREATE VIEW [dbo].[vwDMQuarterlyCollected]
AS

SELECT    Quarter, d.Amount, d.FundraisingYear
FROM    DanceMarathons dm
        INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
        INNER JOIN PledgeData pd ON pd.CampaignDetailsId = cd.CampaignDetailsId
        INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
        LEFT JOIN Disbursements d ON cd.CampaignDetailsId = d.CampaignDetailsId



