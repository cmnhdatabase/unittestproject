﻿

CREATE VIEW [dbo].[vwDMNewSchoolsTotals]
AS
    SELECT  SchoolName, p.Amount, dm.TotalParticipants, p.FundraisingYear
    FROM      DanceMarathons dm
        INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
        INNER JOIN PledgeData p ON cd.CampaignDetailsId = p.CampaignDetailsId
        INNER JOIN Schools s ON dm.SchoolId = s.SchoolId
WHERE   s.SchoolId IN ( SELECT  schoolid
                                  FROM      DanceMarathons
                                  GROUP BY  schoolid
                                  HAVING    COUNT(*) = 1
                                )



