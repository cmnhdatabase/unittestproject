﻿



CREATE VIEW [dbo].[vwDMNewSchools]
AS
    SELECT  p.amount, dm.TotalParticipants, SchoolId, p.FundraisingYear
    FROM      DanceMarathons dm
        INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
        INNER JOIN PledgeData p ON cd.CampaignDetailsId = p.CampaignDetailsId
		WHERE SchoolId IN ( SELECT schoolid
                                  FROM      dbo.DanceMarathons
                                  GROUP BY  schoolid
                                  HAVING    COUNT(*) = 1
								  )




