﻿
CREATE VIEW [dbo].[vwRCTelethons]
AS
WITH	PledgeDataCTE
		  AS (
			   SELECT	c.CountryId ,
						c.CountryName ,
						tv.TVStationId ,
						t.TelethonId ,
						pd.Amount ,
						cd.CampaignYear ,
						pd.Quarter ,
						cd.StartDate ,
						MONTH(cd.StartDate) StartMonth
			   FROM		Core.dbo.TV tv
			   JOIN		Core.dbo.TelethonTvStations tts ON tts.TvStationId = tv.TVStationId
			   JOIN		Core.dbo.Telethons t ON t.TelethonId = tts.TelethonId
			   JOIN		Core.dbo.CampaignDetails cd ON cd.CampaignDetailsId = t.CampaignDetailsId
			   LEFT JOIN		Core.dbo.PledgeData pd ON pd.CampaignDetailsId = t.CampaignDetailsId
												  AND pd.PledgeTypeId = 7
			   JOIN		Core.dbo.Countries c ON c.CountryId = tv.CountryId
			   WHERE	tts.PrimaryStation = 1
			 ),
		Months_CTE
		  AS (
			   SELECT	*
			   FROM		( VALUES ( '1', 'January', '1') , ( '2', 'February', '1') , ( '3', 'March', '1') , ( '4', 'April', '2') , ( '5', 'May', '2') , ( '6', 'June', '2') , ( '7', 'July', '3') , ( '8', 'August', '3') , ( '9', 'Sepetember', '3') , ( '10', 'October', '4') , ( '11', 'November', '4') , ( '12', 'December', '4') ) a ( Number , Month , Quarter )
			 )
	SELECT DISTINCT
			m.Number ,
			m.Month ,
			m.Quarter ,
			mk.MarketId ,
			mk.MarketName ,
			tv.City + ', ' + pv.Abbreviation AS MarketArea ,
			tv.StationOwner AS Owner ,
			CASE WHEN mk.CountryId = '2' THEN 'Telethon - Canada Markets'
				 WHEN mk.CountryId = '1' THEN 'Telethon - US Markets'
				 ELSE NULL
			END AS MarketGroup ,
			CASE WHEN pp.CampaignYear IS NOT NULL THEN 'Yes'
				 ELSE 'No'
			END AS Repeating ,
			co.FirstName ,
			co.LastName ,
			cd.CampaignDetailName ,
			cd.StartDate ,
			cd.EndDate ,
			tv.CallLetters ,
			t.Note ,
			tv.Active ,
			cd.CampaignDetailsId ,
			cd.CampaignId ,
			p.TelethonId ,
			p.CountryId ,
			p.CountryName ,
			p.TVStationId ,
			pp.Amount AS Matched ,
			p.Amount AS Total ,
			p.CampaignYear
	FROM	Months_CTE m
	LEFT JOIN PledgeDataCTE p ON p.StartMonth = m.Number
	LEFT JOIN PledgeDataCTE pp ON pp.TVStationId = p.TVStationId
								  AND pp.StartMonth = p.StartMonth
								  AND pp.CampaignYear = p.CampaignYear - 1
	LEFT JOIN Core.dbo.TV tv ON tv.TVStationId = p.TVStationId
	LEFT JOIN Core.dbo.Telethons t ON t.TelethonId = p.TelethonId
	LEFT JOIN Core.dbo.Markets mk ON mk.MarketId = tv.MarketId
	LEFT JOIN Core.dbo.Provinces pv ON pv.ProvinceId = tv.ProvinceId
	LEFT JOIN Core.dbo.Countries ct ON ct.CountryId = pv.CountryId
	LEFT JOIN Core.dbo.CampaignDetails cd ON t.CampaignDetailsId = cd.CampaignDetailsId
	LEFT JOIN Core.dbo.Languages l ON l.LanguageId = t.LanguageId
	LEFT JOIN Contacts.dbo.Contacts co ON tv.Director = co.ContactId
