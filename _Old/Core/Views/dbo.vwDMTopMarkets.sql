﻿


CREATE VIEW [dbo].[vwDMTopMarkets]
AS
    SELECT  m.MarketName, p.PledgeDate, p.Amount, c.CampaignName, m.RegionId
    FROM   DanceMarathons dm
                    INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
                    INNER JOIN PledgeData p ON cd.CampaignDetailsId = p.CampaignDetailsId
                    INNER JOIN Markets m ON p.MarketId = m.MarketId
                    INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId




