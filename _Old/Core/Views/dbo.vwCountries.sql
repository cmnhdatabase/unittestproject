﻿
CREATE VIEW [dbo].[vwCountries]
AS
	SELECT [CountryId], [CountryName], [Abbreviation], [CurrencyTypeId], [PhonePrefix]
	FROM [dbo].[Countries]


