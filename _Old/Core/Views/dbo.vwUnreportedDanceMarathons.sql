﻿
CREATE VIEW [dbo].[vwUnreportedDanceMarathons]
AS
SELECT DISTINCT 
                         CampaignId, CampaignName, CampaignDetailsId, CampaignDetailName, CampaignDetailName AS ShortDescription, YEAR(EndDate) AS CampaignYear, StartDate, EndDate, DanceMarathonId, 
                         ManagerContactId AS Director
FROM            dbo.vwDanceMarathonEventSummaries AS s
WHERE        (AnnouncedTotal IS NULL) AND (DATEADD(dd, 0, DATEDIFF(dd, 0, EndDate)) < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))) AND (YEAR(EndDate) >= 2014)

