﻿

CREATE VIEW [dbo].[vwDanceMarathonLookup]
AS
	SELECT	cd.CampaignDetailName ,
			s.SchoolName ,
			cd.StartDate ,
			p.Abbreviation AS Province ,
			s.PostalCode ,
			d.Active ,
			d.RegistrationUrl ,
			d.DonationUrl ,
			m.MarketId
	FROM	dbo.DanceMarathons AS d
	INNER JOIN dbo.Schools AS s ON s.SchoolId = d.SchoolId
	INNER JOIN dbo.Provinces p ON p.ProvinceId = s.ProvinceId
	INNER JOIN dbo.CampaignDetails AS cd ON cd.CampaignDetailsId = d.CampaignDetailsId
	INNER JOIN dbo.DanceMarathonHospitals dmh ON dmh.DanceMarathonId = d.DanceMarathonId
	INNER JOIN dbo.Hospitals h ON h.HospitalId = dmh.HospitalId
	INNER JOIN dbo.Markets m ON m.MarketId = h.MarketId;

