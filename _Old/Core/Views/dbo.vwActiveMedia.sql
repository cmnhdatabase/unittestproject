﻿
/**********************************************************************************************/
/* Gets a list of all active Media (Tv & Radio).  This view because there was no existing     */
/* that got a combination of TV & Radio data                                                  */
/**********************************************************************************************/
CREATE VIEW [vwActiveMedia]
AS
SELECT r.[CallLetters], r.[Address1], r.[Address2], r.[City], r.[ProvinceId], p.[Abbreviation] AS 'Province', r.[PostalCode], 
		c.[CountryId], c.[Abbreviation] AS 'Country', r.[Website], r.[MarketTag], m.[MarketId], m.[MarketName], 'Radio' AS 'MediaType'
FROM [dbo].[Radio] r
INNER JOIN [dbo].[Provinces] p ON p.[ProvinceId] = r.[ProvinceId]
INNER JOIN [dbo].[Countries] c ON c.[CountryId] = r.[CountryId]
INNER JOIN [dbo].[Markets] m ON m.[MarketId] = r.[MarketId]
WHERE r.[Active] = 1
AND r.[ProvinceId] IS NOT NULL

UNION

SELECT t.[CallLetters], t.[Address1], t.[Address2], t.[City], p.[ProvinceId], p.[Abbreviation] AS 'Province', t.[PostalCode], 
		c.[CountryId], c.[Abbreviation] AS 'Country', t.[Website], t.[MarketTag], m.[MarketId], m.[MarketName], 'TV' AS 'MediaType'
FROM [dbo].[TV] t
INNER JOIN [dbo].[Provinces] p ON p.[ProvinceId] = t.[ProvinceId]
INNER JOIN [dbo].[Countries] c ON c.[CountryId] = t.[CountryId]
INNER JOIN [dbo].[Markets] m ON m.[MarketId] = t.[MarketId]
WHERE t.[Active] = 1
AND t.[ProvinceId] IS NOT NULL;
