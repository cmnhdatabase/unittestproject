﻿


CREATE VIEW [dbo].[vwRCRadiothons]
AS
	SELECT	MarketId ,
			MarketName ,
			MarketArea ,
			MarketGroup ,
			MarketTag ,
			Country ,
			MAX(RadiothonId) AS RadiothonId ,
			CallLetters ,
			t.Owner ,
			MAX(CampaignDetailName) AS CampaignDetailName ,
			t.CampaignId ,
			CampaignName ,
			LanguageId ,
			Language ,
			FirstName ,
			LastName ,
			MAX(StartDate) AS StartDate ,
			MAX(EndDate) AS EndDate ,
			PrevAmount ,
			SUM(CASE WHEN PledgeTypeId = 3 THEN Amount
					 ELSE 0
				END) AS Amount ,
			Repeating ,
			FundraisingYear ,
			CampaignYear ,
			MAX(CampaignQuarter) AS CampaignQuarter ,
			PrevCampaignYear ,
			t1.Note
	FROM	(
			  SELECT	rs.MarketTag ,
						p.PledgeTypeId ,
						m.MarketId ,
						m.MarketName ,
						rs.MarketArea ,
						rs.Owner ,
						CASE WHEN CountryName = 'Canada' THEN 'Canada Markets'
							 WHEN [Language] = 'Spanish' THEN 'Radiothon - Hispanic Market'
							 ELSE 'Radiothon - General Market'
						END AS MarketGroup ,
						mrc.Abbreviation AS Country ,
						r.RadiothonId ,
						rs.CallLetters ,
						cd.CampaignDetailName ,
						c.CampaignId ,
						c.CampaignName ,
						l.LanguageId ,
						l.Language ,
						co.FirstName ,
						co.LastName ,
						cd.StartDate ,
						cd.EndDate ,
						prev.Amount AS PrevAmount ,
						p.Amount ,
						CASE WHEN nextyr.CampaignYear IS NULL THEN 'No'
							 ELSE 'Yes'
						END AS Repeating ,
						p.FundraisingYear ,
						cd.CampaignYear ,
						DATEPART(q , cd.StartDate) AS CampaignQuarter ,
						prev.CampaignYear AS PrevCampaignYear ,
						r.Note
			  FROM		dbo.Radiothons AS r
			  INNER JOIN dbo.RadiothonRadioStations AS rrs ON r.RadiothonId = rrs.RadiothonId
			  INNER JOIN dbo.RadioStations AS rs ON rrs.RadioStationId = rs.RadioStationId
			  LEFT OUTER JOIN dbo.CampaignDetails AS cd ON r.CampaignDetailsId = cd.CampaignDetailsId
			  LEFT OUTER JOIN dbo.Campaigns AS c ON cd.CampaignId = c.CampaignId
			  LEFT OUTER JOIN dbo.CampaignTypes AS ct ON c.CampaignTypeId = ct.CampaignTypeId
			  LEFT OUTER JOIN dbo.PledgeData AS p ON cd.CampaignDetailsId = p.CampaignDetailsId
			  LEFT OUTER JOIN dbo.PledgeTypes AS pt ON p.PledgeTypeId = pt.PledgeTypeId
			  LEFT OUTER JOIN --MissionControl.dbo.Users AS u ON rs.Director = u.UserId LEFT OUTER JOIN
						Contacts.dbo.Contacts AS co ON rs.Director = co.ContactId
			  LEFT OUTER JOIN dbo.Markets AS m ON rs.MarketId = m.MarketId
			  INNER JOIN dbo.Languages AS l ON r.LanguageId = l.LanguageId
			  INNER JOIN dbo.vwMarketRegionCountry AS mrc ON m.MarketId = mrc.MarketId
			  LEFT OUTER JOIN (
								SELECT DISTINCT
										rs2.CallLetters ,
										cd.CampaignYear ,
										SUM(p.Amount) AS Amount
								FROM	dbo.RadioStations AS rs
								INNER JOIN dbo.RadiothonRadioStations AS rrs ON rs.RadioStationId = rrs.RadioStationId
								INNER JOIN dbo.Radiothons AS r ON rrs.RadiothonId = r.RadiothonId
								INNER JOIN dbo.RadiothonRadioStations AS rrs2 ON r.RadiothonId = rrs2.RadiothonId
								INNER JOIN dbo.RadioStations AS rs2 ON rrs2.RadioStationId = rs2.RadioStationId
								INNER JOIN dbo.CampaignDetails AS cd ON r.CampaignDetailsId = cd.CampaignDetailsId
								INNER JOIN dbo.PledgeData AS p ON cd.CampaignDetailsId = p.CampaignDetailsId
								WHERE	( cd.StartDate IS NOT NULL )
										AND ( p.PledgeTypeId = 3 )
										AND ( r.Active = 1 )
										AND ( rs.Active = 1 )
										AND ( cd.StartDate IS NOT NULL )
										AND ( rrs.PrimaryRadioStation = 1 )
										AND ( cd.StartDate <= DATEADD(wk , DATEDIFF(wk , 5 , GETDATE()) , 5) )
								GROUP BY rs2.CallLetters ,
										cd.CampaignYear
							  ) AS prev ON rs.CallLetters = prev.CallLetters
										   AND cd.CampaignYear = prev.CampaignYear + 1
			  LEFT OUTER JOIN (
								SELECT	cd.CampaignId ,
										cd.CampaignYear
								FROM	dbo.Radiothons AS r
								INNER JOIN dbo.CampaignDetails AS cd ON r.CampaignDetailsId = cd.CampaignDetailsId
								WHERE	( cd.StartDate IS NOT NULL )
								GROUP BY cd.CampaignId ,
										cd.CampaignYear
							  ) AS nextyr ON c.CampaignId = nextyr.CampaignId
											 AND cd.CampaignYear = nextyr.CampaignYear + 1
			  WHERE		( r.Active = 1 )
						AND ( rs.Active = 1 )
						AND ( cd.StartDate IS NOT NULL )
						AND ( rrs.PrimaryRadioStation = 1 )
			) AS t
	LEFT OUTER JOIN (
					  SELECT	CampaignId ,
								MAX(Note1) AS Note
					  FROM		(
								  SELECT	c.CampaignId ,
											Note1 = STUFF((
															SELECT	', ' + Note
															FROM	dbo.Radiothons r
															INNER JOIN dbo.CampaignDetails c1 ON r.CampaignDetailsId = c1.CampaignDetailsId
															WHERE	c1.CampaignId = c.CampaignId
																	AND c1.CampaignYear = c.CampaignYear
														  FOR
															XML	PATH('')
														  ) , 1 , 2 , '')
								  FROM		dbo.Radiothons r
								  INNER JOIN dbo.CampaignDetails c ON r.CampaignDetailsId = c.CampaignDetailsId/*AND CampaignYear = 2014*/
								) AS t
					  GROUP BY	CampaignId
					) AS t1 ON t.CampaignId = t1.CampaignId
	GROUP BY MarketId ,
			MarketName ,
			MarketArea ,
			MarketGroup ,
			Country ,
			CallLetters ,
			t.Owner ,
			t.CampaignId ,
			CampaignName ,
			LanguageId ,
			Language ,
			FirstName ,
			LastName ,
			PrevAmount ,
			Repeating ,
			FundraisingYear ,
			CampaignYear ,
			PrevCampaignYear ,
			t1.Note ,
			MarketTag;




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[18] 4[22] 2[42] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 26
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCRadiothons';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCRadiothons';

