﻿
CREATE VIEW [dbo].[vwRCMiracleSystem]
AS
SELECT        TOP (100) PERCENT AssociateID, Title, LastName, FirstName, Email, OfficeID, OfficeName, CAST(Date AS DATE) AS Date, Address, Address2, City, StateProvince, ZipPostalCode, '$' + CONVERT(VARCHAR(50), 
                         CAST(SUM(Amount) AS MONEY), - 1) AS Amount, MarketID AS MarketId
FROM            (SELECT        a.AssociateIDName AS AssociateID, a.Title, a.LastName, a.FirstName, a.Email, o.OfficeIDName AS OfficeID, o.OfficeName, d.Date, o.Address1 AS Address, o.Address2, o.City, o.StateProvince, 
                                                    o.ZipPostalCode, SUM(d.Amount) AS Amount, o.MarketID
                          FROM            MiracleSystem.dbo.Donation AS d INNER JOIN
                                                    MiracleSystem.dbo.Office AS o ON d.OfficeID = o.OfficeID LEFT OUTER JOIN
                                                    MiracleSystem.dbo.Associate AS a ON d.AssociateID = a.AssociateID
                          WHERE        (YEAR(d.Date) = 2010)
                          GROUP BY a.Title, a.AssociateIDName, a.LastName, a.FirstName, a.Email, o.OfficeIDName, o.OfficeName, o.Address1, o.Address2, o.City, o.StateProvince, o.ZipPostalCode, o.MarketID, d.Date
                          UNION ALL
                          SELECT        a.AssociateIDName AS AssociateID, a.Title, a.LastName, a.FirstName, a.Email, o.OfficeIDName AS OfficeID, o.OfficeName, v.BatchDate, o.Address1 AS Address, o.Address2, o.City, o.StateProvince, 
                                                   o.ZipPostalCode, SUM(vr.Amount) AS Amount, o.MarketID
                          FROM            MiracleSystem.dbo.Voucher AS v INNER JOIN
                                                   MiracleSystem.dbo.VoucherRecord AS vr ON v.VoucherID = vr.VoucherID INNER JOIN
                                                   MiracleSystem.dbo.Office AS o ON vr.OfficeID = o.OfficeID LEFT OUTER JOIN
                                                   MiracleSystem.dbo.Associate AS a ON vr.AssociateID = a.AssociateID
                          WHERE        (YEAR(v.BatchDate) >= 2010)
                          GROUP BY a.Title, a.AssociateIDName, a.LastName, a.FirstName, a.Email, o.OfficeIDName, o.OfficeName, o.Address1, o.Address2, o.City, o.StateProvince, o.ZipPostalCode, o.MarketID, v.BatchDate) AS t
GROUP BY AssociateID, Title, LastName, FirstName, Email, OfficeID, OfficeName, Address, Address2, City, StateProvince, ZipPostalCode, MarketID, Date


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCMiracleSystem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCMiracleSystem';

