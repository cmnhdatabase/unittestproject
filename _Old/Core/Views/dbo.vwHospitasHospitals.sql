﻿


CREATE VIEW [dbo].[vwHospitasHospitals]
AS
    SELECT  h.HospitalName ,
        m.LegacyMarketId ,
        h.Address1 ,
        h.Address2 ,
        h.City ,
        p.Abbreviation ,
        h.PostalCode ,
        co.CountryName ,
        r.RegionName ,
        pn.Phone,
		h.active
FROM    core.dbo.Hospitals h
        INNER JOIN core.dbo.Markets m ON h.MarketId = m.MarketId
        LEFT OUTER JOIN core.dbo.Provinces p ON h.ProvinceId = p.ProvinceId
        LEFT OUTER JOIN core.dbo.Countries co ON h.CountryId = co.CountryId
        LEFT OUTER JOIN core.dbo.Regions r ON m.RegionId = r.RegionId
        LEFT OUTER JOIN core.dbo.HospitalPhoneNumbers hp ON h.HospitalId = hp.hospitalid
        LEFT OUTER JOIN core.dbo.phonenumbers pn ON hp.PhoneId = pn.PhoneId

