﻿




CREATE VIEW [dbo].[vwMarketPopulationRank]
AS
	SELECT	m.MarketId ,
			m.MarketName ,
			ISNULL(m.ShortName , 'Unknown') ShortName ,
			t.PopulationEstimate AS PopulationEstimate ,
			CASE WHEN t.PopulationEstimate < 1000000 THEN 1
				 WHEN t.PopulationEstimate < 3000000 THEN 2
				 ELSE 3
			END AS PopulationRank ,
			co.Abbreviation AS Country
	FROM	(
			  SELECT DISTINCT
						tsub.MarketId ,
						SUM(tsub.PopulationEstimate) PopulationEstimate
			  FROM		(
						  SELECT	pc.MarketId ,
									SUM(CASE WHEN pc.CountryId IN ( 1 , 180 ) THEN pc.PopulationEstimate
											 ELSE 0
										END) AS PopulationEstimate
						  FROM		dbo.PostalCodes pc
						  INNER JOIN dbo.Markets mt ON mt.MarketId = pc.MarketId
						  WHERE		pc.CountryId IN (1,180)
						  GROUP BY	pc.MarketId
						  UNION ALL
						  SELECT	pc.MarketId ,
									MAX(subsub.PopulationEstimate) AS CAPop
						  FROM		Core.dbo.PostalCodes pc
						  INNER JOIN dbo.Markets mt ON mt.MarketId = pc.MarketId
						  INNER JOIN (
									   SELECT	sub.MarketId ,
												sub.MarketName ,
												SUM(Amount) PopulationEstimate
									   FROM		(
												  SELECT	LEFT(PostalCode , 3) Postal ,
															m.MarketId ,
															m.MarketName ,
															MAX(PopulationEstimate) Amount
												  FROM		Core.dbo.PostalCodes pc
												  JOIN		Core.dbo.Markets m ON m.MarketId = pc.MarketId
												  WHERE		pc.CountryId = 2
												  GROUP BY	LEFT(PostalCode , 3) ,
															m.MarketId ,
															m.MarketName
												) sub
									   GROUP BY	sub.MarketId ,
												sub.MarketName
									 ) subsub ON subsub.MarketId = mt.MarketId
						  GROUP BY	pc.MarketId
						) tsub
			  GROUP BY	tsub.MarketId
			) AS t
	INNER JOIN dbo.Markets AS m ON m.MarketId = t.MarketId
	INNER JOIN dbo.PostalCodes AS pc ON pc.MarketId = m.MarketId
	INNER JOIN dbo.Countries AS co ON co.CountryId = m.CountryId
	GROUP BY m.MarketName ,
			m.MarketId ,
			m.ShortName ,
			co.Abbreviation ,
			t.PopulationEstimate;

-------Original---------
--SELECT        m.MarketId, m.MarketName, SUM(CONVERT(BIGINT, pc.PopulationEstimate)) AS PopulationEstimate, CASE WHEN SUM(CONVERT(BIGINT, 
--                         pc.PopulationEstimate)) < 1000000 THEN 1 WHEN SUM(CONVERT(BIGINT, pc.PopulationEstimate)) < 3000000 THEN 2 ELSE 3 END AS PopulationRank, 
--                         co.Abbreviation AS Country
--FROM            dbo.Markets AS m INNER JOIN
--                         dbo.PostalCodes AS pc ON pc.MarketId = m.MarketId INNER JOIN
--                         dbo.Countries AS co ON co.CountryId = m.CountryId
--GROUP BY m.MarketName, m.MarketId, co.Abbreviation








