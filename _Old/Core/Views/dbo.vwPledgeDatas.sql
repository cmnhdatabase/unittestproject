﻿

/****** Object:  View [dbo].[vwPledgeDatas] ******/
CREATE VIEW [dbo].[vwPledgeDatas]
AS
	SELECT [PledgeId], [FundraisingEntityId], [MarketId], [FundraisingYear], [CampaignDetailsId], [CurrencyTypeId], 
		[Amount], [PledgeTypeId], [Quarter], [PledgeDate], [DirectToHospital], [DateRecorded], [CreatedBy], [PledgeBatchId]
	FROM [dbo].[PledgeData]
