﻿

CREATE VIEW [dbo].[vwRadioStations]
AS
	SELECT [RadioStationId], [CallLetters], [MarketId], [Owner], [Frequency], [MarketTag], [MarketArea], 
		[YearStarted], [Director], [Address1], [Address2], [City], [ProvinceId], [PostalCode], 
		[CountryId], [Note], [ModifiedBy], [ModifiedDate], [Active]
	FROM [dbo].[RadioStations];

