﻿
CREATE VIEW [dbo].[vwSchoolDetails]
AS
	SELECT s.[SchoolId], s.[HospitalId], h.[HospitalName], s.[SchoolName], s.[Address1], s.[Address2], s.[City], s.[ProvinceId], p.[Province], 
		p.[Abbreviation] AS [ProvinceAbbreviation], s.[PostalCode], s.[CountryId], c.[CountryName], c.[Abbreviation] AS [CountryAbbreviation], 
		s.[InauguralEvent], s.[SchoolTypeId], st.[SchoolType], h.[MarketId], m.[MarketName], s.[Active]
	FROM [dbo].[Schools] s
		LEFT OUTER JOIN [dbo].[Hospitals] h ON s.[HospitalId] = h.[HospitalId]
		LEFT OUTER JOIN [dbo].[Provinces] p ON s.[ProvinceId] = p.[ProvinceId]
		LEFT OUTER JOIN [dbo].[Countries] c ON s.[CountryId] = c.[CountryId]
		LEFT OUTER JOIN [dbo].[SchoolTypes] st ON s.[SchoolTypeId] = st.[SchoolTypeId]
    	LEFT OUTER JOIN [dbo].[Markets] m ON h.[MarketId]=m.[MarketId]

