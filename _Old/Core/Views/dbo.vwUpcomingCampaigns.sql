﻿
CREATE VIEW [dbo].[vwUpcomingCampaigns]
AS
SELECT        c.FirstName, c.LastName, e.Title, e.StartDate, e.EndDate, e.CampaignYear
FROM            PartnerPages.dbo.FundraisingSnapshots AS e INNER JOIN
                         Core.dbo.FundraisingEntities AS f ON e.FundraisingEntityId = f.FundraisingEntityId INNER JOIN
                         Contacts.dbo.Contacts AS c ON e.AccountDirector = c.ContactId

