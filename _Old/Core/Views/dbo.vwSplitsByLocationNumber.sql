﻿


CREATE VIEW [dbo].[vwSplitsByLocationNumber]
AS
/*converted section*/
SELECT
t.LocationId, t.FundraisingEntityId, t.LocationName, t.LocationNumber, t.Address1, t.Address2, t.City, t.ProvinceId, t.PostalCode, t.CountryId, t.PropertyTypeId, t.Region, t.Division, t.Zone, t.District, t.Latitude, 
t.Longitude, t.Active, t.SplitId, t.SplitFundraisingEntityId, t.SplitPostalCode, t.SplitFrom, t.SplitPercent, t.IsPostalCode, p.PostalCode pcPostalCode, p.MarketId, p.SubMarketId, p.ProvinceId pcProvinceId, p.CountryId pcCountryId, p.City pcCity, 
p.County, p.PopulationEstimate, p.Latitude pcLatitude, p.Longitude pcLongitude
FROM
(
       SELECT
       l.LocationId, l.FundraisingEntityId, l.LocationName, l.LocationNumber,
       l.Address1, l.Address2, l.City, l.ProvinceId, l.PostalCode, l.CountryId,
       l.PropertyTypeId,
       MAX(CASE WHEN at.AreaTypeId = 1 THEN a.Name END) Region,
       MAX(CASE WHEN at.AreaTypeId = 2 THEN a.Name END) Division,
       MAX(CASE WHEN at.AreaTypeId = 3 THEN a.Name END) Zone,
       MAX(CASE WHEN at.AreaTypeId = 4 THEN a.Name END) District, 
       l.Latitude, l.Longitude, l.Active, s.RedirectedLocationId SplitId, l.FundraisingEntityId SplitFundraisingEntityId,
       s.RedirectedToPostalCode SplitPostalCode, s.LocationId SplitFrom, s.RedirectPercent SplitPercent, NULL IsPostalCode
       FROM
       dbo.Locations l
       LEFT JOIN dbo.LocationAreas la ON la.LocationId = l.LocationId
       LEFT JOIN dbo.Areas a ON la.AreaId = a.AreaId AND a.FundraisingEntityId = l.FundraisingEntityId
       LEFT JOIN dbo.AreaTypes at ON at.AreaTypeId = a.AreaTypeId
       LEFT JOIN dbo.RedirectedLocations s ON l.LocationId = s.LocationId
       GROUP BY
       l.LocationId, l.FundraisingEntityId, l.LocationName, l.LocationNumber,
       l.Address1, l.Address2, l.City, l.ProvinceId, l.PostalCode, l.CountryId,
       l.PropertyTypeId,
       l.Latitude, l.Longitude, l.Active, s.RedirectedLocationId, l.FundraisingEntityId,
       s.RedirectedToPostalCode, s.LocationId, s.RedirectPercent
) t
INNER JOIN dbo.PostalCodes AS p ON t.PostalCode = p.PostalCode
/*end converted section*/



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 267
               Bottom = 136
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
        SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwSplitsByLocationNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwSplitsByLocationNumber';

