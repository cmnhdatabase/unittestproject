﻿/****** Object:  View [dbo].[vwCampaigns] ******/
CREATE VIEW [dbo].[vwCampaigns]
AS
	SELECT [CampaignId], [CampaignName], [ShortDescription], [LongDescription], [CampaignTypeId]
	FROM [dbo].[Campaigns];


