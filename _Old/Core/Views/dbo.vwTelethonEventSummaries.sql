﻿


CREATE VIEW [dbo].[vwTelethonEventSummaries]
AS
SELECT        c.[CampaignId], c.[CampaignName], cd.[CampaignDetailsId], cd.[CampaignDetailName], cd.[StartDate], cd.[EndDate], t .[TelethonId], t .[City], t .[OnAirHours], t .[OnAirMinutes], t .[LanguageId], t .[PhoneBank], 
                         t .[Note], t .[SubmittedBy], t .[SubmittedDate], t .[ModifiedBy], t .[ModifiedDate], t .[Active], t .[EventId], l.[Language], tv.Director AS [DirectorId], co.[FirstName] AS [DirectorFirstName], 
                         co.[LastName] AS [DirectorLastName],
                             (SELECT        SUM([Amount])
                               FROM            [dbo].[PledgeData] pd
                               WHERE        pd.[CampaignDetailsId] = cd.[CampaignDetailsId] AND pd.[PledgeTypeId] = 8) AS [GoalAmount],
                             (SELECT        SUM([Amount])
                               FROM            [dbo].[PledgeData] pd
                               WHERE        pd.[CampaignDetailsId] = cd.[CampaignDetailsId] AND pd.[PledgeTypeId] = 7) AS [AnnouncedTotal],
                             (SELECT        [CallLetters] + ' ' + ISNULL([StationOwner], '')
                               FROM            [dbo].[TelethonTvStations] tts INNER JOIN
                                                         [dbo].[TV] tv ON tts.[TvStationId] = tv.[TvStationId]
                               WHERE        tts.[TelethonId] = t .[TelethonId] AND tv.[Active] = 1 FOR XML PATH('')) AS [CallLettersAndOwners]
FROM            [dbo].[Campaigns] c INNER JOIN
                         [dbo].[CampaignDetails] cd ON c.[CampaignId] = cd.[CampaignId] INNER JOIN
                         [dbo].[Telethons] t ON cd.[CampaignDetailsId] = t .[CampaignDetailsId] INNER JOIN
                         [dbo].[Languages] l ON t .[LanguageId] = l.[LanguageId] LEFT OUTER JOIN
                             (SELECT        [TelethonId], [TvStationId]
                               FROM            [dbo].[TelethonTvStations]
                               WHERE        [PrimaryStation] = 1) tts ON t .[TelethonId] = tts.[TelethonId] LEFT OUTER JOIN
                             (SELECT        [TvStationId], [Director]
                               FROM            [dbo].[TV]
                               WHERE        [Active] = 1) tv ON tts.[TvStationId] = tv.[TvStationId] LEFT OUTER JOIN
                             (SELECT        [ContactId], [FirstName], [LastName]
                               FROM            [Contacts].[dbo].[Contacts]
                               WHERE        [Active] = 1) co ON tv.Director = co.[ContactId]
WHERE        t .[Active] = 1 AND cd.[StartDate] IS NOT NULL AND cd.[EndDate] IS NOT NULL;

