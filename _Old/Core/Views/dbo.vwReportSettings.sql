﻿

CREATE VIEW dbo.vwReportSettings
AS
    SELECT  rs.ReportChangeDate
    FROM    [Core].[dbo].[ReportSettings] rs;