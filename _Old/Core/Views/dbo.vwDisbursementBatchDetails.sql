﻿CREATE VIEW [dbo].[vwDisbursementBatchDetails]
AS
SELECT        dbo.BatchTypes.BatchTypeId, dbo.BatchTypes.BatchType, dbo.BatchTypes.Abbreviation, dbo.Batches.BatchId, dbo.Batches.BatchNumber, dbo.Batches.Reconciled, dbo.Batches.DateReconciled, 
                         dbo.Batches.Foundation, dbo.RecordTypes.RecordTypeId, dbo.RecordTypes.RecordType, dbo.Disbursements.DisbursementId, dbo.Disbursements.FundraisingEntityId, dbo.Disbursements.MarketId, 
                         dbo.Disbursements.SubMarketId, dbo.Disbursements.DirectToHospital, dbo.Disbursements.FundraisingYear, dbo.Currencies.CurrencyTypeId, dbo.Currencies.CurrencyType, dbo.Disbursements.Amount, 
                         dbo.Disbursements.CampaignDetailsId, dbo.Disbursements.LocationId, dbo.Disbursements.DateReceived, dbo.Disbursements.DateRecorded, dbo.Disbursements.DonationDate, dbo.FundTypes.FundTypeId, 
                         dbo.FundTypes.FundType, dbo.Disbursements.CampaignPeriod, dbo.Disbursements.UploadId, dbo.Disbursements.Comment, dbo.DisbursementDates.DisbursementDate, 
                         dbo.DisbursementPeriods.DisbursementYear, dbo.DisbursementPeriods.DisbursementQuarter
FROM            dbo.RecordTypes INNER JOIN
                         dbo.Disbursements ON dbo.RecordTypes.RecordTypeId = dbo.Disbursements.RecordTypeId INNER JOIN
                         dbo.Currencies ON dbo.Disbursements.CurrencyTypeId = dbo.Currencies.CurrencyTypeId INNER JOIN
                         dbo.DisbursementDates ON dbo.Disbursements.DisbursementDateId = dbo.DisbursementDates.DisbursementDateId INNER JOIN
                         dbo.DisbursementPeriods ON dbo.Disbursements.DisbursementPeriodId = dbo.DisbursementPeriods.DisbursementPeriodId LEFT OUTER JOIN
                         dbo.Batches LEFT OUTER JOIN
                         dbo.BatchTypes ON dbo.Batches.BatchTypeId = dbo.BatchTypes.BatchTypeId ON dbo.Disbursements.BatchId = dbo.Batches.BatchId LEFT OUTER JOIN
                         dbo.FundTypes ON dbo.Disbursements.FundTypeId = dbo.FundTypes.FundTypeId



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Batches"
            Begin Extent = 
               Top = 0
               Left = 200
               Bottom = 203
               Right = 371
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BatchTypes"
            Begin Extent = 
               Top = 0
               Left = 0
               Bottom = 124
               Right = 170
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Disbursements"
            Begin Extent = 
               Top = 3
               Left = 401
               Bottom = 291
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "RecordTypes"
            Begin Extent = 
               Top = 18
               Left = 695
               Bottom = 117
               Right = 865
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Currencies"
            Begin Extent = 
               Top = 130
               Left = 694
               Bottom = 226
               Right = 867
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FundTypes"
            Begin Extent = 
               Top = 232
               Left = 696
               Bottom = 328
               Right = 866
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DisbursementDates"
            Begin Extent = 
               Top = 222
               Left = 93
               Bottom = 318
               Right = 289
            End
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwDisbursementBatchDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DisbursementPeriods"
            Begin Extent = 
               Top = 6
               Left = 1137
               Bottom = 153
               Right = 1343
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwDisbursementBatchDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwDisbursementBatchDetails';

