﻿

CREATE VIEW [dbo].[vwUnreportedTelethons]
AS 
	SELECT DISTINCT [CampaignId], [CampaignName], s.[CampaignDetailsId], [CampaignDetailName], [CampaignDetailName] AS [ShortDescription], 
	  YEAR([EndDate]) AS [CampaignYear], [StartDate], [EndDate], s.[TelethonId], tv.[TvStationId], s.[City], s.[OnAirHours], 
	  s.[OnAirMinutes], [DirectorId] AS [Director]
	FROM [dbo].[vwTelethonEventSummaries] s
	  JOIN [dbo].[TelethonTVStations] tts ON tts.[TelethonId] = s.[TelethonId]
	  JOIN [dbo].[TV] tv ON tv.[TvStationId] = tts.[TvStationId]
	WHERE [AnnouncedTotal] IS NULL AND [PrimaryStation] = 1 AND (DATEADD(dd, 0, DATEDIFF(dd, 0, [EndDate])) < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))) AND YEAR([EndDate]) >= 2014

