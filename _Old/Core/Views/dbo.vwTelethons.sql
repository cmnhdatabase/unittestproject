﻿
CREATE VIEW [dbo].[vwTelethons]
AS
	SELECT [TelethonId], [CampaignDetailsId], [City], [OnAirHours], [OnAirMinutes],
		[LanguageId], [PhoneBank], [Note], [SubmittedBy], [SubmittedDate], [ModifiedBy], [ModifiedDate],
		[Active], [EventId], [ToteBoardImagePath]
	FROM [dbo].[Telethons]
