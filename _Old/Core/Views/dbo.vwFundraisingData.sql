﻿



CREATE VIEW [dbo].[vwFundraisingData]
AS

SELECT        fc.FundraisingCategoryId, fc.FundraisingCategory, FundraisingYear, dp.DisbursementQuarter, dp.DisbursementYear, d .FundraisingEntityId, p.DisplayName, m.MarketId, m.MarketName, m.ShortName, d .CampaignDetailsId, 
                         cd.CampaignDetailName, c.CampaignId, c.CampaignName, ct.CampaignTypeId, ct.CampaignType, pp.DisplayName AS ProgramDisplayName, CurrencyTypeId, Amount, FundTypeId, RecordTypeId, NULL 
                         AS PledgeTypeId, CAST(CASE WHEN DirectToHospital = 0 THEN 0 ELSE 1 END AS INT) AS DirectToHospital, m.RegionId, l.LocationId, d .Comment, d .DonationDate
FROM            Disbursements d INNER JOIN
                         dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN
                         Markets m ON d .MarketId = m.MarketId INNER JOIN
                         dbo.FundraisingEntities p ON d .FundraisingEntityId = p.FundraisingEntityId INNER JOIN
                         dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = p.FundraisingCategoryId INNER JOIN
                         CampaignDetails cd ON d .CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         Campaigns c ON cd.CampaignId = c.CampaignId INNER JOIN
                         CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId LEFT OUTER JOIN
                         dbo.Locations l ON d .LocationId = l.LocationId AND d .FundraisingEntityId = l.FundraisingEntityId
WHERE        RecordTypeId IN (1, 4) AND d .FundraisingEntityId = 56

UNION ALL

SELECT        fc.FundraisingCategoryId, fc.FundraisingCategory, FundraisingYear, dp.DisbursementQuarter, dp.DisbursementYear, d .FundraisingEntityId, p.DisplayName AS FundraisingEntityName, m.MarketId, 
                         m.MarketName, m.ShortName, d .CampaignDetailsId, cd.CampaignDetailName, c.CampaignId, c.CampaignName, ct.CampaignTypeId, ct.CampaignType, pp.DisplayName, CurrencyTypeId, Amount, FundTypeId, 
                         RecordTypeId, NULL AS PledgeTypeId, DirectToHospital, m.RegionId, l.LocationId, d .Comment, d .DonationDate
FROM            Disbursements d INNER JOIN
                         dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN
                         Markets m ON d .MarketId = m.MarketId INNER JOIN
                         dbo.FundraisingEntities p ON d .FundraisingEntityId = p.FundraisingEntityId INNER JOIN
                         dbo.FundraisingCategories fc ON p.FundraisingCategoryId = fc.FundraisingCategoryId INNER JOIN
                         CampaignDetails cd ON d .CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         Campaigns c ON cd.CampaignId = c.CampaignId INNER JOIN
                         CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId INNER JOIN
                         DisbursementDates dd ON d .DisbursementDateId = dd.DisbursementDateId LEFT OUTER JOIN
                         dbo.Locations l ON d .LocationId = l.LocationId AND d .FundraisingEntityId = l.FundraisingEntityId
WHERE        RecordTypeId IN (1, 4) AND d .FundraisingEntityId NOT IN (56, 20, 21, 130, 131, 132, 133, 136, 137, 146, 147, 135, 138, 157, 312, 306)

/*---------------------update below to use disbursementdate*/ 
UNION ALL
SELECT        fc.FundraisingCategoryId, fc.FundraisingCategory, FundraisingYear, dp.DisbursementQuarter, dp.DisbursementYear, d .FundraisingEntityId, p.DisplayName AS FundraisingEntityName, m.MarketId, 
                         m.MarketName, m.ShortName, d .CampaignDetailsId, cd.CampaignDetailName, c.CampaignId, c.CampaignName, ct.CampaignTypeId, ct.CampaignType, pp.DisplayName, CurrencyTypeId, Amount, FundTypeId, 
                         RecordTypeId, NULL AS PledgeTypeId, DirectToHospital, m.RegionId, l.LocationId, d .Comment, d .DonationDate
FROM            Disbursements d INNER JOIN
                         dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN
                         Markets m ON d .MarketId = m.MarketId INNER JOIN
                         dbo.FundraisingEntities p ON d .FundraisingEntityId = p.FundraisingEntityId INNER JOIN
                         dbo.FundraisingCategories fc ON p.FundraisingCategoryId = fc.FundraisingCategoryId INNER JOIN
                         CampaignDetails cd ON d .CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         Campaigns c ON cd.CampaignId = c.CampaignId INNER JOIN
                         CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId INNER JOIN
                         DisbursementDates dd ON d .DisbursementDateId = dd.DisbursementDateId LEFT OUTER JOIN
                         dbo.Locations l ON d .LocationId = l.LocationId AND d .FundraisingEntityId = l.FundraisingEntityId
WHERE        RecordTypeId = 1 AND (p.FundraisingEntityId IN (20, 21) AND (cp.CampaignTypeFundraisingEntityId IS NULL OR
                         (ps.EndDate IS NOT NULL AND YEAR(ps.EndDate) <= d .FundraisingYear)))
UNION ALL
SELECT        fc.FundraisingCategoryId, fc.FundraisingCategory, FundraisingYear, dp.DisbursementQuarter, dp.DisbursementYear, d .FundraisingEntityId, p.DisplayName AS FundraisingEntityName, m.MarketId, 
                         m.MarketName, m.ShortName, d .CampaignDetailsId, cd.CampaignDetailName, c.CampaignId, c.CampaignName, ct.CampaignTypeId, ct.CampaignType, 
                         CASE WHEN pp.DisplayName LIKE '%Radiothon' THEN 'Radiothon Announced' ELSE pp.DisplayName END AS programDisplayName, CurrencyTypeId, Amount, FundTypeId, RecordTypeId, NULL AS PledgeTypeId,
                          DirectToHospital, m.RegionId, l.LocationId, d .Comment, d .DonationDate
FROM            Disbursements d INNER JOIN
                         dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN
                         Markets m ON d .MarketId = m.MarketId INNER JOIN
                         dbo.FundraisingEntities p ON d .FundraisingEntityId = p.FundraisingEntityId /*INNER JOIN dbo.FundraisingCategories fc ON p.FundraisingCategoryId = fc.FundraisingCategoryId*/ INNER JOIN
                         CampaignDetails cd ON d .CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         Campaigns c ON cd.CampaignId = c.CampaignId INNER JOIN
                         CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId INNER JOIN
                         dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = pp.FundraisingCategoryId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId INNER JOIN
                         DisbursementDates dd ON d .DisbursementDateId = dd.DisbursementDateId LEFT OUTER JOIN
                         dbo.Locations l ON d .LocationId = l.LocationId AND d .FundraisingEntityId = l.FundraisingEntityId
WHERE        RecordTypeId = 1 AND (cp.CampaignTypeFundraisingEntityId IS NOT NULL AND (ps.EndDate IS NULL OR
                         (YEAR(ps.EndDate) - 1 >= d .FundraisingYear/*AND disbursementdate != '1900-01-01'*/ ))) AND NOT (c.CampaignTypeId IN (27, 32) AND d .FundraisingYear >= 2013)
/*-pledge data for ntl programs*/ 
UNION ALL
SELECT        fc.FundraisingCategoryId, fc.FundraisingCategory, FundraisingYear, d .Quarter, YEAR(d .PledgeDate) AS DisbYear, cp.FundraisingEntityId, p.DisplayName AS FundraisingEntityName, m.MarketId, m.MarketName, m.ShortName, 
                         d .CampaignDetailsId, cd.CampaignDetailName, c.CampaignId, c.CampaignName, ct.CampaignTypeId, ct.CampaignType, 
                         CASE WHEN pp.DisplayName LIKE '%Radiothon' THEN 'Radiothon Announced' ELSE pp.DisplayName END AS DisplayName, CurrencyTypeId, Amount, NULL, NULL, PledgeTypeId, directtohospital, 
                         m.RegionId, NULL, NULL, d .PledgeDate
FROM            PledgeData d INNER JOIN
                         Markets m ON d .MarketId = m.MarketId INNER JOIN
                         dbo.FundraisingEntities p ON d .FundraisingEntityId = p.FundraisingEntityId INNER JOIN
                         dbo.FundraisingCategories fc ON p.FundraisingCategoryId = fc.FundraisingCategoryId INNER JOIN
                         CampaignDetails cd ON d .CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         Campaigns c ON cd.CampaignId = c.CampaignId INNER JOIN
                         CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
WHERE        pledgetypeid IN (1, 2)
UNION ALL
SELECT        0 AS FundraisingCategoryId, 'Overlap' AS FundraisingCategory, FundraisingYear, dp.DisbursementQuarter, dp.DisbursementYear, d .FundraisingEntityId, p.DisplayName, m.MarketId, m.MarketName, m.ShortName, 
                         d .CampaignDetailsId, cd.CampaignDetailName, c.CampaignId, c.CampaignName, ct.CampaignTypeId, ct.CampaignType, pp.DisplayName, CurrencyTypeId, Amount * - 1, FundTypeId, RecordTypeId, NULL 
                         AS PledgeTypeId, DirectToHospital, m.RegionId, l.LocationId, d .Comment, d .DonationDate
FROM            Disbursements d INNER JOIN
                         dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN
                         Markets m ON d .MarketId = m.MarketId INNER JOIN
                         dbo.FundraisingEntities p ON d .FundraisingEntityId = p.FundraisingEntityId INNER JOIN
                         dbo.FundraisingCategories fc ON p.FundraisingCategoryId = fc.FundraisingCategoryId INNER JOIN
                         CampaignDetails cd ON d .CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         Campaigns c ON cd.CampaignId = c.CampaignId INNER JOIN
                         CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId INNER JOIN
                         DisbursementDates dd ON d .DisbursementDateId = dd.DisbursementDateId LEFT OUTER JOIN
                         dbo.Locations l ON d .LocationId = l.LocationId AND d .FundraisingEntityId = l.FundraisingEntityId
WHERE        RecordTypeId = 1 
			AND d .FundraisingEntityId NOT IN (56, 20, 21, 130, 131, 132, 133, 136, 137, 146, 147, 135, 154, 157, 138, 312, 306) 
			AND (cp.CampaignTypeFundraisingEntityId IS NOT NULL 
			AND (ps.EndDate IS NULL OR (YEAR(ps.EndDate) - 1 >= d .FundraisingYear AND DisbursementDate != '1900-01-01')))




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwFundraisingData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwFundraisingData';

