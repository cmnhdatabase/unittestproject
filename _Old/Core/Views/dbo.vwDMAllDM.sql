﻿


CREATE VIEW [dbo].[vwDMAllDM]
AS
    SELECT  c.CampaignName, p.PledgeDate, p.Amount AS AnnouncedAmount, d.FundraisingYear, d.Amount AS CollectedAmount
    FROM      DanceMarathons dm
                    INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
                    INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                    LEFT JOIN PledgeData p ON cd.CampaignDetailsId = p.CampaignDetailsId
                    LEFT JOIN Disbursements d ON cd.CampaignDetailsId = d.CampaignDetailsId




