﻿
CREATE VIEW [dbo].[vwHospitals]
AS
	SELECT [HospitalId], [HospitalName], [FriendlyHospitalName], [MarketId], [Address1], [Address2], [City], [ProvinceId], [PostalCode], [CountryId],
		[LongDescription], [ShortDescription], [Website], [Latitude], [Longitude], [Active]
	FROM [dbo].[Hospitals]


