﻿CREATE VIEW [dbo].[vwDisbursements]
AS
	SELECT [DisbursementId], [RecordTypeId], [FundraisingEntityId], [MarketId], [SubMarketId], [DirectToHospital], [FundraisingYear], [Amount], 
		[CurrencyTypeId], [CampaignDetailsId], [LocationId], [DateReceived], [DateRecorded], [DonationDate], [FundTypeId], [BatchId], 
		[CampaignPeriod], [UploadId], [Comment], [DisbursementPeriodId], [DisbursementDateId], [FundraisingCategoryId], [CreatedBy]
	FROM [dbo].[Disbursements];

