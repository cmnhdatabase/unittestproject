﻿

CREATE VIEW [dbo].[vwCampaignTotalsDataSet1]
AS

SELECT  f.Name ,
        l.LocationNumber ,
        d.Amount
		, d.RecordTypeId
		, d.FundraisingEntityId
		, d.FundraisingYear
FROM    dbo.Disbursements d
        INNER JOIN dbo.FundraisingEntities f ON f.FundraisingEntityId = d.FundraisingEntityId
        INNER JOIN dbo.Locations l ON l.LocationId = d.LocationId