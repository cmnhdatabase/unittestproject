﻿
/**********************************************************************************************/
/* Gets a list of all active Hospital Logos (Image.TypeId = 3) that are in a JPG Format       */
/* (ImageFormatId = 4).  This view was created because there was no previously existing view  */
/* that return Image information associated with a Hospital.                                  */
/**********************************************************************************************/
CREATE VIEW [vwActiveHospitalDetailsWithLogo]
AS
SELECT h.[HospitalId], h.[HospitalName], h.[FriendlyHospitalName], h.[MarketId], h.[MarketName], 
	   h.[Address1], h.[Address2], h.[City], p.[ProvinceId], p.[Abbreviation] AS 'Province', 
	   h.[PostalCode], c.[CountryId], c.[Abbreviation] AS 'Country',  i.[Uri] AS 'LogoUri', 
	   h.[LongDescription], h.[ShortDescription], h.[Website], h.[Latitude], h.[Longitude]	   
FROM [dbo].[vwHospitalDetails] h
INNER JOIN [dbo].[Countries] c ON c.[CountryId] = h.[CountryId]
INNER JOIN [dbo].[Provinces] p ON p.[ProvinceId] = h.[ProvinceId]
INNER JOIN [dbo].[Images] i ON i.[Value] = CAST(h.[HospitalId] AS VARCHAR)
INNER JOIN [dbo].[ImageFormats] f ON f.[ImageFormatId] = i.[ImageFormatId]
WHERE i.[TypeId] = 3
AND f.[ImageFormatId] = 4
AND h.[Active] = 1;
