﻿
CREATE VIEW vwPrimaryMarkets AS


SELECT
	m.MarketId, m.MarketName, m.CountryId, m.RegionId, m.Active, m.LegacyMarketId, m.ShortName
FROM
	dbo.ContractHolders ch1
	JOIN dbo.ContractHolders ch2 ON ch1.PrimaryMarketId = ch2.SubMarketId AND ch2.ContractHolderId = ch1.ContractHolderId
	JOIN dbo.Markets m ON m.MarketId = ch1.PrimaryMarketId
