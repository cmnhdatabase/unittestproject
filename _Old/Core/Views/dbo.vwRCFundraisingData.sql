﻿
CREATE VIEW [dbo].[vwRCFundraisingData]
AS
SELECT        d.FundraisingYear AS [Fundraising Year], m.MarketId, m.MarketName AS Market, rg.RegionName AS Region, fe.fundraisingentityId, fe.DisplayName AS [Partner Name], m.MarketName AS [Market Display Name], 
                         l.LocationNumber AS [Partner Location Number], ct.CampaignType AS [Campaign Type], '$' + CONVERT(VARCHAR(50), CAST(d.Amount AS MONEY), - 1) AS [Formated Amount], d.Amount, 
                         cu.CurrencyType AS Currency, c.CampaignName AS [Campaign Name], cd.CampaignDetailName AS [Campaign Detail], d.Comment, f.FundType AS [Transaction Type], d.DirectToHospital AS [Direct To Hospital], 
                         b.BatchNumber AS [CMNH Batch Number], l.LocationName AS [Location Name], l.Address1, l.Address2, l.City, l.PostalCode, pt.PropertyType, dp.DisbursementQuarter, dd.DisbursementDate, 
                         d.DateRecorded
FROM            dbo.Disbursements AS d INNER JOIN
                         dbo.Markets AS m ON d.MarketId = m.MarketId INNER JOIN
                         dbo.Regions AS rg ON m.RegionId = rg.RegionId INNER JOIN
                         dbo.FundraisingEntities AS fe ON d.FundraisingEntityId = fe.FundraisingEntityId INNER JOIN
                         dbo.CampaignDetails AS cd ON d.CampaignDetailsId = cd.CampaignDetailsId INNER JOIN
                         dbo.Campaigns AS c ON cd.CampaignId = c.CampaignId INNER JOIN
                         dbo.CampaignTypes AS ct ON c.CampaignTypeId = ct.CampaignTypeId LEFT OUTER JOIN
                         dbo.CampaignTypeFundraisingEntities AS cp ON ct.CampaignTypeId = cp.CampaignTypeId LEFT OUTER JOIN
                         dbo.FundraisingEntities AS cfe ON cp.FundraisingEntityId = cfe.FundraisingEntityId LEFT OUTER JOIN
                         dbo.FundraisingEntityNationalProgramStatus AS ps ON cfe.FundraisingEntityId = ps.FundraisingEntityId INNER JOIN
                         dbo.DisbursementDates AS dd ON d.DisbursementDateId = dd.DisbursementDateId LEFT OUTER JOIN
                         dbo.Locations AS l ON d.FundraisingEntityId = l.FundraisingEntityId AND d.LocationId = l.LocationId LEFT OUTER JOIN
                         dbo.Currencies AS cu ON d.CurrencyTypeId = cu.CurrencyTypeId LEFT OUTER JOIN
                         dbo.FundTypes AS f ON d.FundTypeId = f.FundTypeId INNER JOIN
                         dbo.Batches AS b ON d.BatchId = b.BatchId LEFT OUTER JOIN
                         dbo.Provinces AS pr ON l.ProvinceId = pr.ProvinceId LEFT OUTER JOIN
                         dbo.PropertyTypes AS pt ON l.PropertyTypeId = pt.PropertyTypeId LEFT OUTER JOIN
                         dbo.DisbursementPeriods AS dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
WHERE        (d.RecordTypeId = 1) AND (d.FundraisingYear >= YEAR(GETDATE()) - 5)


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 136
               Right = 455
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rg"
            Begin Extent = 
               Top = 330
               Left = 454
               Bottom = 426
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 493
               Bottom = 136
               Right = 702
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 6
               Left = 740
               Bottom = 136
               Right = 946
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 984
               Bottom = 136
               Right = 1164
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 6
               Left = 1202
               Bottom = 102
               Right = 1382
            End
            DisplayFlags = 280
            TopColumn = 0
         End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCFundraisingData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'         Begin Table = "cp"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 302
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pp"
            Begin Extent = 
               Top = 102
               Left = 1202
               Bottom = 232
               Right = 1411
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 138
               Left = 340
               Bottom = 268
               Right = 562
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dd"
            Begin Extent = 
               Top = 138
               Left = 600
               Bottom = 234
               Right = 796
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l"
            Begin Extent = 
               Top = 138
               Left = 834
               Bottom = 268
               Right = 1017
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cu"
            Begin Extent = 
               Top = 234
               Left = 600
               Bottom = 330
               Right = 773
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "f"
            Begin Extent = 
               Top = 234
               Left = 1055
               Bottom = 330
               Right = 1225
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 234
               Left = 1263
               Bottom = 364
               Right = 1434
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pr"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 382
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pt"
            Begin Extent = 
               Top = 270
               Left = 246
               Bottom = 366
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dp"
            Begin Extent = 
               Top = 270
               Left = 811
               Bottom = 383
               Right = 1017
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCFundraisingData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane3', @value = N' = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCFundraisingData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 3, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwRCFundraisingData';

