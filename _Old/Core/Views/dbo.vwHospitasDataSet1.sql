﻿


CREATE VIEW [dbo].[vwHospitasDataSet1]
AS
    SELECT --[ContactId]
      --,[HospitalId]
        [HospitalName] ,
        [FriendlyHospitalName] ,
        m.MarketName ,
        [ShippingAddress1] ,
        [ShippingAddress2] ,
        [ShippingCity] ,
        [ShippingProvinceId] ,
        [ShippingPostalCode] ,
        [ShippingCountryId] ,
        [Address1] ,
        [Address2] ,
        [City] ,
        p.Abbreviation Province ,
        [PostalCode] ,
        c.Abbreviation Country ,
        [CompleteHospitalAddress]
      --,[LongDescription]
        ,
        [ShortDescription]
      --,[Website]
      --[SynchId]
      --,[Latitude]
      --,[Longitude]
      --,[Active]
      --,[PhotoUri]
      --,[RegionId]
        ,
        [RegionName]
      --,[SubcategoryId]
        ,
        [Fax] ,
        [Phone],
		d.Active
FROM    [Contacts].[dbo].[view_HospitalContactDetails] d
        LEFT OUTER JOIN core.dbo.Provinces p ON d.ProvinceId = p.ProvinceId
        LEFT OUTER JOIN core.dbo.Countries c ON d.CountryId = c.CountryId
        LEFT OUTER JOIN core.dbo.Markets m ON d.MarketId = m.MarketId
