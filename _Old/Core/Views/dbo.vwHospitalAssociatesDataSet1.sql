﻿


CREATE VIEW [dbo].[vwHospitalAssociatesDataSet1]
AS
    SELECT  [FullName]
      --,[CompleteAddress]
            ,
            [Category]
      --,[SubCategoryId]
            ,
            [SubCategory] ,
            c.[Province] ,
            p.Abbreviation ,
            c.[CountryName]
      --,[CreatedByUserName]
      --,[ModifiedByUserName]
      --,h.[Active]
            ,
            [FirstName]
      --,[ContactId]
            ,
            [BirthDate] ,
            [LastName] ,
            [Email] ,
            [Essentials]
      --,[PhotoUri]
            ,
            [Company] ,
            [Title] ,
            c.[Address1] ,
            c.[Address2] ,
            c.[City] ,
            c.[ProvinceId] ,
            c.[PostalCode] ,
            c.[CountryId] ,
            [Phone] ,
            [Mobile] ,
            [Fax]
      --,[CategoryId]
      --,[CreatedBy]
      --,[CreatedTime]
      --,[ModifiedBy]
      --,[ModifiedTime]
            ,
            m.[MarketName] ,
            [ShippingAddress1] ,
            [ShippingAddress2] ,
            [ShippingCity] ,
            [ShippingProvinceId] ,
            [ShippingPostalCode] ,
            [ShippingCountryId] ,
            h.HospitalName ,
            m.LegacyMarketId AS LegacyMarketName ,
            p.Province ShippingProvince ,
            co.CountryName ShippingCountry ,
            r.RegionName
    FROM    [Contacts].[dbo].[view_Contacts] c --INNER JOIN Core.dbo.Hospitals h1 ON h1.HospitalId = c.HospitalId
            INNER JOIN core.dbo.Hospitals h ON c.HospitalId = h.HospitalId
            INNER JOIN core.dbo.Markets m ON h.MarketId = m.MarketId
            LEFT OUTER JOIN core.dbo.Provinces p ON h.ProvinceId = p.ProvinceId
            LEFT OUTER JOIN core.dbo.Countries co ON h.CountryId = co.CountryId
            LEFT OUTER JOIN core.dbo.Regions r ON m.RegionId = r.RegionId
    WHERE   c.Active = 1




