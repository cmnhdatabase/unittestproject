﻿
CREATE VIEW [dbo].[vwPostalCodes]
AS
	SELECT [PostalCode], [MarketId], [SubMarketId], [ProvinceId], [CountryId], [City], [County], [PopulationEstimate], [Latitude], [Longitude]
	FROM [dbo].[PostalCodes]


