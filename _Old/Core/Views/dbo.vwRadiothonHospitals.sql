﻿
CREATE VIEW [dbo].[vwRadiothonHospitals]
AS
	SELECT [RadiothonId], [HospitalId]
	FROM [dbo].[RadiothonHospitals]
