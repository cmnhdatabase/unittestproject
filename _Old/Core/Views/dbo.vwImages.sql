﻿
CREATE VIEW [dbo].[vwImages]
AS
	SELECT [ImageId], [TypeId], [Value], [URI], [Path], [Width], [Height], [ImageFormatId], [ModifiedDate]
	FROM [dbo].[Images]


