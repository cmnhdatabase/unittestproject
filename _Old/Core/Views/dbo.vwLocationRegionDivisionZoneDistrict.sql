﻿



CREATE VIEW [dbo].[vwLocationRegionDivisionZoneDistrict]
AS
WITH CTE
AS ( 
SELECT *
FROM(
SELECT l.LocationId, AreaType, Name
FROM  dbo.Locations l
INNER	 JOIN dbo.LocationAreas la ON l.LocationId = la.LocationId
LEFT OUTER JOIN dbo.Areas a ON a.AreaId = la.AreaId
LEFT OUTER JOIN dbo.AreaTypes at ON at.AreaTypeId = a.AreaTypeId
) t
PIVOT (MAX(Name) FOR AreaType IN (Region, Division, Zone, District)) AS test
)

SELECT l.*, Region, Division, Zone, District
FROM dbo.Locations l 
INNER JOIN CTE ON CTE.LocationId = l.LocationId




GO
EXECUTE sp_addextendedproperty @name = N'Definition', @value = N'This view is for viewing location areas.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwLocationRegionDivisionZoneDistrict';

