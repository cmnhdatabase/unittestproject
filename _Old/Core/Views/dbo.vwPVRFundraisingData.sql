﻿

CREATE VIEW [dbo].[vwPVRFundraisingData]
AS


     SELECT  3 AS CategoryId ,
            'Local' AS Category ,
			d.FundraisingCategoryId,
			fc.FundraisingCategory,
            FundraisingYear ,
            dp.DisbursementQuarter ,
            dp.DisbursementYear ,
            d.FundraisingEntityId ,
            p.DisplayName ,
            m.MarketId ,
            m.Marketname ,
            d.CampaignDetailsId ,
            cd.CampaignDetailName ,
            c.CampaignName ,
            ct.CampaignTypeId ,
            ct.CampaignType ,
            p.DisplayName AS ProgramDisplayName ,
            CurrencyTypeId ,
            Amount ,
            FundTypeId ,
            RecordTypeId ,
            NULL AS PledgeTypeId ,
            CAST(CASE WHEN DirectToHospital = 0 THEN 0
                      ELSE 1
                 END AS INT) AS DirectToHospital ,
            m.RegionId ,
            l.LocationId ,
            d.Comment ,
            d.DonationDate ,
			cp.CampaignTypeFundraisingEntityId
    FROM    Disbursements d
            INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
            INNER JOIN Markets m ON d.MarketId = m.MarketId
            INNER JOIN dbo.FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
            INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
			INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = d.FundraisingCategoryId
            LEFT OUTER JOIN dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
            LEFT OUTER JOIN dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
            INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
            LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                               AND d.FundraisingEntityId = l.FundraisingEntityId
    WHERE   RecordTypeId in (1, 4)
            AND d.FundraisingEntityId = 56
    UNION ALL
     SELECT  1 AS CategoryId ,
            'National Corporate Partners' AS Category ,
			d.FundraisingCategoryId,
			FundraisingCategory,
            FundraisingYear ,
            dp.DisbursementQuarter ,
            dp.DisbursementYear ,
            d.FundraisingEntityId ,
            p.DisplayName ,
            m.MarketId ,
            m.Marketname ,
            d.CampaignDetailsId ,
            cd.CampaignDetailName ,
            c.CampaignName ,
            ct.CampaignTypeId ,
            ct.CampaignType ,
            p.DisplayName ,
            CurrencyTypeId ,
            Amount ,
            FundTypeId ,
            RecordTypeId ,
            NULL AS PledgeTypeId ,
            DirectToHospital ,
            m.RegionId ,
            l.LocationId ,
            d.Comment ,
            d.DonationDate ,
			cp.CampaignTypeFundraisingEntityId
    FROM    Disbursements d
            INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
            INNER JOIN Markets m ON d.MarketId = m.MarketId
            INNER JOIN dbo.FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
            INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
			INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = d.FundraisingCategoryId
            LEFT OUTER JOIN dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
            LEFT OUTER JOIN dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
            INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
            LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                               AND d.FundraisingEntityId = l.FundraisingEntityId
    WHERE   RecordTypeId in (1, 4)
            AND d.FundraisingEntityId NOT IN ( 56, 20, 21, 154 )
/*---------------------update below to use disbursementdate*/
    UNION ALL

SELECT 
1 AS CategoryId ,
            'National Corporate Partners' AS Category ,
			d.FundraisingCategoryId,
			fc.FundraisingCategory,
            FundraisingYear ,
            dp.DisbursementQuarter ,
            dp.DisbursementYear ,
            d.FundraisingEntityId ,
            p.DisplayName ,
            m.MarketId ,
            m.Marketname ,
            d.CampaignDetailsId ,
            cd.CampaignDetailName ,
            c.CampaignName ,
            ct.CampaignTypeId ,
            ct.CampaignType ,
            p.DisplayName ,
            CurrencyTypeId ,
            Amount ,
            FundTypeId ,
            RecordTypeId ,
            NULL AS PledgeTypeId ,
            DirectToHospital ,
            m.RegionId ,
            l.LocationId ,
            d.Comment ,
            d.DonationDate, 
			cp.CampaignTypeFundraisingEntityId
    FROM    Disbursements d
            INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
            INNER JOIN Markets m ON d.MarketId = m.MarketId
            INNER JOIN dbo.FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
            INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
			INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = d.FundraisingCategoryId
            LEFT OUTER JOIN dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
            LEFT OUTER JOIN dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
            INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
            LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                               AND d.FundraisingEntityId = l.FundraisingEntityId
    WHERE   RecordTypeId = 1
            AND ( p.FundraisingEntityId IN ( 20, 21 )
             AND ( cp.CampaignTypeFundraisingEntityId IS NULL
                     )
                )
    UNION ALL
    SELECT  2 AS CategoryId ,
            'National Programs and Events' AS Category ,
			d.FundraisingCategoryId,
			fc.FundraisingCategory,
            FundraisingYear ,
            dp.DisbursementQuarter ,
            dp.DisbursementYear ,
            cp.FundraisingEntityId ,
            p.DisplayName ,
            m.MarketId ,
            m.Marketname ,
            d.CampaignDetailsId ,
            cd.CampaignDetailName ,
            c.CampaignName ,
            ct.CampaignTypeId ,
            ct.CampaignType ,
            CASE WHEN pp.DisplayName LIKE '%Radiothon'
                 THEN 'Radiothon Announced'
                 ELSE pp.DisplayName
            END AS programDisplayName ,
            CurrencyTypeId ,
            Amount ,
            FundTypeId ,
            RecordTypeId ,
            NULL AS PledgeTypeId ,
            DirectToHospital ,
            m.RegionId ,
            l.LocationId ,
            d.Comment ,
            d.DonationDate ,
			cp.CampaignTypeFundraisingEntityId
    FROM    Disbursements d
            INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
            INNER JOIN Markets m ON d.MarketId = m.MarketId
            INNER JOIN dbo.FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
            INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
			INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = d.FundraisingCategoryId
            LEFT OUTER JOIN dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
            LEFT OUTER JOIN dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
            INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
            LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                               AND d.FundraisingEntityId = l.FundraisingEntityId
    WHERE   RecordTypeId = 1
            AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
                )
            AND NOT ( c.CampaignTypeId IN ( 27, 32 )
                      AND d.FundraisingYear >= 2013
                    )
/*-pledge data for ntl programs*/
    UNION ALL
    SELECT  2 AS CategoryId ,
            'National Programs and Events' AS Category ,
			'' ,
			'' ,
            FundraisingYear ,
            d.Quarter ,
            YEAR(d.PledgeDate) AS DisbYear ,
            d.FundraisingEntityId ,
            p.DisplayName ,
            m.MarketId ,
            m.Marketname ,
            d.CampaignDetailsId ,
            cd.CampaignDetailName ,
            c.CampaignName ,
            ct.CampaignTypeId ,
            ct.CampaignType ,
            CASE WHEN pp.DisplayName LIKE '%Radiothon'
                 THEN 'Radiothon Announced'
                 ELSE pp.DisplayName
            END AS DisplayName ,
            CurrencyTypeId ,
            Amount ,
            NULL ,
            NULL ,
            PledgeTypeId ,
            directtohospital ,
            m.RegionId ,
            NULL ,
            NULL ,
            d.PledgeDate,
			cp.CampaignTypeFundraisingEntityId
    FROM    PledgeData d
            INNER JOIN Markets m ON d.MarketId = m.MarketId
            INNER JOIN dbo.FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
            INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
            LEFT OUTER JOIN dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
            LEFT OUTER JOIN dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
    WHERE   pledgetypeid IN ( 1, 2 )
    UNION ALL
    SELECT  4 AS CategoryId ,
            'Overlap' AS Category ,
			d.FundraisingCategoryId,
			fc.FundraisingCategory,
            FundraisingYear ,
            dp.DisbursementQuarter ,
            dp.DisbursementYear ,
            cp.FundraisingEntityId ,
            p.DisplayName ,
            m.MarketId ,
            m.Marketname ,
            d.CampaignDetailsId ,
            cd.CampaignDetailName ,
            c.CampaignName ,
            ct.CampaignTypeId ,
            ct.CampaignType ,
            pp.DisplayName ,
            CurrencyTypeId ,
            Amount * -1 AS Amount,
            FundTypeId ,
            RecordTypeId ,
            NULL AS PledgeTypeId ,
            DirectToHospital ,
            m.RegionId ,
            l.LocationId ,
            d.Comment ,
            d.DonationDate ,
			cp.CampaignTypeFundraisingEntityId
    FROM    Disbursements d
            INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
            INNER JOIN Markets m ON d.MarketId = m.MarketId
            INNER JOIN dbo.FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
            INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
			INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = d.FundraisingCategoryId
            LEFT OUTER JOIN dbo.CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
            LEFT OUTER JOIN dbo.FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
            INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
            LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                               AND d.FundraisingEntityId = l.FundraisingEntityId
    WHERE   RecordTypeId = 1
            AND cp.FundraisingEntityId NOT IN ( 56, 20, 21, 154 ) /*---------------------update below to use disbursementdate*/
            AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
                  AND (disbursementdate != '1900-01-01'
                           )
                      )


