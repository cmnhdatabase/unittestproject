﻿

CREATE VIEW [dbo].[vwDMQuarterlyEvents]
AS
    SELECT  c.CampaignName ,
            StartDate ,
            cd.CampaignYear
    FROM    DanceMarathons dm
            INNER JOIN CampaignDetails cd ON dm.CampaignDetailsId = cd.CampaignDetailsId
            INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
            INNER JOIN Disbursements d ON cd.CampaignDetailsId = d.CampaignDetailsId



