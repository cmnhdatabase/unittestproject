﻿
CREATE VIEW [dbo].[vwPledgeDataRadiothonTypes]
AS
	SELECT [PledgeId], [RadiothonTypeId]
	FROM [dbo].[PledgeDataRadiothonType]
