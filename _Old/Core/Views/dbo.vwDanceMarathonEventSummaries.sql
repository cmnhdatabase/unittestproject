﻿
CREATE VIEW [dbo].[vwDanceMarathonEventSummaries]
AS
	SELECT c.[CampaignId], c.[CampaignName], cd.[CampaignDetailsId], cd.[CampaignDetailName], cd.[StartDate], cd.[EndDate], dm.[DanceMarathonId], dm.[SchoolId], 
		dm.[TotalHours], dm.[Shifts], dm.[AnticipatedParticipants], dm.[TotalParticipants], dm.[ContactFirstName], dm.[ContactLastName], dm.[ContactEmail], 
		dm.[GrossTotal], dm.[ToteBoardImagePath], dm.[EventId], dm.[Note], dm.[SubmittedBy], dm.[SubmittedDate], dm.[ModifiedBy], dm.[ModifiedDate], dm.[Active], 
		dm.[ManagerId] AS [ManagerContactId], dm.[Nickname], co.[FirstName] AS [ManagerFirstName], co.[LastName] AS [ManagerLastName],
		(SELECT SUM([Amount]) AS Expr1
		FROM [dbo].[PledgeData] AS pd
		WHERE ([CampaignDetailsId] = cd.[CampaignDetailsId]) AND ([PledgeTypeId] = 6)) AS [GoalAmount],
		(SELECT SUM([Amount]) AS Expr1
		FROM [dbo].[PledgeData] AS pd
		WHERE ([CampaignDetailsId] = cd.[CampaignDetailsId]) AND ([PledgeTypeId] = 5)) AS [AnnouncedTotal]
	FROM [dbo].[Campaigns] AS c 
		INNER JOIN [dbo].[CampaignDetails] AS cd ON c.[CampaignId] = cd.[CampaignId]
		INNER JOIN [dbo].[DanceMarathons] AS dm ON cd.[CampaignDetailsId] = dm.[CampaignDetailsId] 
		LEFT OUTER JOIN
			(SELECT [ContactId], [FirstName], [LastName]
			FROM [Contacts].[dbo].[Contacts]
			WHERE ([Active] = 1)) AS co ON dm.[ManagerId] = co.[ContactId]
	WHERE (dm.[Active] = 1) AND (cd.[StartDate] IS NOT NULL) AND (cd.[EndDate] IS NOT NULL);
