﻿

/****** Object:  View [dbo].[vwCurrentZipCodesAllCountry]    Script Date: 08/26/2011 13:50:33 ******/

CREATE VIEW [dbo].[vwCurrentZipCodesAllCountry]
AS

SELECT
 m.MarketName AS PrimaryMarket ,
        m2.MarketName AS Submarket ,
        p.Abbreviation AS State ,
        County AS County ,
        pc.City AS City ,
        PostalCode AS ZipCode
FROM    dbo.PostalCodes pc
        INNER JOIN dbo.Markets m ON m.MarketId = pc.MarketId
        INNER JOIN dbo.Markets m2 ON m2.MarketId = pc.SubMarketId
        INNER JOIN dbo.Provinces p ON p.ProvinceId = pc.ProvinceId
WHERE m.Active > 0

