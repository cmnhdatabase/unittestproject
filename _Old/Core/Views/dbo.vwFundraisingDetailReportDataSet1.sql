﻿

CREATE VIEW [dbo].[vwFundraisingDetailReportDataSet1]
AS
SELECT    fe.Name ,
                    m.MarketName ,
					DirectToHospital,
					d.Amount,
					fundraisingYear ,
                    LocationNumber ,
                    LocationName ,
                    l.Address1 ,
                    l.Address2 ,
                    l.City ,
                    p.Province ,
                    PostalCode ,
                    co.abbreviation ,
                    b.BatchNumber ,
					d.DonationDate,
                    ct.CampaignType ,
                    c.CampaignName,
					RecordTypeId
          FROM      Disbursements d
                    INNER JOIN FundraisingEntities fe ON d.FundraisingEntityId = fe.FundraisingEntityId
                    INNER JOIN Markets m ON d.MarketId = m.MarketId
                    LEFT JOIN Locations l ON d.LocationId = l.LocationId
                    INNER JOIN Provinces p ON l.ProvinceId = p.ProvinceId
                    LEFT JOIN Batches b ON d.BatchId = b.BatchId
                    INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                    INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                    INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                    INNER JOIN Countries co ON l.CountryId = co.CountryId



