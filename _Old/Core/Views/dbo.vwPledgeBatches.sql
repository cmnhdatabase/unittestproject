﻿

CREATE VIEW [dbo].[vwPledgeBatches]
AS
SELECT [PledgeBatchId]
      ,[PledgeBatchNumber]
      ,[PledgeBatchDate]
  FROM [Core].[dbo].[PledgeBatches]

