﻿
CREATE VIEW [dbo].[vwProvinces]
AS
	SELECT [ProvinceId], [CountryId], [Province], [Abbreviation]
	FROM [dbo].[Provinces]


