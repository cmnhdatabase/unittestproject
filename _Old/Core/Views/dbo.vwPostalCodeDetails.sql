﻿CREATE VIEW [dbo].[vwPostalCodeDetails]
AS
SELECT        dbo.PostalCodes.PostalCode, m.MarketId, m.MarketName, ct.CountryId, ct.CountryName, m.RegionId, r.RegionName, mk.MarketName AS SubMarketName, dbo.Provinces.ProvinceId, dbo.Provinces.Province, 
                         dbo.Provinces.Abbreviation AS ProvinceAbbreviation, dbo.PostalCodes.City, dbo.PostalCodes.County, dbo.PostalCodes.PopulationEstimate, dbo.PostalCodes.Latitude, dbo.PostalCodes.Longitude
FROM            dbo.PostalCodes INNER JOIN
                         dbo.Markets AS m ON dbo.PostalCodes.MarketId = m.MarketId INNER JOIN
                         dbo.Provinces ON dbo.PostalCodes.ProvinceId = dbo.Provinces.ProvinceId INNER JOIN
                         dbo.Markets AS mk ON dbo.PostalCodes.SubMarketId = mk.MarketId LEFT OUTER JOIN
                         dbo.Countries AS ct ON dbo.PostalCodes.CountryId = ct.CountryId LEFT OUTER JOIN
                         dbo.Regions AS r ON m.RegionId = r.RegionId



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "m"
            Begin Extent = 
               Top = 0
               Left = 453
               Bottom = 191
               Right = 626
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "mk"
            Begin Extent = 
               Top = 167
               Left = 723
               Bottom = 297
               Right = 896
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "r"
            Begin Extent = 
               Top = 6
               Left = 809
               Bottom = 102
               Right = 979
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 264
               Left = 28
               Bottom = 394
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PostalCodes"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 240
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Provinces"
            Begin Extent = 
               Top = 192
               Left = 269
               Bottom = 322
               Right = 439
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         T', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPostalCodeDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'able = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPostalCodeDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPostalCodeDetails';

