﻿
-- ==========================================================================================
-- = Create new views
-- ==========================================================================================

CREATE VIEW [dbo].[vwDanceMarathonTypes]
AS
	SELECT [DanceMarathonTypeId], [DanceMarathonType]
	FROM [dbo].[DanceMarathonTypes];
