﻿/****** Object:  View [dbo].[vwHospitalPhoneNumbers] ******/
CREATE VIEW [dbo].[vwHospitalPhoneNumbers]
AS
	SELECT [HospitalId], [PhoneId]
	FROM [dbo].[HospitalPhoneNumbers];

