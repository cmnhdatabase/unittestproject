﻿
CREATE VIEW [dbo].[vwFundraisingDataCMNH]
AS
SELECT  df.FundraisingCategoryId ,
        fc.FundraisingCategory ,
        d.FundraisingYear ,
        dp.DisbursementQuarter ,
        dp.DisbursementYear ,
        df.FundraisingEntityId ,
        f.DisplayName ,
        m.MarketId ,
        m.MarketName ,
        d.CampaignDetailsId ,
        cd.CampaignDetailName ,
        c.CampaignId ,
        c.CampaignName ,
        ct.CampaignTypeId ,
        ct.CampaignType ,
        --pp.DisplayName ,
        CurrencyTypeId ,
        CASE WHEN f.FundraisingCategoryId = 0 THEN d.Amount * -1
             ELSE d.Amount
        END Amount ,
        FundTypeId ,
        RecordTypeId ,
        NULL AS PledgeTypeId ,
        DirectToHospital ,
        m.RegionId ,
        l.LocationId ,
        d.Comment ,
        d.DonationDate
FROM    dbo.DisbursementFundraisingEntities df
        INNER JOIN dbo.Disbursements d ON d.DisbursementId = df.DisbursementId
        INNER JOIN dbo.FundraisingEntities f ON f.FundraisingEntityId = df.FundraisingEntityId
        INNER JOIN Core.dbo.CampaignDetails cd ON cd.CampaignDetailsId = d.CampaignDetailsId
        INNER JOIN Core.dbo.Campaigns c ON c.CampaignId = cd.CampaignId
        INNER JOIN Core.dbo.CampaignTypes ct ON ct.CampaignTypeId = c.CampaignTypeId
        INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = f.FundraisingCategoryId
        LEFT OUTER JOIN dbo.DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
        INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
        LEFT OUTER JOIN dbo.Locations l ON l.LocationId = d.LocationId
                                           AND fc.FundraisingCategoryId = 1
WHERE   d.RecordTypeId = 1
UNION ALL
SELECT  f.FundraisingCategoryId ,
        fc.FundraisingCategory ,
        pd.FundraisingYear ,
        pd.Quarter ,
        YEAR(pd.PledgeDate) AS DisbYear ,
        f.FundraisingEntityId ,
        f.DisplayName AS FundraisingEntityName ,
        m.MarketId ,
        m.MarketName ,
        pd.CampaignDetailsId ,
        cd.CampaignDetailName ,
        c.CampaignId ,
        c.CampaignName ,
        ct.CampaignTypeId ,
        ct.CampaignType ,
                    --CASE WHEN f.DisplayName LIKE '%Radiothon'
                    --     THEN 'Radiothon Announced'
                    --     ELSE f.DisplayName
                    --END AS DisplayName ,
        CurrencyTypeId ,
        Amount ,
        NULL ,
        NULL ,
        PledgeTypeId ,
        DirectToHospital ,
        m.RegionId ,
        NULL ,
        NULL ,
        pd.PledgeDate
FROM    dbo.PledgeData pd
        INNER JOIN dbo.FundraisingEntities f ON f.FundraisingEntityId = pd.FundraisingEntityId
        INNER JOIN dbo.FundraisingCategories fc ON fc.FundraisingCategoryId = f.FundraisingCategoryId
        INNER JOIN dbo.Markets m ON m.MarketId = pd.MarketId
        INNER JOIN Core.dbo.CampaignDetails cd ON cd.CampaignDetailsId = pd.CampaignDetailsId
        INNER JOIN Core.dbo.Campaigns c ON c.CampaignId = cd.CampaignId
        INNER JOIN Core.dbo.CampaignTypes ct ON ct.CampaignTypeId = c.CampaignTypeId
WHERE   PledgeTypeId IN ( 1, 2 );


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwFundraisingDataCMNH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwFundraisingDataCMNH';

