﻿
CREATE VIEW [dbo].[vwTelethonTvStations]
AS
	SELECT [TelethonId], [TvStationId], [PrimaryStation]
	FROM [dbo].[TelethonTvStations]
