﻿


CREATE VIEW [dbo].[vwInvoiceDetail]
AS

SELECT 
InvoiceYear, InvoiceQuarter, InvoiceDate, CampaignName, ct.CampaignType, fe.FundraisingEntityId, fe.DisplayName, d.DisbursementId, d.Amount, d.DonationDate, 'D' AS 'Table', DirectToHospital, m.MarketName
FROM dbo.vwInvoices i
LEFT OUTER JOIN dbo.vwInvoicedDisbursements id ON id.InvoiceId = i.InvoiceId
INNER JOIN dbo.Disbursements d ON d.DisbursementId = id.DisbursementId
INNER JOIN dbo.CampaignDetails cd ON cd.CampaignDetailsId = d.CampaignDetailsId
INNER JOIN dbo.Campaigns c ON c.CampaignId = cd.CampaignId
INNER JOIN dbo.CampaignTypes ct ON ct.CampaignTypeId = c.CampaignTypeId
INNER JOIN dbo.FundraisingEntities fe ON fe.FundraisingEntityId = d.FundraisingEntityId
INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
UNION ALL
SELECT 
InvoiceYear, InvoiceQuarter, InvoiceDate, CampaignName, ct.CampaignType, fe.FundraisingEntityId, fe.DisplayName, pd.PledgeId, pd.Amount, pd.PledgeDate, 'P' AS 'Table', pd.DirectToHospital, m.MarketName
FROM dbo.vwInvoices i
LEFT OUTER JOIN dbo.vwInvoicedPledges id ON id.InvoiceId = i.InvoiceId
INNER JOIN dbo.PledgeData pd ON pd.PledgeId = id.PledgeId
INNER JOIN dbo.CampaignDetails cd ON cd.CampaignDetailsId = pd.CampaignDetailsId
INNER JOIN dbo.Campaigns c ON c.CampaignId = cd.CampaignId
INNER JOIN dbo.CampaignTypes ct ON ct.CampaignTypeId = c.CampaignTypeId
INNER JOIN dbo.FundraisingEntities fe ON fe.FundraisingEntityId = pd.FundraisingEntityId
INNER JOIN dbo.Markets m ON m.MarketId = pd.MarketId




