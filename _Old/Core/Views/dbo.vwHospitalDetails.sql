﻿
CREATE VIEW [dbo].[vwHospitalDetails]
AS
	SELECT h.[HospitalId], h.[HospitalName], h.[FriendlyHospitalName], h.[MarketId], m.[MarketName], h.[Address1], h.[Address2], h.[City], h.[ProvinceId], 
		p.[Province], h.[PostalCode], h.[CountryId], c.[CountryName], h.[LongDescription], h.[ShortDescription], h.[Website], h.[Latitude], 
		h.[Longitude], h.[Active]
	FROM [dbo].[Hospitals] h
		LEFT OUTER JOIN [dbo].[Markets] m ON h.[MarketId] = m.[MarketId]
		LEFT OUTER JOIN [dbo].[Provinces] p ON h.[ProvinceId] = p.[ProvinceId]
		LEFT OUTER JOIN [dbo].[Countries] c ON h.[CountryId] = c.[CountryId]


