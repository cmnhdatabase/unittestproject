﻿

CREATE VIEW [dbo].[vwSchoolContacts]
AS
SELECT DISTINCT 
	s.SchoolId, s.Address1, s.Address2, 'Schools' AS Category, '2' AS CategoryId, s.City, m.MarketName AS Company, ISNULL(s.Address1, '') + ' ' + ISNULL(s.Address2, '') + ' ' + ISNULL(s.City, '') 
	+ ' ' + ISNULL(p.Abbreviation, '') + ' ' + ISNULL(s.PostalCode, '') AS CompleteAddress, c.CountryId, c.CountryName, '' AS Fax, '' AS FirstName, s.SchoolName AS FullName, '' AS LastName, 
	isnull(cc.Phone, '') AS Phone, s.PostalCode, p.Abbreviation AS Province, cs.[Description] AS SubCategory, cs.SubcategoryId AS 'SubcategoryId',isnull((cc.FirstName + ' ' + cc.LastName), '')  AS Title, 
	'https://cdn.cmnhospitals.org/MissionControl/Apps/Contacts/ProfileImages/School.png' AS URI, isnull(cc.Email, '') AS Email, s.HospitalId
FROM dbo.Schools AS s 
	LEFT OUTER JOIN [Contacts].[dbo].[SchoolContacts] csc on s.SchoolId = csc.SchoolId
	LEFT OUTER JOIN [Contacts].[dbo].[Contacts] cc on csc.ContactId = cc.ContactId
	LEFT OUTER JOIN [Contacts].[dbo].[ContactsSubcategories] ccs on cc.ContactId = ccs.ContactId
	LEFT OUTER JOIN [Contacts].[dbo].[Subcategories] cs on ccs.SubcategoryId = cs.SubcategoryId
	LEFT OUTER JOIN dbo.Hospitals h ON s.HospitalId = h.HospitalId
	LEFT OUTER JOIN dbo.Markets m on h.MarketId=m.MarketId
	LEFT OUTER JOIN dbo.Provinces AS p ON p.ProvinceId = s.ProvinceId 
	LEFT OUTER JOIN dbo.Countries AS c ON c.CountryId = s.CountryId 
	LEFT OUTER JOIN dbo.Images AS i ON CAST(s.SchoolId AS varchar(50)) = i.Value AND i.TypeId = 3


