﻿




CREATE VIEW [dbo].[vwCampaignTypeTotalsDataSet1]
AS

SELECT  m.MarketName ,
        f.Name ,
        cd.CampaignDetailName,
        d.Comment ,
        d.Amount
		, CD.CampaignYear
		, c.CampaignTypeId
		, ct.CampaignType
		, ct.CampaignTypeId AS CampaignTypeId2
FROM    Core.dbo.CampaignDetails cd
        INNER JOIN Core.dbo.Campaigns c ON cd.CampaignId = c.CampaignId
        INNER JOIN Core.dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
        INNER JOIN dbo.Disbursements d ON d.CampaignDetailsId = cd.CampaignDetailsId
        INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
        INNER JOIN dbo.FundraisingEntities f ON d.FundraisingEntityId = f.FundraisingEntityId


