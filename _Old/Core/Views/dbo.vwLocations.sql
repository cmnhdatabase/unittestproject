﻿
CREATE VIEW [dbo].[vwLocations]
AS
  SELECT [LocationId], [FundraisingEntityId], [LocationName], [LocationNumber], [Address1], [Address2], [City], [ProvinceId], 
    [PostalCode], [CountryId], [PropertyTypeId], [Latitude], [Longitude], [Active]
  FROM [dbo].[Locations];
