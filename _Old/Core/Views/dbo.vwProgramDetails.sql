﻿CREATE VIEW [dbo].[vwProgramDetails]
AS
	SELECT fe.[FundraisingEntityId], fe.[Name], fe.[FriendlyName], fe.[DisplayName], fe.[YearStarted], 
		fe.[CountryId], c.[CountryName], c.[Abbreviation], fe.[Active]
	FROM [dbo].[FundraisingEntities] fe
		LEFT OUTER JOIN [dbo].[Countries] c ON fe.[CountryId] = c.[CountryId]
	WHERE fe.[Active] = 1 AND fe.[FundraisingCategoryId] = 2;


