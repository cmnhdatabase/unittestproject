﻿

CREATE VIEW [dbo].[vwUnreportedRadiothons]
AS 
	SELECT DISTINCT [CampaignId], [CampaignName], s.[CampaignDetailsId], [CampaignDetailName], [CampaignDetailName] as [ShortDescription], 
	  YEAR([EndDate]) as [CampaignYear], [StartDate], [EndDate], s.[RadiothonId], rs.[RadioStationId], s.[City], s.[OnAirHours], 
	  s.[OnAirMinutes], [DirectorId] as [Director]
	FROM [dbo].[vwRadiothonEventSummaries] s
	  JOIN [dbo].[RadiothonRadioStations] rrs ON rrs.[RadiothonId] = s.[RadiothonId]
	  JOIN [dbo].[RadioStations] rs ON rs.[RadioStationId] = rrs.[RadioStationId]
	WHERE [AnnouncedTotal] IS NULL AND [PrimaryRadioStation] = 1 AND (DATEADD(dd, 0, DATEDIFF(dd, 0, [EndDate])) < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))) AND YEAR([EndDate]) >= 2014

