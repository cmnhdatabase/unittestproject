﻿
CREATE VIEW [dbo].[vwPartnerAccountExecutiveDetails]
AS
SELECT        p.FundraisingEntityId, p.ContactId, p.PartnerAccountStaffTypeId, t.Description
FROM            dbo.PartnerAccountExecutives AS p LEFT OUTER JOIN
                         dbo.PartnerAccountStaffTypes AS t ON p.PartnerAccountStaffTypeId = t.PartnerAccountStaffTypeId

