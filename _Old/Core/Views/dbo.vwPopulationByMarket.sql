﻿
CREATE VIEW [dbo].[vwPopulationByMarket]
AS
SELECT [MarketId]
      ,[CampaignYear]
      ,[CountryId]
      ,[PopulationEstimate]
  FROM [Core].[dbo].[PopulationByMarket]