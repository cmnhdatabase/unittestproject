﻿
CREATE VIEW unittest.vwTypeTotals
AS
SELECT	'Tables' Type,
		COUNT(1) total
FROM	sys.tables
UNION ALL
SELECT	'Stored Procs' ,
		COUNT(1)
FROM	sys.procedures
UNION ALL
SELECT	'Views' ,
		COUNT(1)
FROM	sys.views
UNION ALL
SELECT	'Constraints' ,
		COUNT(1)
FROM	sys.sysconstraints
