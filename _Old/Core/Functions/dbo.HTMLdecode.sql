﻿CREATE FUNCTION [dbo].[HTMLdecode]
(@input NVARCHAR (4000))
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [HTMLdecode].[UserDefinedFunctions].[cfnHtmlDecode]

