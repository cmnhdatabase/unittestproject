﻿CREATE FUNCTION [dbo].[REMOVEYEARFROMEVENTNAME](@string VARCHAR(MAX))
RETURNS VARCHAR(MAX)
BEGIN
RETURN Ltrim(Rtrim(replace(replace(
               replace(
               replace(
               replace(
               replace(
               replace(
               replace(
               replace(@string
                  , '2007','')
                  ,'2008','')
                  ,'2009','')
                  ,'2010','')
                  ,'2011','')
                  ,'2012','')
                  ,'2013','')
                  ,'2014','')
				  ,'  ', ' ')))
END
