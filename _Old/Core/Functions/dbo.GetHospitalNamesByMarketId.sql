﻿



CREATE FUNCTION [dbo].[GetHospitalNamesByMarketId]
(
	@MarketId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @HospitalName VARCHAR(2000)

	SELECT @HospitalName = COALESCE(@HospitalName + ',', '') + CAST(h.HospitalName AS VARCHAR(500))
                                   FROM 
								   Markets m
								   JOIN 
								   Core.dbo.Hospitals h ON h.MarketId = M.MarketId AND h.active = 1
								   WHERE 
								   m.marketid = @MarketId
	RETURN (@HospitalName)

END




