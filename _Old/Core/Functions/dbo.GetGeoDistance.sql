﻿


--IF OBJECT_ID('dbo.GetGeoDistance') IS NOT NULL
--	DROP FUNCTION dbo.GetGeoDistance
--END



CREATE FUNCTION [dbo].[GetGeoDistance] (@Lat1 FLOAT, @Long1 FLOAT, @Lat2 FLOAT, @Long2 FLOAT)
RETURNS FLOAT
AS
	BEGIN
		RETURN ( 3960 * ACOS( COS( RADIANS( @Lat1 ) ) *
  COS( RADIANS(@Lat2) ) * COS( RADIANS(  @Long2  ) - RADIANS( @Long1 ) ) +
  SIN( RADIANS( @Lat1 ) ) * SIN( RADIANS( @Lat2 ) ) ) )
	END;


