﻿
/* this function strips all non-numeric values out of a phone number */

CREATE Function [dbo].[fnRemoveNonNumericCharacters](@strText VARCHAR(1000))
RETURNS VARCHAR(1000)
AS
BEGIN
	WHILE PATINDEX('%[^0-9]%', @strText) > 0
	BEGIN
		SET @strText = STUFF(@strText, PATINDEX('%[^0-9]%', @strText), 1, '')
	END
    RETURN @strText
END

/*
select dbo.[fnRemoveNonNumericCharacters]('asdf344534 45dfg') as NumberOnly

NumberOnly
-------------------
34453445

*/
