﻿CREATE TABLE [dbo].[PledgeData] (
    [PledgeId]            INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityId] INT      NULL,
    [MarketId]            INT      NULL,
    [FundraisingYear]     INT      NOT NULL,
    [CampaignDetailsId]   INT      NOT NULL,
    [CurrencyTypeId]      INT      NOT NULL,
    [Amount]              MONEY    NOT NULL,
    [PledgeTypeId]        INT      NOT NULL,
    [Quarter]             SMALLINT NULL,
    [PledgeDate]          DATE     NULL,
    [DirectToHospital]    BIT      NOT NULL,
    [DateRecorded]        DATE     CONSTRAINT [DF_PledgeData_DateRecorded] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           INT      CONSTRAINT [DF_PledgeData_CreatedBy] DEFAULT ((18633)) NOT NULL,
    [PledgeBatchId]       INT      NULL,
    PRIMARY KEY CLUSTERED ([PledgeId] ASC),
    CONSTRAINT [FK_PledgeData_CampaignDetails] FOREIGN KEY ([CampaignDetailsId]) REFERENCES [dbo].[CampaignDetails] ([CampaignDetailsId]),
    CONSTRAINT [FK_PledgeData_Currencies] FOREIGN KEY ([CurrencyTypeId]) REFERENCES [dbo].[Currencies] ([CurrencyTypeId]),
    CONSTRAINT [FK_PledgeData_FundraisingEntities] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId]),
    CONSTRAINT [FK_PledgeData_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_PledgeData_PledgeBatches] FOREIGN KEY ([PledgeBatchId]) REFERENCES [dbo].[PledgeBatches] ([PledgeBatchId]) ON DELETE CASCADE,
    CONSTRAINT [FK_PledgeData_PledgeTypes] FOREIGN KEY ([PledgeTypeId]) REFERENCES [dbo].[PledgeTypes] ([PledgeTypeId])
);


GO
CREATE NONCLUSTERED INDEX [ixVwFundraisingData3]
    ON [dbo].[PledgeData]([FundraisingYear] ASC, [PledgeTypeId] ASC)
    INCLUDE([FundraisingEntityId], [MarketId], [CampaignDetailsId], [CurrencyTypeId], [Amount]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All pledge data', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'PledgeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year the pledge was made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'FundraisingYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to CampaignDetails Table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'CampaignDetailsId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to Currencies table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'CurrencyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Amount of the pledge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PledgeTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'PledgeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Quarter that pledge was made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'Quarter';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Pledge was made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'PledgeDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Was pledge made directly to the hospitol', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeData', @level2type = N'COLUMN', @level2name = N'DirectToHospital';

