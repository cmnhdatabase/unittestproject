﻿CREATE TABLE [dbo].[Provinces] (
    [ProvinceId]   INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CountryId]    INT          NOT NULL,
    [Province]     VARCHAR (50) NOT NULL,
    [Abbreviation] VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_Provinces_1] PRIMARY KEY CLUSTERED ([ProvinceId] ASC),
    CONSTRAINT [FK_Provinces_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table has information on all state or provinces', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Provinces';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Provinces', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Provinces', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Spelled out name of state or province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Provinces', @level2type = N'COLUMN', @level2name = N'Province';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Abbreciation of state or province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Provinces', @level2type = N'COLUMN', @level2name = N'Abbreviation';

