﻿CREATE TABLE [dbo].[DanceMarathons] (
    [DanceMarathonId]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignDetailsId]        INT           NOT NULL,
    [SchoolId]                 INT           NULL,
    [TotalHours]               INT           NOT NULL,
    [Shifts]                   INT           NULL,
    [AnticipatedParticipants]  INT           NULL,
    [TotalParticipants]        INT           NULL,
    [ContactFirstName]         VARCHAR (50)  NULL,
    [ContactLastName]          VARCHAR (50)  NULL,
    [ContactEmail]             VARCHAR (100) NULL,
    [GrossTotal]               MONEY         NULL,
    [ToteBoardImagePath]       VARCHAR (500) NULL,
    [EventId]                  INT           NULL,
    [Note]                     VARCHAR (MAX) NULL,
    [SubmittedDate]            DATETIME      NOT NULL,
    [ModifiedDate]             DATETIME      NOT NULL,
    [Active]                   BIT           NOT NULL,
    [SubmittedBy]              INT           NULL,
    [ModifiedBy]               INT           NULL,
    [ManagerId]                INT           NULL,
    [RegistrationUrl]          VARCHAR (MAX) NULL,
    [DonationUrl]              VARCHAR (MAX) NULL,
    [HasMiniMarathons]         BIT           NOT NULL,
    [MiniMarathonCount]        INT           NOT NULL,
    [MiniMarathonContribution] MONEY         NOT NULL,
    [DanceMarathonTypeId]      INT           NOT NULL,
    [Nickname]                 VARCHAR (300) NULL,
    CONSTRAINT [PK_DanceMarathon] PRIMARY KEY CLUSTERED ([DanceMarathonId] ASC),
    CONSTRAINT [FK_DanceMarathons_CampaignDetailsId] FOREIGN KEY ([CampaignDetailsId]) REFERENCES [dbo].[CampaignDetails] ([CampaignDetailsId]),
    CONSTRAINT [FK_DanceMarathons_DanceMarathonTypeId] FOREIGN KEY ([DanceMarathonTypeId]) REFERENCES [dbo].[DanceMarathonTypes] ([DanceMarathonTypeId]),
    CONSTRAINT [FK_DanceMarathons_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [dbo].[Schools] ([SchoolId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for Dance Marathons', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'DanceMarathonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the CampaignDetails table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'CampaignDetailsId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the schools table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'SchoolId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of hours for Dance Marathon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'TotalHours';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of expected participants', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'AnticipatedParticipants';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Actual number of people', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'TotalParticipants';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dance Marathon contact first name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'ContactFirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dance Marathon contact last name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'ContactLastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dance Marathon contact email address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'ContactEmail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total amount raised from Dance Marathon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'GrossTotal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Image address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'ToteBoardImagePath';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the events table in the Calendar DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'EventId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Any notes for the event', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'Note';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dance Marathon was submitted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'SubmittedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date of last modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is Dance Marathon active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathons', @level2type = N'COLUMN', @level2name = N'Active';

