﻿CREATE TABLE [dbo].[Radiothons] (
    [RadiothonId]               INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignDetailsId]         INT           NOT NULL,
    [City]                      VARCHAR (100) NOT NULL,
    [OnAirHours]                SMALLINT      NULL,
    [OnAirMinutes]              SMALLINT      NULL,
    [LanguageId]                INT           NOT NULL,
    [PhoneBank]                 VARCHAR (100) NOT NULL,
    [Note]                      VARCHAR (MAX) NULL,
    [SubmittedDate]             DATE          NOT NULL,
    [ModifiedDate]              DATE          NOT NULL,
    [Active]                    BIT           NOT NULL,
    [EventId]                   INT           NULL,
    [RadiothonContactEmailSent] BIT           NOT NULL,
    [SubmittedBy]               INT           NULL,
    [ModifiedBy]                INT           NULL,
    [ToteBoardImagePath]        VARCHAR (500) NULL,
    CONSTRAINT [PK_Radiothons] PRIMARY KEY CLUSTERED ([RadiothonId] ASC),
    CONSTRAINT [FK_Radiothons_CampaignDetailsId] FOREIGN KEY ([CampaignDetailsId]) REFERENCES [dbo].[CampaignDetails] ([CampaignDetailsId]),
    CONSTRAINT [FK_Radiothons_Languages] FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Languages] ([LanguageId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table holds radiothon information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'RadiothonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the CampaignDetails table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'CampaignDetailsId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City that radiothon is held in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of hours the radiothon is on air', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'OnAirHours';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of minutes the radiothon is on air', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'OnAirMinutes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Languages table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'LanguageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if the radiothon uses phone bank or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'PhoneBank';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Notes for radiothon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'Note';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date that the readiothon was submitted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'SubmittedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date radiothon data was modified', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is the radiothon active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the events table in the calendar db', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'EventId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to indicate if an email has been sent to the radiothon contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radiothons', @level2type = N'COLUMN', @level2name = N'RadiothonContactEmailSent';

