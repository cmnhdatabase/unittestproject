﻿CREATE TABLE [dbo].[Locations] (
    [LocationId]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityId] INT           NULL,
    [LocationName]        VARCHAR (100) NOT NULL,
    [LocationNumber]      VARCHAR (100) NOT NULL,
    [Address1]            VARCHAR (100) NULL,
    [Address2]            VARCHAR (100) NULL,
    [City]                VARCHAR (100) NULL,
    [ProvinceId]          INT           NULL,
    [PostalCode]          VARCHAR (10)  NULL,
    [CountryId]           INT           NULL,
    [PropertyTypeId]      INT           NULL,
    [Latitude]            FLOAT (53)    NULL,
    [Longitude]           FLOAT (53)    NULL,
    [Active]              BIT           NOT NULL,
    CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED ([LocationId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ixLocations_FundraisingEntityId_Countryid]
    ON [dbo].[Locations]([FundraisingEntityId] ASC, [CountryId] ASC)
    INCLUDE([LocationId]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All location for all partners', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'LocationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the location', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'LocationName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Location store number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'LocationNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line one of the address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line two of the address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City of the address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Provinces table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal or Zip code for address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PropertyTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'PropertyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude coordinates for locations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'Latitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude coordinates for locations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'Longitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is the location active or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Locations', @level2type = N'COLUMN', @level2name = N'Active';

