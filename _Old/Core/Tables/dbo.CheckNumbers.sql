﻿CREATE TABLE [dbo].[CheckNumbers] (
    [CheckNumberID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CheckNumber]   VARCHAR (50) NULL,
    CONSTRAINT [PK_CheckNumbers] PRIMARY KEY CLUSTERED ([CheckNumberID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Check Number Information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for the CheckNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumbers', @level2type = N'COLUMN', @level2name = N'CheckNumberID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Check Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CheckNumbers', @level2type = N'COLUMN', @level2name = N'CheckNumber';

