﻿CREATE TABLE [dbo].[Currencies] (
    [CurrencyTypeId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CurrencyType]   CHAR (3) NOT NULL,
    CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED ([CurrencyTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Currency information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Currencies';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for the Currencies table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Currencies', @level2type = N'COLUMN', @level2name = N'CurrencyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the currency abbreviation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Currencies', @level2type = N'COLUMN', @level2name = N'CurrencyType';

