﻿CREATE TABLE [dbo].[HospitalPhoneNumbers] (
    [HospitalId] INT NOT NULL,
    [PhoneId]    INT NOT NULL,
    CONSTRAINT [PK_HospitalPhones] PRIMARY KEY CLUSTERED ([HospitalId] ASC),
    CONSTRAINT [FK_HospitalPhones_Hospitals] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId]),
    CONSTRAINT [FK_HospitalPhones_PhoneNumbers] FOREIGN KEY ([PhoneId]) REFERENCES [dbo].[PhoneNumbers] ([PhoneId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table tying the Hospitals table and the PhoneNumbers table together', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'HospitalPhoneNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK, it is tied to the Hospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'HospitalPhoneNumbers', @level2type = N'COLUMN', @level2name = N'HospitalId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK, it is tied to the PhoneNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'HospitalPhoneNumbers', @level2type = N'COLUMN', @level2name = N'PhoneId';

