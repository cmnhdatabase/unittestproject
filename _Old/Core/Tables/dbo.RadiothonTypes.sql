﻿CREATE TABLE [dbo].[RadiothonTypes] (
    [RadiothonTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RadiothonType]   VARCHAR (30) NULL,
    CONSTRAINT [PK_RadiothonTypes] PRIMARY KEY CLUSTERED ([RadiothonTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonTypes', @level2type = N'COLUMN', @level2name = N'RadiothonTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type of Radiothon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonTypes', @level2type = N'COLUMN', @level2name = N'RadiothonType';

