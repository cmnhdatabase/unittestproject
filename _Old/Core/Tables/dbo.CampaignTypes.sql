﻿CREATE TABLE [dbo].[CampaignTypes] (
    [CampaignTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignType]   VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_EventTypes] PRIMARY KEY CLUSTERED ([CampaignTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Names of all the campaign types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypes', @level2type = N'COLUMN', @level2name = N'CampaignTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the Campaign type name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypes', @level2type = N'COLUMN', @level2name = N'CampaignType';

