﻿CREATE TABLE [dbo].[SubSchools] (
    [SchoolId]    INT NOT NULL,
    [SubSchoolId] INT NOT NULL,
    CONSTRAINT [PK_SubSchool] PRIMARY KEY CLUSTERED ([SchoolId] ASC, [SubSchoolId] ASC),
    CONSTRAINT [FK_SubSchools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [dbo].[Schools] ([SchoolId]),
    CONSTRAINT [FK_SubSchools_SubSchoolId] FOREIGN KEY ([SubSchoolId]) REFERENCES [dbo].[Schools] ([SchoolId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to schools table primary school id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubSchools', @level2type = N'COLUMN', @level2name = N'SchoolId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to schools table sub school id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubSchools', @level2type = N'COLUMN', @level2name = N'SubSchoolId';

