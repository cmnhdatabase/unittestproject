﻿CREATE TABLE [dbo].[FundTypes] (
    [FundTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundType]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FundTypes] PRIMARY KEY CLUSTERED ([FundTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information on the types of funds we accept', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundTypes', @level2type = N'COLUMN', @level2name = N'FundTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Funding types that we use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundTypes', @level2type = N'COLUMN', @level2name = N'FundType';

