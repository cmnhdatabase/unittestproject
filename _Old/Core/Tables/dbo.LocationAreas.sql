﻿CREATE TABLE [dbo].[LocationAreas] (
    [LocationAreasId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LocationId]      INT NOT NULL,
    [AreaId]          INT NOT NULL,
    CONSTRAINT [PK_LocationAreas] PRIMARY KEY CLUSTERED ([LocationAreasId] ASC),
    CONSTRAINT [FK_LocationAreas_Areas] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Areas] ([AreaId]),
    CONSTRAINT [FK_LocationAreas_Locations] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Locations] ([LocationId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Details for location and types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationAreas';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationAreas', @level2type = N'COLUMN', @level2name = N'LocationAreasId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Locations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationAreas', @level2type = N'COLUMN', @level2name = N'LocationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the LocationTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationAreas', @level2type = N'COLUMN', @level2name = N'AreaId';

