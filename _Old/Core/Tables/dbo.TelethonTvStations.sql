﻿CREATE TABLE [dbo].[TelethonTvStations] (
    [TelethonId]     INT NOT NULL,
    [TvStationId]    INT NOT NULL,
    [PrimaryStation] BIT NOT NULL,
    CONSTRAINT [PK_TelethonTvStations] PRIMARY KEY CLUSTERED ([TelethonId] ASC, [TvStationId] ASC),
    FOREIGN KEY ([TelethonId]) REFERENCES [dbo].[Telethons] ([TelethonId]),
    FOREIGN KEY ([TvStationId]) REFERENCES [dbo].[TV] ([TVStationId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Telethons table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TelethonTvStations', @level2type = N'COLUMN', @level2name = N'TelethonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the TV table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TelethonTvStations', @level2type = N'COLUMN', @level2name = N'TvStationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Primary station for the Telethon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TelethonTvStations', @level2type = N'COLUMN', @level2name = N'PrimaryStation';

