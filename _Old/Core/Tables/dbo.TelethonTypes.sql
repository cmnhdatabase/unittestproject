﻿CREATE TABLE [dbo].[TelethonTypes] (
    [TelethonTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TelethonType]   VARCHAR (30) NULL,
    CONSTRAINT [PK_TelethonType] PRIMARY KEY CLUSTERED ([TelethonTypeId] ASC)
);

