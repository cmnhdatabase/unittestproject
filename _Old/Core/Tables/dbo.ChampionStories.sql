﻿CREATE TABLE [dbo].[ChampionStories] (
    [ChampionId] INT           NOT NULL,
    [LanguageId] INT           NOT NULL,
    [Story]      VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ChampionStories] PRIMARY KEY CLUSTERED ([ChampionId] ASC, [LanguageId] ASC),
    CONSTRAINT [FK_ChampionStories_Champions] FOREIGN KEY ([ChampionId]) REFERENCES [dbo].[Champions] ([ChampionId]),
    CONSTRAINT [FK_ChampionStories_Languages] FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Languages] ([LanguageId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for all Celebrities', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ChampionStories';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK, this ID is tied to the champions table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ChampionStories', @level2type = N'COLUMN', @level2name = N'ChampionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK, this ID is tied to the language table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ChampionStories', @level2type = N'COLUMN', @level2name = N'LanguageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stories of each champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ChampionStories', @level2type = N'COLUMN', @level2name = N'Story';

