﻿CREATE TABLE [dbo].[DisbursementCheckNumbers] (
    [DisbursementId] INT NOT NULL,
    [CheckNumberId]  INT NOT NULL,
    CONSTRAINT [PK_DisbursementCheckNumbers] PRIMARY KEY CLUSTERED ([DisbursementId] ASC, [CheckNumberId] ASC),
    CONSTRAINT [FK_DisbursementCheckNumbers_CheckNumbers] FOREIGN KEY ([CheckNumberId]) REFERENCES [dbo].[CheckNumbers] ([CheckNumberID]),
    CONSTRAINT [FK_DisbursementCheckNumbers_Disbursements] FOREIGN KEY ([DisbursementId]) REFERENCES [dbo].[Disbursements] ([DisbursementId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table tying Disbursement and Check number tables together', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementCheckNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is part of the PK, it is tied to the Disbursements table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementCheckNumbers', @level2type = N'COLUMN', @level2name = N'DisbursementId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is part of the PK, it is tied to the CheckNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementCheckNumbers', @level2type = N'COLUMN', @level2name = N'CheckNumberId';

