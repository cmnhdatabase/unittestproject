﻿CREATE TABLE [dbo].[DanceMarathonHospitals] (
    [DanceMarathonId] INT NOT NULL,
    [HospitalId]      INT NOT NULL,
    CONSTRAINT [PK_DanceMarathonHospitals] PRIMARY KEY CLUSTERED ([DanceMarathonId] ASC, [HospitalId] ASC),
    FOREIGN KEY ([DanceMarathonId]) REFERENCES [dbo].[DanceMarathons] ([DanceMarathonId]),
    CONSTRAINT [FK__DanceMara__Hospi__13A973D6] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This field is tied to DanceMarathoHospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathonHospitals', @level2type = N'COLUMN', @level2name = N'DanceMarathonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This si tied to the Hospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathonHospitals', @level2type = N'COLUMN', @level2name = N'HospitalId';

