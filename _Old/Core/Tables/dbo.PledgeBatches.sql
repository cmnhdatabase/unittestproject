﻿CREATE TABLE [dbo].[PledgeBatches] (
    [PledgeBatchId]     INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PledgeBatchNumber] VARCHAR (25) NOT NULL,
    [PledgeBatchDate]   DATE         NOT NULL,
    CONSTRAINT [PK_PledgeBatches] PRIMARY KEY CLUSTERED ([PledgeBatchId] ASC)
);

