﻿CREATE TABLE [dbo].[FundraisingEntities] (
    [FundraisingEntityId]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                  VARCHAR (100) NOT NULL,
    [FriendlyName]          VARCHAR (100) NOT NULL,
    [DisplayName]           VARCHAR (100) NOT NULL,
    [YearStarted]           INT           NULL,
    [FundraisingCategoryId] INT           NOT NULL,
    [Website]               VARCHAR (255) NULL,
    [Active]                BIT           NOT NULL,
    [SponsorId]             VARCHAR (50)  NOT NULL,
    [CountryId]             INT           NOT NULL,
    [SalesForceId]          VARCHAR (18)  NULL,
    CONSTRAINT [PK_PartnerPrograms] PRIMARY KEY CLUSTERED ([FundraisingEntityId] ASC),
    CONSTRAINT [FK_FundraisingEntities_FundraisingCategories] FOREIGN KEY ([FundraisingCategoryId]) REFERENCES [dbo].[FundraisingCategories] ([FundraisingCategoryId]),
    CONSTRAINT [FK_PartnerPrograms_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of all partners', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Partner Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DB friendly name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'FriendlyName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Display name for Partner', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'DisplayName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year partnership started', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'YearStarted';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PartnerProgramTypes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'FundraisingCategoryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patners website', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'Website';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'If the partner is active or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID used to identify partner', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'SponsorId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntities', @level2type = N'COLUMN', @level2name = N'CountryId';

