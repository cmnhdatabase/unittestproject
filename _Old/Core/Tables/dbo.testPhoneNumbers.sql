﻿CREATE TABLE [dbo].[testPhoneNumbers] (
    [PhoneId]      INT          IDENTITY (1, 1) NOT NULL,
    [Phone]        VARCHAR (15) NOT NULL,
    [PhoneTypeId]  INT          NOT NULL,
    [CountryId]    INT          NOT NULL,
    [Extension]    INT          NULL,
    [SalesForceId] VARCHAR (18) NULL
);

