﻿CREATE TABLE [dbo].[FundraisingEntityInfoTypes] (
    [FundraisingEntityInfoTypeId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityInfoType]   VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_FundraisingEntityInfoTypes] PRIMARY KEY CLUSTERED ([FundraisingEntityInfoTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityInfoTypes', @level2type = N'COLUMN', @level2name = N'FundraisingEntityInfoTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description of Fundraising Entity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityInfoTypes', @level2type = N'COLUMN', @level2name = N'FundraisingEntityInfoType';

