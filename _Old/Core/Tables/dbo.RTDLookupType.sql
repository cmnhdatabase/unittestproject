﻿CREATE TABLE [dbo].[RTDLookupType] (
    [RTDLookupTypeId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RTDLookupType]   NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_RTDLookupType] PRIMARY KEY CLUSTERED ([RTDLookupTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RTDLookupType', @level2type = N'COLUMN', @level2name = N'RTDLookupTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type of lookup', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RTDLookupType', @level2type = N'COLUMN', @level2name = N'RTDLookupType';

