﻿CREATE TABLE [dbo].[Batches] (
    [BatchId]        INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [BatchTypeId]    INT          NOT NULL,
    [BatchNumber]    VARCHAR (25) NOT NULL,
    [Reconciled]     BIT          NOT NULL,
    [DateReconciled] DATETIME     NULL,
    [Foundation]     BIT          NOT NULL,
    CONSTRAINT [PK_Batches] PRIMARY KEY CLUSTERED ([BatchId] ASC),
    CONSTRAINT [FK_Batches_BatchTypes] FOREIGN KEY ([BatchTypeId]) REFERENCES [dbo].[BatchTypes] ([BatchTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Table for all batches that have come in to CMNH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches', @level2type = N'COLUMN', @level2name = N'BatchId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the BatchTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches', @level2type = N'COLUMN', @level2name = N'BatchTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number for the Batch', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches', @level2type = N'COLUMN', @level2name = N'BatchNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Has batch been Reconciled', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches', @level2type = N'COLUMN', @level2name = N'Reconciled';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date batch was reconciled', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches', @level2type = N'COLUMN', @level2name = N'DateReconciled';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to indicate if it was a company or campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Batches', @level2type = N'COLUMN', @level2name = N'Foundation';

