﻿CREATE TABLE [dbo].[Disbursements] (
    [DisbursementId]        INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RecordTypeId]          INT           NOT NULL,
    [FundraisingEntityId]   INT           NOT NULL,
    [MarketId]              INT           NOT NULL,
    [SubMarketId]           INT           NOT NULL,
    [DirectToHospital]      BIT           NOT NULL,
    [FundraisingYear]       INT           NOT NULL,
    [Amount]                MONEY         NOT NULL,
    [CurrencyTypeId]        INT           NOT NULL,
    [CampaignDetailsId]     INT           NULL,
    [LocationId]            INT           NULL,
    [DateReceived]          DATE          NULL,
    [DateRecorded]          DATE          NULL,
    [DonationDate]          DATE          NULL,
    [FundTypeId]            INT           NULL,
    [BatchId]               INT           NULL,
    [CampaignPeriod]        INT           NULL,
    [UploadId]              VARCHAR (20)  NULL,
    [Comment]               VARCHAR (300) NULL,
    [DisbursementPeriodId]  INT           NOT NULL,
    [DisbursementDateId]    INT           NOT NULL,
    [FundraisingCategoryId] INT           NOT NULL,
    [CreatedBy]             INT           CONSTRAINT [DF_Disbursements_CreatedBy] DEFAULT ((18633)) NOT NULL,
    CONSTRAINT [PK_Disbursements] PRIMARY KEY CLUSTERED ([DisbursementId] ASC),
    CONSTRAINT [FK_Disbursements_Batches] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batches] ([BatchId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Disbursements_CampaignDetails] FOREIGN KEY ([CampaignDetailsId]) REFERENCES [dbo].[CampaignDetails] ([CampaignDetailsId]),
    CONSTRAINT [FK_Disbursements_Currencies] FOREIGN KEY ([CurrencyTypeId]) REFERENCES [dbo].[Currencies] ([CurrencyTypeId]),
    CONSTRAINT [FK_Disbursements_DisbursementDates] FOREIGN KEY ([DisbursementDateId]) REFERENCES [dbo].[DisbursementDates] ([DisbursementDateId]),
    CONSTRAINT [FK_Disbursements_DisbursementPeriods] FOREIGN KEY ([DisbursementPeriodId]) REFERENCES [dbo].[DisbursementPeriods] ([DisbursementPeriodId]),
    CONSTRAINT [FK_Disbursements_FundraisingCategories] FOREIGN KEY ([FundraisingCategoryId]) REFERENCES [dbo].[FundraisingCategories] ([FundraisingCategoryId]),
    CONSTRAINT [FK_Disbursements_FundTypes] FOREIGN KEY ([FundTypeId]) REFERENCES [dbo].[FundTypes] ([FundTypeId]),
    CONSTRAINT [FK_Disbursements_Locations] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Locations] ([LocationId]),
    CONSTRAINT [FK_Disbursements_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_Disbursements_Markets1] FOREIGN KEY ([SubMarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_Disbursements_PartnerPrograms] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId]),
    CONSTRAINT [FK_Disbursements_RecordTypes] FOREIGN KEY ([RecordTypeId]) REFERENCES [dbo].[RecordTypes] ([RecordTypeId])
);


GO
CREATE NONCLUSTERED INDEX [ixSMSAmount_CampaignDetailsId_DisbursementDateId]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [MarketId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([Amount], [CampaignDetailsId], [DisbursementDateId]);


GO
CREATE NONCLUSTERED INDEX [ixSMSRecordTypeId_PartnerId_FundraisingYear]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([Amount]);


GO
CREATE NONCLUSTERED INDEX [ixSMSRecordTypeId_PartnerId_FundraisingYear2]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([MarketId], [Amount]);


GO
CREATE NONCLUSTERED INDEX [ixSMSRecordTypeId_PartnerId_FundraisingYear3]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingYear] ASC)
    INCLUDE([FundraisingEntityId], [Amount]);


GO
CREATE NONCLUSTERED INDEX [idxDisbursement_MarketId_Inc_FundraisingYear_Amount_CPDetailsId_DisbDate]
    ON [dbo].[Disbursements]([MarketId] ASC)
    INCLUDE([FundraisingYear], [Amount], [CampaignDetailsId], [DisbursementDateId]);


GO
CREATE NONCLUSTERED INDEX [ixRecordTypeId_PartnerId_FundraisingYear_MarketId_Amount_CampaignDetailsId_DisbursementDateId]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([MarketId], [Amount], [CampaignDetailsId], [DisbursementDateId]);


GO
CREATE NONCLUSTERED INDEX [ixRecordTypeId_FundraisingYear_PartnerId_MarketId_Amount_CampaignDetailsId_DisbursementDateId]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingYear] ASC)
    INCLUDE([FundraisingEntityId], [MarketId], [Amount], [CampaignDetailsId], [DisbursementDateId]);


GO
CREATE NONCLUSTERED INDEX [IX_Disbursments_SinglePartnerSummaryReport_PartnerID_FundraisingYear_MarketID_LocationID]
    ON [dbo].[Disbursements]([FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([MarketId], [LocationId]);


GO
CREATE NONCLUSTERED INDEX [IX_Disbursements_MarketID_Amount_CampaignDetailsID_BatchID_DisbursementDateId]
    ON [dbo].[Disbursements]([MarketId] ASC)
    INCLUDE([Amount], [CampaignDetailsId], [BatchId], [DisbursementDateId]);


GO
CREATE NONCLUSTERED INDEX [IX_DisbCheckDetails_DisbDateId]
    ON [dbo].[Disbursements]([DisbursementDateId] ASC)
    INCLUDE([DisbursementId], [RecordTypeId], [FundraisingEntityId], [MarketId], [FundraisingYear], [Amount], [CampaignDetailsId], [LocationId], [DonationDate], [BatchId], [Comment], [DisbursementPeriodId]);


GO
CREATE NONCLUSTERED INDEX [ix_Disbursements_PartnerID_w_SubMarketId]
    ON [dbo].[Disbursements]([FundraisingEntityId] ASC)
    INCLUDE([SubMarketId]);


GO
CREATE NONCLUSTERED INDEX [ixVwFundraisingData]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([MarketId], [DirectToHospital], [Amount], [CurrencyTypeId], [CampaignDetailsId], [FundTypeId]);


GO
CREATE NONCLUSTERED INDEX [ixVwFundraisingData2]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingYear] ASC, [FundraisingEntityId] ASC)
    INCLUDE([MarketId], [DirectToHospital], [Amount], [CurrencyTypeId], [CampaignDetailsId], [FundTypeId], [DisbursementDateId]);


GO
CREATE NONCLUSTERED INDEX [IX_Disbursments_Walmart]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([Amount], [CampaignDetailsId], [LocationId], [DonationDate]);


GO
CREATE NONCLUSTERED INDEX [ixPartnerReport]
    ON [dbo].[Disbursements]([RecordTypeId] ASC, [MarketId] ASC, [FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([Amount], [CampaignDetailsId], [LocationId], [DonationDate], [BatchId]);


GO
CREATE NONCLUSTERED INDEX [ixDisbursementCampaignDetailsId]
    ON [dbo].[Disbursements]([CampaignDetailsId] ASC)
    INCLUDE([MarketId], [Amount], [Comment]);


GO
CREATE NONCLUSTERED INDEX [ix_Disbursements_FundraisingYear_FundraisingEntityId_Amount]
    ON [dbo].[Disbursements]([FundraisingYear] ASC)
    INCLUDE([FundraisingEntityId], [Amount]);


GO
CREATE NONCLUSTERED INDEX [ix_Disbursements_FundraisingEntityId_FundraisingYear_Amount]
    ON [dbo].[Disbursements]([FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([Amount]);


GO
CREATE NONCLUSTERED INDEX [ixDisbursements_FundraisingEntityId_FundraisingYear]
    ON [dbo].[Disbursements]([FundraisingEntityId] ASC, [FundraisingYear] ASC)
    INCLUDE([RecordTypeId], [Amount], [LocationId]);


GO
CREATE NONCLUSTERED INDEX [ixDisbursements_LocationId]
    ON [dbo].[Disbursements]([LocationId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DisbursementBatchID_DisbursementID_DisbursementDateID]
    ON [dbo].[Disbursements]([BatchId] ASC)
    INCLUDE([DisbursementId], [DisbursementDateId]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Disbursement information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DisbursementId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the RecordTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'RecordTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'SubMarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Was donation given directly to the hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DirectToHospital';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fundraising year', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'FundraisingYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Amount of donation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to Currencies table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'CurrencyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the CampaignDetails table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'CampaignDetailsId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Locations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'LocationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date donation was received', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DateReceived';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date donation was recorded', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DateRecorded';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date of Donation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DonationDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the FundTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'FundTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to Batches table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'BatchId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'CampaignPeriod';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID if file was uploaded by accounting', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'UploadId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Comments on disbursments', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'Comment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to DisbursementPeriods table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DisbursementPeriodId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to DisbursementDates table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Disbursements', @level2type = N'COLUMN', @level2name = N'DisbursementDateId';

