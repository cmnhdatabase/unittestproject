﻿CREATE TABLE [dbo].[Invoices] (
    [InvoiceId]      INT  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [InvoiceYear]    INT  NOT NULL,
    [InvoiceQuarter] INT  NOT NULL,
    [InvoiceDate]    DATE NOT NULL,
    CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED ([InvoiceId] ASC)
);

