﻿CREATE TABLE [dbo].[RTDLookup] (
    [RTDLookupId]     INT           NOT NULL,
    [RTDLookup]       VARCHAR (250) NOT NULL,
    [RTDLookupTypeId] INT           NOT NULL,
    CONSTRAINT [PK_RTDLookup] PRIMARY KEY CLUSTERED ([RTDLookupId] ASC, [RTDLookup] ASC),
    CONSTRAINT [FK_RTDLookup_RTDLookupType] FOREIGN KEY ([RTDLookupTypeId]) REFERENCES [dbo].[RTDLookupType] ([RTDLookupTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RTDLookup', @level2type = N'COLUMN', @level2name = N'RTDLookupId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description of lookup', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RTDLookup', @level2type = N'COLUMN', @level2name = N'RTDLookup';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to RTDLookupType table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RTDLookup', @level2type = N'COLUMN', @level2name = N'RTDLookupTypeId';

