﻿CREATE TABLE [dbo].[FundraisingEntityFundraisingCategoryHistory] (
    [FundraisingEntityFundraisingCategoryStatusId] INT  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingCategoryId]                        INT  NOT NULL,
    [FundraisingEntityId]                          INT  NOT NULL,
    [EndDate]                                      DATE NOT NULL,
    CONSTRAINT [PK_FundraisingEntityFundraisingCategoryHistory] PRIMARY KEY CLUSTERED ([FundraisingEntityFundraisingCategoryStatusId] ASC),
    CONSTRAINT [FK_FundraisingEntityFundraisingCategoryHistory_FundraisingCategories] FOREIGN KEY ([FundraisingCategoryId]) REFERENCES [dbo].[FundraisingCategories] ([FundraisingCategoryId]),
    CONSTRAINT [FK_FundraisingEntityFundraisingCategoryHistory_FundraisingEntities] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityFundraisingCategoryHistory', @level2type = N'COLUMN', @level2name = N'FundraisingEntityFundraisingCategoryStatusId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to FundraisingCategories table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityFundraisingCategoryHistory', @level2type = N'COLUMN', @level2name = N'FundraisingCategoryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to FundraisingEntities table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityFundraisingCategoryHistory', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'End of the Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityFundraisingCategoryHistory', @level2type = N'COLUMN', @level2name = N'EndDate';

