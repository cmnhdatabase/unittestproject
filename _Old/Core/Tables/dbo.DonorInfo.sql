﻿CREATE TABLE [dbo].[DonorInfo] (
    [DonorInfoId]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DisbursementId]    INT           NULL,
    [Amount]            MONEY         NOT NULL,
    [DonationDate]      DATE          NOT NULL,
    [FirstName]         VARCHAR (100) NULL,
    [MiddleName]        VARCHAR (100) NULL,
    [LastName]          VARCHAR (100) NULL,
    [Address1]          VARCHAR (100) NULL,
    [Address2]          VARCHAR (100) NULL,
    [City]              VARCHAR (100) NULL,
    [ProvinceId]        INT           NULL,
    [PostalCode]        VARCHAR (15)  NULL,
    [CountryId]         INT           NOT NULL,
    [Email]             VARCHAR (100) NULL,
    [Receiptable]       BIT           NOT NULL,
    [CompanyName]       VARCHAR (150) NULL,
    [CurrencyTypeId]    INT           NULL,
    [FundTypeId]        INT           CONSTRAINT [DF__DonorInfo__FundT__0A888742] DEFAULT ('0') NOT NULL,
    [AssociateId]       INT           NULL,
    [TransactionID]     NVARCHAR (20) NULL,
    [LegacyAssociateId] VARCHAR (20)  NULL,
    CONSTRAINT [PK_DonorInfo] PRIMARY KEY CLUSTERED ([DonorInfoId] ASC),
    CONSTRAINT [FK_DonorInfo_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_DonorInfo_Currencies] FOREIGN KEY ([CurrencyTypeId]) REFERENCES [dbo].[Currencies] ([CurrencyTypeId]),
    CONSTRAINT [FK_DonorInfo_Disbursements1] FOREIGN KEY ([DisbursementId]) REFERENCES [dbo].[Disbursements] ([DisbursementId]) ON DELETE CASCADE ON UPDATE CASCADE NOT FOR REPLICATION,
    CONSTRAINT [FK_DonorInfo_FundTypes] FOREIGN KEY ([FundTypeId]) REFERENCES [dbo].[FundTypes] ([FundTypeId]),
    CONSTRAINT [FK_DonorInfo_Provinces] FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId])
);


GO
CREATE NONCLUSTERED INDEX [IX_DisbCheckDetails_DonorInfo]
    ON [dbo].[DonorInfo]([DisbursementId] ASC)
    INCLUDE([Amount], [FirstName], [LastName], [Address1], [Address2], [City], [ProvinceId], [PostalCode], [Email], [Receiptable], [FundTypeId]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is all the donor information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'DonorInfoId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Disbursements table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'DisbursementId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Amount of the donation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date of the donation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'DonationDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'First name of the donor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Middle name of the donor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'MiddleName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last name of the donor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line one of the donor address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line two of the donor address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City of the donor address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Provinces table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal code or zip code of donor address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Counties table ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Donors email address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'Email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Was the donation receiptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'Receiptable';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Donor company name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'CompanyName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the CurrencyTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'CurrencyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to FundTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'FundTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the AssociateIDName field in [MiracleSystem].[dbo].[Associate]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'AssociateId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Transaction ID for this transaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfo', @level2type = N'COLUMN', @level2name = N'TransactionID';

