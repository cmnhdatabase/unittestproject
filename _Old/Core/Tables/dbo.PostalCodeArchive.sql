﻿CREATE TABLE [dbo].[PostalCodeArchive] (
    [PostalCode]  VARCHAR (10) NOT NULL,
    [MarketId]    INT          NOT NULL,
    [SubMarketId] INT          NOT NULL,
    [EndDate]     DATE         NOT NULL,
    CONSTRAINT [PK_PostalCodeArchive] PRIMARY KEY CLUSTERED ([PostalCode] ASC),
    FOREIGN KEY ([SubMarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_PostalCodeArchive_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is a table that holds all the archived postal codes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeArchive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeArchive', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeArchive', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeArchive', @level2type = N'COLUMN', @level2name = N'SubMarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the end date of when we stip using this postal code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeArchive', @level2type = N'COLUMN', @level2name = N'EndDate';

