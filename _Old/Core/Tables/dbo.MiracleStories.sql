﻿CREATE TABLE [dbo].[MiracleStories] (
    [MiracleChildId] INT           NOT NULL,
    [LanguageId]     INT           NOT NULL,
    [Story]          VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_MiracleStories] PRIMARY KEY CLUSTERED ([MiracleChildId] ASC, [LanguageId] ASC),
    CONSTRAINT [FK_MiracleStories_Languages] FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Languages] ([LanguageId]),
    CONSTRAINT [FK_MiracleStories_MiracleChildren] FOREIGN KEY ([MiracleChildId]) REFERENCES [dbo].[MiracleChildren] ([MiracleChildId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stories on miracle children', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleStories';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to MiracleChildren table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleStories', @level2type = N'COLUMN', @level2name = N'MiracleChildId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to Languages table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleStories', @level2type = N'COLUMN', @level2name = N'LanguageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short story about Miracle Child', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleStories', @level2type = N'COLUMN', @level2name = N'Story';

