﻿CREATE TABLE [dbo].[testLocations] (
    [LocationId]          INT           IDENTITY (1, 1) NOT NULL,
    [FundraisingEntityId] INT           NULL,
    [LocationName]        VARCHAR (100) NOT NULL,
    [LocationNumber]      VARCHAR (100) NOT NULL,
    [Address1]            VARCHAR (100) NULL,
    [Address2]            VARCHAR (100) NULL,
    [City]                VARCHAR (100) NULL,
    [ProvinceId]          INT           NULL,
    [PostalCode]          VARCHAR (10)  NULL,
    [CountryId]           INT           NULL,
    [PropertyTypeId]      INT           NULL,
    [Latitude]            FLOAT (53)    NULL,
    [Longitude]           FLOAT (53)    NULL,
    [Active]              BIT           NOT NULL
);

