﻿CREATE TABLE [dbo].[RedirectedLocations] (
    [RedirectedLocationId]   INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LocationId]             INT            NOT NULL,
    [RedirectedToPostalCode] VARCHAR (10)   NOT NULL,
    [RedirectPercent]        DECIMAL (4, 3) NOT NULL,
    CONSTRAINT [PK_RedirectedLocations] PRIMARY KEY CLUSTERED ([RedirectedLocationId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for the table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedLocations', @level2type = N'COLUMN', @level2name = N'RedirectedLocationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Locations table. Location funds are coming from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedLocations', @level2type = N'COLUMN', @level2name = N'LocationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal code funds are redirected to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedLocations', @level2type = N'COLUMN', @level2name = N'RedirectedToPostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Percentage of funds going to that market', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedLocations', @level2type = N'COLUMN', @level2name = N'RedirectPercent';

