﻿CREATE TABLE [dbo].[LegacyCampaignTypes] (
    [CampaignTypeId]     INT          NOT NULL,
    [LegacyCampaignType] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_LegacyCampaignTypes] PRIMARY KEY CLUSTERED ([CampaignTypeId] ASC),
    CONSTRAINT [FK_LegacyCampaignTypes_CampaignTypes] FOREIGN KEY ([CampaignTypeId]) REFERENCES [dbo].[CampaignTypes] ([CampaignTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Campaigns table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LegacyCampaignTypes', @level2type = N'COLUMN', @level2name = N'CampaignTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type of Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LegacyCampaignTypes', @level2type = N'COLUMN', @level2name = N'LegacyCampaignType';

