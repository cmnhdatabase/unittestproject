﻿CREATE TABLE [dbo].[Schools] (
    [SchoolId]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [HospitalId]     INT           NULL,
    [SchoolName]     VARCHAR (255) NULL,
    [Address1]       VARCHAR (100) NULL,
    [Address2]       VARCHAR (100) NULL,
    [City]           VARCHAR (100) NULL,
    [ProvinceId]     INT           NULL,
    [PostalCode]     VARCHAR (10)  NULL,
    [CountryId]      INT           NULL,
    [InauguralEvent] INT           NOT NULL,
    [SchoolTypeId]   INT           NOT NULL,
    [Active]         BIT           NOT NULL,
    CONSTRAINT [PK_Schools] PRIMARY KEY CLUSTERED ([SchoolId] ASC),
    FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    FOREIGN KEY ([PostalCode]) REFERENCES [dbo].[PostalCodes] ([PostalCode]),
    FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId]),
    CONSTRAINT [FK_Schools_Hospitals] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId]),
    CONSTRAINT [FK_Schools_SchoolTypeId] FOREIGN KEY ([SchoolTypeId]) REFERENCES [dbo].[SchoolTypes] ([SchoolTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'List of High schools and Colleges in the USA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'SchoolId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the hospital table ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'HospitalId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the school', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'SchoolName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Address line one for the school', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Address line two for the school', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City of the school', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Provinces table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal code for the school', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date of first school event', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'InauguralEvent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the schoolTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'SchoolTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is school active or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Schools', @level2type = N'COLUMN', @level2name = N'Active';

