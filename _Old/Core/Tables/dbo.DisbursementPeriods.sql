﻿CREATE TABLE [dbo].[DisbursementPeriods] (
    [DisbursementPeriodId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DisbursementYear]     INT NOT NULL,
    [DisbursementQuarter]  INT NOT NULL,
    CONSTRAINT [PK_DisbursementPeriods] PRIMARY KEY CLUSTERED ([DisbursementPeriodId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Quarters that disbursements were made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementPeriods';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementPeriods', @level2type = N'COLUMN', @level2name = N'DisbursementPeriodId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year of disbursement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementPeriods', @level2type = N'COLUMN', @level2name = N'DisbursementYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Quarter of disbursement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementPeriods', @level2type = N'COLUMN', @level2name = N'DisbursementQuarter';

