﻿CREATE TABLE [dbo].[Celebrities] (
    [CelebrityId]    INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FirstName]      VARCHAR (50)     NOT NULL,
    [LastName]       VARCHAR (50)     NULL,
    [Content]        VARCHAR (MAX)    NULL,
    [Bio]            VARCHAR (MAX)    NULL,
    [Website]        VARCHAR (100)    NULL,
    [Rank]           INT              NULL,
    [OldCelebrityID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Celebrities] PRIMARY KEY CLUSTERED ([CelebrityId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for all Celebrities', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for the Celebrities table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'CelebrityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The first name of each celebrity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The last name of each celebrity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Content', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'Content';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Bio of each celebrity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'Bio';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The website of each celebrity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'Website';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rank for each celebrity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'Rank';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The old celebrity ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Celebrities', @level2type = N'COLUMN', @level2name = N'OldCelebrityID';

