﻿CREATE TABLE [dbo].[PartnerAccountStaffTypes] (
    [PartnerAccountStaffTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Description]               VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PartnerAccountStaffTypes] PRIMARY KEY CLUSTERED ([PartnerAccountStaffTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table is a list of partner staff types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerAccountStaffTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerAccountStaffTypes', @level2type = N'COLUMN', @level2name = N'PartnerAccountStaffTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description of the partner staff type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerAccountStaffTypes', @level2type = N'COLUMN', @level2name = N'Description';

