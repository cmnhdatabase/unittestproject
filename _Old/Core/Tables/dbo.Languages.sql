﻿CREATE TABLE [dbo].[Languages] (
    [LanguageId]   INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Language]     VARCHAR (25) NOT NULL,
    [Abbreviation] CHAR (2)     NOT NULL,
    CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED ([LanguageId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lists all the languages we use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Languages';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Languages', @level2type = N'COLUMN', @level2name = N'LanguageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the full name of the language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Languages', @level2type = N'COLUMN', @level2name = N'Language';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the abbreviation for the language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Languages', @level2type = N'COLUMN', @level2name = N'Abbreviation';

