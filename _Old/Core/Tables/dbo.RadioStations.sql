﻿CREATE TABLE [dbo].[RadioStations] (
    [RadioStationId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CallLetters]    VARCHAR (10)  NOT NULL,
    [MarketId]       INT           NOT NULL,
    [Owner]          VARCHAR (100) NULL,
    [Frequency]      VARCHAR (10)  NOT NULL,
    [MarketTag]      VARCHAR (50)  NULL,
    [MarketArea]     VARCHAR (50)  NULL,
    [YearStarted]    SMALLINT      NULL,
    [Note]           VARCHAR (MAX) NULL,
    [ModifiedDate]   DATE          NOT NULL,
    [Active]         BIT           NOT NULL,
    [Address1]       VARCHAR (100) NULL,
    [Address2]       VARCHAR (100) NULL,
    [City]           VARCHAR (100) NULL,
    [ProvinceId]     INT           NULL,
    [PostalCode]     VARCHAR (10)  NULL,
    [CountryId]      INT           NULL,
    [ModifiedBy]     INT           NULL,
    [Director]       INT           NULL,
    CONSTRAINT [PK_RadioStations] PRIMARY KEY CLUSTERED ([RadioStationId] ASC),
    FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId]),
    CONSTRAINT [FK_MarketId_RadioStations] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [uxUniqueCalletters] UNIQUE NONCLUSTERED ([CallLetters] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for all radio stations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'RadioStationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Call letters of the radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'CallLetters';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Owner of the radio Station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'Owner';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Radio Station Frequwncy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'Frequency';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The public name of the radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'MarketTag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Market area of the radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'MarketArea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year the Radio station was started', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'YearStarted';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Notes on radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'Note';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date record was last modified', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if radio station is active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioStations', @level2type = N'COLUMN', @level2name = N'Active';

