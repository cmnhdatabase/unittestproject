﻿CREATE TABLE [dbo].[DanceMarathonSubSchools] (
    [DanceMarathonId] INT NOT NULL,
    [SchoolId]        INT NOT NULL,
    CONSTRAINT [PK_DanceMarathonSubSchool] PRIMARY KEY CLUSTERED ([DanceMarathonId] ASC, [SchoolId] ASC),
    CONSTRAINT [FK_DanceMarathonSubSchools_DanceMarathonId] FOREIGN KEY ([DanceMarathonId]) REFERENCES [dbo].[DanceMarathons] ([DanceMarathonId]),
    CONSTRAINT [FK_DanceMarathonSubSchools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [dbo].[Schools] ([SchoolId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for sub schools and Dance Marathons ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathonSubSchools';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to DanceMarathons table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathonSubSchools', @level2type = N'COLUMN', @level2name = N'DanceMarathonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to Schools Table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DanceMarathonSubSchools', @level2type = N'COLUMN', @level2name = N'SchoolId';

