﻿CREATE TABLE [dbo].[CampaignDetails] (
    [CampaignDetailsId]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignId]         INT           NOT NULL,
    [CampaignDetailName] VARCHAR (300) NOT NULL,
    [ShortDescription]   VARCHAR (300) NULL,
    [LongDescription]    VARCHAR (500) NULL,
    [CampaignYear]       INT           NULL,
    [LegacyEventId]      VARCHAR (50)  NULL,
    [StartDate]          DATETIME      NULL,
    [EndDate]            DATETIME      NULL,
    CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED ([CampaignDetailsId] ASC),
    CONSTRAINT [FK_CampaignDetails_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaigns] ([CampaignId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [ixCampaignDetailsCampaignId]
    ON [dbo].[CampaignDetails]([CampaignId] ASC)
    INCLUDE([CampaignDetailsId], [CampaignDetailName], [ShortDescription], [LongDescription], [CampaignYear], [LegacyEventId], [StartDate], [EndDate]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Details for each Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'CampaignDetailsId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Campaigns table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'CampaignId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the name of the Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'CampaignDetailName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short Description of Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'ShortDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Long Description of Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'LongDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year of the Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'CampaignYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Legacy Event ID for Campaigns', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'LegacyEventId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Start of the Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'StartDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'End of the Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignDetails', @level2type = N'COLUMN', @level2name = N'EndDate';

