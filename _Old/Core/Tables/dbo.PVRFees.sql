﻿CREATE TABLE [dbo].[PVRFees] (
    [PVRFeesID]      INT            IDENTITY (1, 1) NOT NULL,
    [MarketId]       FLOAT (53)     NULL,
    [FixedFees]      FLOAT (53)     NULL,
    [VariableFees]   FLOAT (53)     NULL,
    [OtherFees]      FLOAT (53)     NULL,
    [MarketProvided] FLOAT (53)     NULL,
    [OverHead]       FLOAT (53)     NULL,
    [FTE]            FLOAT (53)     NULL,
    [COLIRate]       FLOAT (53)     NULL,
    [Year]           FLOAT (53)     NULL,
    [Peer1]          NVARCHAR (255) NULL,
    [Peer2]          NVARCHAR (255) NULL,
    CONSTRAINT [PK_PVRFees] PRIMARY KEY CLUSTERED ([PVRFeesID] ASC)
);

