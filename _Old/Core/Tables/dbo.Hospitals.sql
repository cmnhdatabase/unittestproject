﻿CREATE TABLE [dbo].[Hospitals] (
    [HospitalName]         VARCHAR (255) NOT NULL,
    [FriendlyHospitalName] VARCHAR (255) NOT NULL,
    [MarketId]             INT           NOT NULL,
    [Address1]             VARCHAR (100) NULL,
    [Address2]             VARCHAR (100) NULL,
    [City]                 VARCHAR (100) NULL,
    [ProvinceId]           INT           NULL,
    [PostalCode]           VARCHAR (10)  NULL,
    [CountryId]            INT           NULL,
    [LongDescription]      VARCHAR (MAX) NULL,
    [ShortDescription]     VARCHAR (MAX) NULL,
    [Website]              VARCHAR (255) NULL,
    [HospitalId]           INT           IDENTITY (1, 1) NOT NULL,
    [Latitude]             FLOAT (53)    NULL,
    [Longitude]            FLOAT (53)    NULL,
    [Active]               BIT           NOT NULL,
    [SalesForceId]         VARCHAR (18)  NULL,
    [SubDomain]            VARCHAR (255) NULL,
    CONSTRAINT [PK_Hospitals] PRIMARY KEY CLUSTERED ([HospitalId] ASC),
    CONSTRAINT [FK_Hospitals_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_Hospitals_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_Hospitals_PostalCodes] FOREIGN KEY ([PostalCode]) REFERENCES [dbo].[PostalCodes] ([PostalCode]),
    CONSTRAINT [FK_Hospitals_Provinces] FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId])
);


GO
ALTER TABLE [dbo].[Hospitals] NOCHECK CONSTRAINT [FK_Hospitals_PostalCodes];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information on all the Hospitals that we have in the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'HospitalName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'If they have another name that they go by in public', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'FriendlyHospitalName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'First line of the hospitals address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Second line of the hospitals address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City the hospital is located in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Provinces table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Post Code for the hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HTML formating tags in description for hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'LongDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Text only description for hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'ShortDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Website address for the hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'Website';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the core DB and is the new hospital id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'HospitalId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude coordinates for Hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'Latitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude coordinates for Hospital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'Longitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if the hospital in active or not (0: Inactive, 1: Active) ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hospitals', @level2type = N'COLUMN', @level2name = N'Active';

