﻿CREATE TABLE [dbo].[FundraisingEntityTypeStatus] (
    [FundraisingEntityTypeStatusId] INT  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityId]           INT  NOT NULL,
    [FundraisingEntityTypeId]       INT  NOT NULL,
    [EndDate]                       DATE NULL,
    CONSTRAINT [PK_FundraisingEntityTypeStatus] PRIMARY KEY CLUSTERED ([FundraisingEntityTypeStatusId] ASC),
    CONSTRAINT [FK_FundraisingEntityTypeStatus_FundraisingEntities] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId]),
    CONSTRAINT [FK_FundraisingEntityTypeStatus_FundraisingEntityTypes] FOREIGN KEY ([FundraisingEntityTypeId]) REFERENCES [dbo].[FundraisingEntityTypes] ([FundraisingEntityTypeId]),
    CONSTRAINT [FK_FundraisingEntityTypeStatus_FundraisingEntityTypeStatus] FOREIGN KEY ([FundraisingEntityTypeStatusId]) REFERENCES [dbo].[FundraisingEntityTypeStatus] ([FundraisingEntityTypeStatusId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityTypeStatus', @level2type = N'COLUMN', @level2name = N'FundraisingEntityTypeStatusId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the FundrasingEntities table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityTypeStatus', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to FundraisingEntityType table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityTypeStatus', @level2type = N'COLUMN', @level2name = N'FundraisingEntityTypeId';

