﻿CREATE TABLE [dbo].[ReportSettings] (
    [ReportChangeDate] DATE NOT NULL,
    CONSTRAINT [PK_ReportSettings] PRIMARY KEY CLUSTERED ([ReportChangeDate] ASC)
);

