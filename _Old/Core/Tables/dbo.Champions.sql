﻿CREATE TABLE [dbo].[Champions] (
    [ChampionId]      INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DisplayProvince] VARCHAR (50)  NULL,
    [FirstName]       VARCHAR (50)  NULL,
    [LastName]        VARCHAR (50)  NULL,
    [Age]             VARCHAR (10)  NULL,
    [Illness]         VARCHAR (100) NULL,
    [Year]            INT           NOT NULL,
    [HospitalId]      INT           NULL,
    [CountryId]       INT           NULL,
    [BlogUri]         VARCHAR (255) NULL,
    CONSTRAINT [PK_Champions] PRIMARY KEY CLUSTERED ([ChampionId] ASC),
    CONSTRAINT [FK_Champions_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_Champions_Hospitals] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId])
);


GO
ALTER TABLE [dbo].[Champions] NOCHECK CONSTRAINT [FK_Champions_Hospitals];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champion Stories', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'ChampionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Province or State the Champion is from.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'DisplayProvince';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'First name of each champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last name of each champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Age when the champion was a champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'Age';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Illness of each champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'Illness';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year champion was champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'Year';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to Hospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'HospitalId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL of the blog for each champion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Champions', @level2type = N'COLUMN', @level2name = N'BlogUri';

