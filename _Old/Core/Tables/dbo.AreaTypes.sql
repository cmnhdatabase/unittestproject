﻿CREATE TABLE [dbo].[AreaTypes] (
    [AreaTypeId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AreaType]   NVARCHAR (32) NOT NULL,
    CONSTRAINT [PK_AreaTypes] PRIMARY KEY CLUSTERED ([AreaTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Types of locations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AreaTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AreaTypes', @level2type = N'COLUMN', @level2name = N'AreaTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the type of locations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AreaTypes', @level2type = N'COLUMN', @level2name = N'AreaType';

