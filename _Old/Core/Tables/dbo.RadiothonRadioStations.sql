﻿CREATE TABLE [dbo].[RadiothonRadioStations] (
    [RadiothonId]         INT NOT NULL,
    [RadioStationId]      INT NOT NULL,
    [PrimaryRadioStation] BIT NOT NULL,
    CONSTRAINT [PK_RadiothonRadioStation] PRIMARY KEY CLUSTERED ([RadioStationId] ASC, [RadiothonId] ASC),
    CONSTRAINT [FK_RadiothonRadioStation_RadioStations1] FOREIGN KEY ([RadioStationId]) REFERENCES [dbo].[RadioStations] ([RadioStationId]),
    CONSTRAINT [FK_RadiothonRadioStation_Radiothons1] FOREIGN KEY ([RadiothonId]) REFERENCES [dbo].[Radiothons] ([RadiothonId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table that ties the Radiothon table with the RadioStations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonRadioStations';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to the Radiothons table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonRadioStations', @level2type = N'COLUMN', @level2name = N'RadiothonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to the RadioStations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonRadioStations', @level2type = N'COLUMN', @level2name = N'RadioStationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicates if this is a Primary radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonRadioStations', @level2type = N'COLUMN', @level2name = N'PrimaryRadioStation';

