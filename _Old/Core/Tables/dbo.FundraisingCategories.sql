﻿CREATE TABLE [dbo].[FundraisingCategories] (
    [FundraisingCategoryId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingCategory]   VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_FundraisingCategorys] PRIMARY KEY CLUSTERED ([FundraisingCategoryId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingCategories', @level2type = N'COLUMN', @level2name = N'FundraisingCategoryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type of fundraising Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingCategories', @level2type = N'COLUMN', @level2name = N'FundraisingCategory';

