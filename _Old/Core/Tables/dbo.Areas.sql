﻿CREATE TABLE [dbo].[Areas] (
    [AreaId]              INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityId] INT           NOT NULL,
    [AreaTypeId]          INT           NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [ContactId]           INT           NULL,
    CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED ([AreaId] ASC),
    CONSTRAINT [FK_Areas_AreaTypes] FOREIGN KEY ([AreaTypeId]) REFERENCES [dbo].[AreaTypes] ([AreaTypeId]),
    CONSTRAINT [FK_Areas_PartnerPrograms] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Types of areas for each partner', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Areas';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Areas', @level2type = N'COLUMN', @level2name = N'AreaId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Areas', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the AreaTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Areas', @level2type = N'COLUMN', @level2name = N'AreaTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name for the area', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Areas', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Contacts.dbo.Contacts table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Areas', @level2type = N'COLUMN', @level2name = N'ContactId';

