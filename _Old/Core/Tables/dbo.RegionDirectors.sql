﻿CREATE TABLE [dbo].[RegionDirectors] (
    [RegionId]  INT NOT NULL,
    [ContactId] INT NOT NULL,
    CONSTRAINT [PK_RegionDirectors] PRIMARY KEY CLUSTERED ([RegionId] ASC, [ContactId] ASC),
    CONSTRAINT [FK_RegionDirectors_Regions] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Regions] ([RegionId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Holds the Region and the Userid for the Region Directors', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionDirectors';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to the Regions table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionDirectors', @level2type = N'COLUMN', @level2name = N'RegionId';

