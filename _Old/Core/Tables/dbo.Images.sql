﻿CREATE TABLE [dbo].[Images] (
    [ImageId]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TypeId]        INT           NOT NULL,
    [Value]         VARCHAR (255) NOT NULL,
    [URI]           VARCHAR (255) NULL,
    [Path]          VARCHAR (255) NULL,
    [Width]         INT           NULL,
    [Height]        INT           NULL,
    [ImageFormatId] INT           NULL,
    [ModifiedDate]  DATE          NULL,
    CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED ([ImageId] ASC),
    CONSTRAINT [FK_Images_ImageFormats] FOREIGN KEY ([ImageFormatId]) REFERENCES [dbo].[ImageFormats] ([ImageFormatId]),
    CONSTRAINT [FK_Images_Images] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[ImageTypes] ([ImageTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'information for all the Images that we use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'ImageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the ImageTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'TypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the SponsorID on the partnerprograms table ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'Value';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The URI of the image', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'URI';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'File path to the image', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'Path';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Width of the image', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'Width';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Height of the image', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'Height';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the ImageFormats table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'ImageFormatId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last date the image was modified', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Images', @level2type = N'COLUMN', @level2name = N'ModifiedDate';

