﻿CREATE TABLE [dbo].[PledgeDataTelethonType] (
    [PledgeId]       INT NOT NULL,
    [TelethonTypeId] INT NOT NULL,
    CONSTRAINT [PK_PledgeDataTelethonType] PRIMARY KEY CLUSTERED ([PledgeId] ASC, [TelethonTypeId] ASC),
    CONSTRAINT [FK_PledgeDataTelethonType_PledgeId] FOREIGN KEY ([PledgeId]) REFERENCES [dbo].[PledgeData] ([PledgeId]),
    CONSTRAINT [FK_PledgeDataTelethonType_TelethonTypeId] FOREIGN KEY ([TelethonTypeId]) REFERENCES [dbo].[TelethonTypes] ([TelethonTypeId])
);

