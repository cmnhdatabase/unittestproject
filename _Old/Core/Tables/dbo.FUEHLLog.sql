﻿CREATE TABLE [dbo].[FUEHLLog] (
    [LogId]    INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LogEntry] VARCHAR (MAX) NOT NULL,
    [LogDate]  DATETIME      NOT NULL,
    [TestLog]  BIT           NOT NULL,
    CONSTRAINT [PK_FUEHLLog] PRIMARY KEY CLUSTERED ([LogId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Logs for what happends in the FUEHL system', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FUEHLLog';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FUEHLLog', @level2type = N'COLUMN', @level2name = N'LogId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Info on File and what happened with it', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FUEHLLog', @level2type = N'COLUMN', @level2name = N'LogEntry';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date that it was uploaded', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FUEHLLog', @level2type = N'COLUMN', @level2name = N'LogDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Set if this is a test file or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FUEHLLog', @level2type = N'COLUMN', @level2name = N'TestLog';

