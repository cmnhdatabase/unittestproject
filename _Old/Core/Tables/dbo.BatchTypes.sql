﻿CREATE TABLE [dbo].[BatchTypes] (
    [BatchTypeId]  INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [BatchType]    VARCHAR (25) NOT NULL,
    [Abbreviation] CHAR (2)     NOT NULL,
    CONSTRAINT [PK_BatchTypes] PRIMARY KEY CLUSTERED ([BatchTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Types of batches', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BatchTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BatchTypes', @level2type = N'COLUMN', @level2name = N'BatchTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Where batch is from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BatchTypes', @level2type = N'COLUMN', @level2name = N'BatchType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Abbreviation for BatchType', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BatchTypes', @level2type = N'COLUMN', @level2name = N'Abbreviation';

