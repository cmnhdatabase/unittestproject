﻿CREATE TABLE [dbo].[RedirectedPostalCodes] (
    [RedirectedPostalCodeId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OriginalPostalCode]     VARCHAR (10)   NOT NULL,
    [RedirectedToPostalCode] VARCHAR (10)   NOT NULL,
    [RedirectPercent]        DECIMAL (4, 3) NOT NULL,
    CONSTRAINT [PK_RedirectedPostalCodes] PRIMARY KEY CLUSTERED ([RedirectedPostalCodeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedPostalCodes', @level2type = N'COLUMN', @level2name = N'RedirectedPostalCodeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The original postal code funds are coming from', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedPostalCodes', @level2type = N'COLUMN', @level2name = N'OriginalPostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The postal funds are being sent to', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedPostalCodes', @level2type = N'COLUMN', @level2name = N'RedirectedToPostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The amount of funds being sent to new postal code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RedirectedPostalCodes', @level2type = N'COLUMN', @level2name = N'RedirectPercent';

