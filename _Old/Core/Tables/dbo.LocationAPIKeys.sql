﻿CREATE TABLE [dbo].[LocationAPIKeys] (
    [LocationAPIKeysID] INT           IDENTITY (1, 1) NOT NULL,
    [APIKey]            VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([LocationAPIKeysID] ASC)
);

