﻿CREATE TABLE [dbo].[DisbursementFundraisingEntities] (
    [DisbursementId]        INT NOT NULL,
    [FundraisingEntityId]   INT NOT NULL,
    [FundraisingCategoryId] INT NOT NULL,
    CONSTRAINT [PK_DisbursementFundraisingEntities] PRIMARY KEY CLUSTERED ([DisbursementId] ASC, [FundraisingEntityId] ASC),
    CONSTRAINT [FK_DisbursementFundraisingEntities_Disbursements] FOREIGN KEY ([DisbursementId]) REFERENCES [dbo].[Disbursements] ([DisbursementId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_DisbursementFundraisingEntities_FundraisingEntities] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);

