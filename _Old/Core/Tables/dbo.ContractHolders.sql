﻿CREATE TABLE [dbo].[ContractHolders] (
    [ContractHolderId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PrimaryMarketId]  INT NOT NULL,
    [SubMarketId]      INT NOT NULL,
    CONSTRAINT [PK_ ContractHolderId] PRIMARY KEY CLUSTERED ([ContractHolderId] ASC),
    CONSTRAINT [FK_ContractHolders_Markets] FOREIGN KEY ([PrimaryMarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_ContractHolders_SubMarkets] FOREIGN KEY ([SubMarketId]) REFERENCES [dbo].[Markets] ([MarketId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary and Sub Market information for each contract', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractHolders';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for the ContractHolders table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractHolders', @level2type = N'COLUMN', @level2name = N'ContractHolderId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This ID is for the primary market, it is tied to the markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractHolders', @level2type = N'COLUMN', @level2name = N'PrimaryMarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This ID is for the sub market, it is tied to the markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractHolders', @level2type = N'COLUMN', @level2name = N'SubMarketId';

