﻿CREATE TABLE [dbo].[Donations] (
    [TransactionId] VARCHAR (50)  NOT NULL,
    [FirstName]     VARCHAR (50)  NOT NULL,
    [LastName]      VARCHAR (50)  NOT NULL,
    [Email]         VARCHAR (255) NOT NULL,
    [Amount]        MONEY         NOT NULL,
    [MarketId]      INT           NOT NULL,
    [Recurring]     BIT           NOT NULL,
    [Term]          INT           NULL,
    [PipeDelimited] VARCHAR (255) NULL,
    [Token]         VARCHAR (100) NULL,
    [DonationDate]  DATE          NULL,
    [EmailContact]  BIT           NOT NULL,
    CONSTRAINT [PK_Donations] PRIMARY KEY CLUSTERED ([TransactionId] ASC),
    CONSTRAINT [FK_Donations_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personal donations information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'TransactionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'First name of donor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last name of donor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email address of donor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'Email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total of donation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'Amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is donation recurring', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'Recurring';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'How often is donation made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'Term';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date the dontaion was made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'DonationDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to indicate if we can email contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Donations', @level2type = N'COLUMN', @level2name = N'EmailContact';

