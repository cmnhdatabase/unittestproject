﻿CREATE TABLE [dbo].[PhoneTypes] (
    [PhoneTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PhoneType]   VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_PhoneTypes] PRIMARY KEY CLUSTERED ([PhoneTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table lists all the types of phone numbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneTypes', @level2type = N'COLUMN', @level2name = N'PhoneTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type of phone numbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneTypes', @level2type = N'COLUMN', @level2name = N'PhoneType';

