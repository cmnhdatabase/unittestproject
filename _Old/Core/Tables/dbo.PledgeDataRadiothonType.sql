﻿CREATE TABLE [dbo].[PledgeDataRadiothonType] (
    [PledgeId]        INT NOT NULL,
    [RadiothonTypeId] INT NOT NULL,
    CONSTRAINT [PK_PledgeDataRadiothonType] PRIMARY KEY CLUSTERED ([PledgeId] ASC, [RadiothonTypeId] ASC),
    FOREIGN KEY ([PledgeId]) REFERENCES [dbo].[PledgeData] ([PledgeId]),
    FOREIGN KEY ([RadiothonTypeId]) REFERENCES [dbo].[RadiothonTypes] ([RadiothonTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the PledgeData table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeDataRadiothonType', @level2type = N'COLUMN', @level2name = N'PledgeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the RadiothonTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeDataRadiothonType', @level2type = N'COLUMN', @level2name = N'RadiothonTypeId';

