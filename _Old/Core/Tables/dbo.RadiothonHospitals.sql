﻿CREATE TABLE [dbo].[RadiothonHospitals] (
    [RadiothonId] INT NOT NULL,
    [HospitalId]  INT NOT NULL,
    CONSTRAINT [PK_RadiothonHospitals] PRIMARY KEY CLUSTERED ([RadiothonId] ASC, [HospitalId] ASC),
    FOREIGN KEY ([RadiothonId]) REFERENCES [dbo].[Radiothons] ([RadiothonId]),
    CONSTRAINT [FK__Radiothon__Hospi__0FD8E2F2] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Radiothon table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonHospitals', @level2type = N'COLUMN', @level2name = N'RadiothonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Hospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadiothonHospitals', @level2type = N'COLUMN', @level2name = N'HospitalId';

