﻿CREATE TABLE [dbo].[DisbursementDates] (
    [DisbursementDateId] INT  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DisbursementDate]   DATE NOT NULL,
    CONSTRAINT [PK_DisbursementDates] PRIMARY KEY CLUSTERED ([DisbursementDateId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dates that disbursements where made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementDates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementDates', @level2type = N'COLUMN', @level2name = N'DisbursementDateId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date of disbursement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementDates', @level2type = N'COLUMN', @level2name = N'DisbursementDate';

