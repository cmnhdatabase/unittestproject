﻿CREATE TABLE [dbo].[PopulationByMarket] (
    [MarketId]           INT    NOT NULL,
    [CampaignYear]       INT    NOT NULL,
    [CountryId]          INT    NOT NULL,
    [PopulationEstimate] BIGINT NOT NULL,
    CONSTRAINT [PK_PopulationByMarket_1] PRIMARY KEY CLUSTERED ([MarketId] ASC, [CampaignYear] ASC),
    CONSTRAINT [FK_PopulationByMarket_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_PopulationByMarket_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table is used for estimates on population for each market', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PopulationByMarket';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PopulationByMarket', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the campaign year of the estimate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PopulationByMarket', @level2type = N'COLUMN', @level2name = N'CampaignYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PopulationByMarket', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PopulationByMarket', @level2type = N'COLUMN', @level2name = N'PopulationEstimate';

