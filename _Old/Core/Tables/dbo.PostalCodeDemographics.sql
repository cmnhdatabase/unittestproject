﻿CREATE TABLE [dbo].[PostalCodeDemographics] (
    [PostalCode]           VARCHAR (10)    NOT NULL,
    [Population]           INT             NULL,
    [HouseholdsPerZipcode] DECIMAL (18, 8) NULL,
    [AverageHouseValue]    DECIMAL (18, 8) NULL,
    [IncomePerHousehold]   DECIMAL (18, 8) NULL,
    [PersonsPerHousehold]  DECIMAL (18, 8) NULL,
    [AsianPopulation]      INT             NULL,
    [BlackPopulation]      INT             NULL,
    [FemalePopulation]     INT             NULL,
    [HawaiianPopulation]   INT             NULL,
    [HispanicPopulation]   INT             NULL,
    [IndianPopulation]     INT             NULL,
    [MalePopulation]       INT             NULL,
    [OtherPopulation]      INT             NULL,
    [WhitePopulation]      INT             NULL,
    [MedianAge]            DECIMAL (18, 8) NULL,
    [MedianAgeMale]        DECIMAL (18, 8) NULL,
    [MedianAgeFemale]      DECIMAL (18, 8) NULL,
    [AnnualPayroll]        INT             NULL,
    [FirstQuarterPayroll]  INT             NULL,
    [Employment]           INT             NULL,
    [Establishments]       INT             NULL,
    CONSTRAINT [PK_PostalCodeDemographics] PRIMARY KEY CLUSTERED ([PostalCode] ASC),
    CONSTRAINT [FK_PostalCodeDemographics_PostalCodes] FOREIGN KEY ([PostalCode]) REFERENCES [dbo].[PostalCodes] ([PostalCode])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Demographic information for most zip codes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'Population';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated households per zip code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'HouseholdsPerZipcode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The average house value', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'AverageHouseValue';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The income per household', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'IncomePerHousehold';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The people per household', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'PersonsPerHousehold';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated Asian population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'AsianPopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated Black population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'BlackPopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated female population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'FemalePopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated Hawiian population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'HawaiianPopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated Hispanic population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'HispanicPopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated Indian population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'IndianPopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated male population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'MalePopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated other population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'OtherPopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated White population', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'WhitePopulation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated median age', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'MedianAge';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated median age of males', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'MedianAgeMale';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated median age of females', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'MedianAgeFemale';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated annual payroll', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'AnnualPayroll';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated first quarter payroll', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'FirstQuarterPayroll';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated emplyment for each zip code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'Employment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Estimated number of establishments', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodeDemographics', @level2type = N'COLUMN', @level2name = N'Establishments';

