﻿CREATE TABLE [dbo].[DisbursementSoftCredits] (
    [DisbursementId]      INT NOT NULL,
    [FundraisingEntityId] INT NOT NULL,
    CONSTRAINT [PK_DisbursementSoftCredits] PRIMARY KEY CLUSTERED ([DisbursementId] ASC, [FundraisingEntityId] ASC),
    CONSTRAINT [FK_DisbursementSoftCredits_Disbursements] FOREIGN KEY ([DisbursementId]) REFERENCES [dbo].[Disbursements] ([DisbursementId]) ON DELETE CASCADE,
    CONSTRAINT [FK_DisbursementSoftCredits_FundraisingEntities] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Disbursements table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementSoftCredits', @level2type = N'COLUMN', @level2name = N'DisbursementId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the FundraisingEntities table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DisbursementSoftCredits', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';

