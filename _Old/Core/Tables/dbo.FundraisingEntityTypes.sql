﻿CREATE TABLE [dbo].[FundraisingEntityTypes] (
    [FundraisingEntityTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityType]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FundraisingEntityTypes] PRIMARY KEY CLUSTERED ([FundraisingEntityTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityTypes', @level2type = N'COLUMN', @level2name = N'FundraisingEntityTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Descriptioin of the type of entity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityTypes', @level2type = N'COLUMN', @level2name = N'FundraisingEntityType';

