﻿CREATE TABLE [dbo].[PropertyTypes] (
    [PropertyTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PropertyType]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PropertyTypes] PRIMARY KEY CLUSTERED ([PropertyTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table holds all the property types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypes', @level2type = N'COLUMN', @level2name = N'PropertyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the Property types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypes', @level2type = N'COLUMN', @level2name = N'PropertyType';

