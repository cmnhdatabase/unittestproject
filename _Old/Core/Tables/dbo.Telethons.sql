﻿CREATE TABLE [dbo].[Telethons] (
    [TelethonId]         INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignDetailsId]  INT           NOT NULL,
    [City]               VARCHAR (100) NOT NULL,
    [OnAirHours]         SMALLINT      NULL,
    [OnAirMinutes]       SMALLINT      NULL,
    [LanguageId]         INT           NOT NULL,
    [PhoneBank]          VARCHAR (100) NOT NULL,
    [Note]               VARCHAR (MAX) NULL,
    [SubmittedDate]      DATE          NOT NULL,
    [ModifiedDate]       DATE          NOT NULL,
    [Active]             BIT           NOT NULL,
    [EventId]            INT           NULL,
    [SubmittedBy]        INT           NULL,
    [ModifiedBy]         INT           NULL,
    [ToteBoardImagePath] VARCHAR (500) NULL,
    CONSTRAINT [PK_Telethons] PRIMARY KEY CLUSTERED ([TelethonId] ASC),
    FOREIGN KEY ([CampaignDetailsId]) REFERENCES [dbo].[CampaignDetails] ([CampaignDetailsId]),
    FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Languages] ([LanguageId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'TelethonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the CampaignDetails table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'CampaignDetailsId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City of Telethon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of hours on air', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'OnAirHours';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of minutes on air', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'OnAirMinutes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Languages table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'LanguageId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Notes on Telethon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'Note';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date telethon was submitted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'SubmittedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date telethon was modified', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to indicate if telethon is active or not', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The event ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Telethons', @level2type = N'COLUMN', @level2name = N'EventId';

