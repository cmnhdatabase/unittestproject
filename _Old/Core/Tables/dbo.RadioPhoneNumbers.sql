﻿CREATE TABLE [dbo].[RadioPhoneNumbers] (
    [RadioStationId] INT NOT NULL,
    [PhoneId]        INT NOT NULL,
    CONSTRAINT [PK_RadioPhones] PRIMARY KEY CLUSTERED ([RadioStationId] ASC, [PhoneId] ASC),
    CONSTRAINT [FK_RadioPhones_PhoneNumbers] FOREIGN KEY ([PhoneId]) REFERENCES [dbo].[PhoneNumbers] ([PhoneId]),
    CONSTRAINT [FK_RadioPhones_Radio] FOREIGN KEY ([RadioStationId]) REFERENCES [dbo].[Radio] ([RadioStationId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is a lookup table that ties the RadioStations and the PhoneNumbers tables together ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioPhoneNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to the RadioStations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioPhoneNumbers', @level2type = N'COLUMN', @level2name = N'RadioStationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to the PhoneNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RadioPhoneNumbers', @level2type = N'COLUMN', @level2name = N'PhoneId';

