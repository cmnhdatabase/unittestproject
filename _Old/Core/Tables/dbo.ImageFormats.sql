﻿CREATE TABLE [dbo].[ImageFormats] (
    [ImageFormatId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ImageFormat]   CHAR (4) NOT NULL,
    CONSTRAINT [PK_ImageFormats] PRIMARY KEY CLUSTERED ([ImageFormatId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The different formats of pictures we use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageFormats';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'THis is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageFormats', @level2type = N'COLUMN', @level2name = N'ImageFormatId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the format type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageFormats', @level2type = N'COLUMN', @level2name = N'ImageFormat';

