﻿CREATE TABLE [dbo].[ImageTypes] (
    [ImageTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ImageType]   VARCHAR (50) NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ImageTypeList] PRIMARY KEY CLUSTERED ([ImageTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The different types of pictures we use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'THis is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageTypes', @level2type = N'COLUMN', @level2name = N'ImageTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short name for image types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageTypes', @level2type = N'COLUMN', @level2name = N'ImageType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Long name for image types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ImageTypes', @level2type = N'COLUMN', @level2name = N'Description';

