﻿CREATE TABLE [dbo].[DonorInfoCheckNumbers] (
    [DonorInfoId]   INT NOT NULL,
    [CheckNumberId] INT NOT NULL,
    CONSTRAINT [PK_DonorInfoCheckNumbers] PRIMARY KEY CLUSTERED ([DonorInfoId] ASC, [CheckNumberId] ASC),
    CONSTRAINT [FK_DonorInfoCheckNumbers_CheckNumbers] FOREIGN KEY ([CheckNumberId]) REFERENCES [dbo].[CheckNumbers] ([CheckNumberID]),
    CONSTRAINT [FK_DonorInfoCheckNumbers_DonorInfo] FOREIGN KEY ([DonorInfoId]) REFERENCES [dbo].[DonorInfo] ([DonorInfoId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table tying DonorInfo to CheckNumbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfoCheckNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the DonorInfo table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfoCheckNumbers', @level2type = N'COLUMN', @level2name = N'DonorInfoId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the CheckNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfoCheckNumbers', @level2type = N'COLUMN', @level2name = N'CheckNumberId';

