﻿CREATE TABLE [dbo].[Countries] (
    [CountryId]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CountryName]    VARCHAR (50) NOT NULL,
    [Abbreviation]   CHAR (2)     NOT NULL,
    [CurrencyTypeId] INT          NULL,
    [PhonePrefix]    INT          NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED ([CountryId] ASC),
    CONSTRAINT [FK_Countries_Currencies] FOREIGN KEY ([CurrencyTypeId]) REFERENCES [dbo].[Currencies] ([CurrencyTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for each Country', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Countries';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Countries', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the full name of the country', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Countries', @level2type = N'COLUMN', @level2name = N'CountryName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the abbreviation of the country', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Countries', @level2type = N'COLUMN', @level2name = N'Abbreviation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Currency Type ID is tied to the Currencies table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Countries', @level2type = N'COLUMN', @level2name = N'CurrencyTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prefix needed to make a call to that country', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Countries', @level2type = N'COLUMN', @level2name = N'PhonePrefix';

