﻿CREATE TABLE [dbo].[InvoicedDisbursements] (
    [InvoiceId]      INT NOT NULL,
    [DisbursementId] INT NOT NULL,
    CONSTRAINT [PK_InvoicedDisbursements] PRIMARY KEY CLUSTERED ([InvoiceId] ASC, [DisbursementId] ASC),
    CONSTRAINT [FK_InvoicedDisbursements_Disbursements] FOREIGN KEY ([DisbursementId]) REFERENCES [dbo].[Disbursements] ([DisbursementId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_InvoicedDisbursements_Invoices] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoices] ([InvoiceId])
);

