﻿CREATE TABLE [dbo].[PostalCodes] (
    [PostalCode]         VARCHAR (10)  NOT NULL,
    [MarketId]           INT           NOT NULL,
    [SubMarketId]        INT           NOT NULL,
    [ProvinceId]         INT           NOT NULL,
    [CountryId]          INT           NOT NULL,
    [City]               VARCHAR (100) NOT NULL,
    [County]             VARCHAR (50)  NULL,
    [PopulationEstimate] BIGINT        NOT NULL,
    [Latitude]           FLOAT (53)    NULL,
    [Longitude]          FLOAT (53)    NULL,
    [LastModified]       DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PostalCodes] PRIMARY KEY CLUSTERED ([PostalCode] ASC),
    FOREIGN KEY ([SubMarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_PostalCodes_Countries1] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_PostalCodes_Markets1] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_PostalCodes_Provinces1] FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId])
);


GO
CREATE NONCLUSTERED INDEX [ixSMSPostalCodes]
    ON [dbo].[PostalCodes]([MarketId] ASC)
    INCLUDE([PostalCode]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table holds all the infomration for zip codes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'SubMarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Provincies table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the City that zip code is in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the county that this zip code is in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'County';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Population estimate for zip code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'PopulationEstimate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude coordinates for zip code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'Latitude';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude coordinates for zip code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PostalCodes', @level2type = N'COLUMN', @level2name = N'Longitude';

