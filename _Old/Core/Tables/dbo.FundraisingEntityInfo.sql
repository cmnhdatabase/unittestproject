﻿CREATE TABLE [dbo].[FundraisingEntityInfo] (
    [FundraisingEntityInfoId]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityId]         INT           NOT NULL,
    [FundraisingEntityInfoTypeId] INT           NOT NULL,
    [Info]                        VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_FundraisingEntityInfo] PRIMARY KEY CLUSTERED ([FundraisingEntityInfoId] ASC),
    CONSTRAINT [FK_FundraisingEntityInfo_FundraisingEntities] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId]),
    CONSTRAINT [FK_FundraisingEntityInfo_FundraisingEntityInfoTypes] FOREIGN KEY ([FundraisingEntityInfoTypeId]) REFERENCES [dbo].[FundraisingEntityInfoTypes] ([FundraisingEntityInfoTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityInfo', @level2type = N'COLUMN', @level2name = N'FundraisingEntityInfoId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the FundraisingEntities table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityInfo', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the FundraisingEntityInfo table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityInfo', @level2type = N'COLUMN', @level2name = N'FundraisingEntityInfoTypeId';

