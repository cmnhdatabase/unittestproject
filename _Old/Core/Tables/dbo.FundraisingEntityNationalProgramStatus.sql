﻿CREATE TABLE [dbo].[FundraisingEntityNationalProgramStatus] (
    [NationalProgramStatusId] INT  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FundraisingEntityId]     INT  NOT NULL,
    [StartDate]               DATE NULL,
    [EndDate]                 DATE NULL,
    CONSTRAINT [PK_CampaignTypeStatus] PRIMARY KEY CLUSTERED ([NationalProgramStatusId] ASC),
    CONSTRAINT [FK_FundraisingEntityNationalProgramStatus_FundraisingEntitys] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Table used to track start and end dates for events', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityNationalProgramStatus';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityNationalProgramStatus', @level2type = N'COLUMN', @level2name = N'NationalProgramStatusId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Start date of event', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityNationalProgramStatus', @level2type = N'COLUMN', @level2name = N'StartDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'End date of event', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FundraisingEntityNationalProgramStatus', @level2type = N'COLUMN', @level2name = N'EndDate';

