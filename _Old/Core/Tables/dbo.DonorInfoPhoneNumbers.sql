﻿CREATE TABLE [dbo].[DonorInfoPhoneNumbers] (
    [DonorInfoId] INT NOT NULL,
    [PhoneId]     INT NOT NULL,
    CONSTRAINT [PK_DonorPhones] PRIMARY KEY CLUSTERED ([DonorInfoId] ASC, [PhoneId] ASC),
    CONSTRAINT [FK_DonorInfoPhones_DonorInfo] FOREIGN KEY ([DonorInfoId]) REFERENCES [dbo].[DonorInfo] ([DonorInfoId]) ON DELETE CASCADE,
    CONSTRAINT [FK_DonorInfoPhones_PhoneNumbers] FOREIGN KEY ([PhoneId]) REFERENCES [dbo].[PhoneNumbers] ([PhoneId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table tying DonorInfo to PhoneNumbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfoPhoneNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the DonorInfo table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfoPhoneNumbers', @level2type = N'COLUMN', @level2name = N'DonorInfoId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PhoneNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DonorInfoPhoneNumbers', @level2type = N'COLUMN', @level2name = N'PhoneId';

