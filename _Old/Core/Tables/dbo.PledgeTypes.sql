﻿CREATE TABLE [dbo].[PledgeTypes] (
    [PledgeTypeId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PledgeType]   VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([PledgeTypeId] ASC),
    UNIQUE NONCLUSTERED ([PledgeType] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table lists all the pledge types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeTypes', @level2type = N'COLUMN', @level2name = N'PledgeTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type of pledge made', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PledgeTypes', @level2type = N'COLUMN', @level2name = N'PledgeType';

