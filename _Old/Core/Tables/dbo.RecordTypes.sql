﻿CREATE TABLE [dbo].[RecordTypes] (
    [RecordTypeId] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RecordType]   VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_RecordTypes] PRIMARY KEY CLUSTERED ([RecordTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Holds all the Records type that we use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RecordTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RecordTypes', @level2type = N'COLUMN', @level2name = N'RecordTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Record types', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RecordTypes', @level2type = N'COLUMN', @level2name = N'RecordType';

