﻿CREATE TABLE [dbo].[LocationPhoneNumbers] (
    [LocationId] INT NOT NULL,
    [PhoneId]    INT NOT NULL,
    CONSTRAINT [PK_LocationPhones] PRIMARY KEY CLUSTERED ([LocationId] ASC, [PhoneId] ASC),
    CONSTRAINT [FK_LocationPhones_Locations] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Locations] ([LocationId]),
    CONSTRAINT [FK_LocationPhones_PhoneNumbers] FOREIGN KEY ([PhoneId]) REFERENCES [dbo].[PhoneNumbers] ([PhoneId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table that ties Locations to PhoneNumbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationPhoneNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Locations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationPhoneNumbers', @level2type = N'COLUMN', @level2name = N'LocationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PhoneNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LocationPhoneNumbers', @level2type = N'COLUMN', @level2name = N'PhoneId';

