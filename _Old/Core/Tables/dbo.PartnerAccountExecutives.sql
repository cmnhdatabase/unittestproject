﻿CREATE TABLE [dbo].[PartnerAccountExecutives] (
    [FundraisingEntityId]       INT NOT NULL,
    [PartnerAccountStaffTypeId] INT NOT NULL,
    [ContactId]                 INT NOT NULL,
    CONSTRAINT [PK_PartnerAccountExecutives] PRIMARY KEY CLUSTERED ([FundraisingEntityId] ASC, [ContactId] ASC, [PartnerAccountStaffTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is a lookup tabel it ties PartnerPrograms, Users, and PartnerAccountStaffTypes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerAccountExecutives';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK, this is tied to the PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerAccountExecutives', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK, this is tied to the PartnerAccountStaffTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerAccountExecutives', @level2type = N'COLUMN', @level2name = N'PartnerAccountStaffTypeId';

