﻿CREATE TABLE [dbo].[SchoolTypes] (
    [SchoolTypeId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SchoolType]   VARCHAR (500) NOT NULL,
    CONSTRAINT [PK_SchoolType] PRIMARY KEY CLUSTERED ([SchoolTypeId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Types of schools high schools or colleges', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SchoolTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SchoolTypes', @level2type = N'COLUMN', @level2name = N'SchoolTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Types of schools', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SchoolTypes', @level2type = N'COLUMN', @level2name = N'SchoolType';

