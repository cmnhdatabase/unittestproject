﻿CREATE TABLE [dbo].[Radio] (
    [CallLetters]    VARCHAR (10)  NOT NULL,
    [MarketId]       INT           NOT NULL,
    [ListenLiveUri]  VARCHAR (255) NULL,
    [Owner]          VARCHAR (50)  NULL,
    [Frequency]      VARCHAR (25)  NULL,
    [Website]        VARCHAR (125) NULL,
    [Address1]       VARCHAR (100) NULL,
    [Address2]       VARCHAR (100) NULL,
    [Address3]       VARCHAR (100) NULL,
    [City]           VARCHAR (100) NULL,
    [ProvinceId]     INT           NULL,
    [PostalCode]     VARCHAR (10)  NULL,
    [CountryId]      INT           NULL,
    [Format]         VARCHAR (50)  NULL,
    [MarketTag]      VARCHAR (100) NULL,
    [MarketArea]     VARCHAR (50)  NULL,
    [YearStarted]    INT           NULL,
    [Note]           VARCHAR (MAX) NULL,
    [Consultant]     VARCHAR (50)  NULL,
    [ForestersCore]  VARCHAR (50)  NULL,
    [CUME]           INT           NULL,
    [ModifiedDate]   DATE          NULL,
    [Active]         BIT           NOT NULL,
    [RadioStationId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    CONSTRAINT [PK_Radio_1] PRIMARY KEY CLUSTERED ([RadioStationId] ASC),
    CONSTRAINT [FK_Radio_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_Radio_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_Radio_Provinces] FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table is for all the Radio station information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Call letters for radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'CallLetters';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URI to listen to radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'ListenLiveUri';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Owner of the station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Owner';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Radio station Frequency', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Frequency';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Radio station website', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Website';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line one of radio stations address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line two of radio stations address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line three of radio stations address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Address3';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City of radio stations address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Provinces table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal code for radio stations address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Radio station format', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Format';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name of the radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'MarketTag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Market area of the radio station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'MarketArea';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year radio station was started', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'YearStarted';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Notes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Note';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Consultant';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'ForestersCore';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'CUME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date that record was last modified', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is to verify that the radio station is active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Radio', @level2type = N'COLUMN', @level2name = N'RadioStationId';

