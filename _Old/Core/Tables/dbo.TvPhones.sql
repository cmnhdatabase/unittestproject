﻿CREATE TABLE [dbo].[TvPhones] (
    [TVStationId] INT NOT NULL,
    [PhoneId]     INT NOT NULL,
    CONSTRAINT [PK_TvPhones] PRIMARY KEY CLUSTERED ([TVStationId] ASC, [PhoneId] ASC),
    CONSTRAINT [FK_TvPhones_PhoneNumbers] FOREIGN KEY ([PhoneId]) REFERENCES [dbo].[PhoneNumbers] ([PhoneId]),
    CONSTRAINT [FK_TvPhones_TV] FOREIGN KEY ([TVStationId]) REFERENCES [dbo].[TV] ([TVStationId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table for TV and PhoneNumbers tables', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TvPhones';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to TV table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TvPhones', @level2type = N'COLUMN', @level2name = N'TVStationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to PhoneNumbers table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TvPhones', @level2type = N'COLUMN', @level2name = N'PhoneId';

