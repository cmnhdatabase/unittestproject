﻿CREATE TABLE [dbo].[Campaigns] (
    [CampaignId]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignName]     VARCHAR (300) NULL,
    [ShortDescription] VARCHAR (300) NULL,
    [LongDescription]  VARCHAR (500) NULL,
    [CampaignTypeId]   INT           NOT NULL,
    CONSTRAINT [PK_Campaigns] PRIMARY KEY CLUSTERED ([CampaignId] ASC),
    CONSTRAINT [FK_Campaigns_CampaignTypes] FOREIGN KEY ([CampaignTypeId]) REFERENCES [dbo].[CampaignTypes] ([CampaignTypeId])
);


GO
CREATE NONCLUSTERED INDEX [ixCampaignsCampaignName]
    ON [dbo].[Campaigns]([CampaignName] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name and descriptions for all the Campaigns', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Campaigns';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Campaigns', @level2type = N'COLUMN', @level2name = N'CampaignId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of each Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Campaigns', @level2type = N'COLUMN', @level2name = N'CampaignName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short Description of Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Campaigns', @level2type = N'COLUMN', @level2name = N'ShortDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Long Description of Campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Campaigns', @level2type = N'COLUMN', @level2name = N'LongDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the type of Campaign it is. This is tied to the CampaignTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Campaigns', @level2type = N'COLUMN', @level2name = N'CampaignTypeId';

