﻿CREATE TABLE [dbo].[DanceMarathonTypes] (
    [DanceMarathonTypeId] INT          IDENTITY (1, 1) NOT NULL,
    [DanceMarathonType]   VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_DanceMarathonTypes] PRIMARY KEY CLUSTERED ([DanceMarathonTypeId] ASC)
);

