﻿CREATE TABLE [dbo].[PartnerMarketLocationTotalsByYear] (
    [MarketId]            INT NOT NULL,
    [FundraisingEntityId] INT NOT NULL,
    [CampaignYear]        INT NOT NULL,
    [Locations]           INT NOT NULL,
    CONSTRAINT [PK_PartnerMarketLocationTotalsByYear] PRIMARY KEY CLUSTERED ([MarketId] ASC, [FundraisingEntityId] ASC, [CampaignYear] ASC),
    CONSTRAINT [FK_PartnerMarketLocationTotalsByYear_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_PartnerMarketLocationTotalsByYear_PartnerPrograms] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is a Lookup table that ties Markets, and PartnerPrograms', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerMarketLocationTotalsByYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to Markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerMarketLocationTotalsByYear', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part of the PK and is tied to PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerMarketLocationTotalsByYear', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the year of the campaign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerMarketLocationTotalsByYear', @level2type = N'COLUMN', @level2name = N'CampaignYear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Location of the campaign and is tied to Locations table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PartnerMarketLocationTotalsByYear', @level2type = N'COLUMN', @level2name = N'Locations';

