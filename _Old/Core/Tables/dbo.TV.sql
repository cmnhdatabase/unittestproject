﻿CREATE TABLE [dbo].[TV] (
    [CallLetters]  VARCHAR (10)  NOT NULL,
    [Affiliation]  VARCHAR (10)  NULL,
    [Address1]     VARCHAR (100) NULL,
    [Address2]     VARCHAR (100) NULL,
    [City]         VARCHAR (100) NULL,
    [ProvinceId]   INT           NULL,
    [PostalCode]   VARCHAR (10)  NULL,
    [CountryId]    INT           NULL,
    [Website]      VARCHAR (100) NULL,
    [MarketId]     INT           NULL,
    [TVStationId]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [StationOwner] VARCHAR (100) NULL,
    [MarketTag]    VARCHAR (100) NULL,
    [Note]         VARCHAR (MAX) NULL,
    [ModifiedDate] DATETIME      NOT NULL,
    [Active]       BIT           NOT NULL,
    [Director]     INT           NULL,
    [ModifiedBy]   INT           NULL,
    CONSTRAINT [PK_TV] PRIMARY KEY CLUSTERED ([TVStationId] ASC),
    CONSTRAINT [FK_TV_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_TV_Markets] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Markets] ([MarketId]),
    CONSTRAINT [FK_TV_Provinces] FOREIGN KEY ([ProvinceId]) REFERENCES [dbo].[Provinces] ([ProvinceId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Table of TV stations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TV call letters', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'CallLetters';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Affiliate station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'Affiliation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line one of address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Line two of address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City station is located in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to Province ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'ProvinceId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Postal code of station', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'PostalCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stations website address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'Website';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the markets table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TV', @level2type = N'COLUMN', @level2name = N'TVStationId';

