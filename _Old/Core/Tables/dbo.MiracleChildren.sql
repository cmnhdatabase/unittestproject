﻿CREATE TABLE [dbo].[MiracleChildren] (
    [MiracleChildId]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [HospitalId]       INT           NULL,
    [FirstName]        VARCHAR (50)  NULL,
    [LastName]         VARCHAR (50)  NULL,
    [ShortDescription] VARCHAR (500) NULL,
    [Year]             INT           NOT NULL,
    [CountryId]        INT           NOT NULL,
    CONSTRAINT [PK_MiracleChildren] PRIMARY KEY CLUSTERED ([MiracleChildId] ASC),
    CONSTRAINT [FK_MiracleChildren_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_MiracleChildren_Hospitals] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information for all the Miracle children', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'MiracleChildId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Hospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'HospitalId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'First name of the Miracle Child', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last name of the Miracle Child', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Short Story of the Child', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'ShortDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year they were Miracle Children', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'Year';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiracleChildren', @level2type = N'COLUMN', @level2name = N'CountryId';

