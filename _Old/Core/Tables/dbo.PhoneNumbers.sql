﻿CREATE TABLE [dbo].[PhoneNumbers] (
    [PhoneId]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Phone]        VARCHAR (15) NOT NULL,
    [PhoneTypeId]  INT          NOT NULL,
    [CountryId]    INT          NOT NULL,
    [Extension]    INT          NULL,
    [SalesForceId] VARCHAR (18) NULL,
    CONSTRAINT [PK_PhoneNumbers] PRIMARY KEY CLUSTERED ([PhoneId] ASC),
    CONSTRAINT [FK_PhoneNumbers_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_PhoneNumbers_PhoneTypes] FOREIGN KEY ([PhoneTypeId]) REFERENCES [dbo].[PhoneTypes] ([PhoneTypeId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This table lists all the phone numbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneNumbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneNumbers', @level2type = N'COLUMN', @level2name = N'PhoneId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Phone number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneNumbers', @level2type = N'COLUMN', @level2name = N'Phone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the PhoneTypes table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneNumbers', @level2type = N'COLUMN', @level2name = N'PhoneTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PhoneNumbers', @level2type = N'COLUMN', @level2name = N'CountryId';

