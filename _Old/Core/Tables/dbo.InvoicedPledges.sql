﻿CREATE TABLE [dbo].[InvoicedPledges] (
    [InvoiceId] INT NOT NULL,
    [PledgeId]  INT NOT NULL,
    CONSTRAINT [PK_InvoicedPledges] PRIMARY KEY CLUSTERED ([InvoiceId] ASC, [PledgeId] ASC),
    CONSTRAINT [FK_InvoicedPledges_InvoicedPledges] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoices] ([InvoiceId]),
    CONSTRAINT [FK_InvoicedPledges_PledgeData] FOREIGN KEY ([PledgeId]) REFERENCES [dbo].[PledgeData] ([PledgeId])
);

