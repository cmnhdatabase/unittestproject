﻿CREATE TABLE [dbo].[Markets] (
    [MarketId]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MarketName]     VARCHAR (100) NOT NULL,
    [CountryId]      INT           NOT NULL,
    [RegionId]       INT           NOT NULL,
    [Active]         BIT           NOT NULL,
    [LegacyMarketId] VARCHAR (100) NULL,
    [ShortName]      VARCHAR (17)  NULL,
    CONSTRAINT [PK_Markets] PRIMARY KEY CLUSTERED ([MarketId] ASC),
    CONSTRAINT [FK_Markets_Countries] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([CountryId]),
    CONSTRAINT [FK_Markets_Regions] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Regions] ([RegionId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Information on all markets', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets', @level2type = N'COLUMN', @level2name = N'MarketId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the market', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets', @level2type = N'COLUMN', @level2name = N'MarketName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Countries table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets', @level2type = N'COLUMN', @level2name = N'CountryId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is tied to the Regions table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets', @level2type = N'COLUMN', @level2name = N'RegionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Is the market active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets', @level2type = N'COLUMN', @level2name = N'Active';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Legacy Market ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Markets', @level2type = N'COLUMN', @level2name = N'LegacyMarketId';

