﻿CREATE TABLE [dbo].[TelethonHospitals] (
    [TelethonId] INT NOT NULL,
    [HospitalId] INT NOT NULL,
    CONSTRAINT [PK_TelethonHospitals] PRIMARY KEY CLUSTERED ([HospitalId] ASC, [TelethonId] ASC),
    FOREIGN KEY ([TelethonId]) REFERENCES [dbo].[Telethons] ([TelethonId]),
    CONSTRAINT [FK__TelethonH__Hospi__177A04BA] FOREIGN KEY ([HospitalId]) REFERENCES [dbo].[Hospitals] ([HospitalId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the telethons table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TelethonHospitals', @level2type = N'COLUMN', @level2name = N'TelethonId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tied to the Hospitals table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TelethonHospitals', @level2type = N'COLUMN', @level2name = N'HospitalId';

