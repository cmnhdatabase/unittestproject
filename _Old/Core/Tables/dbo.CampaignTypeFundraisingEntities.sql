﻿CREATE TABLE [dbo].[CampaignTypeFundraisingEntities] (
    [CampaignTypeFundraisingEntityId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CampaignTypeId]                  INT NOT NULL,
    [FundraisingEntityId]             INT NOT NULL,
    CONSTRAINT [PK_CampaignTypePartnerPrograms] PRIMARY KEY CLUSTERED ([CampaignTypeFundraisingEntityId] ASC),
    CONSTRAINT [FK_CampaignTypePartnerPrograms_CampaignTypes] FOREIGN KEY ([CampaignTypeId]) REFERENCES [dbo].[CampaignTypes] ([CampaignTypeId]),
    CONSTRAINT [FK_CampaignTypePartnerPrograms_PartnerPrograms] FOREIGN KEY ([FundraisingEntityId]) REFERENCES [dbo].[FundraisingEntities] ([FundraisingEntityId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lookup table tying CampaignTypes and PartnerPrograms table together', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypeFundraisingEntities';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the PK for this table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypeFundraisingEntities', @level2type = N'COLUMN', @level2name = N'CampaignTypeFundraisingEntityId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This ID is tied to the CampaignType table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypeFundraisingEntities', @level2type = N'COLUMN', @level2name = N'CampaignTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This ID is tied to the PartnerPrograms table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CampaignTypeFundraisingEntities', @level2type = N'COLUMN', @level2name = N'FundraisingEntityId';

