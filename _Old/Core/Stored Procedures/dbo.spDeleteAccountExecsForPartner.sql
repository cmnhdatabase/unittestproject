﻿
CREATE PROCEDURE [dbo].[spDeleteAccountExecsForPartner]
	@FundraisingEntityId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

  delete
  FROM [Core].[dbo].[PartnerAccountExecutives]
  where [FundraisingEntityId] = @FundraisingEntityId

  SELECT CAST(SCOPE_IDENTITY() AS INT);
END

