﻿


-- =============================================
-- Author:		Travis Poll
-- Create date: 4/27/2015
-- Description:	This will grab the invoice details for the specified year and quarter.  There is also an optional parameter for market if it is not specified all marekts will be returned.
-- =============================================
-- Modified By: Darrin Olsen
-- Modified Date: 8/18/2016
-- Description: Adjusted Rates to be more accurate.
-- =============================================
CREATE PROCEDURE [dbo].[spGetInvoiceSummary]
-- Add the parameters for the stored procedure here
	@yr INT , @qrtr INT
AS
	BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
		SET NOCOUNT ON;
        -- Insert statements for procedure here
		DECLARE	@invid INT = (
							   SELECT DISTINCT
										InvoiceId
							   FROM		Invoices iv
							   WHERE	iv.InvoiceYear = @yr
										AND iv.InvoiceQuarter = @qrtr
							 );
		SELECT	MarketName ,
				FundraisingYear ,
				SUM(CASE WHEN DirectToHospital = 1
							  AND CategoryId != 3 THEN Amount
						 ELSE 0
					END) AS DirectToHospital ,
				SUM(CASE WHEN DirectToHospital = 0
							  AND CategoryId != 3 THEN Amount
						 ELSE 0
					END) AS ThroughCMN ,
				SUM(CASE WHEN CategoryId != 3 THEN Amount
						 ELSE 0
					END) AS Total ,
				CASE WHEN CountryId = 1 THEN .055
					 ELSE CASE WHEN FundraisingYear = 2015 THEN 0.055
							   WHEN FundraisingYear = 2016 THEN 0.060
							   WHEN FundraisingYear = 2017 THEN 0.070
							   ELSE 0.060
						  END
				END AS InvoiceRate ,
				SUM(CASE WHEN CategoryId != 3 THEN Amount
						 ELSE 0
					END) * CASE	WHEN CountryId = 1 THEN .055
								ELSE CASE WHEN FundraisingYear = 2015 THEN 0.055
										  WHEN FundraisingYear = 2016 THEN 0.060
										  WHEN FundraisingYear = 2017 THEN 0.070
										  ELSE 0.060
									 END
						   END AS TotalInvoiced
        --INTO #tempInvoices
		FROM	(
				  SELECT	@invid AS invoiceid ,
							3 AS CategoryId ,
							'Local' AS Category ,
							FundraisingYear ,
							0 disbursementquarter ,
							0 dispursementyear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName AS ProgramDisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							CAST(CASE WHEN DirectToHospital = 0 THEN 0
									  ELSE 1
								 END AS INT) AS DirectToHospital ,
							m.RegionId ,
							l.LocationId ,
							m.CountryId
				  FROM		Disbursements d
				  INNER JOIN /*dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN*/ Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  WHERE		RecordTypeId = 1
							AND d.FundraisingEntityId = 56
							AND d.FundraisingYear = @yr
							AND DATEPART(QUARTER , d.DateRecorded) = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							1 ,
							'National Corporate Partners' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId ,
							m.CountryId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND d.FundraisingEntityId NOT IN ( 56 , 20 , 21 ) 

                  /*---------------------update below to use disbursementdate*/
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							1 ,
							'National Corporate Partners' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId ,
							m.CountryId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND ( p.FundraisingEntityId IN ( 20 , 21 )
								  AND ( cp.CampaignTypeFundraisingEntityId IS NULL
										OR ( ps.EndDate IS NOT NULL
											 AND YEAR(ps.EndDate) <= d.FundraisingYear ) ) )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							2 ,
							'National Programs and Events' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							CASE WHEN pp.DisplayName LIKE '%Radiothon' THEN 'Radiothon Announced'
								 ELSE pp.DisplayName
							END AS programDisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId ,
							m.CountryId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
								  AND ( ps.EndDate IS NULL
										OR 

                      /*y(ear(ps.EndDate) >= d .FundraisingYear AND disbursementdate != '1900-01-01')))*/ ( YEAR(ps.EndDate) - 1 >= d.FundraisingYear

                      /*AND disbursementdate != '1900-01-01'*/ ) ) )
							AND NOT ( c.CampaignTypeId IN ( 27 , 32 )
									  AND d.FundraisingYear >= 2013 )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
                               
                 /*-pledge data for ntl programs*/
				  UNION ALL
				  SELECT	i.InvoiceId ,
							4 ,
							'Overlap' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName ,
							CurrencyTypeId ,
							Amount * -1 ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId ,
							m.CountryId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND d.FundraisingEntityId NOT IN ( 56 , 20 , 21 ) 

                  /*---------------------update below to use disbursementdate*/
							AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
								  AND ( ps.EndDate IS NULL
										OR ( YEAR(ps.EndDate) - 1 >= d.FundraisingYear
											 AND DisbursementDate != '1900-01-01' ) ) )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							2 ,
							'National Programs and Events' AS Category ,
							FundraisingYear ,
							d.Quarter ,
							YEAR(d.PledgeDate) AS DisbYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							CASE WHEN pp.DisplayName LIKE '%Radiothon' THEN 'Radiothon Announced'
								 ELSE pp.DisplayName
							END AS DisplayName ,
							CurrencyTypeId ,
							Amount ,
							NULL ,
							NULL ,
							PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							NULL ,
							m.CountryId
				  FROM		PledgeData d
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  LEFT OUTER JOIN InvoicedPledges i ON d.PledgeId = i.PledgeId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		PledgeTypeId IN ( 1 , 2 )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				) t
		GROUP BY MarketName ,
				FundraisingYear ,
				t.CountryId
		ORDER BY 1;
	END;


