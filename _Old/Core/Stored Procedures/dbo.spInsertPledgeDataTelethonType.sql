﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 08/11/2015
-- Description:	This procedure is used to add a new PledgeDataTelethonType record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertPledgeDataTelethonType]
	@PledgeId int,
	@TelethonTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[PledgeDataTelethonType] (
		[PledgeId],
		[TelethonTypeId]
	)
	VALUES (
		@PledgeId,
		@TelethonTypeId
	)
END






















