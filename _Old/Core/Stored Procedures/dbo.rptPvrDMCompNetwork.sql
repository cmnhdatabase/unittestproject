﻿

CREATE PROCEDURE [dbo].[rptPvrDMCompNetwork] ( @year INT, @market INT )
AS
 /****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market INT = 39
/*****************/

    IF OBJECT_ID('tempdb..#tempDMCompNetwork', 'U') IS NOT NULL
        DROP TABLE #tempDMCompNetwork

    SELECT  
	SUM(CYNatTotal) / NULLIF(SUM(a.DmCount), 0) AS NatAverage,
            SUM(a.CYNatTotal) - SUM(a.CYMinus1NatTotal)/NULLIF(SUM(a.DmCount), 0) AS Increase ,
            ( SUM(CYNatTotal) - SUM(CYMinus1NatTotal) )
            / NULLIF(SUM(CYNatTotal), 0) AS PctChange ,
            SUM(CYNatTotal) / SUM(a.pop) AS PerCap 
    INTO    #tempDMCompNetwork
    FROM    ( 
	SELECT    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                        THEN d.Amount
                                   END), 0) AS CYNatTotal ,
                        ISNULL(SUM(CASE WHEN d.FundraisingYear = @year - 1
                                        THEN d.Amount
                                   END), 0) AS CYMinus1NatTotal ,
                        '' AS DMCount ,
                        '' AS pop ,
                        MarketId
              FROM      dbo.vwFundraisingData d
              WHERE     ( d.FundraisingYear >= @year - 1 )
                        AND d.CampaignTypeId = 10
              GROUP BY  MarketId
              UNION ALL
              SELECT    '' ,
                        '' ,
                        COUNT(DISTINCT DanceMarathonId) AS DMCount ,
                        '' ,
                        ''
              FROM      dbo.vwDanceMarathons dm
                        INNER JOIN dbo.vwCampaignDetails cd ON cd.CampaignDetailsId = dm.CampaignDetailsId
              WHERE     cd.CampaignYear = @year
			  AND Active = 1
              UNION ALL
              SELECT    '' ,
                        '' ,
                        '' ,
                        PopulationEstimate ,
                        ''
              FROM      dbo.vwMarketPopulationRank
            ) AS a

			

    SELECT  *
    FROM    #tempDMCompNetwork
