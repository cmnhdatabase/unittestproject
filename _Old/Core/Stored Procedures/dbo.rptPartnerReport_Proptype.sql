﻿







CREATE PROCEDURE [dbo].[rptPartnerReport_Proptype]
(
	@prtnr INT 
)
AS

BEGIN
	/***********	TEST SECTION	***********/
--DECLARE
--@prtnr INT = 123


	/*********** END TEST SECTION	***********/

SELECT DISTINCT CAST(l.PropertyTypeId AS int) AS PropertyTypeID, pt.PropertyType
FROM            Locations AS l INNER JOIN
                         PropertyTypes AS pt ON pt.PropertyTypeId = l.PropertyTypeId
WHERE        (l.FundraisingEntityID = @prtnr)
END








