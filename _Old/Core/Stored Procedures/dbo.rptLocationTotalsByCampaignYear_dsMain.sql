﻿



CREATE PROCEDURE [dbo].[rptLocationTotalsByCampaignYear_dsMain]
( @yr			INT,
  @MarketId		NVARCHAR(MAX),
  @prtnr		NVARCHAR(MAX), 
  @IncludeSubMarkets BIT
)
AS

BEGIN
	SET NOCOUNT ON

	/***********	TEST SECTION	***********/

		--DECLARE @yr			INT = 2014
		--DECLARE @MarketId	NVARCHAR(MAX) = '95'
		--DECLARE @prtnr		NVARCHAR(MAX) = '20, 26, 28'
		--DECLARE @IncludeSubMarkets	BIT = 1

		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/
	
	IF OBJECT_ID('tempdb..#TotalsByYear', 'U') IS NOT NULL		DROP TABLE #TotalsByYear
	IF OBJECT_ID('tempdb..#PivotedInfo', 'U') IS NOT NULL		DROP TABLE #PivotedInfo

	DECLARE @Markets TABLE 
	 (	MarketID	INT
	  , MarketName	NVARCHAR(100)
	  , RegionId	INT
	 )
	IF @IncludeSubMarkets = 1
		INSERT INTO @Markets
		SELECT mkt.MarketId
			 , mkt.MarketName
			 , mkt.RegionId
		  FROM [Core].[dbo].[ContractHolders] AS ch 
		    INNER JOIN [Core].[dbo].[Markets] AS mkt ON ch.SubMarketId = mkt.MarketId
		  WHERE ch.PrimaryMarketId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@MarketId, ','))  
	  ELSE
		INSERT INTO @Markets
		SELECT mkt.MarketId
			 , mkt.MarketName
			 , mkt.RegionId
		  FROM [Core].[dbo].[Markets] AS mkt 
		  WHERE mkt.MarketId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@MarketId, ','))  
		
			--select * from @Markets

	SELECT FundraisingYear, 
		   (CASE WHEN FundraisingYear = @yr THEN 'CY' /* CurrentYear*/ 
				 WHEN FundraisingYear = @yr - 1 THEN 'CY-1' /* Current Year minus 1*/ 
				 WHEN FundraisingYear = @yr - 2 THEN 'CY-2' /* Current Year minus 2*/
				 WHEN FundraisingYear = @yr - 3 THEN 'CY-3' /* Current Year minus 3*/ 
		    END)					AS RelativeYear, 
		   LocationId,
		   SUM(Amount)				AS TotalAmount, 
		   pp.FundraisingEntityId, 
		   r.RegionName, 
		   m.MarketName				AS MarketName,
		   pp.Name					AS PartnerName
	  INTO #TotalsByYear
	  FROM [Core].[dbo].[Disbursements] AS db 
	    INNER JOIN [Core].[dbo].FundraisingEntities AS pp ON db.FundraisingEntityId = pp.FundraisingEntityId 
		INNER JOIN @Markets AS m ON db.MarketId = m.MarketID
		----INNER JOIN @Markets AS m ON db.SubMarketId = m.MarketID -- this would allow displaying the name of the Submarket
		INNER JOIN [dbo].[Regions] AS r ON m.RegionId = r.RegionId
	  WHERE FundraisingYear >= @yr - 3 
		AND pp.FundraisingEntityId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@prtnr, ','))  
		AND RecordTypeId = 1
	  GROUP BY FundraisingYear
			 , LocationId
			 , pp.FundraisingEntityId
			 , r.RegionName
			 , m.MarketName
			 , pp.name

	--FROM [Core].[dbo].[Disbursements] AS db INNER JOIN
	--	[Core].[dbo].[PartnerPrograms] AS pp ON db.PartnerId = pp.PartnerProgramId 
	--	INNER JOIN [dbo].[Markets] AS m ON db.MarketId = m.MarketId 
	--	INNER JOIN [dbo].[Regions] AS r ON m.RegionId = r.RegionId
	--WHERE FundraisingYear >= @yr - 3 
	--	AND m.MarketId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@MarketId, ',')) 
	--	AND r.RegionId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@RegionId, ',')) 
	--	AND pp.PartnerProgramId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@prtnr, ','))  
	--GROUP BY FundraisingYear, LocationId, pp.PartnerProgramId, r.RegionName, m.MarketName, pp.name
  
--  	select * from #TotalsByYear                           
	/*--select count(*) AS CountTBY FROM #TotalsByYear*/ 

	SELECT *
	  INTO #PivotedInfo
	  FROM (
			SELECT ty.TotalAmount
				 , ty.RelativeYear
				 , (SELECT @yr)									AS CurrentYear
				 , loc.LocationName							AS FacilityName
				--, stmgr.FacilityManager
				 , ISNULL(loc.LocationNumber, 'Unknown')	AS LocationNumber
				 , loc.LocationId
				 , loc.Address1
				 , loc.Address2
				 , loc.City
				 , prv.Abbreviation
				 , loc.PostalCode
				 , (CASE WHEN loc.Active = 1 THEN 'Open' 
						 WHEN loc.Active IS NULL THEN 'Unknown' 
						 ELSE 'Closed' 
					END)									AS StoreStat
				 , ty.MarketName							AS MarketName
				 , ty.PartnerName							AS PartnerName
				 , prt.PropertyType
			  FROM #TotalsByYear AS ty 
				LEFT JOIN [Core].[dbo].[Locations] AS loc ON ty.FundraisingEntityId = loc.FundraisingEntityId AND ty.LocationId = loc.LocationId 
				LEFT JOIN [Core].[dbo].[Provinces] AS prv ON loc.ProvinceId = prv.ProvinceId
				LEFT OUTER JOIN core.dbo.PropertyTypes AS prt ON prt.PropertyTypeId = loc.PropertyTypeId
			) AS ank 
	  PIVOT (SUM(TotalAmount) FOR RelativeYear IN ([CY], [CY-1], [CY-2], [CY-3])) AS pv
	  ORDER BY FacilityName
			 , LocationNumber

	/*--select count(*) AS CountPVI FROM #PivotedInfo*/ 
	SELECT *
	  FROM #PivotedInfo AS pvi /*left join*/ 
		OUTER APPLY 
		 (	SELECT TOP 1 ctc.LastName + ', ' + ctc.FirstName AS 'FacilityManager'
		, ctc.Phone
		/*, ltdc.*, ltd.*, lt.**/ 
FROM [Contacts].[dbo].[Contacts] AS ctc 
	LEFT JOIN Core.dbo.Areas AS A ON ctc.ContactId = a.ContactId
	LEFT JOIN Core.dbo.LocationAreas AS la ON a.AreaId = la.AreaId
	LEFT JOIN Core.dbo.Locations AS l ON la.LocationId = l.LocationId
--LEFT JOIN [Core].[dbo].[LocationTypeDetailsContacts] AS ltdc ON ctc.ContactId = ltdc.ContactId 
--LEFT JOIN [Core].[dbo].[LocationTypeDetails] AS ltd ON ltdc.LocationTypeDetailsId = ltd.LocationTypeDetailsId 
--LEFT JOIN [Core].[dbo].[LocationTypes] AS lt ON ltd.LocationTypeId = lt.LocationTypeId
WHERE 1 = 1 
AND A.AreaTypeId = 5
AND l.LocationId = pvi.LocationId
ORDER BY ctc.ContactId DESC
		  ) AS stmgr
END


