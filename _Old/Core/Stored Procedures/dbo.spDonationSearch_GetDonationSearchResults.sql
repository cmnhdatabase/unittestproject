﻿


-- "= NULL" Means the parameters is optional
CREATE PROCEDURE [dbo].[spDonationSearch_GetDonationSearchResults]
   @Country         INT = NULL, 
   @CampaignYear    INT = NULL,
   @FundraisingEntityId  INT = NULL,
   @MarketId          INT = NULL,
   @FirstName       NVARCHAR (150) = NULL,
   @LastName        NVARCHAR (150) = NULL,
   @LocationNumber  NVARCHAR (100) = NULL,
   @LocationName    NVARCHAR (255) = NULL,
   @BatchNumber     NVARCHAR (25) = NULL,
   @MinAmount       MONEY = NULL,
   @MaxAmount       MONEY = NULL,
   @Address         NVARCHAR (255) = NULL,
   @City            NVARCHAR (100) = NULL,
   @ProvinceId      INT = NULL,
   @Zip             NVARCHAR (15) = NULL,
   @StartDate       DATETIME = NULL,
   @EndDate         DATETIME = NULL,
   @Reconciled      BIT = NULL
AS
   BEGIN
      SELECT d.fundraisingyear AS 'CampaignYear',
			s.Name AS 'Partner',
             m.MarketName AS 'Market',
             di.FirstName as 'FirstName',
             di.MiddleName as 'MiddleName',
             di.LastName as 'LastName',
             l.LocationNumber as 'LocationNumber',
             di.Amount as 'Amount',
             d.DonationDate as 'DonationDate',
             b.BatchNumber as 'BatchNumber',
             d.Comment as 'Comment',
             l.LocationName AS 'LocationName',
             l.Address1 AS 'Address',
             l.City as 'City',
             p.Abbreviation as 'State',
             l.PostalCode as 'ZipCode',
             cd.CampaignDetailName as 'EventName',
             cd.CampaignDetailName as 'EventSubname',
             d.DateReceived as 'DateReceived'
        FROM vwDisbursements d
             INNER JOIN vwDonorInfoDetails di
                ON (d.DisbursementID = di.DisbursementID)
             LEFT OUTER JOIN vwLocationDetails l
                ON (d.FundraisingEntityId = l.FundraisingEntityId AND d.LocationID = l.LocationID)
             INNER JOIN vwFundraisingEntityDetails s
                ON (d.FundraisingEntityId = s.FundraisingEntityId)
			 INNER JOIN vwCampaignDetails cd
				ON (d.CampaignDetailsId = cd.CampaignDetailsId)
			 INNER JOIN vwCampaigns c
				ON (cd.CampaignId = c.CampaignId)
			 INNER JOIN vwCampaignTypes ct
				ON (c.CampaignTypeId = ct.CampaignTypeId)
			 INNER JOIN vwProvinces p
				ON (l.ProvinceId = p.ProvinceId)
			 INNER JOIN vwCountries co
				ON (l.CountryId = co.CountryId)
			 INNER JOIN vwMarkets m 
				ON (d.MarketId = m.MarketId)
			 INNER JOIN vwDisbursementDates dd
				ON (d.DisbursementDateId = dd.DisbursementDateId)
             INNER JOIN [vwBatches] b
                ON (d.BatchId = b.BatchId)
       WHERE (   (@Country IS NULL)
              OR (co.countryId = @Country)
              OR LTRIM (RTRIM (co.Abbreviation)) = ''
              OR co.Abbreviation IS NULL)
             AND ( (@CampaignYear IS NULL)
                  OR (d.FundraisingYear = @CampaignYear))
             AND ( (@FundraisingEntityId IS NULL) OR (s.FundraisingEntityId = @FundraisingEntityId))
             AND ( (@MarketId IS NULL) OR (d.MarketId = @MarketId))
             AND ( (@FirstName IS NULL)
                  OR (di.FirstName LIKE '%' + @FirstName + '%'))
             AND ( (@LastName IS NULL)
                  OR (di.LastName LIKE '%' + @LastName + '%'))
             AND ( (@LocationNumber IS NULL)
                  OR (l.LocationNumber LIKE '%' + @LocationNumber + '%'))
             AND ( (@LocationName IS NULL)
                  OR (l.LocationName LIKE '%' + @LocationName + '%'))
             AND ( (@BatchNumber IS NULL)
                  OR (b.BatchNumber LIKE '%' + @BatchNumber + '%'))
             AND ( (@MinAmount IS NULL) OR (di.[Amount] >= @MinAmount))
			 AND ( (@MaxAmount IS NULL) OR (di.[Amount] = @MaxAmount))
             AND ( (@Address IS NULL)
                  OR (l.Address1 LIKE '%' + @Address + '%'))
             AND ( (@City IS NULL) OR (l.City LIKE '%' + @City + '%'))
             AND ( (@ProvinceId IS NULL)
                  OR (p.ProvinceId = @ProvinceId))
             AND ( (@Zip IS NULL) OR (l.PostalCode LIKE '%' + @Zip + '%'))
             AND ( (@StartDate IS NULL AND @EndDate IS NULL)
                  OR (d.[DonationDate] BETWEEN @StartDate AND @EndDate))
             AND d.RecordTypeId = 1
             AND ct.CampaignType NOT LIKE '%DIRECT%MAIL%'
             AND ( (@Reconciled IS NULL)
                  OR (b.Reconciled != @Reconciled))
   END








