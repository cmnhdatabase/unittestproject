﻿

CREATE PROCEDURE [dbo].[spInsertPartnerAccountExec]
	@FundraisingEntityId int,
	@userId int,
	@PartnerAccountStaffTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

  	INSERT INTO [dbo].[PartnerAccountExecutives] (
	   [FundraisingEntityId]
      ,[ContactId]
      ,[PartnerAccountStaffTypeId] 
	)
	VALUES (
		@FundraisingEntityId,
		@userId,
		@PartnerAccountStaffTypeId
	)

  SELECT CAST(SCOPE_IDENTITY() AS INT);
END


