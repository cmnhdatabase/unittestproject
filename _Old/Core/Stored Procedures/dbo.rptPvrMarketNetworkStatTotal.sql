﻿





CREATE PROCEDURE [dbo].[rptPvrMarketNetworkStatTotal]
(
@year INT
, @market INT
)
 AS

/****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market	INT = 98
/*****************/

IF OBJECT_ID('tempdb..#tempMarketNetworkStatTotal', 'U') IS NOT NULL
    DROP TABLE #tempMarketNetworkStatTotal

SELECT
(SUM(cy) - SUM(CYMinus1)) / SUM(counts) AS CYIncrease,
((SUM(cy) - SUM(CYMinus1)) / SUM(counts))/ (SUM(CY) / SUM(counts)) AS CYPercentIncrease,
(SUM(CY)/SUM(Counts))/(SUM(Population)/SUM(counts)) AS PerCap,
(SUM(TotalCosts)/SUM(counts))/(SUM(cy)/SUM(counts)) AS CRD,
((SUM(cy) - SUM(TotalCosts))/(SUM(TotalCosts))) AS ROI,
(SUM(CYFTE))/(SUM(counts)) AS CYFET
INTO #tempMarketNetworkStatTotal
FROM (
SELECT 
fd.MarketId AS MarketId,
COUNT(DISTINCT fd.MarketId) AS counts,
SUM(CASE WHEN FundraisingYear = @year -1 THEN Amount ELSE 0 END) AS CYMinus1,
SUM(CASE WHEN FundraisingYear = @year THEN Amount ELSE 0 END) AS CY,
MAX(PopulationEstimate) AS Population
FROM dbo.vwFundraisingData fd
INNER JOIN dbo.vwMarketPopulationRank mp ON mp.MarketId = fd.MarketId
WHERE FundraisingYear BETWEEN @year - 1 AND @year
GROUP BY fd.MarketId
) AS a
INNER JOIN 
(
SELECT MarketId AS ID,
(FixedFees + VariableFees + OtherFees + Overhead) AS TotalCosts,
 FTE AS CYFTE
FROM Core.dbo.PVRFees tpd
) AS b ON b.Id = a.MarketId


SELECT *
FROM #tempMarketNetworkStatTotal
--WHERE MarketId = @market








