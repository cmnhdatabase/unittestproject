﻿



CREATE PROCEDURE [dbo].[rptPvrMarketOnlyOverall]
(
@year INT
, @market INT
)
 AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 232
/*****************/

IF OBJECT_ID('tempdb..#tempMarketOverall', 'U') IS NOT NULL
    DROP TABLE #tempMarketOverall

SELECT  *
INTO    #tempMarketOverall
FROM    ( SELECT    d.MarketId AS TotalID ,
                    d.ShortName AS TotalName ,
                    mpr.PopulationRank AS TotalPopRank ,
                    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                    THEN d.Amount
                               END), 0) AS CyTotal ,
                    RANK() OVER ( PARTITION BY mpr.PopulationRank ORDER BY SUM(Amount) DESC ) AS TotalRank
          FROM      dbo.vwFundraisingData d
                    INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
                    INNER JOIN dbo.vwMarkets m ON m.MarketId = mpr.MarketId
          WHERE     ( d.FundraisingYear >= @year - 1 )
					--AND CountryId = 1
					--AND mpr.PopulationRank = 1
          GROUP BY  PopulationRank ,
                    d.MarketId ,
                    d.ShortName
        ) AS a
        INNER JOIN ( SELECT d.MarketId AS ChangeID ,
                            d.ShortName AS ChangeName ,
                            mpr.PopulationRank AS ChangePopRank ,
                            ( ( SUM(CASE WHEN d.FundraisingYear = @year
                                         THEN d.Amount
                                    END)
                                - SUM(CASE WHEN d.FundraisingYear = @year - 1
                                           THEN d.Amount
                                      END) )
                              / NULLIF(( SUM(CASE WHEN d.FundraisingYear = @year
                                                       - 1 THEN d.Amount
                                             END) ), 0) ) AS ChangePer ,
                            RANK() OVER ( PARTITION BY mpr.PopulationRank ORDER BY ( ( ( SUM(CASE
                                                              WHEN d.FundraisingYear = @year
                                                              THEN d.Amount
                                                              END) )
                                                              - ( SUM(CASE
                                                              WHEN d.FundraisingYear = @year
                                                              - 1
                                                              THEN d.Amount
                                                              END) ) )
                                                              / ( SUM(CASE
                                                              WHEN d.FundraisingYear = @year
                                                              - 1
                                                              THEN d.Amount
                                                              END) ) ) DESC ) AS ChangeRank
                     FROM   dbo.vwFundraisingData d
                            INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
                            INNER JOIN dbo.vwMarkets m ON m.MarketId = mpr.MarketId
                     WHERE  ( d.FundraisingYear >= @year - 1 )
					--AND CountryId = 1
                     GROUP BY PopulationRank ,
                            d.MarketId ,
                            d.ShortName
                   ) AS b ON b.ChangePopRank = a.TotalPopRank
        INNER JOIN ( SELECT d.MarketId AS PerCapID ,
                            d.ShortName AS PerCapName ,
                            mpr.PopulationRank AS PerCapPopRank ,
                            SUM(CASE WHEN d.FundraisingYear = @year
                                     THEN d.Amount
                                END) / NULLIF(mpr.PopulationEstimate, 0) AS PerCap ,
                            RANK() OVER ( PARTITION BY mpr.PopulationRank ORDER BY ( SUM(CASE
                                                              WHEN d.FundraisingYear = @year
                                                              THEN d.Amount
                                                              END)
                                                              / NULLIF(mpr.PopulationEstimate,
                                                              0) ) DESC ) AS PerCapRank
                     FROM   dbo.vwFundraisingData d
                            INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
                            INNER JOIN dbo.vwMarkets m ON m.MarketId = mpr.MarketId
                     WHERE  ( d.FundraisingYear = @year )
					--AND CountryId = 1
                     GROUP BY PopulationRank ,
                            d.MarketId ,
                            PopulationEstimate ,
                            d.ShortName
                   ) AS c ON c.PerCapPopRank = a.TotalPopRank
WHERE   TotalRank = 1
        AND ChangeRank = 1
        AND PerCapRank = 1
        OR ( a.TotalID = @market
             AND b.ChangeID = @market
             AND c.PerCapID = @market
           )

SELECT  TotalID ,
        TotalPopRank ,
        TotalName AS TotalName ,
        CyTotal ,
        ChangeID ,
        ChangeName AS ChangeName ,
        ChangePer ,
        PerCapID ,
        PerCapName AS PerCapName ,
        PerCap
FROM    #tempMarketOverall d





