﻿







CREATE PROCEDURE [dbo].[rptPartnerReport_CMNRegion]
(
	@prtnr INT 
)
AS

BEGIN
	/***********	TEST SECTION	***********/
--DECLARE
--@prtnr INT = 123


	/*********** END TEST SECTION	***********/

SELECT DISTINCT r.RegionId, r.RegionName
FROM            Locations AS l INNER JOIN
                         PostalCodes AS pc ON pc.PostalCode = l.PostalCode INNER JOIN
                         Markets AS m ON m.MarketId = pc.MarketId INNER JOIN
                         Regions AS r ON r.RegionId = m.RegionId
WHERE        (l.FundraisingEntityID = @prtnr)
END








