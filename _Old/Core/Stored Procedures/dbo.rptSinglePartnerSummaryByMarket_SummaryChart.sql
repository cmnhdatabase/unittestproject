﻿
CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryByMarket_SummaryChart]
(
@year INT,
@partnerid NVARCHAR(MAX),
@marketid NVARCHAR(MAX)
)
AS
BEGIN
SELECT FundraisingYear,
       CASE
           WHEN DirectToHospital = 0
           THEN 'Thru CMN Hospitals'
           ELSE 'Direct To Market'
       END AS DirectToHospital,
       SUM(Amount) AS Amount
FROM vwFundraisingData vfd
WHERE( vfd.FundraisingCategoryId = 1 )
 AND ( FundraisingYear >= @year - 3 )
 AND ( vfd.FundraisingEntityId   = @partnerid )
 AND MarketId IN( 
                  SELECT Item
                  FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
GROUP BY FundraisingYear,
         DirectToHospital;
END
