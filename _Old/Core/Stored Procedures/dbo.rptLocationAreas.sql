﻿






-- =============================================
-- Author: Travis Poll
-- Create date: 12/3/2014
-- Description:	Procedure for getting the location area types and areas
-- =============================================
CREATE PROCEDURE [dbo].[rptLocationAreas] 

AS
BEGIN
/***************** Test Section *****************/



/***************** End Test Section ***************/
	SET NOCOUNT ON;


CREATE TABLE #temptable
(
    LocationId int,
    Value varchar(250),
    Category VARCHAR(250)
)

insert into #temptable
SELECT l.LocationId, a.Name, at.AreaType
FROM dbo.Locations l 
LEFT OUTER JOIN  dbo.LocationAreas la ON l.LocationId = la.LocationId
LEFT OUTER JOIN  dbo.Areas a ON la.AreaId = a.AreaId 
LEFT OUTER JOIN  dbo.AreaTypes at ON a.AreaTypeId = at.AreaTypeId

DECLARE @cols AS NVARCHAR(MAX),
    @query  AS NVARCHAR(MAX);

SET @cols = STUFF((SELECT distinct ',' + QUOTENAME(c.category) 
            FROM #temptable c
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query = '
select * from (
SELECT LocationId, ' + @cols + ' from 
            (
                select LocationId
                    , value
                    , category
                from #temptable
           ) x
            pivot 
            (
                 max(value)
                for category in (' + @cols + ')
            ) p) as t'


EXECUTE(@query)
DROP TABLE #temptable

END







