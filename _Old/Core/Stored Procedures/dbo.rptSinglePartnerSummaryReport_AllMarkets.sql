﻿








CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryReport_AllMarkets]
(	@year     INT 
  , @partnerid INT 
)
AS


BEGIN
	/***********	TEST SECTION	***********/
		--DECLARE @partnerid INT = 71, 
		--		@year      INT = 2015


		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/

	IF OBJECT_ID('tempdb..#tempMarketInfo', 'U') IS NOT NULL DROP TABLE #tempMarketInfo

	
	DECLARE @prmYear	  INT = @year,
			@prmPartnerID INT = @partnerid

SELECT f.marketid,
	   f.MarketName, 
       SUM(CASE 
             WHEN f.fundraisingyear = @year-3 THEN f.amount 
           END)              AS 'CYminus3', 
       SUM(CASE 
             WHEN f.fundraisingyear = @year-2 THEN f.amount 
           END)              AS 'CYminus2', 
       SUM(CASE 
             WHEN f.fundraisingyear = @year-1 THEN f.amount 
           END)              AS 'CYminus1', 
       SUM(CASE 
             WHEN f.fundraisingyear = @year THEN f.amount 
           END)              AS 'CY', 
       d.loccount AS 'loccount 2013', 
       SUM(CASE 
             WHEN f.fundraisingyear = @year-1 THEN f.amount 
           END) / NULLIF (d.loccount, 0) AS 'Average 2013',
		   d2.loccount2 AS 'loccount 2014',
		SUM(CASE
			 WHEN f.fundraisingyear = @year THEN f.amount
			 END) / NULLIF (d2.loccount2, 0) AS 'Average 2014',
			 r.RegionName
FROM   vwfundraisingdata f
INNER JOIN dbo.Regions r ON r.RegionId = f.RegionId
LEFT OUTER JOIN (SELECT marketid, 
                  COUNT(DISTINCT locationid) loccount 
                   FROM   disbursements 
                   WHERE  fundraisingyear = @year - 1
                          AND FundraisingEntityId = @partnerid
                   GROUP  BY marketid) d 
               ON f.marketid = d.marketid
LEFT OUTER JOIN (SELECT m.MarketId, CONVERT(INT, SUM(cnt)) AS loccount2
	FROM (
	SELECT l.PostalCode, COUNT(DISTINCT LocationId) cnt
	FROM dbo.Locations l
	WHERE FundraisingEntityId = @partnerid
		AND l.Active = 1
		AND CAST(LocationId AS VARCHAR(100)) NOT IN (SELECT splitfrom FROM dbo.Splits)
	GROUP BY l.PostalCode
UNION ALL
SELECT PostalCode, SUM(SplitPercent)
FROM dbo.Splits
WHERE FundraisingEntityId = @partnerid
GROUP BY PostalCode
) AS t 
INNER JOIN dbo.PostalCodes m ON t.PostalCode = m.PostalCode 
GROUP BY MarketId) AS d2
ON d2.MarketId = f.MarketId

		/*LEFT OUTER JOIN (SELECT marketid, 
                          COUNT(DISTINCT locationid) loccount2 
                   FROM   disbursements 
                   WHERE  fundraisingyear = @year 
                          AND partnerid = @partnerid 
                   GROUP  BY marketid) d2 
               ON f.marketid = d2.marketid */
WHERE  fundraisingyear >= @year - 3 
       AND FundraisingEntityId = @partnerid 
	   AND f.FundraisingCategoryId = 1
GROUP  BY f.marketid,
		  f.MarketName, 
          d.loccount,
		  d2.loccount2,
		  r.RegionName 
ORDER BY CYminus1 DESC


END









