﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new DisbursementPeriod
-- =============================================
Create PROCEDURE [dbo].[spInsertDisbursementPeriod]
@DisbursementYear int,
	          @DisbursementQuarter int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.DisbursementPeriods
	        ( DisbursementYear ,
	          DisbursementQuarter
	        )
	VALUES  ( @DisbursementYear ,
	          @DisbursementQuarter
	        )
END


