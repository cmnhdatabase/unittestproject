﻿




/*
	===========================================================================================
	  Author:      John Sanford
	  Create date: 20130605
	  Description: Get the comparable markets for the passed market, by Region/Country or Overall
	===========================================================================================
--*/
CREATE PROCEDURE [dbo].[spGetComparableMarketsByPopulation] 
(	@MarketID	INT 
  , @Year		INT
  , @Coverage	NVARCHAR(10) = NULL 
)
AS

BEGIN
	SET NOCOUNT ON;

	/***********	TEST SECTION	***********/

		--DECLARE @MarketID	INT = 9	--91	--	10	--179--
		--DECLARE @Year		INT = 2012
		--DECLARE @Coverage	NVARCHAR(10) = 'Overall'	--	NULL	--'Country'	--	'Region'	--

		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/

	/* Parameter sniffin'	*/
	DECLARE @prmMarketID	INT = @MarketID
	DECLARE @prmCoverage	NVARCHAR(10) = ISNULL(@Coverage, 'Country')

	DECLARE @prmYear		INT
    SET @prmYear = ISNULL(@Year, (SELECT CASE WHEN GETDATE() >= reportchangedate THEN YEAR(reportchangedate) ELSE YEAR(reportchangedate)-1 END AS yr 
									FROM Core.dbo.ReportSettings) -1 )
--select @prmYear as YearParam

	DECLARE @CountryID	INT
		  , @A			INT
		  , @B			INT
		  , @RegionId	INT

	DECLARE @TopBound INT 
	DECLARE @BottomBound INT
	
	SELECT @CountryID	= mkt.CountryId
		 , @RegionId	= mkt.RegionId
	  FROM Core.dbo.Markets AS mkt 
	  WHERE mkt.MarketId = @prmMarketID
	
	DECLARE @MarketPops TABLE
	(	MarketID			INT
	  , MarketName			NVARCHAR(100)
	  , PopulationEstimate	BIGINT
	  , CampaignYear		INT
	  , RegionPopRank		INT
	  , CountryPopRank		INT
	  , OverallPopRank		INT
	  , CountryID			INT
	  , CountryName			NVARCHAR(50)
	  , RegionID			INT
	)
	INSERT INTO @MarketPops
	SELECT mkt.MarketId
		 , mkt.MarketName
		 , pbm.PopulationEstimate
		 , pbm.CampaignYear
		 , ROW_NUMBER() OVER (PARTITION BY mkt.RegionId ORDER BY pbm.populationestimate DESC) AS RegionPopRank
		 , ROW_NUMBER() OVER (PARTITION BY mkt.CountryId ORDER BY pbm.populationestimate DESC) AS CountryPopRank
		 , ROW_NUMBER() OVER (ORDER BY pbm.populationestimate DESC) AS OverallPopRank
		 , co.CountryId
		 , co.CountryName
		 , mkt.RegionId
	  FROM Core.dbo.vwPopulationByMarket AS pbm
		INNER JOIN Core.dbo.vwMarkets AS mkt ON mkt.MarketId = pbm.MarketId
		INNER JOIN Core.dbo.vwCountries AS co ON co.CountryId = pbm.CountryId
	  WHERE campaignyear = @prmYear

	DECLARE @RegionMaxRank		INT = (SELECT MAX(RegionPopRank) AS MaxRank FROM @MarketPops AS mpr WHERE RegionID = @RegionId)
	DECLARE @CountryMaxRank		INT = (SELECT MAX(CountryPopRank) AS MaxRank FROM @MarketPops AS mpr WHERE CountryID = @CountryID)
	DECLARE @OverallMaxRank		INT = (SELECT MAX(OverallPopRank) AS MaxRank FROM @MarketPops AS mpr)
	
	DECLARE @RelevantMarket TABLE
	(	MarketID			INT
	  , RegionPopRank		INT
	  , CountryPopRank		INT
	  , OverallPopRank		INT
	  , CountryID			INT
	  , RegionID			INT
	  , RegionTopBound		INT
	  , RegionBottomBound	INT
	  , CountryTopBound		INT
	  , CountryBottomBound	INT
	  , OverallTopBound		INT
	  , OverallBottomBound	INT
	)
	INSERT INTO @RelevantMarket
	SELECT mpr.MarketID
		 , mpr.RegionPopRank
		 , mpr.CountryPopRank
		 , mpr.OverallPopRank
		 , mpr.CountryId
		 , mpr.RegionID
		 , (CASE WHEN mpr.RegionPopRank < 4 THEN 1
				 WHEN mpr.RegionPopRank > @RegionMaxRank - 3 THEN (mpr.RegionPopRank - 6) + (@RegionMaxRank - mpr.RegionPopRank)
				 ELSE mpr.RegionPopRank -3
			END)							AS RegionTopBound
		 , (CASE WHEN mpr.RegionPopRank < 4 THEN mpr.RegionPopRank + 3 + (4 - mpr.RegionPopRank)
				 WHEN mpr.RegionPopRank > @RegionMaxRank - 3 THEN @RegionMaxRank
				 ELSE mpr.RegionPopRank + 3 
			END)							AS RegionBottomBound
		 , (CASE WHEN mpr.CountryPopRank < 4 THEN 1
				 WHEN mpr.CountryPopRank > @CountryMaxRank - 3 THEN (mpr.CountryPopRank - 6) + (@CountryMaxRank - mpr.CountryPopRank)
				 ELSE mpr.CountryPopRank -3
			END)							AS CountryTopBound
		 , (CASE WHEN mpr.CountryPopRank < 4 THEN mpr.CountryPopRank + 3 + (4 - mpr.CountryPopRank)
				 WHEN mpr.CountryPopRank > @CountryMaxRank - 3 THEN @CountryMaxRank
				 ELSE mpr.CountryPopRank + 3 
			END)							AS CountryBottomBound
		 , (CASE WHEN mpr.OverallPopRank < 4 THEN 1
				 WHEN mpr.OverallPopRank > @OverallMaxRank - 3 THEN (mpr.OverallPopRank - 6) + (@OverallMaxRank - mpr.OverallPopRank)
				 ELSE mpr.OverallPopRank -3
			END)							AS OverallTopBound
		 , (CASE WHEN mpr.OverallPopRank < 4 THEN mpr.OverallPopRank + 3 + (4 - mpr.OverallPopRank)
				 WHEN mpr.OverallPopRank > @OverallMaxRank - 3 THEN @OverallMaxRank
				 ELSE mpr.OverallPopRank + 3 
			END)							AS OverallBottomBound
	  FROM @MarketPops AS mpr
	  WHERE MarketID = @prmMarketID
	  GROUP BY mpr.MarketId
			 , mpr.RegionPopRank
			 , mpr.CountryPopRank
			 , mpr.OverallPopRank
			 , mpr.CountryId
			 , mpr.RegionID

	SELECT @TopBound = (CASE @prmCoverage 
							WHEN 'Country' THEN CountryTopBound 
							WHEN 'Region' THEN RegionTopBound
							ELSE OverallTopBound
						END)
		 , @BottomBound = (CASE @prmCoverage 
								WHEN 'Country' THEN CountryBottomBound 
								WHEN 'Region' THEN RegionBottomBound
								ELSE OverallBottomBound
							END )
	  FROM @RelevantMarket 
	  WHERE MarketID = @prmMarketID

	SELECT (CASE WHEN @prmCoverage = 'Country' THEN CountryPopRank
				 WHEN @prmCoverage = 'Region' THEN RegionPopRank
				 ELSE OverallPopRank
			END)	AS rownumber
		 , MarketName
		 , MarketID
		 , PopulationEstimate
		 , CountryName AS MarketCountry
		 , RegionID
		  --, RegionPopRank
		  --, CountryPopRank
		  --, OverallPopRank
		  --, CountryId
	  FROM @MarketPops AS mpr
	  WHERE 1 = (CASE WHEN @prmCoverage = 'Country' AND (mpr.CountryPopRank BETWEEN @TopBound AND @BottomBound) AND (mpr.CountryID = @CountryID) THEN 1
					  WHEN @prmCoverage = 'Region' AND (mpr.RegionPopRank BETWEEN @TopBound AND @BottomBound) AND (mpr.RegionID = @RegionId) THEN 1
					  WHEN @prmCoverage = 'Overall' AND (mpr.OverallPopRank BETWEEN @TopBound AND @BottomBound) THEN 1
					  ELSE 0
				 END)
END



