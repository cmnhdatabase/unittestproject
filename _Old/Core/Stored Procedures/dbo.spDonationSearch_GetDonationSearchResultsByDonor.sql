﻿




CREATE PROCEDURE [dbo].[spDonationSearch_GetDonationSearchResultsByDonor]
  -- "= NULL" Means the parameters is optional
   @Country INT = NULL,                             
   @CampaignYear    INT = NULL,
   @FundraisingEntityId       INT = NULL,
   @MarketId          INT = NULL,
   @FirstName       NVARCHAR (150) = NULL,
   @LastName        NVARCHAR (150) = NULL,
   @BatchNumber     NVARCHAR (25) = NULL,
   @MinAmount       MONEY = NULL,
   @MaxAmount       MONEY = NULL,
   @Address         NVARCHAR (255) = NULL,
   @City            NVARCHAR (100) = NULL,
   @ProvinceId      INT = NULL,
   @Zip             NVARCHAR (15) = NULL,
   @StartDate       DATETIME = NULL,
   @EndDate         DATETIME = NULL,
   @Reconciled      BIT = NULL
AS
   BEGIN
      SELECT d.fundraisingyear as 'CampaignYear',
			 pp.Name AS 'SponsorName',
             m.MarketName AS 'Market',
             di.FirstName as 'FirstName',
             di.MiddleName as 'MiddleName',
             di.LastName as 'LastName',
             di.Amount as 'Amount',
             d.DonationDate as 'DonationDate',
             b.BatchNumber as 'BatchNumber',
             d.Comment as 'Comment',
             di.Address1 AS 'Address',
             di.City AS 'City',
             p.Abbreviation AS 'State',
             di.PostalCode AS 'ZipCode',
             cd.CampaignDetailName as 'EventName',
             cd.CampaignDetailName as 'EventSubname',
             d.DateReceived as 'DateReceived',
			 b.Reconciled as 'Reconciled',
			 di.TransactionID as 'TransactionId'
        FROM    vwDisbursements d
             INNER JOIN vwDonorInfoDetails di
                ON (d.DisbursementID = di.DisbursementID)
             INNER JOIN vwFundraisingEntityDetails pp
                ON (d.FundraisingEntityId = pp.FundraisingEntityId)
                     INNER JOIN vwMarkets m
                           ON (d.MarketId = m.MarketId)
                     INNER JOIN vwCampaignDetails cd
                           ON (d.CampaignDetailsId = cd.CampaignDetailsId)
                     INNER JOIN vwCampaigns c
                           ON (cd.CampaignId = c.CampaignId)
                     INNER JOIN vwCampaignTypes ct
                           ON (c.CampaignTypeId = ct.CampaignTypeId)
                     Left JOIN vwLocationDetails l
                           ON (l.LocationId = d.LocationId)
                     Left JOIN vwProvinces p
                           ON (di.ProvinceId = p.ProvinceId)
                     INNER JOIN vwDisbursementDates dd
                           ON (d.DisbursementDateId = dd.DisbursementDateId)
            INNER JOIN [vwBatches] b
            ON (d.BatchId = b.BatchId)
       WHERE ( (@CampaignYear IS NULL) OR (d.FundraisingYear = @CampaignYear))
             AND ( (@FundraisingEntityId IS NULL) OR (pp.FundraisingEntityId = @FundraisingEntityId))
             AND ( (@MarketId IS NULL) OR (m.MarketId = @MarketId))
			 AND ( (@Country IS NULL) OR (di.countryId = @Country))
             AND ( (@FirstName IS NULL)
                  OR (di.FirstName LIKE '%' + @FirstName + '%'))
             AND ( (@LastName IS NULL)
                  OR (di.LastName LIKE '%' + @LastName + '%'))
             AND ( (@BatchNumber IS NULL)
                  OR (b.BatchNumber LIKE '%' + @BatchNumber + '%'))
             AND ( (@MinAmount IS NULL) OR (di.[Amount] >= @MinAmount))
			 AND ( (@MaxAmount IS NULL) OR (di.[Amount] <= @MaxAmount))
             AND ( (@Address IS NULL)
                  OR (di.Address1 LIKE '%' + @Address + '%'))
             AND ( (@City IS NULL) OR (di.City LIKE '%' + @City + '%'))
             AND ( (@ProvinceId IS NULL)
                  OR (p.ProvinceId = @ProvinceId))
             AND ( (@Zip IS NULL) OR (di.PostalCode LIKE '%' + @Zip + '%'))
             AND ( (@StartDate IS NULL AND @EndDate IS NULL)
                  OR (d.[DonationDate] BETWEEN @StartDate AND @EndDate))
             AND d.RecordTypeId = 1
             AND ct.CampaignType NOT LIKE '%DIRECT%MAIL%'
             AND b.Reconciled = 1
             AND ( (@Reconciled IS NULL AND b.Reconciled = 1)
                  OR (b.Reconciled != @Reconciled))
             AND LTRIM (RTRIM (di.FirstName)) <> ''
      AND LTRIM(RTRIM(di.Address1)) <> ''
END










