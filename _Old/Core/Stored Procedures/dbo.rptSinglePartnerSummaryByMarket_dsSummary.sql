﻿
CREATE PROCEDURE	[dbo].[rptSinglePartnerSummaryByMarket_dsSummary]
(
@year INT,
@partnerid nvarchar(MAX),
@marketid nvarchar(MAX)
)
AS
BEGIN
	SELECT CASE
           WHEN DirectToHospital = 0
           THEN 'Thru CMN Hospitals'
           ELSE 'Direct to Market'
       END AS Cateogry,
       SUM(CASE
               WHEN FundraisingYear = @year - 3
               THEN Amount
           END) AS cyminus3,
       SUM(CASE
               WHEN FundraisingYear = @year - 2
               THEN Amount
           END) AS cyminus2,
       SUM(CASE
               WHEN FundraisingYear = @year - 1
               THEN Amount
           END) AS cyminus1,
       SUM(CASE
               WHEN FundraisingYear = @year
               THEN Amount
           END) AS cy
FROM vwFundraisingData
WHERE( FundraisingCategoryId = 1 )
 AND ( FundraisingYear >= @year - 3 )
 AND ( FundraisingEntityId = @partnerid )
 AND MarketId IN( 
                  SELECT Item
                  FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
GROUP BY DirectToHospital;
END
