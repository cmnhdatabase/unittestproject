﻿
-- =============================================
-- Author			: Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to add a new Telethon record.
-- Updates    : 08/12/2015 - Remove HospitalContactId, TelethonContactId
--            : 06/21/2016 - Add ToteBoardImagePath
--
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateTelethon]
	@TelethonId int,
	@CampaignDetailsId int,
	@City varchar(100),
	@OnAirHours smallint,
	@OnAirMinutes smallint,
	@LanguageId int,
	@PhoneBank varchar(100),
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit,
	@EventId int,
	@ToteBoardImagePath varchar(500)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Telethons] SET
		[CampaignDetailsId] = @CampaignDetailsId,
		[City] = @City,
		[OnAirHours] = @OnAirHours,
		[OnAirMinutes] = @OnAirMinutes,
		[LanguageId] = @LanguageId,
		[PhoneBank] = @PhoneBank,
		[Note] = @Note,
		[ModifiedBy] = @ModifiedBy,
		[ModifiedDate] = GETDATE(),
		[Active] = @Active,
		[EventId] = @EventId,
		[ToteBoardImagePath] = @ToteBoardImagePath
	WHERE [TelethonId] = @TelethonId;
END
