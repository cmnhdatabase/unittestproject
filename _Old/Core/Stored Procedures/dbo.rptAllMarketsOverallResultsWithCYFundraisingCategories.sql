﻿

CREATE PROCEDURE [dbo].[rptAllMarketsOverallResultsWithCYFundraisingCategories]
  (	@Year		INT,
	@RegionID	INT  
  )
AS

BEGIN
	SET NOCOUNT ON;

	/***********	TEST SECTION	***********/

		----IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	
		--DECLARE @Year		INT = NULL	-- 2012	--YEAR(GETDATE())  -- 
		--DECLARE @RegionID	INT = 6 -- -1	--NULL	--	6 --	/* Value of -1 means ALL Regions */

	/*********** END TEST SECTION	***********/

	DECLARE @prmYear		INT = (CASE WHEN @Year IS NULL THEN YEAR(GETDATE()) ELSE @Year END)
	DECLARE @prmRegionID	INT = @RegionID	

	IF OBJECT_ID('tempdb..#RolledByYear', 'U') IS NOT NULL DROP TABLE #RolledByYear
	
	/*  This temp table is used in the Stored Procedure for speed.  To create a view from this query, use the CTE instead.  The view is MUCH slower. */
	SELECT vfd.FundraisingYear
		 , vfd.MarketId
		 , vfd.Marketname
		 , (CASE WHEN vfd.FundraisingYear = @prmYear THEN 'CY'		-- CurrentYear
				 ELSE 'CYMinus' + CAST(@prmYear - vfd.FundraisingYear AS NVARCHAR(2))
			END)											AS RelativeYear
		 , vfd.FundraisingCategory
		 , vfd.FundraisingCategoryId
		 , SUM(vfd.Amount)									AS FundraisingAmount
		 , mkt.RegionId
	  INTO #RolledByYear
	  FROM [dbo].[vwFundraisingData] AS vfd
		INNER JOIN [dbo].[Markets] AS mkt ON vfd.MarketID = mkt.MarketId
	  WHERE vfd.FundraisingYear >= @prmYear - 3 AND vfd.FundraisingYear <= @prmYear
	    AND (@prmRegionID = -1 OR @prmRegionID = mkt.RegionId)
	  GROUP BY vfd.FundraisingYear
			 , vfd.FundraisingCategory
			 , vfd.FundraisingCategoryId
			 , vfd.MarketId
			 , vfd.Marketname
			 , mkt.RegionId

	/*	This CTE will return ALL regions and is based on the current year, i.e. YEAR(GETDATE()), only.  */
	--WITH cteCY AS
	--  (
	--	SELECT vfd.FundraisingYear
	--		 , vfd.MarketId
	--		 , vfd.Marketname
	--		 , (CASE WHEN vfd.FundraisingYear = YEAR(GETDATE()) THEN 'CY'		-- CurrentYear
	--				 ELSE 'CYMinus' + CAST(YEAR(GETDATE()) - vfd.FundraisingYear AS NVARCHAR(2))
	--			END)											AS RelativeYear
	--		 , vfd.FundraisingCategory
	--		 , vfd.FundraisingCategoryId
	--		 , SUM(vfd.Amount) AS FundraisingAmount
	--		 , mkt.RegionId
	--	  FROM [dbo].[vwFundraisingData] AS vfd
		--INNER JOIN [dbo].[Markets] AS mkt ON vfd.MarketID = mkt.MarketId
			--	  WHERE vfd.FundraisingYear >= YEAR(GETDATE()) - 3 AND vfd.FundraisingYear <= YEAR(GETDATE())
	--	  GROUP BY vfd.FundraisingYear
	--			 , vfd.FundraisingCategory
	--			 , vfd.FundraisingCategoryId
	--			 , vfd.MarketId
	--			 , vfd.Marketname
	--			 , mkt.RegionId
	--  )
	SELECT ccy.MarketId
		 , ccy.Marketname
		 , ccy.RegionId
		 , sq1.CY
		 , sq1.CYMinus1
		 , sq1.CYMinus2
		 , sq1.CYMinus3
		 , sq1.pctChangeY2toY1
		 , sq1.pctChangeY3toY2
		 , cats.[National Corporate Partners]
 		 , cats.[National Programs and Events]
		 , cats.[Local]
		 , cats.[Overlap]
	  FROM 
	  (	SELECT DISTINCT 
			   MarketID
			 , MarketName 
			 , RegionId
		  FROM #RolledByYear -- cteCY
	  ) AS ccy
		LEFT JOIN 
		  (	SELECT MarketId
				  , CYminus1
				  , CYminus2
				  , CYminus3
				  , (CASE WHEN CYminus3 = 0 THEN 0
						  ELSE ((CYMinus2 - CYMinus3) / CYMinus3) * 100
					 END)						AS pctChangeY3toY2
				  , (CASE WHEN CYminus2 = 0 THEN 0
						  ELSE ((CYMinus1 - CYMinus2) / CYMinus2) * 100
					 END)						AS pctChangeY2toY1
				  , CY
			  FROM 
			  (	SELECT MarketId
					 , Marketname
					 , FundraisingAmount
					 , RelativeYear
				  FROM #RolledByYear --cteCY
			  ) AS ank
			  PIVOT
			  (	SUM(FundraisingAmount) FOR RelativeYear IN ([CY],[CYMinus1],[CYMinus2],[CYMinus3])) AS pv
			) sq1 ON ccy.MarketId = sq1.MarketId
			LEFT JOIN
			  (	SELECT * FROM
				  (	SELECT MarketId
						 , FundraisingCategory
						 , FundraisingAmount
					  FROM #RolledByYear --cteCY
					  WHERE RelativeYear = 'CY'
				  ) AS ank2
				  PIVOT (SUM(FundraisingAmount) FOR FundraisingCategory IN ([National Corporate Partners],[National Programs and Events],[Local],[Overlap])) AS pv
			  ) AS cats ON ccy.MarketId = cats.MarketId

END




