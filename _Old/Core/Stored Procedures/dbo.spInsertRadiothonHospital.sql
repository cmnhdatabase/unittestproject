﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/11/2015
-- Description:	This procedure is used to add a new RadiothonHospital record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertRadiothonHospital]
	@RadiothonId int,
	@HospitalId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[RadiothonHospitals] (
		[RadiothonId],
		[HospitalId]
	)
	VALUES (
		@RadiothonId,
		@HospitalId
	)
END
