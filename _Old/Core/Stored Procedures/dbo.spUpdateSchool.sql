﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/20/2015
-- Description:	This procedure is used to update an existing School record.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateSchool]
	@SchoolId int,
	@HospitalId int,
	@SchoolName varchar(255),
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId int,
	@PostalCode varchar(10),
	@CountryId int,
	@InauguralEvent int,
	@SchoolTypeId int,
	@Active bit
AS
BEGIN
	UPDATE [dbo].[Schools] SET
		[HospitalId] = @HospitalId,
		[SchoolName] = @SchoolName,
		[Address1] = @Address1,
		[Address2] = @Address2,
		[City] = @City,
		[ProvinceId] = @ProvinceId,
		[PostalCode] = @PostalCode,
		[CountryId] = @CountryId,
		[InauguralEvent] = @InauguralEvent,
		[SchoolTypeId] = @SchoolTypeId,
		[Active] = @Active
	WHERE [SchoolId] = @SchoolId;
END

