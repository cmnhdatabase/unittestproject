﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to delete an existing TelethonHospital record.
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteTelethonHospital]
	@TelethonId int,
	@HospitalId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM [dbo].[TelethonHospitals] 
	WHERE [TelethonId] = @TelethonId AND [HospitalId] = @HospitalId;
END
