﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new FUEHLLog
-- =============================================
Create PROCEDURE [dbo].[spInsertFUEHLLog]
@LogEntry VARCHAR(max), @LogDate DATETIME, @TestLog BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.FUEHLLog
	        ( LogEntry, LogDate, TestLog )
	VALUES  ( @LogEntry, @LogDate, @TestLog )
END


