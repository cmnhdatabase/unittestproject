﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This will set the specified disbursements disbursementdateid field.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateDisbursementDateId]
	@DisbursementId INT,
	@DisbursementDateId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Disbursements
	SET DisbursementDateId = @DisbursementDateId
	WHERE DisbursementId = @DisbursementId
END


