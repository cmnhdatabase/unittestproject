﻿

-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/10/2015
-- Description:	This procedure is used to add a new RadioStation record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertRadioStation]
	@CallLetters varchar(10),
	@MarketId int,
	@Owner varchar(100),
	@Frequency varchar(10),
	@MarketTag varchar(50),
	@MarketArea varchar(50),
	@YearStarted smallint,
	@Director int,
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId int,
	@PostalCode varchar(10),
	@CountryId int,
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[RadioStations] (
		[CallLetters],
		[MarketId],
		[Owner],
		[Frequency],
		[MarketTag],
		[MarketArea],
		[YearStarted],
		[Director],
		[Address1],
		[Address2],
		[City],
		[ProvinceId],
		[PostalCode],
		[CountryId],
		[Note],
		[ModifiedBy],
		[ModifiedDate],
		[Active]
	)
	VALUES (
		@CallLetters,
		@MarketId,
		@Owner,
		@Frequency,
		@MarketTag,
		@MarketArea,
		@YearStarted,
		@Director,
		@Address1,
		@Address2,
		@City,
		@ProvinceId,
		@PostalCode,
		@CountryId,
		@Note,
		@ModifiedBy,
		GETDATE(),
		@Active
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END

