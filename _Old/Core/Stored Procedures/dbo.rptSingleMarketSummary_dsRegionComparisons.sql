﻿


CREATE PROCEDURE [dbo].[rptSingleMarketSummary_dsRegionComparisons]
  (	  @Year		INT
	, @MarketID	INT	
  )

AS

BEGIN
	SET NOCOUNT ON;

	/***********	TEST SECTION	***********/
		--DECLARE @Year		INT = 2014
		--DECLARE @MarketID	INT = 149	--	10  

		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/

	DECLARE @prmYear		INT = @Year
	DECLARE @prmMarketID	INT = @MarketID
	DECLARE @RegionID INT = (SELECT RegionID FROM Core.[dbo].[Markets] WHERE MarketID = @prmMarketID)

	DECLARE @MarketPops TABLE
	(	MarketID			INT
	  , MarketName			NVARCHAR(100)
	  , PopulationEstimate	BIGINT
	  , RegionPopRank		INT
	  , CountryPopRank		INT
	  , OverallPopRank		INT
	  , CountryID			INT
	  , Country				NVARCHAR(2)
	  , RegionID		INT
	)
	INSERT INTO @MarketPops
	SELECT mkts.MarketId
		 , mkts.MarketName
		 , mkts.PopulationEstimate
		 , ROW_NUMBER() OVER ( PARTITION BY mkts.RegionID ORDER BY mkts.PopulationEstimate DESC ) AS RegionPopRank 
		 , ROW_NUMBER() OVER ( PARTITION BY mkts.CountryID ORDER BY mkts.PopulationEstimate DESC ) AS CountryPopRank 
		 , ROW_NUMBER() OVER ( ORDER BY mkts.PopulationEstimate DESC ) AS OverallPopRank 
		 , co.CountryID
		 , co.Abbreviation AS Country
		 , mkts.RegionId
	  FROM Core.[dbo].[Countries] AS co
		CROSS APPLY
		  (
			SELECT m.MarketId
					, m.MarketName
					, m.CountryId
					, m.RegionID
					, SUM(CONVERT(BIGINT, pc.PopulationEstimate)) AS PopulationEstimate
					, CASE WHEN SUM(CONVERT(BIGINT, pc.PopulationEstimate)) < 1000000 THEN 1 
						WHEN SUM(CONVERT(BIGINT, pc.PopulationEstimate)) < 3000000 THEN 2 
						ELSE 3 
					END AS PopulationRank
			  FROM Core.[dbo].[Markets] AS m 
				INNER JOIN Core.[dbo].[PostalCodes] AS pc ON pc.MarketId = m.MarketId 
			  WHERE m.RegionId = @RegionID
				AND m.CountryId = co.CountryId AND pc.CountryId = 1
			  GROUP BY m.MarketName
					 , m.MarketId
					 , m.CountryId
					 , m.RegionID
		  UNION
			SELECT mkt.MarketID
				 , mkt.MarketName
				 , mkt.CountryId
				 , mkt.RegionID
				 , SUM(CONVERT(BIGINT, pc.PopulationEstimate)) AS PopulationEstimate
				 , CASE WHEN SUM(CONVERT(BIGINT, pc.PopulationEstimate)) < 1000000 THEN 1 
						WHEN SUM(CONVERT(BIGINT, pc.PopulationEstimate)) < 3000000 THEN 2 
						ELSE 3 
					END AS PopulationRank
			  FROM Core.[dbo].[Markets] AS mkt 
				INNER JOIN 
				  (
					SELECT DISTINCT
						   LEFT([PostalCode],3) AS 'PostalCodeFirst3'
						 , [MarketId]
						 , [CountryId]
						 , [PopulationEstimate]
					  FROM Core.[dbo].[PostalCodes]
					  WHERE countryID = 2
				  ) AS pc ON mkt.MarketId = pc.MarketId AND mkt.CountryId = pc.CountryId AND mkt.CountryId = co.CountryId
				WHERE mkt.RegionId = @RegionID
				GROUP BY mkt.MarketId
						, mkt.MarketName
						, mkt.CountryId
						, mkt.RegionID
		) mkts

	DECLARE @MaxRank		INT = (SELECT MAX(CountryPopRank) AS MaxRank FROM @MarketPops AS mpr)
	
	DECLARE @RelevantMarkets TABLE
	(	MarketID		INT
	  , RegionPopRank	INT
	  , CountryID		INT
	  , RegionID		INT
	  , TopBound		INT
	  , BottomBound		INT
	)
	INSERT INTO @RelevantMarkets
	SELECT mpr.MarketID
			, mpr.RegionPopRank
			, mpr.CountryId
			, mpr.RegionID
			, (CASE WHEN mpr.RegionPopRank < 4 THEN 1
					WHEN mpr.RegionPopRank > @MaxRank - 3 THEN (mpr.RegionPopRank - 6) + (@MaxRank - mpr.RegionPopRank)
					ELSE mpr.RegionPopRank -3
			END)							AS TopBound
			, (CASE WHEN mpr.RegionPopRank < 4 THEN mpr.RegionPopRank + 3 + (4 - mpr.RegionPopRank)
					WHEN mpr.RegionPopRank > @MaxRank - 3 THEN @MaxRank
					ELSE mpr.RegionPopRank + 3 
			END)							AS BottomBound
		FROM @MarketPops AS mpr
		GROUP BY mpr.MarketId
				, mpr.RegionPopRank
				, mpr.CountryId
				, mpr.RegionID

	DECLARE @TopBound INT = (SELECT TopBound FROM @RelevantMarkets WHERE MarketID = @prmMarketID)
	DECLARE @BottomBound INT = (SELECT BottomBound FROM @RelevantMarkets WHERE MarketID = @prmMarketID)

	SELECT sq1.*
		 , (CASE WHEN CYminus3 = 0 THEN 0
				 ELSE ((CYMinus2 - CYMinus3) / CYMinus3) * 100
			END)												AS pctChangeY3toY2
		 , (CASE WHEN CYminus1 = 0 THEN 0
				 ELSE ((CYMinus1 - CYMinus2) / CYMinus1) * 100
			END)												AS pctChangeY2toY1
		 , sq1.PopulationEstimate / CYMinus1					AS CYMinus1PerCapita
		 , sq1.PopulationEstimate / CYMinus2					AS CYMinus2PerCapita
		 , rg.RegionName
	  FROM 
	  (	SELECT mpr.MarketID
			 , mpr.MarketName
			 , SUM(CASE WHEN vfd.FundraisingYear = @prmYear - 3 THEN Amount ELSE 0 END)	AS CYminus3
			 , SUM(CASE WHEN vfd.FundraisingYear = @prmYear - 2 THEN Amount ELSE 0 END)	AS CYminus2
			 , SUM(CASE WHEN vfd.FundraisingYear = @prmYear - 1 THEN Amount ELSE 0 END)	AS CYminus1
			 , SUM(CASE WHEN vfd.FundraisingYear = @prmYear THEN Amount ELSE 0 END)		AS CY 
			 , mpr.RegionPopRank
			 , mpr.PopulationEstimate
			 , mpr.RegionID
		  FROM @MarketPops AS mpr 
			INNER JOIN dbo.vwFundraisingData AS vfd ON vfd.MarketId = mpr.MarketID
		  WHERE mpr.RegionPopRank BETWEEN @TopBound AND @BottomBound
			AND vfd.FundraisingYear >= @prmYear - 3 
		  GROUP BY mpr.MarketID
				 , mpr.MarketName
				 , mpr.RegionPopRank
				 , mpr.PopulationEstimate
				 , mpr.RegionID
	  ) sq1
		INNER JOIN Core.[dbo].[Regions] AS rg ON sq1.RegionID = rg.RegionId
	  ORDER BY RegionPopRank



END



