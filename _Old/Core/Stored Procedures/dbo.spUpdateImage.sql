﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 01/26/2015
-- Description:	This procedure is used to update an existing Image.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateImage]
	@ImageId int,
	@TypeId int,
	@Value varchar(255),
	@URI varchar(255),
	@Path varchar(255),
	@Width int,
	@Height int,
	@ImageFormatId int
AS
BEGIN
	UPDATE [dbo].[Images] SET
		[TypeId] = @TypeId,
		[Value] = @Value,
		[URI] = @URI,
		[Path] = @Path,
		[Width] = @Width,
		[Height] = @Height,
		[ImageFormatId] = @ImageFormatId,
		[ModifiedDate] = GETDATE()
	WHERE [ImageId] = @ImageId;
END


