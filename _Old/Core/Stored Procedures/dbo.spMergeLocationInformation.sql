﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 12/16/2014
-- Description:	This will merge all location information from our core database to the old Core database.
-- =============================================
CREATE PROCEDURE [dbo].[spMergeLocationInformation]
AS
--BEGIN
	/*-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	--EXEC dbo.spMergePartnerPrograms
	
	--SET IDENTITY_INSERT Core.dbo.PropertyTypes ON

	--MERGE Core.dbo.PropertyTypes AS t
	--USING core.dbo.PropertyTypes AS s
	--ON (
	--	t.PropertyTypeId = s.PropertyTypeId
	--)
	--WHEN NOT MATCHED
	--	THEN INSERT(PropertyTypeId, PropertyType) 
	--	VALUES(s.PropertyTypeId,s.PropertyType)
	--WHEN MATCHED AND t.PropertyType != s.PropertyType
	--	THEN UPDATE SET t.PropertyType = s.PropertyType
	--WHEN NOT MATCHED BY SOURCE
	--	THEN DELETE
	--OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.PropertyTypes OFF


	SET IDENTITY_INSERT Core.dbo.AreaTypes ON

	MERGE Core.dbo.AreaTypes AS t
	USING core.dbo.AreaTypes AS s
	ON (
		t.AreaTypeId = s.AreaTypeId
	)
	WHEN NOT MATCHED
		THEN INSERT(AreaTypeId, AreaType) 
		VALUES(s.AreaTypeId,s.AreaType)
	WHEN MATCHED AND t.AreaType != s.AreaType
		THEN UPDATE SET t.AreaType = s.AreaType
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.AreaTypes OFF

	SET IDENTITY_INSERT Core.dbo.Areas ON

	MERGE Core.dbo.Areas AS t
	USING core.dbo.Areas AS s
	ON (
		t.AreaId = s.AreaId
	)
	WHEN NOT MATCHED
		THEN INSERT(AreaId, PartnerProgramId, AreaTypeId, Name, ContactId) 
		VALUES(s.AreaId,s.FundraisingEntityId, s.AreaTypeId, s.Name, s.ContactId)
	WHEN MATCHED AND t.PartnerProgramId != s.FundraisingEntityId Or t.AreaTypeId != s.AreaTypeId
				Or t.Name != s.Name Or t.ContactId != s.ContactId
		THEN UPDATE SET t.PartnerProgramId = s.FundraisingEntityId, t.AreaTypeId = s.AreaTypeId, t.Name = s.Name, t.ContactId = s.ContactId
	--WHEN NOT MATCHED BY SOURCE
	--	THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.Areas OFF

	SET IDENTITY_INSERT Core.dbo.Locations ON

	MERGE Core.dbo.Locations AS t
	USING core.dbo.Locations AS s
	ON (
		t.LocationId = s.LocationId
	)
	WHEN NOT MATCHED
		THEN INSERT(LocationId,[PartnerProgramId],[LocationName],[LocationNumber],[Address1],[Address2],[City],[ProvinceId],[PostalCode],[CountryId],[PropertyTypeId],[Latitude],[Longitude],[Active]) 
		VALUES(s.LocationId, s.FundraisingEntityId, s.LocationName,s.LocationNumber,s.Address1, s.Address2, s.City, s.ProvinceId, s.PostalCode, s.CountryId, s.PropertyTypeId, s.Latitude, s.Longitude, s.Active)
	WHEN MATCHED AND t.PartnerProgramId != s.FundraisingEntityId Or t.LocationName != s.LocationName
				Or t.LocationNumber != s.LocationNumber Or t.Address1 != s.Address1 Or t.Address2 != s.Address2 Or t.City != s.City OR t.PostalCode != s.PostalCode OR t.PropertyTypeId != s.PropertyTypeId
				Or t.ProvinceId != s.ProvinceId Or t.CountryId != s.CountryId Or t.Latitude != s.Latitude Or t.Longitude != s.Longitude Or t.Active != s.Active
		THEN UPDATE SET t.PartnerProgramId = s.FundraisingEntityId, t.LocationName = s.LocationName, 
				 t.LocationNumber = s.LocationNumber, t.Address1 = s.Address1, t.Address2 = s.Address2, t.City = s.City, t.PostalCode = s.PostalCode, t.PropertyTypeId = s.PropertyTypeId,
				 t.ProvinceId = s.ProvinceId, t.CountryId = s.CountryId, t.Latitude = s.Latitude, t.Longitude = s.Longitude, t.Active = s.Active
	--WHEN NOT MATCHED BY SOURCE
	--	THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.Locations OFF

	SET IDENTITY_INSERT Core.dbo.LocationAreas ON

	MERGE Core.dbo.LocationAreas AS t
	USING core.dbo.LocationAreas AS s
	ON (
		t.LocationAreasId = s.LocationAreasId
	)
	WHEN NOT MATCHED
		THEN INSERT(LocationAreasId, LocationId, AreaId) 
		VALUES(s.LocationAreasId, s.LocationId, s.AreaId)
	WHEN MATCHED AND t.LocationId != s.LocationId or t.AreaId != s.AreaId
		THEN UPDATE SET t.LocationId = s.LocationId, t.AreaId = s.AreaId
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.LocationAreas OFF

	--SET IDENTITY_INSERT Core.dbo.Splits ON

	--MERGE Core.dbo.Splits AS t
	--USING core.dbo.Splits AS s
	--ON (
	--	t.SplitId = s.SplitId
	--)
	--WHEN NOT MATCHED
	--	THEN INSERT(SplitId, PartnerProgramId, PostalCode, SplitFrom, SplitPercent, IsPostalCode) 
	--	VALUES(s.SplitId, s.FundraisingEntityId, s.PostalCode, s.SplitFrom, s.SplitPercent, s.IsPostalCode)
	--WHEN MATCHED AND t.PartnerProgramId != s.FundraisingEntityId or t.PostalCode != s.PostalCode OR t.SplitFrom != t.SplitFrom
	--				OR t.SplitPercent != s.SplitPercent OR t.IsPostalCode != s.IsPostalCode
	--	THEN UPDATE SET t.PartnerProgramId = s.FundraisingEntityId, t.PostalCode = s.PostalCode, t.SplitFrom = t.SplitFrom,
	--				    t.SplitPercent = s.SplitPercent, t.IsPostalCode = s.IsPostalCode
	--WHEN NOT MATCHED BY SOURCE
	--	THEN DELETE
	--OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.Splits OFF

	END TRY

			BEGIN CATCH
				SELECT 
					 ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;

				IF @@TRANCOUNT > 0
					BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Rollback'
					END
			END CATCH;

			IF @@TRANCOUNT > 0
				BEGIN
				COMMIT TRANSACTION;
				PRINT 'Commit'
			END*/
--END


