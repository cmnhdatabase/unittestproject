﻿
CREATE PROCEDURE [dbo].[spMergeOfficeImport]
AS
	BEGIN
		SET NOCOUNT ON;
		
		IF OBJECT_ID('tempdb..#RemaxOfficeChanges' , 'U') IS NOT NULL
			DROP TABLE #RemaxOfficeChanges;

		IF OBJECT_ID('tempdb..#StagingTable' , 'U') IS NOT NULL
			DROP TABLE #StagingTable;
	
		IF OBJECT_ID('tempdb..#Counts' , 'U') IS NOT NULL
			DROP TABLE #Counts;

		DECLARE	@PartnerCounts TABLE --For counting Before and After
	(
	  [LoadStage] NVARCHAR(8) ,
	  [FundraisingEntityId] INT ,
	  [PrimaryPartner Active] INT ,
	  [PrimaryPartner Inactive] INT ,
	  [PrimaryPartner Total] INT ,
	  [OtherPartners Active] INT ,
	  [OtherPartners Inactive] INT ,
	  [OtherPartners Total] INT
	);	

		INSERT	INTO @PartnerCounts
		SELECT	'Before' ,
				fe.FundraisingEntityId ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId IN ( 96 , 97 )
									 AND l.Active = 1 THEN 1
								ELSE 0
						   END) , 0) AS 'PrimaryPartner Active' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId IN ( 96 , 97 )
									 AND l.Active = 0 THEN 1
								ELSE 0
						   END) , 0) AS 'PrimaryPartner InActive' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId IN ( 96 , 97 ) THEN 1
								ELSE 0
						   END) , 0) AS 'PrimaryPartner Total' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId NOT IN ( 96 , 97 )
									 AND l.Active = 1 THEN 1
								ELSE 0
						   END) , 0) AS 'OtherPartners Active' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId NOT IN ( 96 , 97 )
									 AND l.Active = 0 THEN 1
								ELSE 0
						   END) , 0) AS 'OtherPartners InActive' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId NOT IN ( 96 , 97 ) THEN 1
								ELSE 0
						   END) , 0) AS 'OtherPartners Total'
		FROM	Core.dbo.FundraisingEntities fe
		LEFT JOIN dbo.Locations l ON l.FundraisingEntityId = fe.FundraisingEntityId
		GROUP BY fe.FundraisingEntityId;

		CREATE TABLE #StagingTable
		(
		  FundraisingEntityId INT NULL ,
		  LocationName VARCHAR(100) NULL ,
		  LocationNumber VARCHAR(100) NULL ,
		  Address1 VARCHAR(100) NULL ,
		  Address2 VARCHAR(100) NULL ,
		  City VARCHAR(100) NULL ,
		  ProvinceId INT NULL ,
		  PostalCode VARCHAR(10) NULL ,
		  CountryId INT NULL ,
		  PropertyTypeId INT NULL ,
		  Latitude FLOAT NULL ,
		  Longitude FLOAT NULL ,
		  Active BIT NOT NULL,
		); 

		CREATE TABLE #Counts
		(
		  ProcessStage NVARCHAR(255) ,
		  CountryId NVARCHAR(255) ,
		  Active BIT ,
		  RecordCount NVARCHAR(255)
		);
		INSERT	INTO #Counts
		SELECT	'Staging' ,
				CASE Country
				  WHEN 'Canada' THEN 2
				  WHEN 'UnitedStated' THEN 1
				  ELSE 0
				END AS CountryId ,
				1 AS Active ,
				COUNT(*)
		FROM	TemporaryData.dbo.ReMaxOfficesImportStaging
		GROUP BY Country ,
				( CASE WHEN Status LIKE '%OPEN' THEN 1
					   ELSE 0
				  END );
  
		INSERT	INTO #Counts
		SELECT	'Pre Load' ,
				q1.*
		FROM	(
				  SELECT	CountryId ,
							Active ,
							COUNT(*) AS 'OfficeCount'
				  FROM		Core.dbo.Locations
				  GROUP BY	CountryId ,
							Active
				) q1
		ORDER BY CountryId DESC ,
				Active;

		INSERT	INTO #StagingTable
		SELECT DISTINCT
				CASE taz.Country
				  WHEN 'Canada' THEN 97
				  WHEN 'United States' THEN 96
				END AS FundraisingEntityId ,
				OfficeName AS LocationName ,
				CAST(OfficeID AS NVARCHAR(100)) AS LocationNumber ,
				taz.Address1 AS Address1 ,
				ISNULL(taz.Address2 , '') AS Address2 ,
				taz.City AS City ,
				p.ProvinceId AS ProvinceId ,
				CAST(taz.PostalCode AS NVARCHAR(50)) AS PostalCode ,
				p.CountryId AS CountryId ,
				0 AS PropertyTypeId ,
				ISNULL(l.Latitude , NULL) AS Latitude ,
				ISNULL(l.Longitude , NULL) AS Longitude ,
				( CASE WHEN Status LIKE '%OPEN' THEN 1
					   ELSE 0
				  END ) AS Active
		FROM	TemporaryData.dbo.ReMaxOfficesImportStaging AS taz
		LEFT JOIN Core.dbo.Locations l ON l.LocationNumber = taz.OfficeID
										  AND l.FundraisingEntityId IN ( 96 , 97 )
		INNER JOIN Core.dbo.PostalCodes AS pc ON pc.PostalCode = taz.PostalCode
		INNER JOIN Core.dbo.Provinces p ON p.ProvinceId = pc.ProvinceId;	

		BEGIN TRANSACTION;
		BEGIN TRY
			CREATE TABLE #RemaxOfficeChanges
			(
			  OldFundraisingEntityId INT ,
			  OldLocationName NVARCHAR(255) ,
			  OldOfficeName NVARCHAR(255) ,
			  OldLocationNumber NVARCHAR(255) ,
			  OldAddress1 NVARCHAR(255) ,
			  OldAddress2 NVARCHAR(255) ,
			  OldCity NVARCHAR(255) ,
			  OldProvinceId INT ,
			  OldPostalCode NVARCHAR(255) ,
			  OldCountryId INT ,
			  OldPropertyTypeId INT ,
			  OldLatitude FLOAT ,
			  OldLongitude FLOAT ,
			  OldActive BIT ,
			  Action VARCHAR(8) NOT NULL ,
			  NewFundraisingEntityId INT ,
			  NewLocationName NVARCHAR(255) ,
			  NewOfficeName NVARCHAR(255) ,
			  NewLocationNumber NVARCHAR(255) ,
			  NewAddress1 NVARCHAR(255) ,
			  NewAddress2 NVARCHAR(255) ,
			  NewCity NVARCHAR(255) ,
			  NewProvinceId INT ,
			  NewPostalCode NVARCHAR(255) ,
			  NewCountryId INT ,
			  NewPropertyTypeId INT ,
			  NewLatitude FLOAT ,
			  NewLongitude FLOAT ,
			  NewActive BIT,
			);
			MERGE Core.dbo.Locations AS target
			USING
				(
				  SELECT	*
				  FROM		#StagingTable
				) AS source ( FundraisingEntityId, LocationName, LocationNumber, Address1, Address2, City, ProvinceId, PostalCode, CountryId, PropertyTypeId, Latitude, Longitude, Active )
			ON ( target.LocationNumber = source.LocationNumber
				 AND target.FundraisingEntityId = source.FundraisingEntityId )
			WHEN MATCHED THEN
				UPDATE SET
						 target.FundraisingEntityId = source.FundraisingEntityId ,
						 target.LocationName = source.LocationName ,
						 target.LocationNumber = source.LocationNumber ,
						 target.Address1 = source.Address1 ,
						 target.Address2 = source.Address2 ,
						 target.City = source.City ,
						 target.ProvinceId = source.ProvinceId ,
						 target.PostalCode = source.PostalCode ,
						 target.CountryId = source.CountryId ,
						 target.PropertyTypeId = source.PropertyTypeId ,
						 target.Latitude = source.Latitude ,
						 target.Longitude = source.Longitude ,
						 target.Active = source.Active
			WHEN NOT MATCHED BY TARGET THEN
				INSERT ( FundraisingEntityId ,
						 LocationName ,
						 LocationNumber ,
						 Address1 ,
						 Address2 ,
						 City ,
						 ProvinceId ,
						 PostalCode ,
						 CountryId ,
						 PropertyTypeId ,
						 Latitude ,
						 Longitude ,
						 Active )
				VALUES ( source.FundraisingEntityId ,
						 source.LocationName ,
						 source.LocationNumber ,
						 source.Address1 ,
						 source.Address2 ,
						 source.City ,
						 source.ProvinceId ,
						 source.PostalCode ,
						 source.CountryId ,
						 source.PropertyTypeId ,
						 source.Latitude ,
						 source.Longitude ,
						 source.Active )
			OUTPUT
				deleted.* ,
				$action ,
				inserted.*
				INTO #RemaxOfficeChanges;

			DECLARE	@RemaxOfficeDeactivates TABLE
		(
		  OldLocationId NVARCHAR(255) ,
		  OldLocationNumber NVARCHAR(255) ,
		  OldLocationName NVARCHAR(255) ,
		  OldCity NVARCHAR(255) ,
		  OldProvinceId NVARCHAR(255) ,
		  OldActive BIT ,
		  NewActive BIT
		);

			UPDATE	Core.dbo.Locations
			SET		Active = 0
			OUTPUT	inserted.LocationId AS LocationId ,
					inserted.LocationNumber AS LocationNumber ,
					inserted.LocationName AS LocationName ,
					inserted.City AS City ,
					inserted.ProvinceId AS ProvinceId ,
					deleted.Active AS OldActive ,
					inserted.Active AS NewActive
					INTO @RemaxOfficeDeactivates
			FROM	Core.dbo.Locations AS ofc
			WHERE	ofc.Active = 1
					AND ofc.LocationNumber NOT IN ( SELECT	LocationNumber
													FROM	#StagingTable
													WHERE	FundraisingEntityId IN ( 96 , 97 ) )
					AND ofc.FundraisingEntityId IN ( 96 , 97 );
		    
		END TRY
		BEGIN CATCH
			SELECT	ERROR_NUMBER() AS ErrorNumber ,
					ERROR_SEVERITY() AS ErrorSeverity ,
					ERROR_STATE() AS ErrorState ,
					ERROR_PROCEDURE() AS ErrorProcedure ,
					ERROR_LINE() AS ErrorLine ,
					ERROR_MESSAGE() AS ErrorMessage;

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

		INSERT	INTO #Counts
		SELECT	'Post Load' ,
				q1.*
		FROM	(
				  SELECT	CountryId ,
							Active ,
							COUNT(*) AS 'OfficeCount'
				  FROM		Core.dbo.Locations
				  GROUP BY	CountryId ,
							Active
				) q1
		ORDER BY CountryId DESC ,
				Active;

		SELECT	roc.* ,
				ic.InsertCount ,
				act.DeActivates ,
				act.Activates ,
				nacu."OfficesNowActive-USA" ,
				nacc."OfficesNowActive-CAN"
		FROM	(
				  SELECT	'Load Result' AS FakeJoin ,
							COUNT(*) AS UpdateCount
				  FROM		#RemaxOfficeChanges
				  WHERE		Action = 'UPDATE'
				) roc
		LEFT JOIN (
					SELECT	'Load Result' AS FakeJoin ,
							COUNT(*) AS InsertCount
					FROM	#RemaxOfficeChanges
					WHERE	Action = 'INSERT'
				  ) AS ic ON roc.FakeJoin = ic.FakeJoin
		LEFT JOIN (
					SELECT	'Load Result' AS FakeJoin ,
							(
							  SELECT	COUNT(*)
							  FROM		@RemaxOfficeDeactivates
							) AS DeActivates ,
							ISNULL(SUM(CASE	WHEN OldActive = 0
												 AND NewActive = 1 THEN 1
											ELSE 0
									   END) , 0) AS Activates
					FROM	#RemaxOfficeChanges
					WHERE	Action = 'UPDATE'
							AND OldActive <> NewActive
				  ) AS act ON roc.FakeJoin = act.FakeJoin
		LEFT JOIN (
					SELECT	'Load Result' AS FakeJoin ,
							COUNT(*) AS "OfficesNowActive-USA"
					FROM	Core.dbo.Locations
					WHERE	Active = 1
							AND CountryId = '1'
							AND FundraisingEntityId IN ( 96 , 97 )
				  ) AS nacu ON roc.FakeJoin = nacu.FakeJoin
		LEFT JOIN (
					SELECT	'Load Result' AS FakeJoin ,
							COUNT(*) AS "OfficesNowActive-CAN"
					FROM	Core.dbo.Locations
					WHERE	Active = 1
							AND CountryId <> '1'
							AND FundraisingEntityId IN ( 96 , 97 )
				  ) AS nacc ON roc.FakeJoin = nacc.FakeJoin;
		SELECT	*
		FROM	#Counts;

		UPDATE	Core..Locations
		SET		Active = 1
		WHERE	LocationId IN ( '675200' , '675201' , '675202' )
				AND FundraisingEntityId = 96;

		--Insert into PartnerCounts After Results
		INSERT	INTO @PartnerCounts
		SELECT	'After' ,
				fe.FundraisingEntityId ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId IN ( 96 , 97 )
									 AND l.Active = 1 THEN 1
								ELSE 0
						   END) , 0) AS 'PrimaryPartner Active' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId IN ( 96 , 97 )
									 AND l.Active = 0 THEN 1
								ELSE 0
						   END) , 0) AS 'PrimaryPartner InActive' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId IN ( 96 , 97 ) THEN 1
								ELSE 0
						   END) , 0) AS 'PrimaryPartner Total' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId NOT IN ( 96 , 97 )
									 AND l.Active = 1 THEN 1
								ELSE 0
						   END) , 0) AS 'OtherPartners Active' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId NOT IN ( 96 , 97 )
									 AND l.Active = 0 THEN 1
								ELSE 0
						   END) , 0) AS 'OtherPartners InActive' ,
				ISNULL(SUM(CASE	WHEN fe.FundraisingEntityId NOT IN ( 96 , 97 ) THEN 1
								ELSE 0
						   END) , 0) AS 'OtherPartners Total'
		FROM	Core.dbo.FundraisingEntities fe
		LEFT JOIN dbo.Locations l ON l.FundraisingEntityId = fe.FundraisingEntityId
		GROUP BY fe.FundraisingEntityId;

		INSERT	INTO SqlManagement.dbo.LocationUpdateHistory
				( FundraisingEntityId ,
				  Added ,
				  Removed ,
				  MarkedActive ,
				  MarkedInactive ,
				  TotalActiveLocations ,
				  TotalInactiveLocations ,
				  ChangeDate )
		SELECT	after.FundraisingEntityId ,
				ISNULL(after.[PrimaryPartner Total] , 0) - ISNULL(before.[PrimaryPartner Total] , 0) Added ,
				ISNULL(before.[PrimaryPartner Total] , 0) - ISNULL(after.[PrimaryPartner Total] , 0) Removed ,
				ISNULL(after.[PrimaryPartner Active] , 0) - ISNULL(before.[PrimaryPartner Active] , 0) MarkedActive ,
				ISNULL(after.[PrimaryPartner Inactive] , 0) - ISNULL(before.[PrimaryPartner Inactive] , 0) MarkedInactive ,
				ISNULL(after.[PrimaryPartner Active] , 0) TotalActiveLocations ,
				ISNULL(after.[PrimaryPartner Inactive] , 0) TotalInactiveLocations ,
				GETDATE()
		FROM	@PartnerCounts before
		JOIN	@PartnerCounts after ON after.FundraisingEntityId = before.FundraisingEntityId
		WHERE	before.LoadStage = 'Before'
				AND after.LoadStage = 'After'
				AND after.FundraisingEntityId IN ( 96 , 97 )
		ORDER BY after.FundraisingEntityId;
	END;

