﻿

CREATE PROCEDURE [dbo].[rptPvrPartnerRegionTotal]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 175
/******************/
IF OBJECT_ID('tempdb..#tempPartnerRegionTotal', 'U') IS NOT NULL
    DROP TABLE #tempPartnerRegionTotal

SELECT ((SUM(PartnerCYTotal) - SUM(PartnerCYminus1Total))/NULLIF(SUM(Counts),0)) AS CyIncrease,
((SUM(PartnerCYTotal)/NULLIF(SUM(Counts),0)) - (SUM(PartnerCYminus1Total)/NULLIF(SUM(Counts),0)))/NULLIF(SUM(PartnerCYminus1Total)/NULLIF(SUM(Counts),0),0) AS CYPercentIncrease,
(SUM(PartnerCYTotal)/NULLIF(SUM(Counts),0))/NULLIF(SUM(pop)/NULLIF(SUM(Counts),0),0) AS PerCap
INTO #tempPartnerRegionTotal
FROM (SELECT
fd.MarketId,
COUNT(DISTINCT fd.MarketId) AS counts,
SUM(CASE WHEN FundraisingYear = @year THEN amount ELSE 0 END) AS PartnerCYTotal,
SUM(CASE WHEN FundraisingYear = @year -1 THEN Amount END) AS PartnerCYminus1Total,
mpr.PopulationEstimate AS Pop
FROM dbo.vwFundraisingData fd
INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = fd.MarketId
WHERE FundraisingYear >= @year - 1
AND FundraisingCategoryId = 1
AND fd.RegionId = (SELECT MAX(RegionId) FROM dbo.vwFundraisingData WHERE MarketId = @market)
GROUP BY fd.MarketId, PopulationEstimate) AS a


SELECT *
FROM #tempPartnerRegionTotal






