﻿




CREATE PROCEDURE [dbo].[etlBingMapsAPIUpdate]
AS
	BEGIN
		MERGE TemporaryData.dbo.TempStoreListUpload t
		USING TemporaryData.dbo.TempBingMapsAPI s
		ON t.LocationNumber = s.locationNumber
			AND t.LocationName = s.locationName
		WHEN MATCHED THEN
			UPDATE SET
					t.Latitude = s.[Latitude] ,
					t.Longitude = s.[Longitude]
		OUTPUT
			$action ,
			inserted.* ,
			deleted.*;

		DELETE FROM TemporaryData.dbo.TempBingMapsAPI
	END;



