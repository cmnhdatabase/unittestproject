﻿


CREATE PROCEDURE [dbo].[spUWXRUDYEZSMySQLInsert]
AS
	BEGIN
		IF OBJECT_ID('tempdb..##Champions' , 'U') IS NOT NULL
			DROP TABLE ##Champions;
		IF OBJECT_ID('tempdb..##FundraisingEntities' , 'U') IS NOT NULL
			DROP TABLE ##FundraisingEntities;
		--IF OBJECT_ID('tempdb..##IPAddresses' , 'U') IS NOT NULL
		--	DROP TABLE ##IPAddresses;
		IF OBJECT_ID('tempdb..##Hospitals' , 'U') IS NOT NULL
			DROP TABLE ##Hospitals;
		IF OBJECT_ID('tempdb..##Celebrities' , 'U') IS NOT NULL
			DROP TABLE ##Celebrities;
		IF OBJECT_ID('tempdb..##RadioStations' , 'U') IS NOT NULL
			DROP TABLE ##RadioStations;
		IF OBJECT_ID('tempdb..##HospitalSocialMedia' , 'U') IS NOT NULL
			DROP TABLE ##HospitalSocialMedia;
		IF OBJECT_ID('tempdb..##Television' , 'U') IS NOT NULL
			DROP TABLE ##Television;
		IF OBJECT_ID('tempdb..##SubMarkets' , 'U') IS NOT NULL
			DROP TABLE ##SubMarkets;
		--IF OBJECT_ID('tempdb..##MiracleMillion' , 'U') IS NOT NULL
		--	DROP TABLE ##MiracleMillion;
		IF OBJECT_ID('tempdb..##Markets' , 'U') IS NOT NULL
			DROP TABLE ##Markets;
		IF OBJECT_ID('tempdb..##PostalCodes' , 'U') IS NOT NULL
			DROP TABLE ##PostalCodes;

--PostalCodes
		SELECT DISTINCT
				pc.PostalCode ,
				pc.MarketId ,
				pc.SubMarketId ,
				pc.ProvinceId ,
				pc.CountryId ,
				pc.City ,
				pc.County
		INTO	##PostalCodes
		FROM	Core.dbo.PostalCodes pc
		JOIN	UWXRUDYEZS...PostalCodes ppc ON pc.PostalCode = ppc.PostalCode
												AND ( pc.MarketId <> ppc.MarketId
													  OR pc.SubMarketId <> ppc.SubMarketId
													  OR pc.ProvinceId <> ppc.ProvinceId
													  OR pc.CountryId <> ppc.CountryId
													  OR pc.City <> ppc.City
													  OR pc.County <> ppc.County );

--Champions
		SELECT	c.ChampionId ,
				c.Age AS Age ,
				cs.Story AS Bio ,
				c.CountryId AS CountryId ,
				c.DisplayProvince AS DisplayProvince ,
				c.FirstName AS FirstName ,
				'' AS FundsInAction ,
				c.HospitalId AS HospitalId ,
				c.Illness AS Illness ,
				c.LastName AS LastName ,
				i.URI AS PhotoUrl ,
				'' AS SocialMediaLinks ,
				c.BlogUri AS Website ,
				c.[Year] AS [Year]
		INTO	##Champions
		FROM	Core.dbo.Champions c
		JOIN	Core.dbo.ChampionStories cs ON cs.ChampionId = c.ChampionId
		LEFT JOIN (
					SELECT	* ,
							ROW_NUMBER() OVER ( PARTITION BY i.Value ORDER BY i.Value ) AS RowNumber
					FROM	Core.dbo.Images i
					WHERE	i.TypeId = 1
				  ) i ON i.Value = CAST(c.ChampionId AS VARCHAR)
		WHERE	( i.RowNumber = 1
				  OR i.RowNumber IS NULL )
				AND cs.LanguageId = 1;

--FundraisingEntities
		SELECT	DISTINCT
				fe.FundraisingEntityId ,
				'' AS Address ,
				fe.CountryId AS CountryId ,
				fe.DisplayName AS DisplayName ,
				fe.FriendlyName AS FriendlyName ,
				fe.FundraisingCategoryId AS FundraisingCategoryId ,
				CASE WHEN fe.FundraisingEntityId = 31 THEN 'https://cdn.putyourmoneywherethemiraclesare.co/images/CorporateWebsite/2016/OurPartners/CU4K-COOP.svg'
					 WHEN i.URI IS NULL THEN ''
					 ELSE i.URI
				END AS LogoUrl ,
				'' AS LongDescription ,
				fe.Name AS Name ,
				'' AS PhoneNumber ,
				'' AS ShortDescription ,
				'' AS SocialMediaLinks ,
				fe.Website AS Website,
				fe.SponsorId AS SponsorID
		INTO	##FundraisingEntities
		FROM	Core.dbo.FundraisingEntities fe
		--LEFT JOIN USWebsite.dbo.Sponsor s ON s.LinkId = fe.FundraisingEntityId
		LEFT JOIN (
					SELECT	i.* ,
							ROW_NUMBER() OVER ( PARTITION BY i.Value ORDER BY i.Value ) AS RowNumber
					FROM	[Core].[dbo].[Images] i
					JOIN	Core.dbo.ImageTypes it ON it.ImageTypeId = i.TypeId
					JOIN	Core.dbo.ImageFormats im ON im.ImageFormatId = i.ImageFormatId
					WHERE	im.ImageFormat LIKE '%svg%'
							AND i.TypeId = 9
				  ) i ON CAST(fe.FundraisingEntityId AS VARCHAR) = i.Value
		WHERE	fe.Active = 1
				AND ( i.RowNumber = 1
					  OR i.RowNumber IS NULL )
				AND fe.FundraisingEntityId <> 24
		ORDER BY 1;

--Hospitals
		SELECT DISTINCT
				h.HospitalId AS HospitalId ,
				h.Address1 AS Address1 ,
				h.Address2 AS Address2 ,
				h.City AS City ,
				hc.Phone AS ContactPhoneNumber ,
				h.CountryId AS CountryId ,
				h.FriendlyHospitalName AS FriendlyHospitalName ,
				h.SubDomain AS SubDomain ,
				i.URI AS HospitalLogoUrl ,
				h.HospitalName AS HospitalName ,
				'' AS HospitalPictureUrl ,
				h.Latitude AS Latitude ,
				h.LongDescription AS LongDescription ,
				h.Longitude AS Longitude ,
				h.PostalCode AS PostalCode ,
				h.ProvinceId AS ProvinceId ,
				h.ShortDescription AS ShortDescription ,
				'' AS SocialMediaLinks ,
				h.Website AS Website ,
				h.MarketId AS MarketID ,
				hc.DirectorName
		INTO	##Hospitals
		FROM	Core.dbo.Hospitals h
		--LEFT JOIN USWebsite.dbo.Hospital uh ON uh.LinkId = h.HospitalId
		LEFT JOIN (
					SELECT	hc.HospitalId ,
							CONCAT(c.FirstName,' ',c.LastName) AS DirectorName,
							MAX(c.Phone) AS Phone
					FROM	Contacts.dbo.HospitalContacts hc
					JOIN	Contacts.dbo.Contacts c ON c.ContactId = hc.ContactId
					LEFT JOIN Contacts.dbo.ContactsSubcategories csc ON csc.ContactId = c.ContactId
					WHERE	csc.SubcategoryId = 15
							AND c.Active = 1
					GROUP BY hc.HospitalId,CONCAT(c.FirstName,' ',c.LastName)
				  ) hc ON h.HospitalId = hc.HospitalId
		LEFT JOIN (
					SELECT	i.* ,
							ROW_NUMBER() OVER ( PARTITION BY i.Value ORDER BY i.Value ) AS RowNumber
					FROM	[Core].[dbo].[Images] i
					JOIN	Core.dbo.ImageTypes it ON it.ImageTypeId = i.TypeId
					JOIN	Core.dbo.ImageFormats im ON im.ImageFormatId = i.ImageFormatId
					WHERE	i.TypeId = 3
							AND i.Value NOT LIKE '%-%'
							AND im.ImageFormat LIKE '%jpg%'
				  ) i ON i.Value = h.HospitalId
		--LEFT JOIN USWebsite.dbo.HospitalSocialNetworkingSites hsns ON hsns.HospitalId = uh.HospitalID
		WHERE	( i.RowNumber = 1
				  OR i.RowNumber IS NULL )
				AND h.Active = 1
		ORDER BY Address1;

--IPAddresses
	--SELECT	ROW_NUMBER() OVER ( PARTITION BY sub.Fakejoin ORDER BY sub.Fakejoin ) AS IPId ,
	--		IP_FROM AS IPAddressFrom ,
	--		IP_TO AS IPAddressTo ,
	--		m.MarketId AS MarketId ,
	--		ZIPCODE AS PostalCode
	--INTO	##IPAddresses
	--FROM	IPDatabase.dbo.IPAddresses i
	--LEFT JOIN Core.dbo.PostalCodes pc ON pc.PostalCode = i.ZIPCODE
	--LEFT JOIN Core.dbo.Markets m ON m.MarketId = pc.SubMarketId
	--OUTER APPLY (
	--			  SELECT	'1' AS Fakejoin
	--			) sub;

--Celebrities
		SELECT	DISTINCT
				c.CelebrityId ,
				c.Bio AS Bio ,
				c.FirstName AS FirstName ,
				c.LastName AS LastName ,
				i.URI AS PhotoUrl ,
				'' AS Profession ,
				'' AS SocialMediaLinks ,
				c.Website AS Website
		INTO	##Celebrities
		FROM	Core.dbo.Celebrities c
		LEFT JOIN (
					SELECT	i.* ,
							ROW_NUMBER() OVER ( PARTITION BY i.Value ORDER BY i.Value ) AS RowNumber
					FROM	Core.dbo.Images i
					WHERE	i.TypeId = 6
				  ) i ON i.Value = CAST(c.CelebrityId AS VARCHAR)
		WHERE	c.FirstName IS NOT NULL;

----MiracleMillion
--		SELECT	ROW_NUMBER() OVER ( ORDER BY SUM(d.Amount) DESC ) MiracleMillionId ,
--				CASE WHEN fe.FundraisingEntityId IN ( 31 , 24 ) THEN 'Credit Unions for Kids'
--					 ELSE fe.Name
--				END Name ,
--				MAX(fe.FundraisingEntityId) FundraisingEntityId ,
--				ISNULL(d.FundraisingYear,YEAR(GETDATE()) - 1) FundraisingYear,
--				SUM(ISNULL(d.Amount,0)) Amount
--		--INTO	##MiracleMillion
--		FROM	Core.dbo.FundraisingEntities fe
--		LEFT JOIN	Core.dbo.Disbursements d ON fe.FundraisingEntityId = d.FundraisingEntityId AND d.FundraisingYear = YEAR(GETDATE()) - 1
--				AND d.FundraisingCategoryId = 1
--		WHERE	fe.FundraisingCategoryId = 1
--				AND fe.Active = 1
--				AND fe.CountryId = 1
--		GROUP BY CASE WHEN fe.FundraisingEntityId IN ( 31 , 24 ) THEN 'Credit Unions for Kids'
--					  ELSE fe.Name
--				 END ,
--				d.FundraisingYear
--		ORDER BY Amount DESC, Name;

--RadioStations
		SELECT	rs.RadioStationId ,
				rs.CallLetters ,
				rs.Address1 ,
				rs.Address2 ,
				rs.CountryId ,
				rs.MarketTag ,
				rs.MarketId ,
				rs.[Owner] ,
				rs.Frequency
		INTO	##RadioStations
		FROM	Core.dbo.RadioStations rs
		WHERE	rs.Active = 1;

--Television
		SELECT	t.TVStationId ,
				t.CallLetters ,
				t.Affiliation ,
				t.Address1 ,
				t.Address2 ,
				t.City ,
				t.ProvinceId ,
				t.PostalCode ,
				t.CountryId ,
				t.Website ,
				t.MarketId ,
				t.StationOwner ,
				t.MarketTag
		INTO	##Television
		FROM	Core.dbo.TV t
		WHERE	Active = 1;

--HospitalSocialMedia
		--SELECT	Id ,
		--		HospitalId ,
		--		SiteId ,
		--		CategoryId ,
		--		Link ,
		--		[Description]
		--INTO	##HospitalSocialMedia
		--FROM	USWebsite.dbo.HospitalSocialNetworkingSites;

--Markets
		SELECT	MarketId ,
				MarketName ,
				CountryId ,
				RegionId ,
				Active ,
				LegacyMarketId
		INTO	##Markets
		FROM	Core.dbo.Markets;

--SubMarkets
		SELECT	CountryId ,
				CountryName ,
				MarketId ,
				MarketName ,
				RegionId ,
				RegionName ,
				ISNULL(SubMarketId , MarketId) SubMarketId ,
				ISNULL(SubMarketName , MarketName) SubMarketName ,
				Active
		INTO	##SubMarkets
		FROM	Core.dbo.vwMarketDetails
		WHERE	MarketId <> ISNULL(SubMarketId , MarketId);

--Clearing and Inserting
		DELETE	UWXRUDYEZS...Champions;
		INSERT	INTO UWXRUDYEZS...Champions
		SELECT	ChampionId ,
				Age ,
				Bio ,
				CountryId ,
				DisplayProvince ,
				FirstName ,
				FundsInAction ,
				HospitalId ,
				Illness ,
				LastName ,
				PhotoUrl ,
				SocialMediaLinks ,
				Website ,
				[Year]
		FROM	##Champions;

--FundraisingEntities
		DELETE FROM UWXRUDYEZS...FundraisingEntities
		INSERT	INTO UWXRUDYEZS...FundraisingEntities
		SELECT	t.FundraisingEntityId ,
				t.[Address] ,
				t.CountryId ,
				t.DisplayName ,
				t.FriendlyName ,
				t.FundraisingCategoryId ,
				t.LogoUrl ,
				t.LongDescription ,
				t.Name ,
				t.PhoneNumber ,
				t.ShortDescription ,
				t.SocialMediaLinks ,
				t.Website ,
				t.SponsorID
		FROM	##FundraisingEntities t
		LEFT JOIN OPENQUERY(UWXRUDYEZS , 'SELECT * FROM FundraisingEntities') s ON s.FundraisingEntityId = t.FundraisingEntityId
		WHERE	s.FundraisingEntityId IS NULL;

--Hospitals	
	--setting values to '0' to prevent error in update
		UPDATE	t
		SET		Address1 = '0' ,
				Address2 = '0' ,
				City = '0' ,
				ContactPhoneNumber = '0' ,
				CountryId = '0' ,
				FriendlyHospitalName = '0' ,
				SubDomain = '0' ,
				HospitalLogoUrl = '0' ,
				HospitalName = '0' ,
				HospitalPictureUrl = '0' ,
				Latitude = '0' ,
				LongDescription = '0' ,
				Longitude = '0' ,
				PostalCode = '0' ,
				ProvinceId = '0' ,
				ShortDescription = '0' ,
				SocialMediaLinks = '0' ,
				Website = '0' ,
				MarketID = '0' ,
				DirectorName = '0'
		FROM	OPENQUERY(UWXRUDYEZS , 'SELECT * FROM Hospitals') AS t
		JOIN	##Hospitals AS s ON s.HospitalId = t.HospitalId;

	--updating values from 0 to real values
		UPDATE	t
		SET		Address1 = s.Address1 ,
				Address2 = s.Address2 ,
				City = s.City ,
				ContactPhoneNumber = s.ContactPhoneNumber ,
				CountryId = s.CountryId ,
				FriendlyHospitalName = s.FriendlyHospitalName ,
				SubDomain = s.SubDomain ,
				HospitalLogoUrl = s.HospitalLogoUrl ,
				HospitalName = s.HospitalName ,
				HospitalPictureUrl = s.HospitalPictureUrl ,
				Latitude = s.Latitude ,
				LongDescription = s.LongDescription ,
				Longitude = s.Longitude ,
				PostalCode = s.PostalCode ,
				ProvinceId = s.ProvinceId ,
				ShortDescription = s.ShortDescription ,
				SocialMediaLinks = s.SocialMediaLinks ,
				Website = s.Website ,
				MarketID = s.MarketID ,
				DirectorName = s.DirectorName
		FROM	OPENQUERY(UWXRUDYEZS , 'SELECT * FROM Hospitals') AS t
		JOIN	##Hospitals AS s ON s.HospitalId = t.HospitalId;
		
	--inserting new values
		INSERT	INTO UWXRUDYEZS...Hospitals
		SELECT	t.HospitalId ,
				t.Address1 ,
				t.Address2 ,
				t.City ,
				t.ContactPhoneNumber ,
				t.CountryId ,
				t.FriendlyHospitalName ,
				t.SubDomain ,
				t.HospitalLogoUrl ,
				t.HospitalName ,
				t.HospitalPictureUrl ,
				t.Latitude ,
				t.LongDescription ,
				t.Longitude ,
				t.PostalCode ,
				t.ProvinceId ,
				t.ShortDescription ,
				t.SocialMediaLinks ,
				t.Website ,
				t.MarketID ,
				t.DirectorName
		FROM	##Hospitals t
		LEFT JOIN OPENQUERY(UWXRUDYEZS , 'SELECT * FROM Hospitals') s ON s.HospitalId = t.HospitalId
		WHERE	s.HospitalId IS NULL;

	--deleting old values
		DELETE	t
		FROM	UWXRUDYEZS...Hospitals t
		LEFT JOIN ##Hospitals s ON s.HospitalId = t.HospitalId
		WHERE	s.HospitalId IS NULL OR t.HospitalName = '0';

--INSERT	INTO UWXRUDYEZS...IPAddresses
--SELECT	IPId ,
--		IPAddressFrom ,
--		IPAddressTo ,
--		MarketId ,
--		PostalCode
--FROM	##IPAddresses;

		DELETE	UWXRUDYEZS...Celebrities;
		INSERT	INTO UWXRUDYEZS...Celebrities
		SELECT	CelebrityId ,
				Bio ,
				FirstName ,
				LastName ,
				PhotoUrl ,
				Profession ,
				SocialMediaLinks ,
				Website
		FROM	##Celebrities;
		
		--DELETE	UWXRUDYEZS...MiracleMillionClub;
		--INSERT	INTO UWXRUDYEZS...MiracleMillionClub
		--SELECT	MiracleMillionId ,
		--		FundraisingEntityId ,
		--		FundraisingYear ,
		--		Amount
		--FROM	##MiracleMillion;

		DELETE	UWXRUDYEZS...RadioStations;
		INSERT	INTO UWXRUDYEZS...RadioStations
		SELECT	RadioStationId ,
				CallLetters ,
				Address1 ,
				Address2 ,
				CountryId ,
				MarketTag ,
				MarketId ,
				[Owner] ,
				Frequency
		FROM	##RadioStations;


		--DELETE	UWXRUDYEZS...HospitalSocialMedia;
		--INSERT	INTO UWXRUDYEZS...HospitalSocialMedia
		--SELECT	Id ,
		--		HospitalId ,
		--		SiteId ,
		--		CategoryId ,
		--		Link ,
		--		[Description]
		--FROM	##HospitalSocialMedia;

		DELETE	UWXRUDYEZS...Television;
		INSERT	INTO UWXRUDYEZS...Television
		SELECT	TVStationId ,
				CallLetters ,
				Affiliation ,
				Address1 ,
				Address2 ,
				City ,
				ProvinceId ,
				PostalCode ,
				CountryId ,
				Website ,
				MarketId ,
				StationOwner ,
				MarketTag
		FROM	##Television;

--Markets
		DELETE	UWXRUDYEZS...Markets;
		INSERT	INTO UWXRUDYEZS...Markets
		SELECT	t.MarketId ,
				t.MarketName ,
				t.CountryId ,
				t.RegionId ,
				t.Active ,
				t.LegacyMarketId
		FROM	##Markets t;

--SubMarkets
		DELETE	UWXRUDYEZS...SubMarkets;
		INSERT	INTO UWXRUDYEZS...SubMarkets
		SELECT	t.CountryId ,
				t.CountryName ,
				t.MarketId ,
				t.MarketName ,
				t.RegionId ,
				t.RegionName ,
				ISNULL(t.SubMarketId , t.MarketId) ,
				ISNULL(t.SubMarketName , t.MarketName) ,
				t.Active
		FROM	##SubMarkets t;

--PostalCodes
		DELETE FROM UWXRUDYEZS...PostalCodes WHERE PostalCode IN (SELECT DISTINCT PostalCode FROM ##PostalCodes)
		INSERT	INTO UWXRUDYEZS...PostalCodes
		SELECT DISTINCT
				pc.PostalCode ,
				pc.MarketId ,
				pc.SubMarketId ,
				pc.ProvinceId ,
				pc.CountryId ,
				pc.City ,
				pc.County
		FROM	##PostalCodes pc


--Cleaning Up
		IF OBJECT_ID('tempdb..##Champions' , 'U') IS NOT NULL
			DROP TABLE ##Champions;
		IF OBJECT_ID('tempdb..##FundraisingEntities' , 'U') IS NOT NULL
			DROP TABLE ##FundraisingEntities;
	--IF OBJECT_ID('tempdb..##IPAddresses' , 'U') IS NOT NULL
	--	DROP TABLE ##IPAddresses;
		IF OBJECT_ID('tempdb..##Hospitals' , 'U') IS NOT NULL
			DROP TABLE ##Hospitals;
		IF OBJECT_ID('tempdb..##Celebrities' , 'U') IS NOT NULL
			DROP TABLE ##Celebrities;
		IF OBJECT_ID('tempdb..##RadioStations' , 'U') IS NOT NULL
			DROP TABLE ##RadioStations;
		IF OBJECT_ID('tempdb..##HospitalSocialMedia' , 'U') IS NOT NULL
			DROP TABLE ##HospitalSocialMedia;
		IF OBJECT_ID('tempdb..##Television' , 'U') IS NOT NULL
			DROP TABLE ##Television;
		IF OBJECT_ID('tempdb..##SubMarkets' , 'U') IS NOT NULL
			DROP TABLE ##SubMarkets;
		--IF OBJECT_ID('tempdb..##MiracleMillion' , 'U') IS NOT NULL
		--	DROP TABLE ##MiracleMillion;
		IF OBJECT_ID('tempdb..##Markets' , 'U') IS NOT NULL
			DROP TABLE ##Markets;
		IF OBJECT_ID('tempdb..##PostalCodes' , 'U') IS NOT NULL
			DROP TABLE ##PostalCodes;
	END;



















