﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new Campaign.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertCampaign]
	@CampaignName VARCHAR(300),
	@ShortDescription VARCHAR(300),
	@LongDescription VARCHAR(500),
	@CampaignTypeId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Campaigns] ( 
		CampaignName,
	    ShortDescription,
	    LongDescription,
	    CampaignTypeId
	)
	VALUES  ( 
		@CampaignName,
	    @ShortDescription,
	    @LongDescription,
	    @CampaignTypeId
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END


