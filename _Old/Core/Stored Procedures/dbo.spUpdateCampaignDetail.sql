﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/20/2015
-- Description:	This procedure is used to update an existing CampaignDetail record.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCampaignDetail]
	@CampaignDetailsId int,
	@CampaignId int,
	@CampaignDetailName varchar(300),
	@ShortDescription varchar(300),
	@LongDescription varchar(500),
	@CampaignYear int,
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
	UPDATE [dbo].[CampaignDetails] SET
		[CampaignId] = @CampaignId,
	    [CampaignDetailName] = @CampaignDetailName,
	    [ShortDescription] = @ShortDescription,
	    [LongDescription] = @LongDescription,
	    [CampaignYear] = @CampaignYear,
	    [StartDate] = @StartDate,
	    [EndDate] = @EndDate
	WHERE [CampaignDetailsId] = @CampaignDetailsId;
END

