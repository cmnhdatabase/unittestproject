﻿
CREATE PROCEDURE [dbo].[delspDisbursedToBeInvoiced]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @year INT = YEAR(GETDATE()) ,
    @qrtr INT = 2;
--	(SELECT TOP 1 dp.DisbursementQuarter 
--FROM dbo.InvoicedDisbursements id INNER JOIN dbo.Disbursements d ON d.DisbursementId = id.DisbursementId
--	INNER JOIN dbo.DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
--ORDER BY id.InvoiceId desc) + 1;

WITH    inv
          AS ( SELECT   d.MarketId ,
                        SUM(d.Amount) Amount
               FROM     dbo.Disbursements d
                        INNER JOIN Core.dbo.CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                        INNER JOIN Core.dbo.Campaigns c ON cd.CampaignId = c.CampaignId
                        INNER JOIN Core.dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                        INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
               WHERE    d.FundraisingYear <= @year
                        AND db.DisbursementQuarter <= @qrtr
                        AND ct.CampaignTypeId NOT IN ( 27, 32, 18, 11 )--Dont pull Telethon, Radiothon or Local
                        AND d.FundraisingYear > 2014
                        AND d.RecordTypeId = 1
                        AND FundraisingEntityId != 56
                        AND db.DisbursementPeriodId != 0
                        AND ( d.DisbursementId NOT IN (
                              SELECT    DisbursementId
                              FROM      dbo.InvoicedDisbursements ) 
--remove this part this is only too compare q2 numbers
                              OR db.DisbursementQuarter = 2
                            )
               GROUP BY d.MarketId
             ),
        disb
          AS ( SELECT   m.MarketId ,
                        m.MarketName ,
                        SUM(CASE WHEN d.RecordTypeId = 1 THEN d.Amount
                                 ELSE 0
                            END) Amount ,
                        SUM(CASE WHEN d.RecordTypeId = 2 THEN d.Amount
                                 ELSE 0
                            END) Expense
               FROM     dbo.Disbursements d
                        INNER JOIN dbo.DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
                        INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
                        INNER JOIN dbo.Batches b ON b.BatchId = d.BatchId
               WHERE    FundraisingYear = @year
                        AND dp.DisbursementQuarter = @qrtr
                        AND b.BatchTypeId = 1
               GROUP BY m.MarketId ,
                        m.MarketName
             )
    SELECT  disb.MarketId, disb.MarketName, inv.Amount AS ToBeInvoiced, disb.Amount AS Disbursed, Expense
    FROM    inv
            INNER JOIN disb ON disb.MarketId = inv.MarketId
    ORDER BY 2;
END

