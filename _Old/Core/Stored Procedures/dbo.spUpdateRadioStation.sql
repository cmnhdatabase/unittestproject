﻿

-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/20/2015
-- Description:	This procedure is used to update an existing RadioStation record.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateRadioStation]
	@RadioStationId int,
	@CallLetters varchar(10),
	@MarketId int,
	@Owner varchar(100),
	@Frequency varchar(10),
	@MarketTag varchar(50),
	@MarketArea varchar(50),
	@YearStarted smallint,
	@Director int,
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId int,
	@PostalCode varchar(10),
	@CountryId int,
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[RadioStations] SET
		[CallLetters] = @CallLetters,
		[MarketId] = @MarketId,
		[Owner] = @Owner,
		[Frequency] = @Frequency,
		[MarketTag] = @MarketTag,
		[MarketArea] = @MarketArea,
		[YearStarted] = @YearStarted,
		[Director] = @Director,
		[Address1] = @Address1,
		[Address2] = @Address2,
		[City] = @City,
		[ProvinceId] = @ProvinceId,
		[PostalCode] = @PostalCode,
		[CountryId] = @CountryId,
		[Note] = @Note,
		[ModifiedBy] = @ModifiedBy,
		[ModifiedDate] = GETDATE(),
		[Active] = @Active
	WHERE [RadioStationId] = @RadioStationId;
END

