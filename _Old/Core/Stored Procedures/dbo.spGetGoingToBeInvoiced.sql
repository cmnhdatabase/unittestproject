﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetGoingToBeInvoiced]
-- Add the parameters for the stored procedure here
--@yr INT, @qrtr INT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @yr         INT,
                @qrtr       INT,
                @invoicable INT = 0;

        --Get the last quarter invoiced
        SET @yr = ( SELECT TOP 1 CASE
                                     WHEN InvoiceQuarter = 4
                                     THEN InvoiceYear + 1
                                     ELSE InvoiceYear
                                 END
                    FROM dbo.Invoices
                    ORDER BY InvoiceYear DESC,
                             InvoiceQuarter DESC );
        SET @qrtr = ( SELECT TOP 1 CASE
                                       WHEN InvoiceQuarter = 4
                                       THEN 1
                                       ELSE InvoiceQuarter + 1
                                   END
                      FROM dbo.Invoices
                      ORDER BY InvoiceYear DESC,
                               InvoiceQuarter DESC );
        IF @yr IS NULL
            BEGIN
                SET @yr = 2015;
                SET @qrtr = 1;
            END;
        IF @qrtr != DATEPART(q, GETDATE())
        SET @invoicable = 1;
        SELECT MarketName,
               FundraisingYear,
               SUM(CASE
                       WHEN DirectToHospital = 1
                        AND CategoryId != 3
                       THEN amount
                       ELSE 0
                   END) AS DirectToHospital,
               SUM(CASE
                       WHEN DirectToHospital = 0
                        AND CategoryId != 3
                       THEN amount
                       ELSE 0
                   END) AS ThroughCMN,
               SUM(CASE
                       WHEN CategoryId != 3
                       THEN amount
                       ELSE 0
                   END) AS Total,
                           CASE
                               WHEN CountryId = 1
                               THEN .055
                               ELSE .055
                           END AS InvoiceRate,
               SUM(CASE
                       WHEN CategoryId != 3
                       THEN amount
                       ELSE 0
                   END) * CASE
                              WHEN CountryId = 1
                              THEN .055
                              ELSE .055
                          END AS TotalInvoiced,
               @yr AS InvoicingYear,
               @qrtr AS InvoicingQuarter,
               @invoicable AS CanInvoice
        FROM(
            SELECT 1 AS CategoryId,
                   'National Corporate Partners' AS Category,
                   FundraisingYear,
                   dp.DisbursementQuarter,
                   dp.DisbursementYear,
                   d.FundraisingEntityId,
                   p.DisplayName,
                   m.MarketId,
                   m.Marketname,
                   d.CampaignDetailsId,
                   cd.CampaignDetailName,
                   c.CampaignName,
                   ct.CampaignTypeId,
                   ct.CampaignType,
                   pp.DisplayName ProgramDisplayName,
                   CurrencyTypeId,
                   Amount,
                   FundTypeId,
                   RecordTypeId,
                   NULL AS PledgeTypeId,
                   DirectToHospital,
                   m.RegionId,
                   l.LocationId,
                   m.CountryId
            FROM Disbursements d
                 INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
                 INNER JOIN Markets m ON d.MarketId = m.MarketId
                 INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
                 INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                 INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                 INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                 LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
                 LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
                 LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
                 INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
                 LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                                AND d.FundraisingEntityId = l.FundraisingEntityId
                 INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
            WHERE RecordTypeId = 1
              AND d.FundraisingEntityId NOT IN( 56, 20, 21 )
              AND d.FundraisingYear = @yr
              AND DATEPART(q, d.DateReceived) = @qrtr
              AND ct.CampaignTypeId NOT IN( 27, 32, 18, 11 )
              AND d.FundraisingYear > 2014
            UNION ALL
            SELECT 1,
                   'National Corporate Partners' AS Category,
                   FundraisingYear,
                   dp.DisbursementQuarter,
                   dp.DisbursementYear,
                   d.FundraisingEntityId,
                   p.DisplayName,
                   m.MarketId,
                   m.Marketname,
                   d.CampaignDetailsId,
                   cd.CampaignDetailName,
                   c.CampaignName,
                   ct.CampaignTypeId,
                   ct.CampaignType,
                   pp.DisplayName,
                   CurrencyTypeId,
                   Amount,
                   FundTypeId,
                   RecordTypeId,
                   NULL AS PledgeTypeId,
                   DirectToHospital,
                   m.RegionId,
                   l.LocationId,
                   m.CountryId
            FROM Disbursements d
                 INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
                 INNER JOIN Markets m ON d.MarketId = m.MarketId
                 INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
                 INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                 INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                 INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                 LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
                 LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
                 LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
                 INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
                 LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                                AND d.FundraisingEntityId = l.FundraisingEntityId
                 INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
            WHERE RecordTypeId = 1
              AND ( p.FundraisingEntityId IN( 20, 21 )
                AND ( cp.CampaignTypeFundraisingEntityId IS NULL
                   OR ( ps.EndDate IS NOT NULL
                    AND YEAR(ps.EndDate) <= d.FundraisingYear
                      )
                    )
                  )
              AND d.FundraisingYear = @yr
              AND DATEPART(q, d.DateReceived) = @qrtr
              AND ct.CampaignTypeId NOT IN( 27, 32, 18, 11 )
              AND d.FundraisingYear > 2014
            UNION ALL
            SELECT 2,
                   'National Programs and Events' AS Category,
                   FundraisingYear,
                   dp.DisbursementQuarter,
                   dp.DisbursementYear,
                   d.FundraisingEntityId,
                   p.DisplayName,
                   m.MarketId,
                   m.Marketname,
                   d.CampaignDetailsId,
                   cd.CampaignDetailName,
                   c.CampaignName,
                   ct.CampaignTypeId,
                   ct.CampaignType,
                      CASE
                          WHEN pp.DisplayName LIKE '%Radiothon'
                          THEN 'Radiothon Announced'
                          ELSE pp.DisplayName
                      END AS programDisplayName,
                   CurrencyTypeId,
                   Amount,
                   FundTypeId,
                   RecordTypeId,
                   NULL AS PledgeTypeId,
                   DirectToHospital,
                   m.RegionId,
                   l.LocationId,
                   m.CountryId
            FROM Disbursements d
                 INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
                 INNER JOIN Markets m ON d.MarketId = m.MarketId
                 INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
                 INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                 INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                 INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                 LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
                 LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
                 LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
                 INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
                 LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                                AND d.FundraisingEntityId = l.FundraisingEntityId
                 INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
            WHERE RecordTypeId = 1
              AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
                AND ( ps.EndDate IS NULL
                   OR 

                      /*y(ear(ps.EndDate) >= d .FundraisingYear AND disbursementdate != '1900-01-01')))*/

                      ( YEAR(ps.EndDate) - 1 >= d.FundraisingYear

                      /*AND disbursementdate != '1900-01-01'*/

                      )
                    )
                  )
              AND NOT( c.CampaignTypeId IN( 27, 32 )
                   AND d.FundraisingYear >= 2013
                     )
              AND d.FundraisingYear = @yr
              AND DATEPART(q, d.DateReceived) = @qrtr
              AND ct.CampaignTypeId NOT IN( 27, 32, 18, 11 )
              AND d.FundraisingYear > 2014
                               
                 /*-pledge data for ntl programs*/

            UNION ALL
            SELECT 4,
                   'Overlap' AS Category,
                   FundraisingYear,
                   dp.DisbursementQuarter,
                   dp.DisbursementYear,
                   d.FundraisingEntityId,
                   p.DisplayName,
                   m.MarketId,
                   m.Marketname,
                   d.CampaignDetailsId,
                   cd.CampaignDetailName,
                   c.CampaignName,
                   ct.CampaignTypeId,
                   ct.CampaignType,
                   pp.DisplayName,
                   CurrencyTypeId,
                   Amount * -1,
                   FundTypeId,
                   RecordTypeId,
                   NULL AS PledgeTypeId,
                   DirectToHospital,
                   m.RegionId,
                   l.LocationId,
                   m.CountryId
            FROM Disbursements d
                 INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
                 INNER JOIN Markets m ON d.MarketId = m.MarketId
                 INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
                 INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                 INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                 INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                 LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
                 LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
                 LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
                 INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
                 LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
                                                AND d.FundraisingEntityId = l.FundraisingEntityId
                 INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
            WHERE RecordTypeId = 1
              AND d.FundraisingEntityId NOT IN( 56, 20, 21 ) 

                  /*---------------------update below to use disbursementdate*/

              AND ( cp.FundraisingEntityId IS NOT NULL
                AND ( ps.EndDate IS NULL
                   OR ( YEAR(ps.EndDate) - 1 >= d.FundraisingYear
                    AND disbursementdate != '1900-01-01'
                      )
                    )
                  )
              AND d.FundraisingYear = @yr
              AND DATEPART(q, d.DateReceived) = @qrtr
              AND ct.CampaignTypeId NOT IN( 27, 32, 18, 11 )
              AND d.FundraisingYear > 2014
            UNION ALL
            SELECT 2,
                   'National Programs and Events' AS Category,
                   FundraisingYear,
                   d.Quarter,
                   YEAR(d.PledgeDate) AS DisbYear,
                   d.FundraisingEntityId,
                   p.DisplayName,
                   m.MarketId,
                   m.Marketname,
                   d.CampaignDetailsId,
                   cd.CampaignDetailName,
                   c.CampaignName,
                   ct.CampaignTypeId,
                   ct.CampaignType,
                      CASE
                          WHEN pp.DisplayName LIKE '%Radiothon'
                          THEN 'Radiothon Announced'
                          ELSE pp.DisplayName
                      END AS DisplayName,
                   CurrencyTypeId,
                   Amount,
                   NULL,
                   NULL,
                   PledgeTypeId,
                   directtohospital,
                   m.RegionId,
                   NULL,
                   m.CountryId
            FROM PledgeData d
                 INNER JOIN Markets m ON d.MarketId = m.MarketId
                 INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
                 INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                 INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
                 INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                 LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
                 LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
                 LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
                 LEFT OUTER JOIN invoicedpledges i ON d.PledgeId = i.pledgeid
            WHERE pledgetypeid IN( 1, 2 )
              AND d.FundraisingYear = @yr
              AND d.Quarter = @qrtr
              AND ct.CampaignTypeId IN( 27, 32 )--Only pull Telethon & Radiothon
              AND d.PledgeTypeId <= 2 --Only get hospital announced radiothons and telethons.
              AND m.CountryId = 1
              AND d.FundraisingYear > 2014 ) t
        GROUP BY t.MarketId,
                 MarketName,
                 t.FundraisingYear, -- category, 
                 CountryId
        ORDER BY 1;
    END;

