﻿
CREATE PROCEDURE [dbo].[spDisbursedToBeInvoiced]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        DECLARE @year INT = ( SELECT TOP 1
                                        CASE WHEN InvoiceQuarter = 4
                                             THEN InvoiceYear + 1
                                             ELSE InvoiceYear
                                        END
                              FROM      dbo.Invoices
                              ORDER BY  InvoiceYear DESC ,
                                        InvoiceQuarter DESC
                            ) ,
            @qrtr INT = ( SELECT TOP 1
                                    CASE WHEN InvoiceQuarter = 4 THEN 1
                                         ELSE InvoiceQuarter + 1
                                    END
                          FROM      dbo.Invoices
                          ORDER BY  InvoiceYear DESC ,
                                    InvoiceQuarter DESC
                        );

        WITH    inv
                  AS ( SELECT   d.MarketId ,
                                m.MarketName ,
                                SUM(d.Amount) Amount
                       FROM     dbo.Disbursements d
                                INNER JOIN Core.dbo.CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
                                INNER JOIN Core.dbo.Campaigns c ON cd.CampaignId = c.CampaignId
                                INNER JOIN Core.dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
                                INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
                                INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
                       WHERE    d.FundraisingYear <= @year
                                AND db.DisbursementQuarter <= @qrtr
                                AND ct.CampaignTypeId NOT IN ( 27, 32, 18, 11 )--Dont pull Telethon, Radiothon or Local
                                AND d.FundraisingYear > 2014
                                AND d.RecordTypeId = 1
                                AND d.FundraisingEntityId != 56
                                AND (db.DisbursementPeriodId != 0 OR d.DirectToHospital = 1)
                                AND ( d.DisbursementId NOT IN (
                                      SELECT    DisbursementId
                                      FROM      dbo.InvoicedDisbursements ) 
--remove this part this is only too compare q2 numbers
                              --OR db.DisbursementQuarter = 2
                                      )
                       GROUP BY d.MarketId ,
                                m.MarketName
                     ),
                disb
                  AS ( SELECT   m.MarketId ,
                                m.MarketName ,
                                SUM(CASE WHEN d.RecordTypeId = 1 THEN d.Amount
                                         ELSE 0
                                    END) Amount ,
                                SUM(CASE WHEN d.RecordTypeId = 2 THEN d.Amount
                                         ELSE 0
                                    END) Expense
                       FROM     dbo.Disbursements d
                                INNER JOIN dbo.DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
                                INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
                                LEFT OUTER JOIN dbo.Batches b ON b.BatchId = d.BatchId
                       WHERE    FundraisingYear = @year
                                AND dp.DisbursementQuarter = @qrtr
                        --AND b.BatchTypeId = 1
                       GROUP BY m.MarketId ,
                                m.MarketName
                     )
            SELECT  ISNULL(disb.MarketId, inv.MarketId) AS MarketId ,
                    ISNULL(disb.MarketName, inv.MarketName) AS MarketName ,
                    inv.Amount AS ToBeInvoiced ,
                    disb.Amount AS Disbursed ,
                    Expense
            FROM    inv
                    FULL OUTER JOIN disb ON disb.MarketId = inv.MarketId
            ORDER BY 2;
    END;

