﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new location.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertLocation]
@FundraisingEntityId int ,
          @LocationName VARCHAR(100),
          @LocationNumber VARCHAR(100),
          @Address1 VARCHAR(100),
          @Address2 VARCHAR(100),
          @City VARCHAR(100),
          @ProvinceId int,
          @PostalCode VARCHAR(10),
          @CountryId int,
          @PropertyTypeId int,
          @Latitude float,
          @Longitude float,
          @Active BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.Locations
        ( FundraisingEntityId ,
          LocationName ,
          LocationNumber ,
          Address1 ,
          Address2 ,
          City ,
          ProvinceId ,
          PostalCode ,
          CountryId ,
          PropertyTypeId ,
          Latitude ,
          Longitude ,
          Active
        )
	VALUES  ( @FundraisingEntityId ,
			  @LocationName ,
			  @LocationNumber ,
			  @Address1 ,
			  @Address2 ,
			  @City ,
			  @ProvinceId ,
			  @PostalCode ,
			  @CountryId ,
			  @PropertyTypeId ,
			  @Latitude ,
			  @Longitude ,
			  @Active
			)
END


