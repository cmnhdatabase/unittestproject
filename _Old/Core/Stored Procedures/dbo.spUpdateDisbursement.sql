﻿-- =============================================
-- Author:           Joel Nelson
-- Create date: 1 Sept 2015
-- Modified:  21 July 2015
-- Description:      Modifies a Disbursement record with the specified information
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateDisbursement]
       @DisbursementId AS INT
       ,@CampaignDetailsId AS INT NULL
       ,@LocationId AS INT NULL
       ,@FundTypeId AS INT NULL
       ,@CampaignPeriod AS INT NULL
       ,@Comment AS VARCHAR(MAX) NULL
       ,@FundraisingCategoryId AS INT NULL
       ,@AssociateId AS INT NULL
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
       SET NOCOUNT ON;

       UPDATE [dbo].[Disbursements]
              SET [CampaignDetailsId] = CASE @CampaignDetailsId WHEN 0 THEN [CampaignDetailsId] ELSE @CampaignDetailsId END
                     ,[LocationId] = CASE @LocationId WHEN 0 THEN [LocationId] ELSE @LocationId END
                     ,[FundTypeId] = CASE @FundTypeId WHEN 0 THEN [FundTypeId] ELSE @FundTypeId END
                     ,[CampaignPeriod] = CASE @CampaignPeriod WHEN 0 THEN [CampaignPeriod] ELSE @CampaignPeriod END
                     ,[Comment] = CASE @Comment WHEN '*blank' THEN NULL ELSE LTRIM(RTRIM(CONCAT([Comment], ' ', ISNULL(@Comment, '')))) END
                     ,[FundraisingCategoryId] = CASE @FundraisingCategoryId WHEN 0 THEN [FundraisingCategoryId] ELSE @FundraisingCategoryId END
              WHERE [disbursementid] = @DisbursementId

       UPDATE [dbo].[DonorInfo]
              SET [AssociateId] = CASE @AssociateId WHEN 0 THEN [AssociateId] ELSE @AssociateId END
              WHERE [disbursementid] = @DisbursementId
END
