﻿
-- ==========================================================================================
-- = Update stored procedures
-- ==========================================================================================

-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/12/2015
-- Description:	This procedure is used to add a new DanceMarathon record.
-- Updates    : 08/12/2015 - Remove HospitalContactId
--            : 10/27/2015 - Changed @SubmittedBy & @ManagerId to INT
--            : 12/10/2015 - Added RegistrationUrl and DonationUrl
--            : 06/21/2016 - Add HasMiniMarathons, MiniMarathonCount, MiniMarathonContribution, DanceMarathonTypeId and Nickname
--
-- =============================================
CREATE PROCEDURE [dbo].[spInsertDanceMarathon]
	@CampaignDetailsId int,
	@SchoolId int,
	@TotalHours int,
	@Shifts int,
	@AnticipatedParticipants int,
	@TotalParticipants int,
	@ContactFirstName varchar(50),
	@ContactLastName varchar(50),
	@ContactEmail varchar(100),
	@GrossTotal money,
	@ToteBoardImagePath varchar(500),
	@EventId int,
	@Note varchar(MAX),
	@SubmittedBy INT,
	@Active bit,
	@ManagerId INT,
	@RegistrationUrl varchar(1000),
	@DonationUrl varchar(1000),
	@HasMiniMarathons bit,
	@MiniMarathonCount INT,
	@MiniMarathonContribution money,
	@DanceMarathonTypeId int,
	@Nickname varchar(300)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[DanceMarathons] (
		[CampaignDetailsId],
		[SchoolId],
		[TotalHours],
		[Shifts],
		[AnticipatedParticipants],
		[TotalParticipants],
		[ContactFirstName],
		[ContactLastName],
		[ContactEmail],
		[GrossTotal],
		[ToteBoardImagePath],
		[EventId],
		[Note],
		[SubmittedBy],
		[SubmittedDate],
		[ModifiedBy],
		[ModifiedDate],
		[Active],
		[ManagerId],
		[RegistrationUrl],
		[DonationUrl],
		[HasMiniMarathons],
		[MiniMarathonCount],
		[MiniMarathonContribution],
		[DanceMarathonTypeId],
		[Nickname]
	)
	VALUES (
		@CampaignDetailsId,
		@SchoolId,
		@TotalHours,
		@Shifts,
		@AnticipatedParticipants,
		@TotalParticipants,
		@ContactFirstName,
		@ContactLastName,
		@ContactEmail,
		@GrossTotal,
		@ToteBoardImagePath,
		@EventId,
		@Note,
		@SubmittedBy,
		GETDATE(),
		@SubmittedBy,
		GETDATE(),
		@Active,
		@ManagerId,
		@RegistrationUrl,
		@DonationUrl,
		@HasMiniMarathons,
		@MiniMarathonCount,
		@MiniMarathonContribution,
		@DanceMarathonTypeId,
		@Nickname
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END
