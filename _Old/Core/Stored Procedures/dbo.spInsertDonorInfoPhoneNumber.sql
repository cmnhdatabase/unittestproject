﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new DonorInfoPhoneNumber.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertDonorInfoPhoneNumber]
	@DonorInfoId INT, @PhoneId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		INSERT INTO dbo.DonorInfoPhoneNumbers
		        ( DonorInfoId, PhoneId )
		VALUES  ( @DonorInfoId, @PhoneId )
END


