﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateChampionStory] 
	-- Add the parameters for the stored procedure here
	@ChampionId int, @LanguageId int, @Story varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[ChampionStories]
	  SET [LanguageId] = @LanguageId
		,[Story] = @Story
	WHERE championid = @ChampionId AND LanguageId = @LanguageId
END
