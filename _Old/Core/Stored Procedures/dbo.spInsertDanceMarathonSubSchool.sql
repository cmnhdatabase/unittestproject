﻿/****** Object:  StoredProcedure [dbo].[spInsertDanceMarathonSubSchool] ******/
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/12/2015
-- Description:	This procedure is used to add a new DanceMarathonSubSchool record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertDanceMarathonSubSchool]
	@DanceMarathonId int,
	@SchoolId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[DanceMarathonSubSchools] (
		[DanceMarathonId],
		[SchoolId]
	)
	VALUES (
		@DanceMarathonId,
		@SchoolId
	);
END

