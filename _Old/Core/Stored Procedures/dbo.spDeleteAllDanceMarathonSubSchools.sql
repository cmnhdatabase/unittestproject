﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/20/2015
-- Description:	Delete all DanceMarathonSubSchool records associated with the supplied Dance Marathon.
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteAllDanceMarathonSubSchools]
	@DanceMarathonId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM [dbo].[DanceMarathonSubSchools] 
	WHERE [DanceMarathonId] = @DanceMarathonId;
END

