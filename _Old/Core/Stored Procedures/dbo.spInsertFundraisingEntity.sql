﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 01/16/2015
-- Description:	This procedure is used to add a new Fundraising Entity.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertFundraisingEntity]
	@Name VARCHAR(100),
	@FriendlyName VARCHAR(100),
	@DisplayName VARCHAR(100),
	@YearStarted INT,
	@FundraisingCategoryId INT,
	@Website VARCHAR(255),
	@Active BIT,
	@CountryId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[FundraisingEntities] (
		[Name],
      	[FriendlyName],
      	[DisplayName],
      	[YearStarted],
      	[FundraisingCategoryId],
      	[Website],
      	[Active],
      	[SponsorId],
      	[CountryId] 
	)
	VALUES (
		@Name,
		@FriendlyName,
		@DisplayName,
		@YearStarted,
		@FundraisingCategoryId,
		@Website,
		@Active,
		NULL,
		@CountryId
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END


