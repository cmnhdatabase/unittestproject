﻿

/****** Object:  StoredProcedure [dbo].[spInsertPledgeData] ******/
-- =============================================
-- Author:           Ethan Tipton
-- Create date: 02/10/2015
-- Description:      This procedure is used to add a new PledgeData record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertPledgeData]
       @FundraisingEntityId int,
       @MarketId int,
       @FundraisingYear int,
       @CampaignDetailsId int,
       @CurrencyTypeId int,
       @Amount money,
       @PledgeTypeId int,
       @Quarter smallint,
       @PledgeDate date,
       @DirectToHospital bit,
       @CreatedBy int,
       @PledgeBatchId int = NULL
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
       SET NOCOUNT ON;

       INSERT INTO [dbo].[PledgeData] (
              [FundraisingEntityId],
              [MarketId],
              [FundraisingYear],
              [CampaignDetailsId],
              [CurrencyTypeId],
              [Amount],
              [PledgeTypeId],
              [Quarter],
              [PledgeDate],
              [DirectToHospital],
              [DateRecorded],
              [CreatedBy],
              [PledgeBatchId]
       )
       VALUES (
              @FundraisingEntityId,
              @MarketId,
              @FundraisingYear,
              @CampaignDetailsId,
              @CurrencyTypeId,
              @Amount,
              @PledgeTypeId,
              @Quarter,
              @PledgeDate,
              @DirectToHospital,
              GETDATE(),
              @CreatedBy,
              @PledgeBatchId
       )

       SELECT CAST(SCOPE_IDENTITY() AS INT);
END

