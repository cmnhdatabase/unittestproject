﻿
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to add a new TV record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertTvStation]
	@CallLetters varchar(10),
	@Affiliation varchar(10),
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId int,
	@PostalCode varchar(10),
	@CountryId int,
	@Website varchar(100),
	@MarketId int,
	@StationOwner varchar(100),
	@MarketTag varchar(100),
	@Director int,
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[TV] (
		[CallLetters],
		[Affiliation],
		[Address1],
		[Address2],
		[City],
		[ProvinceId],
		[PostalCode],
		[CountryId],
		[Website],
		[MarketId],
		[StationOwner],
		[MarketTag],
		[Director],
		[Note],
		[ModifiedBy],
		[ModifiedDate],
		[Active]
	)
	VALUES (
		@CallLetters,
		@Affiliation,
		@Address1,
		@Address2,
		@City,
		@ProvinceId,
		@PostalCode,
		@CountryId,
		@Website,
		@MarketId,
		@StationOwner,
		@MarketTag,
		@Director,
		@Note,
		@ModifiedBy,
		GETDATE(),
		@Active
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END

