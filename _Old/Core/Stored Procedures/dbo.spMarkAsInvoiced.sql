﻿
CREATE PROCEDURE [dbo].[spMarkAsInvoiced]
	-- Add the parameters for the stored procedure here
	--@year INT, @qrtr INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @year INT, @qrtr INT

	--Get the last quarter invoiced
	SET @year = (SELECT TOP 1 CASE WHEN InvoiceQuarter = 4 THEN InvoiceYear+1 ELSE InvoiceYear END
	FROM dbo.Invoices
	ORDER BY InvoiceYear DESC, InvoiceQuarter DESC)

	SET @qrtr = (SELECT TOP 1 CASE WHEN InvoiceQuarter = 4 THEN 1 ELSE InvoiceQuarter+1 END
	FROM dbo.Invoices
	ORDER BY InvoiceYear DESC, InvoiceQuarter DESC)

    IF @year IS NULL
    BEGIN
	   SET @year = 2015
	   SET @qrtr = 1
    END 


    DECLARE @id INT 

--Check if the Invoice entry exists already.
IF EXISTS(SELECT 1 FROM [dbo].[Invoices]
              WHERE [InvoiceYear] = @year
		    AND [InvoiceQuarter] = @qrtr) 
begin
    --Exists
    SET @id = (select InvoiceId from [dbo].[Invoices]
            WHERE [InvoiceYear] = @year
		  AND [InvoiceQuarter] = @qrtr )
END 
ELSE
BEGIN  
    --Doesn't
    INSERT INTO [dbo].[Invoices]
           ([InvoiceYear]
           ,[InvoiceQuarter]
           ,[InvoiceDate])
     VALUES
           (@year, @qrtr, GETDATE());
    SET @id = (SELECT SCOPE_IDENTITY())
END 

INSERT INTO [dbo].[InvoicedDisbursements]
           ([InvoiceId]
           ,[DisbursementId])
SELECT @id, d.DisbursementId
FROM	dbo.Disbursements d	   
    INNER JOIN Core.dbo.CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
    INNER JOIN Core.dbo.Campaigns c ON cd.CampaignId = c.CampaignId
    INNER JOIN Core.dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
    INNER JOIN Core.dbo.DisbursementPeriods db ON db.DisbursementPeriodId = d.DisbursementPeriodId
WHERE d.FundraisingYear <= @year
AND db.DisbursementQuarter <= @qrtr
AND ct.CampaignTypeId NOT IN (27,32,18,11)--Dont pull Telethon, Radiothon or Local
AND d.FundraisingYear > 2014
AND d.RecordTypeId = 1
AND FundraisingEntityId != 56
AND (db.DisbursementPeriodId != 0 OR d.DirectToHospital = 1)
AND d.DisbursementId NOT IN (SELECT DisbursementId FROM dbo.InvoicedDisbursements)

--SELECT SUM(d.Amount) 
--FROM dbo.Disbursements d INNER JOIN dbo.InvoicedDisbursements id ON id.DisbursementId = d.DisbursementId
--WHERE id.InvoiceId = 6
--AND d.MarketId = 22

--This part is only ran for US
INSERT INTO [dbo].[InvoicedPledges]
           ([InvoiceId]
           ,[PledgeId])
SELECT @id, d.PledgeId
FROM dbo.PledgeData d	   
    INNER JOIN Core.dbo.CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
    INNER JOIN Core.dbo.Campaigns c ON cd.CampaignId = c.CampaignId
    INNER JOIN Core.dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
    INNER JOIN  dbo.Markets m ON m.MarketId = d.MarketId
WHERE d.FundraisingYear <= @year
AND d.Quarter <= @qrtr
AND ct.CampaignTypeId IN (27,32)--Only pull Telethon & Radiothon
AND d.PledgeTypeId <= 2 --Only get hospital announced radiothons and telethons.
AND m.CountryId = 1
AND d.FundraisingYear > 2014
AND d.PledgeId NOT IN (SELECT PledgeId FROM dbo.InvoicedPledges)

END

