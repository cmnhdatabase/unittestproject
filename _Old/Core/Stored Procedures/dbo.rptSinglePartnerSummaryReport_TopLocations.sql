﻿







CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryReport_TopLocations]
(	@year     INT 
  , @partnerid INT 
  , @topLocationCount INT = 10
)
AS

BEGIN
	/***********	TEST SECTION	***********/
		--DECLARE @partnerid INT = 123, 
		--		@year      INT = 2014,
		--		@topLocationCount INT = 20

		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/
IF OBJECT_ID('tempdb..#tmpLocationInfo', 'U') IS NOT NULL DROP TABLE #tmpLocationInfo	

	DECLARE @prmYear	  INT = @year,
			@prmPartnerID INT = @partnerid


SELECT
	   l.LocationId AS LocationID
	 , l.LocationNumber
	 , m.MarketName AS 'Benefitting Market'
	 , SUM(CASE WHEN d.FundraisingYear = @prmYear - 3 THEN Amount ELSE 0 END)	AS CYminus3
	 , SUM(CASE WHEN d.FundraisingYear = @prmYear - 2 THEN Amount ELSE 0 END)	AS CYminus2
	 , SUM(CASE WHEN d.FundraisingYear = @prmYear - 1 THEN Amount ELSE 0 END)	AS CYminus1
	 , SUM(CASE WHEN d.FundraisingYear = @prmYear THEN Amount ELSE 0 END)		AS CY 
	 , UPPER(l.LocationName) AS 'Location Name'
	 , UPPER(l.city) AS 'Shipping City'
	 , p.Abbreviation AS 'State'
	 , l.PostalCode
INTO #tmpLocationInfo		    
FROM   Locations l
	INNER JOIN Disbursements d ON l.LocationId = d.LocationId
	INNER JOIN Provinces p ON  l.ProvinceId = p.ProvinceId
	INNER JOIN Markets m ON d.MarketId = m.MarketId
WHERE  d.fundraisingyear >= @prmYear - 3 
       AND d.FundraisingEntityId = @prmPartnerID 
GROUP BY l.LocationId,
l.LocationName, l.LocationNumber, l.City, p.Abbreviation, m.MarketName, l.PostalCode
ORDER BY CYminus1 DESC


DECLARE @GrandTotal MONEY = (SELECT SUM(CYminus1) FROM #tmpLocationInfo)
DECLARE @TopsTotal MONEY = (SELECT SUM(CYminus1) FROM (SELECT TOP (@topLocationCount) CYminus1 FROM #tmpLocationInfo ORDER BY CYminus1 DESC) tt )


SELECT TOP (@topLocationCount)
	 * 
	, @TopsTotal / NULLIF (@GrandTotal, 0) AS RankedPartnersPct
	, CAST(@TopsTotal/ NULLIF(@topLocationCount, 0) AS DECIMAL(18,2)) AS TopAverage
FROM #tmpLocationInfo AS tli
ORDER BY CYminus1 DESC

END




