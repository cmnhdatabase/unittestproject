﻿


CREATE PROCEDURE [dbo].[rptPvrProgramPopulationTotal]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 175
/******************/
IF OBJECT_ID('tempdb..#tempProgramPopulationTotal', 'U') IS NOT NULL
    DROP TABLE #tempProgramPopulationTotal

SELECT ((SUM(PartnerCYTotal) - SUM(PartnerCYminus1Total))/NULLIF(SUM(Counts),0)) AS CyIncrease,
((SUM(PartnerCYTotal)/NULLIF(SUM(Counts),0)) - (SUM(PartnerCYminus1Total)/NULLIF(SUM(Counts),0)))/NULLIF(SUM(PartnerCYminus1Total)/NULLIF(SUM(Counts),0),0) AS CYPercentIncrease,
(SUM(PartnerCYTotal)/NULLIF(SUM(Counts),0))/NULLIF(SUM(pop)/NULLIF(SUM(Counts),0),0) AS PerCap
INTO #tempProgramPopulationTotal
FROM (SELECT
fd.MarketId,
COUNT(DISTINCT fd.MarketId) AS counts,
SUM(CASE WHEN FundraisingYear = @year THEN amount ELSE 0 END) AS PartnerCYTotal,
SUM(CASE WHEN FundraisingYear = @year -1 THEN Amount END) AS PartnerCYminus1Total,
mpr.PopulationEstimate AS Pop
FROM dbo.vwFundraisingData fd
INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = fd.MarketId
WHERE FundraisingYear >= @year - 1
AND FundraisingCategoryId = 2
AND PopulationRank = (SELECT MAX(PopulationRank) FROM dbo.vwMarketPopulationRank WHERE MarketId = @market)
GROUP BY fd.MarketId, PopulationEstimate) AS a


SELECT *
FROM #tempProgramPopulationTotal







