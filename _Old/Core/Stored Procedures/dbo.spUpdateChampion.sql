﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spUpdateChampion
	-- Add the parameters for the stored procedure here
	@ChampionId int, @DisplayProvince varchar(50)
				,@FirstName varchar(50)
				,@LastName varchar(50)
				,@Age varchar(10)
				,@Illness varchar(100)
				,@Year int
				,@HospitalId int
				,@CountryId int
				,@BlogUri varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[Champions]
	  SET [DisplayProvince] = @DisplayProvince
		,[FirstName] = @FirstName
		,[LastName] = @LastName
		,[Age] = @Age
		,[Illness] = @Illness
		,[Year] = @Year
		,[HospitalId] = @HospitalId
		,[CountryId] = @CountryId
		,[BlogUri] = @BlogUri
	WHERE ChampionId = @ChampionId

END
