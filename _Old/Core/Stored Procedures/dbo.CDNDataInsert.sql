﻿



CREATE PROCEDURE [dbo].[CDNDataInsert] AS
BEGIN

Delete From CDN...Celebrities
Delete From CDN...FundraisingEntities
Delete From CDN...Champions
Delete From CDN...Hospitals

Insert Into CDN...Celebrities
Select
 CelebrityId
,FirstName
,LastName
,Content
,Bio
,Website
,Rank
,OldCelebrityID
From
Core.dbo.Celebrities
Where
CelebrityId not in (Select CelebrityId From CDN...Celebrities)
;

INSERT INTO CDN...FundraisingEntities
SELECT
FundraisingEntityId
,Name
,FriendlyName
,DisplayName
,YearStarted
,FundraisingCategoryId
,Website
,Active
,SponsorId
,CountryId
,SalesForceId
FROM
Core.dbo.FundraisingEntities
WHERE
FundraisingEntityId NOT IN (SELECT FundraisingEntityId FROM CDN...FundraisingEntities)
;

INSERT INTO CDN...Champions
SELECT
ChampionId
,DisplayProvince
,FirstName
,LastName
,Age
,Illness
,Year
,HospitalId
,CountryId
,BlogUri
FROM
Core.dbo.Champions
WHERE
ChampionId NOT IN (SELECT ChampionId FROM CDN...Champions)
;

INSERT INTO CDN...Hospitals
SELECT
HospitalName
,FriendlyHospitalName
,MarketId
,Address1
,Address2
,City
,ProvinceId
,PostalCode
,CountryId
,LongDescription
,ShortDescription
,Website
,HospitalId
,Latitude
,Longitude
,Active
,SalesForceId
FROM
Core.dbo.Hospitals
WHERE
HospitalId NOT IN (SELECT HospitalId FROM CDN...Hospitals)
;

DELETE FROM CMNHOSPITALS...cmn_Markets

INSERT INTO CMNHOSPITALS...cmn_Markets
SELECT
MarketId ,
MarketName ,
CountryId ,
RegionId ,
Active ,
LegacyMarketId
FROM
Core.dbo.Markets
;

DELETE FROM CMNHOSPITALS...cmn_Hospitals

INSERT INTO CMNHOSPITALS...cmn_Hospitals
SELECT
HospitalName
,FriendlyHospitalName
,MarketId
,Address1
,Address2
,City
,ProvinceId
,PostalCode
,CountryId
,LongDescription
,ShortDescription
,Website
,HospitalId
,Latitude
,Longitude
,Active
FROM
Core.dbo.Hospitals

END


