﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 12/16/2014
-- Description:	This will merge all disbursement information from our core database to the old Core database.
-- =============================================
CREATE PROCEDURE [dbo].[spMergeDisbursementInformation]
AS
--BEGIN
/*	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	--EXEC [dbo].[spMergeCampaignInfo]
	--EXEC [dbo].[spMergePartnerPrograms]
	--EXEC [dbo].[spMergeLocationInformation]

	SET IDENTITY_INSERT Core.dbo.BatchTypes ON

	MERGE Core.dbo.BatchTypes AS t
	USING core.dbo.BatchTypes AS s
	ON (
		t.BatchTypeId = s.BatchTypeId
	)
	WHEN NOT MATCHED
		THEN INSERT(BatchTypeId, BatchType, Abbreviation) 
		VALUES(s.BatchTypeId,s.BatchType,s.Abbreviation)
	WHEN MATCHED AND t.BatchType != s.BatchType OR t.abbreviation != s.abbreviation
		THEN UPDATE SET t.BatchType = s.BatchType, t.Abbreviation = s.Abbreviation
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.BatchTypes OFF

	--was
	--SET IDENTITY_INSERT Core.dbo.Batches ON

	--MERGE Core.dbo.Batches AS t
	--USING core.dbo.Batches AS s
	--ON (
	--	t.BatchId = s.BatchId
	--)
	--WHEN NOT MATCHED
	--	THEN INSERT(BatchId, BatchTypeId, BatchNumber, Reconciled, DateReconciled, Foundation) 
	--	VALUES(s.BatchId,s.BatchTypeId, s.BatchNumber, s.Reconciled, s.DateReconciled, s.Foundation)
	--WHEN MATCHED AND t.BatchNumber != s.BatchNumber OR t.Reconciled != s.Reconciled OR t.DateReconciled != s.DateReconciled OR t.Foundation != s.Foundation
	--	THEN UPDATE SET t.BatchNumber = s.BatchNumber, t.Reconciled = s.Reconciled, t.DateReconciled = s.DateReconciled, t.Foundation = s.Foundation
	--OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.Batches OFF

      SET IDENTITY_INSERT Core.dbo.Batches ON

      MERGE Core.dbo.Batches AS t
      USING core.dbo.Batches AS s
      ON (
            t.BatchId = s.BatchId
      )
      WHEN NOT MATCHED
            THEN INSERT(BatchId, BatchTypeId, BatchNumber, Reconciled, DateReconciled, Foundation) 
            VALUES(s.BatchId,s.BatchTypeId, s.BatchNumber, s.Reconciled, s.DateReconciled, s.Foundation)
      WHEN MATCHED AND t.BatchTypeId != s.BatchTypeId OR t.BatchNumber != s.BatchNumber OR t.Reconciled != s.Reconciled OR t.DateReconciled != s.DateReconciled OR t.Foundation != s.Foundation
            THEN UPDATE SET t.BatchTypeId = s.BatchTypeId, t.BatchNumber = s.BatchNumber, t.Reconciled = s.Reconciled, t.DateReconciled = s.DateReconciled, t.Foundation = s.Foundation
      OUTPUT $action, inserted.*, deleted.*;

      SET IDENTITY_INSERT Core.dbo.Batches OFF


	SET IDENTITY_INSERT Core.dbo.DisbursementDates ON

	MERGE Core.dbo.DisbursementDates AS t
	USING core.dbo.DisbursementDates AS s
	ON (
		t.DisbursementDateId = s.DisbursementDateId
	)
	WHEN NOT MATCHED
		THEN INSERT(DisbursementDateId, DisbursementDate) 
		VALUES(s.DisbursementDateId,s.DisbursementDate)
	WHEN MATCHED AND t.DisbursementDate != s.DisbursementDate
		THEN UPDATE SET t.DisbursementDate = s.DisbursementDate
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.DisbursementDates OFF

		SET IDENTITY_INSERT Core.dbo.DisbursementPeriods ON

	MERGE Core.dbo.DisbursementPeriods AS t
	USING core.dbo.DisbursementPeriods AS s
	ON (
		t.DisbursementPeriodId = s.DisbursementPeriodId
	)
	WHEN NOT MATCHED
		THEN INSERT(DisbursementPeriodId, DisbursementYear, DisbursementQuarter) 
		VALUES(s.DisbursementPeriodId,s.DisbursementYear, s.DisbursementQuarter)
	WHEN MATCHED AND t.DisbursementYear != s.DisbursementYear OR t.DisbursementQuarter != s.DisbursementQuarter
		THEN UPDATE SET t.DisbursementYear = s.DisbursementYear, t.DisbursementQuarter = s.DisbursementQuarter 
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.DisbursementPeriods OFF

	--was
	--SET IDENTITY_INSERT Core.dbo.Disbursements ON

	--MERGE Core.dbo.Disbursements AS t
	--USING core.dbo.Disbursements AS s
	--ON (
	--	t.DisbursementId = s.DisbursementId
	--)
	--WHEN NOT MATCHED
	--	THEN INSERT(DisbursementId,[RecordTypeId],[PartnerId],[ProgramEventId],[MarketId],[SubMarketId],[DirectToHospital],[FundraisingYear],[Amount],[CurrencyTypeId],[CampaignDetailsId],[LocationId],[DateReceived],[DateRecorded],[DonationDate],[FundTypeId],[BatchId],[CampaignPeriod],[UploadId],[Comment],[DisbursementPeriodId],[DisbursementDateId]) 
	--	VALUES(s.DisbursementId,s.[RecordTypeId],s.FundraisingEntityId, NULL,s.[MarketId],s.[SubMarketId],s.[DirectToHospital],s.[FundraisingYear],s.[Amount],s.[CurrencyTypeId],s.[CampaignDetailsId],s.[LocationId],s.[DateReceived],s.[DateRecorded],s.[DonationDate],s.[FundTypeId],s.[BatchId],s.[CampaignPeriod],s.[UploadId],s.[Comment],s.[DisbursementPeriodId],s.[DisbursementDateId])
	--WHEN MATCHED AND t.[RecordTypeId] != s.RecordTypeId OR t.[PartnerId] != s.FundraisingEntityId OR t.[MarketId] != s.MarketId OR t.[SubMarketId] != s.SubMarketId OR t.[DirectToHospital] != s.DirectToHospital
	--				OR t.[FundraisingYear] != s.FundraisingYear OR t.[Amount] != s.Amount OR t.[CurrencyTypeId] != s.CurrencyTypeId OR t.[CampaignDetailsId] != s.CampaignDetailsId OR t.[LocationId] != s.LocationId
	--				OR t.[DateReceived] != s.DateReceived OR t.[DateRecorded] != t.DateRecorded OR t.[DonationDate] != s.DonationDate OR t.[FundTypeId] != s.FundTypeId OR t.[BatchId] != s.BatchId OR t.[CampaignPeriod] != s.CampaignPeriod
	--				OR t.[UploadId] != s.UploadId OR t.[Comment] != s.Comment OR t.[DisbursementPeriodId] != s.DisbursementPeriodId OR t.[DisbursementDateId] != s.DisbursementDateId
	--	THEN UPDATE SET t.[RecordTypeId] = s.RecordTypeId, t.[PartnerId] = s.FundraisingEntityId, t.[MarketId] = s.MarketId, t.[SubMarketId] = s.SubMarketId, t.[DirectToHospital] = s.DirectToHospital,
	--				t.[FundraisingYear] = s.FundraisingYear, t.[Amount] = s.Amount, t.[CurrencyTypeId] = s.CurrencyTypeId, t.[CampaignDetailsId] = s.CampaignDetailsId, t.[LocationId] = s.LocationId,
	--				t.[DateReceived] = s.DateReceived, t.[DateRecorded] = t.DateRecorded, t.[DonationDate] = s.DonationDate, t.[FundTypeId] = s.FundTypeId, t.[BatchId] = s.BatchId, t.[CampaignPeriod] = s.CampaignPeriod,
	--				t.[UploadId] = s.UploadId, t.[Comment] = s.Comment, t.[DisbursementPeriodId] = s.DisbursementPeriodId, t.[DisbursementDateId] = s.DisbursementDateId
	----WHEN NOT MATCHED BY SOURCE
	----	THEN DELETE
	--OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.Disbursements OFF	

SET IDENTITY_INSERT Core.dbo.Disbursements ON

      MERGE Core.dbo.Disbursements AS t
      USING Core.dbo.Disbursements AS s
      ON ( t.DisbursementId = s.DisbursementId )
      WHEN NOT MATCHED THEN
        INSERT ( DisbursementId ,
                 [RecordTypeId] ,
                 [PartnerId] ,
                 [ProgramEventId] ,
                 [MarketId] ,
                 [SubMarketId] ,
                 [DirectToHospital] ,
                 [FundraisingYear] ,
                 [Amount] ,
                 [CurrencyTypeId] ,
                 [CampaignDetailsId] ,
                 [LocationId] ,
                 [DateReceived] ,
                 [DateRecorded] ,
                 [DonationDate] ,
                 [FundTypeId] ,
                 [BatchId] ,
                 [CampaignPeriod] ,
                 [UploadId] ,
                 [Comment] ,
                 [DisbursementPeriodId] ,
                 [DisbursementDateId]
               )
        VALUES ( s.DisbursementId ,
                 s.[RecordTypeId] ,
                 s.FundraisingEntityId ,
                 NULL ,
                 s.[MarketId] ,
                 s.[SubMarketId] ,
                 s.[DirectToHospital] ,
                 s.[FundraisingYear] ,
                 s.[Amount] ,
                 s.[CurrencyTypeId] ,
                 s.[CampaignDetailsId] ,
                 s.[LocationId] ,
                 s.[DateReceived] ,
                 s.[DateRecorded] ,
                 s.[DonationDate] ,
                 s.[FundTypeId] ,
                 s.[BatchId] ,
                 s.[CampaignPeriod] ,
                 s.[UploadId] ,
                 s.[Comment] ,
                 s.[DisbursementPeriodId] ,
                 s.[DisbursementDateId]
               )
      WHEN MATCHED AND ISNULL(t.[RecordTypeId], '') != ISNULL(s.RecordTypeId,'')
        OR ISNULL(t.[PartnerId], '') != ISNULL(s.FundraisingEntityId, '')
        OR ISNULL(t.[MarketId], '') != ISNULL(s.MarketId, '')
        OR ISNULL(t.[SubMarketId], '') != ISNULL(s.SubMarketId, '')
        OR ISNULL(t.[DirectToHospital], '') != ISNULL(s.DirectToHospital, '')
        OR ISNULL(t.[FundraisingYear], '') != ISNULL(s.FundraisingYear, '')
        OR ISNULL(t.[Amount], '') != ISNULL(s.Amount, '')
        OR ISNULL(t.[CurrencyTypeId], '') != ISNULL(s.CurrencyTypeId, '')
        OR ISNULL(t.[CampaignDetailsId], '') != ISNULL(s.CampaignDetailsId, '')
        OR ISNULL(t.[LocationId], '') != ISNULL(s.LocationId, '')
        OR ISNULL(t.[DateReceived], '') != ISNULL(s.DateReceived, '')
        OR ISNULL(t.[DateRecorded], '') != ISNULL(s.DateRecorded, '')
        OR ISNULL(t.[DonationDate], '') != ISNULL(s.DonationDate, '')
        OR ISNULL(t.[FundTypeId], '') != ISNULL(s.FundTypeId, '')
        OR ISNULL(t.[BatchId], '') != ISNULL(s.BatchId, '')
        OR ISNULL(t.[CampaignPeriod], '') != ISNULL(s.CampaignPeriod, '')
        OR ISNULL(t.[UploadId], '') != ISNULL(s.UploadId, '')
        OR ISNULL(t.[Comment], '') != ISNULL(s.Comment, '')
        OR ISNULL(t.[DisbursementPeriodId], '') != ISNULL(s.DisbursementPeriodId,'')
        OR ISNULL(t.[DisbursementDateId], '') != ISNULL(s.DisbursementDateId,'') THEN
        UPDATE SET
               t.[RecordTypeId] = s.RecordTypeId ,
               t.[PartnerId] = s.FundraisingEntityId ,
               t.[MarketId] = s.MarketId ,
               t.[SubMarketId] = s.SubMarketId ,
               t.[DirectToHospital] = s.DirectToHospital ,
               t.[FundraisingYear] = s.FundraisingYear ,
               t.[Amount] = s.Amount ,
               t.[CurrencyTypeId] = s.CurrencyTypeId ,
               t.[CampaignDetailsId] = s.CampaignDetailsId ,
               t.[LocationId] = s.LocationId ,
               t.[DateReceived] = s.DateReceived ,
               t.[DateRecorded] = s.DateRecorded ,
               t.[DonationDate] = s.DonationDate ,
               t.[FundTypeId] = s.FundTypeId ,
               t.[BatchId] = s.BatchId ,
               t.[CampaignPeriod] = s.CampaignPeriod ,
               t.[UploadId] = s.UploadId ,
               t.[Comment] = s.Comment ,
               t.[DisbursementPeriodId] = s.DisbursementPeriodId ,
               t.[DisbursementDateId] = s.DisbursementDateId
      --WHEN NOT MATCHED BY SOURCE
      --    THEN DELETE
      OUTPUT
        $action ,
        inserted.* ,
        deleted.*;

      SET IDENTITY_INSERT Core.dbo.Disbursements OFF  


	--SET IDENTITY_INSERT Core.dbo.DisbursementCheckNumbers ON

	MERGE Core.dbo.DisbursementCheckNumbers AS t
	USING core.dbo.DisbursementCheckNumbers AS s
	ON (
		t.DisbursementId = s.DisbursementId
	)
	WHEN NOT MATCHED
		THEN INSERT(DisbursementId, CheckNumberId) 
		VALUES(s.DisbursementId,s.CheckNumberId)
	WHEN MATCHED AND t.CheckNumberId != s.CheckNumberId
		THEN UPDATE SET t.CheckNumberId = s.CheckNumberId
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.DisbursementCheckNumbers OFF

	----Does this need to be done?  Not really using this field anywhere.
	--UPDATE Core.dbo.Disbursements
	--SET ProgramEventId = s.FundraisingEntityId
	--FROM Core.dbo.Disbursements INNER JOIN
	--		DisbursementSoftCredits AS s ON Core.dbo.Disbursements.DisbursementId = s.DisbursementId

	END TRY

			BEGIN CATCH
				SELECT 
					 ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;

				IF @@TRANCOUNT > 0
					BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Rollback'
					END
			END CATCH;

			IF @@TRANCOUNT > 0
				BEGIN
				COMMIT TRANSACTION;
				PRINT 'Commit'
			END*/
--END


