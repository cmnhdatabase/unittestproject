﻿
-- =============================================
-- Author	    : Ethan Tipton
-- Create date: 02/20/2015
-- Description:	This procedure is used to update an existing Radiothon.
-- Updates    : 08/12/2015 - Remove HospitalContactId, RadiothonContactId
--            : 06/21/2016 - Add ToteBoardImagePath
--
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateRadiothon]
	@RadiothonId int,
	@CampaignDetailsId int,
	@City varchar(100),
	@OnAirHours smallint,
	@OnAirMinutes smallint,
	@LanguageId int,
	@PhoneBank varchar(100),
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit,
	@EventId int,
	@RadiothonContactEmailSent bit,
	@ToteBoardImagePath varchar(500)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Radiothons] SET
		[CampaignDetailsId] = @CampaignDetailsId,
		[City] = @City,
		[OnAirHours] = @OnAirHours,
		[OnAirMinutes] = @OnAirMinutes,
		[LanguageId] = @LanguageId,
		[PhoneBank] = @PhoneBank,
		[Note] = @Note,
		[ModifiedBy] = @ModifiedBy,
		[ModifiedDate] = GETDATE(),
		[Active] = @Active,
		[EventId] = @EventId,
		[RadiothonContactEmailSent] = @RadiothonContactEmailSent,
		[ToteBoardImagePath] = @ToteBoardImagePath
	WHERE [RadiothonId] = @RadiothonId;
END
