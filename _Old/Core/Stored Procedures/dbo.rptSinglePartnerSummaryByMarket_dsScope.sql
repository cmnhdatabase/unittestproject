﻿


CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryByMarket_dsScope]
(
@year INT,
@partnerid NVARCHAR(MAX),
@marketid NVARCHAR(MAX)
)
AS
BEGIN


SELECT 'Total # of Locations' Description,
       MAX(CASE
               WHEN CampaignYear = @year - 3
               THEN mcnt
           END) cyminus3,
       MAX(CASE
               WHEN CampaignYear = @year - 2
               THEN mcnt
           END) cyminus2,
       MAX(CASE
               WHEN CampaignYear = @year - 1
               THEN mcnt
           END) cyminus1,
       MAX(CASE
               WHEN CampaignYear = @year
               THEN mcnt
           END) cy
FROM(
    SELECT CampaignYear,
        SUM(Locations) mcnt
    FROM PartnerMarketLocationTotalsByYear
    WHERE FundraisingEntityId = @partnerid
	   AND CampaignYear >= @year - 3
	   AND MarketId IN(
			 SELECT Item
			 FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
    GROUP BY CampaignYear
    UNION ALL
    SELECT
    YEAR(getdate()) AS CampaignYear,
    Count(l.LocationId) AS mcnt
    FROM
    dbo.Locations l
    JOIN dbo.vwLocationDetails vld ON vld.LocationId = l.LocationId
    WHERE
    l.FundraisingEntityId = @partnerid
    AND l.Active = 1
    AND vld.MarketId IN(
			 SELECT Item
			 FROM dbo.DelimitedSplitN4K( @marketid, ',' )))t
UNION ALL
SELECT '# of Fundraising Locations' Description,
       SUM(cyminus3),
       SUM(cyminus2),
       SUM(cyminus1),
       SUM(cy)
FROM( 
      SELECT locationid,
             CASE
                 WHEN cyminus3 IS NULL
                 THEN 0
                 ELSE 1
             END cyminus3,
                 CASE
                     WHEN cyminus2 IS NULL
                     THEN 0
                     ELSE 1
                 END cyminus2,
                     CASE
                         WHEN cyminus1 IS NULL
                         THEN 0
                         ELSE 1
                     END cyminus1,
                         CASE
                             WHEN cy IS NULL
                             THEN 0
                             ELSE 1
                         END cy
      FROM(
               SELECT vfd.locationid,
                      SUM(CASE
                              WHEN fundraisingyear = @year - 3
                              THEN amount
                          END) AS cyminus3,
                      SUM(CASE
                              WHEN fundraisingyear = @year - 2
                              THEN amount
                          END) AS cyminus2,
                      SUM(CASE
                              WHEN fundraisingyear = @year - 1
                              THEN amount
                          END) AS cyminus1,
                      SUM(CASE
                              WHEN fundraisingyear = @year
                              THEN amount
                          END) AS cy
               FROM dbo.vwFundraisingData vfd
			JOIN dbo.Locations l ON l.FundraisingEntityId = vfd.FundraisingEntityId AND l.LocationId  = vfd.locationid
               WHERE( fundraisingyear >= @year - 3 )
                AND ( vfd.FundraisingEntityId = @partnerid )
                AND MarketId IN(
                        SELECT Item
                        FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
               GROUP BY vfd.locationid ) AS t ) AS t
UNION ALL
SELECT '# of Benefiting Markets' Description,
       MAX(CASE
               WHEN FundraisingYear = @year - 3
               THEN mcnt
           END) cyminus3,
       MAX(CASE
               WHEN FundraisingYear = @year - 2
               THEN mcnt
           END) cyminus2,
       MAX(CASE
               WHEN FundraisingYear = @year - 1
               THEN mcnt
           END) cyminus1,
       MAX(CASE
               WHEN FundraisingYear = @year
               THEN mcnt
           END) cy
FROM( 
      SELECT FundraisingYear,
             COUNT(DISTINCT MarketId) mcnt
      FROM disbursements
      WHERE( fundraisingyear >= @year - 3 )
       AND ( FundraisingEntityId = @partnerid )
       AND MarketId IN(
               SELECT Item
               FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
      GROUP BY FundraisingYear ) t;
END

