﻿


CREATE PROCEDURE [dbo].[rptPvrDMMarket]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market INT = 175
/*****************/

IF OBJECT_ID('tempdb..#tempDMrank', 'U') IS NOT NULL
    DROP TABLE #tempDMrank

SELECT  b.MarketId ,
b.RegionId ,
        b.bCampaignTypeId ,
        b.CYTotal ,
        b.CYMinus1 ,
		( ( CYTotal- CYminus1 ) / NULLIF(CYminus1, 0) ) AS PctChange,
		CYTotal / NULLIF(Pop, 0) AS PerCap ,
		CYTotal - CYminus1 AS AmountIncrease ,
		RANK() OVER (PARTITION BY RegionId ORDER BY (CYTotal / NULLIF(Pop, 0)) DESC) AS RegionPerCapRank,
		RANK() OVER (PARTITION BY RegionId ORDER BY CYTotal DESC) AS RegionCYTotalRank,
		RANK() OVER (PARTITION BY RegionId ORDER BY (CYTotal - CYminus1) DESC) AS RegionAmountIncreaseRank,
		RANK() OVER (PARTITION BY RegionId ORDER BY ( ( CYTotal - CYminus1 ) / NULLIF(CYminus1, 0) ) DESC) AS RegionPctChangeRank,
		
		RANK() OVER (PARTITION BY bCampaignTypeId ORDER BY (CYTotal / NULLIF(Pop, 0)) DESC) AS NationalPerCapRank,
		RANK() OVER (PARTITION BY bCampaignTypeId ORDER BY CYTotal DESC) AS NationalCYTotalRank,
		RANK() OVER (PARTITION BY bCampaignTypeId ORDER BY (CYTotal- CYminus1) DESC) AS NationalAmountIncreaseRank,
		RANK() OVER (PARTITION BY bCampaignTypeId ORDER BY ( ( CYTotal - CYminus1) / NULLIF(CYminus1, 0) ) DESC) AS NationalPctChangeRank,
        b.NationalRank ,
        --a.RegionRank ,
        c.pop
INTO    #tempDMrank
FROM    ( SELECT    d.MarketId ,
					d.RegionId,
                    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END) , 0) AS CYTotal ,
					ISNULL(SUM(CASE WHEN d.FundraisingYear = @year -1 THEN d.Amount END) , 0) AS CYMinus1,
                    d.CampaignTypeId AS bCampaignTypeId ,
                    RANK() OVER ( PARTITION BY d.CampaignTypeId ORDER BY SUM(Amount) DESC ) AS NationalRank

          FROM      dbo.vwFundraisingData d
          WHERE     ( d.FundraisingYear >= @year - 1 )
                    AND d.CampaignTypeId = 10
          GROUP BY  d.MarketId ,
                    d.CampaignTypeId,
					RegionId
        ) AS b
        INNER JOIN ( SELECT PopulationEstimate AS pop ,
                            MarketId
                     FROM   dbo.vwMarketPopulationRank
                   ) AS c ON b.MarketId = c.MarketId
UNION ALL
SELECT @market,'','','','','','','','','','','','','','','','',''

SELECT TOP 1 
d.MarketId ,
		m.MarketName,
		r.RegionName,
		d.RegionId,
		CYTotal,
		AmountIncrease,
		ISNULL(PctChange, 0) AS PctChange,
		PerCap, 
		NationalCYTotalRank,
		NationalAmountIncreaseRank,
		NationalPctChangeRank,
		NationalPerCapRank,
		RegionCYTotalRank,
		RegionAmountIncreaseRank,
		RegionPctChangeRank,
		RegionPerCapRank
FROM    #tempDMrank d
LEFT OUTER JOIN dbo.vwMarkets m ON m.MarketId = d.MarketId
LEFT OUTER JOIN dbo.vwRegions r ON r.RegionId = d.RegionId
WHERE d.MarketId = @market
ORDER BY CYTotal DESC

