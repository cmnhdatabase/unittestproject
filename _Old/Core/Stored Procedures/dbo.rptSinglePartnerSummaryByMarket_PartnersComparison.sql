﻿
CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryByMarket_PartnersComparison](
      @year      INT,
      @partnerid NVARCHAR(MAX),
      @marketid  NVARCHAR(MAX))
AS
    BEGIN
        IF OBJECT_ID('tempdb..#goodPartners', 'U') IS NOT NULL
        DROP TABLE #goodPartners;
        DECLARE @PartnerTotals TABLE( Amount      INT,
                                      PartnerId   INT,
                                      PartnerRank INT );
        INSERT INTO @PartnerTotals
               SELECT q1.*,
                      ROW_NUMBER() OVER(ORDER BY q1.Amount DESC) AS PartnerRank
               FROM( 
                     SELECT SUM(Amount) AS Amount,
                            FundraisingEntityId AS PartnerId
                     FROM vwFundraisingData
                     WHERE FundraisingYear = @year - 1
                       AND FundraisingEntityId NOT IN( 20, 56 )
                AND MarketId IN(
                       SELECT Item
                       FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
                     GROUP BY FundraisingEntityId ) q1;
 
        /*SELECT * FROM @PartnerTotals*/

        DECLARE @MaxRank INT = ( SELECT MAX(PartnerRank) AS MaxRank
                                 FROM @PartnerTotals AS pt );
        DECLARE @RelevantMarkets TABLE( PartnerID   INT,
                                        PartnerName NVARCHAR(100),
                                        TopBound    INT,
                                        BottomBound INT,
                                        PartnerRank INT );
        INSERT INTO @RelevantMarkets
               SELECT mpr.PartnerID,
                      pp.Name AS DisplayName,
                      ( CASE
                            WHEN mpr.PartnerRank < 4
                            THEN 1
                            WHEN mpr.PartnerRank > @MaxRank - 3
                            THEN( mpr.PartnerRank - 6 ) + ( @MaxRank - mpr.PartnerRank )
                            ELSE mpr.PartnerRank - 3
                        END ) AS TopBound,
                      ( CASE
                            WHEN mpr.PartnerRank < 4
                            THEN mpr.PartnerRank + 3 + ( 4 - mpr.PartnerRank )
                            WHEN mpr.PartnerRank > @MaxRank - 3
                            THEN @MaxRank
                            ELSE mpr.PartnerRank + 3
                        END ) AS BottomBound,
                      PartnerRank
               FROM @PartnerTotals AS mpr
                    INNER JOIN dbo.FundraisingEntities AS pp ON mpr.PartnerId = pp.FundraisingEntityId;
 
        /*SELECT * FROM @RelevantMarkets*/

        DECLARE @TopBound INT = ( SELECT TopBound
                                  FROM @RelevantMarkets
                                  WHERE PartnerID = @partnerid );
        DECLARE @BottomBound INT = ( SELECT BottomBound
                                     FROM @RelevantMarkets
                                     WHERE PartnerID = @partnerid );
        SELECT sq1.* INTO #goodPartners
        FROM( 
              SELECT mpr.PartnerID,
                     mpr.PartnerName,
                     SUM(CASE
                             WHEN vfd.FundraisingYear = @year - 3
                             THEN Amount
                             ELSE 0
                         END) AS CYminus3,
                     SUM(CASE
                             WHEN vfd.FundraisingYear = @year - 2
                             THEN Amount
                             ELSE 0
                         END) AS CYminus2,
                     SUM(CASE
                             WHEN vfd.FundraisingYear = @year - 1
                             THEN Amount
                             ELSE 0
                         END) AS CYminus1,
                     SUM(CASE
                             WHEN vfd.FundraisingYear = @year
                             THEN Amount
                             ELSE 0
                         END) AS CY,
                     PartnerRank
              FROM @RelevantMarkets AS mpr
                   INNER JOIN dbo.Disbursements AS vfd ON vfd.FundraisingEntityId = mpr.PartnerID
              WHERE mpr.PartnerRank BETWEEN @TopBound AND @BottomBound
                AND vfd.FundraisingYear >= @year - 3
                AND MarketId IN(
                       SELECT Item
                       FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
              GROUP BY mpr.PartnerID,
                       mpr.PartnerName,
                       PartnerRank ) sq1
        ORDER BY PartnerRank;
                                                                                                                       
        /*SELECT * FROM #goodPartners*/

        SELECT DISTINCT gp.*,
                        d.markcount AS '# of Benefitting Markets',
                        d2.loccount AS '# of Fundraising Locations',
                        d3.loccount AS 'Total # of Locations'
        FROM vwfundraisingdata f
             INNER JOIN #goodPartners AS gp ON gp.PartnerId = f.FundraisingEntityId
             INNER JOIN( 
                         SELECT FundraisingEntityId,
                                COUNT(DISTINCT MarketId) markcount
                         FROM vwfundraisingdata
                         WHERE fundraisingyear = @year - 1
                           AND MarketId IN(
                                  SELECT Item
                                  FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
                         GROUP BY FundraisingEntityId ) d ON d.FundraisingEntityId = gp.PartnerID
             INNER JOIN( 
                         SELECT gp2.PartnerID,
                                COUNT(DISTINCT locationid) loccount
                         FROM disbursements di
                              INNER JOIN #goodPartners gp2 ON gp2.PartnerID = di.FundraisingEntityId
                         WHERE fundraisingyear = @year - 1
                           AND MarketId IN(
                                  SELECT Item
                                  FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
                         GROUP BY gp2.PartnerID ) d2 ON gp.PartnerID = d2.PartnerID
             INNER JOIN( 
                         SELECT l.FundraisingEntityId AS PartnerID,
                                COUNT(DISTINCT l.LocationId) loccount
                         FROM Locations l
                              INNER JOIN #goodPartners gp3 ON gp3.PartnerID = l.FundraisingEntityId
                              LEFT OUTER JOIN dbo.vwFundraisingData f ON f.FundraisingEntityId = gp3.PartnerID AND f.locationid = l.LocationId
                         WHERE FundraisingYear = @year
                           AND Active = 1
                           AND MarketId IN(
                                  SELECT Item
                                  FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
                         GROUP BY l.FundraisingEntityId
					
                    /*      
							COUNT(DISTINCT locationid) loccount
                     FROM   disbursements di
                            INNER JOIN #goodPartners gp3 ON gp3.PartnerID = di.PartnerId
                     WHERE  fundraisingyear = @year
                     GROUP BY gp3.PartnerID
					*/

                         ) d3 ON gp.PartnerID = d3.PartnerID
        WHERE fundraisingyear >= @year - 3
          AND MarketId IN( 
                           SELECT Item
                           FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
        ORDER BY gp.PartnerRank;
    END;

