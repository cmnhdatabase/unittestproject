﻿-- =============================================
-- Author: Travis 
-- Create date: 5-16-2014
-- Description: Add record to pledgebatces
-- =============================================
CREATE PROCEDURE spInsertPledgeBatch
       -- Add the parameters for the stored procedure here
       @BatchNumber VARCHAR(25)
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO [dbo].[PledgeBatches]
                     ([PledgeBatchNumber]
                     ,[PledgeBatchDate])
           VALUES
                     (@BatchNumber
                     ,GETDATE())
END
