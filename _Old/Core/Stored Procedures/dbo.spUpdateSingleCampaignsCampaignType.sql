﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 2/3/2016
-- Description:	This takes report takes 2 parameters the campaignid of the campaign to be modified and the campaigntypeid
--				that we are changing to.
-- =============================================
Create PROCEDURE [dbo].[spUpdateSingleCampaignsCampaignType]
	-- Add the parameters for the stored procedure here
    @campaignid INT ,
    @newCampaignType INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        DECLARE @sql NVARCHAR(MAX);
        DECLARE @output VARCHAR(MAX);

        SET @sql = 'UPDATE core.dbo.Campaigns
SET CampaignTypeId = ' + CAST(@newCampaignType AS CHAR) + '
WHERE CampaignId = ' + CAST(@campaignid AS CHAR) + '';

        SET @output = 'Orignal campaign type was '
            + ( SELECT  CampaignType
                FROM    Core.dbo.Campaigns c
                        INNER JOIN dbo.CampaignTypes ct ON ct.CampaignTypeId = c.CampaignTypeId
                WHERE   CampaignId = @campaignid
              );

        EXEC (@sql);

        SET @output = @output + CHAR(13) + 'It has been updated to '
            + ( SELECT  CampaignType
                FROM    Core.dbo.Campaigns c
                        INNER JOIN dbo.CampaignTypes ct ON ct.CampaignTypeId = c.CampaignTypeId
                WHERE   CampaignId = @campaignid
              );

        PRINT @output + CHAR(13);
        PRINT 'Statment' + CHAR(13) + '------------------' + CHAR(13) + @sql;
        EXEC (@sql);
    END;
