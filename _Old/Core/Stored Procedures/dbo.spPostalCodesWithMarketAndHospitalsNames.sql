﻿



-- =============================================
-- Author:           Travis Poll   
-- Create date: 05/01/2014
-- Description:      Returns a list of postal codes with market and hospital names.
-- =============================================
CREATE PROCEDURE [dbo].[spPostalCodesWithMarketAndHospitalsNames]

AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
       DECLARE @tempmarketdata TABLE
         ( 
               marketid      INT, 
               hospitalnames VARCHAR(2000) 
         ) 

       INSERT INTO @tempmarketdata 
       SELECT t.marketid, 
                 t.hospitalnames 
       FROM   (SELECT marketid, 
                              dbo.Gethospitalnamesbymarketid(marketid) AS hospitalNames 
                     FROM   Core..markets) t 

       --postal codes with hospital names
       SELECT DISTINCT REPLACE((LOWER(pc.postalcode)),' ','') AS postalcode, 
       pc.countryid AS country,
       CASE WHEN s.[RedirectedToPostalCode] IS NULL THEN REPLACE((LOWER(pc.postalcode)),' ','') ELSE REPLACE((LOWER(s.[RedirectedToPostalCode])),' ','') END AS derivedpostalcode, 
       CASE WHEN s.[RedirectedToPostalCode] IS NULL THEN m.MarketName ELSE sm.MarketName END AS Market,
       CASE WHEN s.[RedirectedToPostalCode] IS NULL THEN t.hospitalnames ELSE st.hospitalnames END AS Hospitals
       FROM postalcodes pc 
       LEFT JOIN @tempmarketdata t ON t.marketid = pc.marketid
       LEFT JOIN RedirectedPostalCodes s ON s.[OriginalPostalCode] = pc.postalcode
       LEFT JOIN postalcodes spc ON spc.postalcode = s.[RedirectedToPostalCode]
       LEFT JOIN @tempmarketdata st ON st.marketid = spc.marketid
       LEFT JOIN Markets m ON pc.marketid = m.MarketId
          LEFT JOIN Markets sm ON spc.marketid = sm.MarketId

END






