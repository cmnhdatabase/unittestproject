﻿


CREATE PROCEDURE [dbo].[rptSingleMarketSummary_dsChart]
(	@Year     INT = 2014
  , @MarketID INT = 10 
)
AS

BEGIN
	SET NOCOUNT ON;

	/***********	TEST SECTION	***********/

		--DECLARE @Year     INT = 2014, 
		--		@MarketID INT = 10				 

	/*********** END TEST SECTION	***********/

	IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	DECLARE @prmYear     INT = @Year
	DECLARE @prmMarketID INT = @MarketID

	------SMS DS
	SELECT marketid
		 , marketname
		 , categoryid
		 , SUM(YearlyAmount) AS YearlyAmount
		 , FundraisingYear
		 , category
	  INTO #Temp
	  FROM 
	  (	SELECT * 
		  FROM
		  (	SELECT d.marketid, 
				   d.marketname, 
				   d.FundraisingCategoryId as categoryid,
				   SUM(d.amount) AS YearlyAmount,
				   d.FundraisingYear,
				   d.FundraisingCategory as Category
			  FROM vwfundraisingdata AS d 
			  WHERE d.fundraisingyear >= @prmYear - 3 
				AND d.marketid = @prmMarketID 
				AND d.FundraisingCategoryId != 2 
				--and d.PartnerId not in (56,20) 
			  GROUP BY d.marketid, 
					   d.marketname, 
					   d.FundraisingCategoryId, 
					   d.FundraisingYear,
					   d.FundraisingCategory
	  UNION ALL 
		  	SELECT d.marketid, 
				   d.marketname, 
				   d.FundraisingCategoryId as categoryid, 
				   SUM(d.amount) AS YearlyAmount,
				   d.FundraisingYear,
				   d.FundraisingCategory as Category
			  FROM vwfundraisingdata AS d 
			  WHERE fundraisingyear >= @prmYear - 3 
				AND d.marketid = @prmMarketID 
				AND d.FundraisingCategoryId = 2 
				--and d.PartnerId not in (56,20) 
			  GROUP BY d.marketid, 
					   d.marketname,
					   d.FundraisingCategoryId, 
					   d.FundraisingYear,
					   d.FundraisingCategory
		  ) AS t 
	)qqz
	  GROUP BY marketid
			 , marketname
			 , categoryid
			 , FundraisingYear
			 , category

	/* Aggregate the National Totals  */
		SELECT * FROM #Temp
	UNION
		SELECT MarketID
			 , Marketname
			 , 0 AS CategoryId
			 , SUM(YearlyAmount) AS YearlyAmount
			 , FundraisingYear
			 , 'Total Fundraising Amount'
		  FROM #Temp 
		  GROUP BY MarketID
				 , Marketname
				 , FundraisingYear
	  ORDER BY CategoryId,
			   FundraisingYear
END


