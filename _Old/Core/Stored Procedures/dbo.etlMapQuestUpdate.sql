﻿


CREATE PROCEDURE [dbo].[etlMapQuestUpdate]
AS
BEGIN
	MERGE dbo.Locations t
	   USING TemporaryData.dbo.TempBingMapsAPI s 
	   ON t.LocationId  = s.LocationID
	   WHEN MATCHED
		  THEN UPDATE SET t.Latitude = s.Latitude,
					   t.Longitude	= s.Longitude
	   OUTPUT $action, inserted.*, deleted.*;
END

