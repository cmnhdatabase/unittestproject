﻿
-- =============================================
-- Author:		Josh Walker
-- Create date: 2016-5-19
-- Description:	For a given market and parter, this shows all locations belonging to that partner within the market, and each store's distance to that market's hospital.
-- =============================================
CREATE PROCEDURE [dbo].[spStoreDistancesForMarketAndFundraisingEntity] 
	-- Add the parameters for the stored procedure here
	@marketId INT,
	@fundraisingEntityId INT
AS
BEGIN

	/*TEST DATA - ALL WALMARTS IN SLC MARKET AND DISTANCE FROM PRIMARY CHILDRENS'*/
	--DECLARE @marketId INT = 175
	--DECLARE @fundraisingEntityId INT = 123


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @hospitalId INT;

	/*Get HospitalId for the selected market*/
	SELECT @hospitalId =
		ISNULL(MAX(HospitalId),0)/*TODO: USING MAX() HERE BECUASE THERE ARE 5 MARKETS THAT HAVE MORE THAN ONE HOSPITAL. NEED TO FIGURE OUT WHICH HOSPITAL IS PRIMARY FOR THOSE MARKETS*/
	FROM
		dbo.vwHospitals
	WHERE
		MarketId = @marketId

	/*
	Get all active locations for the given fundraising entity in the same market as the hospital.
	You have to join to hte Hospitals view here so you can calculate distance, so you might as well use Location.market = Hospital.market as the limiting factor
	*/
	SELECT
		fe.Name PartnerName,
		l.LocationNumber,
		l.LocationName,
		l.Address1 AS 'Address',
		l.City,
		p.Abbreviation AS 'State',
		l.PostalCode,
		Core.dbo.GetGeoDistance(l.Latitude, l.Longitude, h.Latitude, h.Longitude) AS Distance,
		h.HospitalName
	FROM
		dbo.vwLocationDetails l
		JOIN dbo.Hospitals h ON h.MarketId = l.MarketId
		JOIN dbo.vwProvinces p ON p.ProvinceId = l.ProvinceId
		JOIN dbo.vwFundraisingEntities fe ON fe.FundraisingEntityId = l.FundraisingEntityId
	WHERE
		l.FundraisingEntityId = @fundraisingEntityId
		AND h.HospitalId = @hospitalId
		AND l.Active = 1
END

