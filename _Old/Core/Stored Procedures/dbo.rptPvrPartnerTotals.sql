﻿




CREATE PROCEDURE [dbo].[rptPvrPartnerTotals]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market INT = 232
/******************/
IF OBJECT_ID('tempdb..#tempPartner', 'U') IS NOT NULL
    DROP TABLE #tempPartner

SELECT *
INTO #tempPartner
FROM (SELECT
MarketId,
SUM(CASE WHEN FundraisingYear = @year AND FundraisingCategoryId = 1 THEN amount ELSE 0 END) AS PartnerCYTotal,
SUM(CASE WHEN FundraisingYear = @year AND FundraisingCategoryId = 2 THEN amount ELSE 0 END) AS ProgramCYTotal,
SUM(CASE WHEN FundraisingYear = @year AND FundraisingCategoryId = 5 THEN amount ELSE 0 END) AS LocalCYTotal,
RANK() OVER (PARTITION BY FundraisingCategoryId ORDER BY SUM(CASE WHEN FundraisingYear = @year AND FundraisingCategoryId = 1 THEN amount ELSE 0 END) DESC) AS PartnerRanking,
RANK() OVER (PARTITION BY FundraisingCategoryId ORDER BY SUM(CASE WHEN FundraisingYear = @year AND FundraisingCategoryId = 2 THEN amount ELSE 0 END) DESC) AS ProgramRanking,
RANK() OVER (PARTITION BY FundraisingCategoryId ORDER BY SUM(CASE WHEN FundraisingYear = @year AND FundraisingCategoryId = 5 THEN amount ELSE 0 END) DESC) AS LocalRanking
FROM dbo.vwFundraisingData
WHERE FundraisingYear = @year
--AND MarketId = @market
GROUP BY MarketId, FundraisingCategoryId) AS a

SELECT
MarketId, 
SUM(PartnerCYTotal) AS PartnerCyTotal ,
SUM(ProgramCYTotal) AS ProgramCYTotal, 
SUM(LocalCYTotal) AS LocalCYTotal, 
CASE WHEN (SUM(PartnerRanking) - 4) >= 1 THEN SUM(PartnerRanking) ELSE 153 END AS PartnerRanking, 
CASE WHEN (SUM(ProgramRanking) - 4) >= 1 THEN SUM(ProgramRanking) ELSE 153 END AS ProgramRanking, 
CASE WHEN (SUM(LocalRanking) - 4) >= 1 THEN SUM(LocalRanking) ELSE 153 END AS LocalRanking
FROM #tempPartner
WHERE MarketId = @market
GROUP BY MarketId







