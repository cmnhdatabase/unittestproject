﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to add a new DanceMarathonHospital record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertDanceMarathonHospital]
	@DanceMarathonId int,
	@HospitalId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[DanceMarathonHospitals] (
		[DanceMarathonId],
		[HospitalId]
	)
	VALUES (
		@DanceMarathonId,
		@HospitalId
	)
END
