﻿


-- =============================================
-- Author:		Eric S. Shimada
-- Create date: 10/20/2016
-- Description:	Running this will insert a new record into the DanceMarathonHosptials table
-- =============================================

CREATE PROCEDURE [dbo].[spDmHospital]
	-- Add the parameters for the stored procedure here

@dmId INT,
@hospitalId INT

AS	
SET NOCOUNT ON;

BEGIN	
INSERT INTO dbo.DanceMarathonHospitals
        ( DanceMarathonId, HospitalId )
VALUES  ( @dmId, -- DanceMarathonId - int
          @hospitalId  -- HospitalId - int
          )
END	