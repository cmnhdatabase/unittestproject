﻿



----CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryReport_TopMarkets]
----  (	  @year     INT 
----	, @partnerid INT 	
----	, @topMarketCount INT = 10
----  )
----AS

CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryReport_TopMarkets](
      @year           INT,
      @partnerid      INT,
      @topMarketCount INT = 10 )
AS
    BEGIN
	
	   /***********  TEST SECTION  ***********/

        --DECLARE @partnerid INT = 71, 
        --             @year      INT = 2015,
        --             @topMarketCount INT = 10
        --IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp    

        /*********** END TEST SECTION    ***********/

        IF OBJECT_ID('tempdb..#tempMarketInfo', 'U') IS NOT NULL
        DROP TABLE #tempMarketInfo;
        DECLARE @prmYear      INT = @year,
                @prmPartnerID INT = @partnerid;
        SELECT f.marketid,
               f.MarketName,
               ISNULL(SUM(CASE
                              WHEN f.fundraisingyear = @year - 3
                              THEN f.amount
                          END), 0) AS 'CYminus3',
               ISNULL(SUM(CASE
                              WHEN f.fundraisingyear = @year - 2
                              THEN f.amount
                          END), 0) AS 'CYminus2',
               ISNULL(SUM(CASE
                              WHEN f.fundraisingyear = @year - 1
                              THEN f.amount
                          END), 0) AS 'CYminus1',
               ISNULL(SUM(CASE
                              WHEN f.fundraisingyear = @year
                              THEN f.amount
                          END), 0) AS 'CY',
               d.loccount AS loccount,
               SUM(CASE
                       WHEN f.fundraisingyear = @year - 1
                       THEN f.amount
                   END) / NULLIF(d.loccount, 0) AS 'AverageCYminus1',
               d2.loccount2 AS 'loccount2',
               COALESCE(SUM(CASE
                                WHEN f.fundraisingyear = @year
                                THEN f.amount
                            END) / NULLIF(d2.loccount2, 0), 0) AS 'AverageCY' INTO #tempMarketInfo
        FROM vwfundraisingdata f
             INNER JOIN( 
                         SELECT marketid,
                                COUNT(DISTINCT locationid) loccount
                         FROM disbursements
                         WHERE fundraisingyear = @year - 1
                           AND FundraisingEntityId = @partnerid
                         GROUP BY marketid ) d ON f.marketid = d.marketid
             INNER JOIN( 
                         SELECT m.MarketId,
                                CONVERT( INT, SUM(cnt)) AS loccount2
                         FROM(
                                  SELECT l.PostalCode,
                                         COUNT(DISTINCT LocationId) cnt
                                  FROM dbo.Locations l
                                  WHERE FundraisingEntityId = @partnerid
                                    AND l.Active = 1
                                    AND CAST(LocationId AS VARCHAR(100)) NOT IN(
                                           SELECT splitfrom
                                           FROM dbo.Splits)
                                  GROUP BY l.PostalCode
                                  UNION ALL
                                  SELECT PostalCode,
                                         SUM(SplitPercent)
                                  FROM dbo.Splits
                                  WHERE FundraisingEntityId = @partnerid
                                  GROUP BY PostalCode ) t
                             INNER JOIN dbo.PostalCodes m ON t.PostalCode = m.PostalCode
                         GROUP BY MarketId ) AS d2 ON d2.MarketId = f.MarketId


        --INNER JOIN (SELECT marketid, 
        --                        COUNT(DISTINCT locationid) loccount2 
        --                 FROM   disbursements 
        --                 WHERE  fundraisingyear = @year 
        --                        AND partnerid = @partnerid 
        --                 GROUP  BY marketid) d2 
        --             ON f.marketid = d2.marketid 
        WHERE fundraisingyear >= @year - 3
          AND FundraisingEntityId = @partnerid
          AND f.FundraisingCategoryId = 1
        GROUP BY f.marketid,
                 f.MarketName,
                 d.loccount,
                 d2.loccount2
        ORDER BY CYminus1 DESC;
        DECLARE @GrandTotal MONEY = ( SELECT SUM(CYminus1)
                                      FROM #tempMarketInfo );
        DECLARE @TopsTotal MONEY = ( SELECT SUM(CYminus1)
                                     FROM( 
                                           SELECT TOP ( @topMarketCount ) CYminus1
                                           FROM #tempmarketInfo
                                           ORDER BY CYminus1 DESC ) tt );
        SELECT TOP ( @topMarketCount ) *,
                                       @TopsTotal / NULLIF(@GrandTotal, 0) AS RankedPartnersPct,
                                       @TopsTotal AS TopTotal,
                                       ( @GrandTotal / NULLIF(@GrandTotal, 0)) - ( @TopsTotal / NULLIF(@GrandTotal, 0)) AS Pct,
                                       @GrandTotal / NULLIF(@GrandTotal, 0) AS GrandTotal
        FROM #tempMarketInfo AS tli
        ORDER BY CYminus1 DESC;
    END;


