﻿
CREATE PROCEDURE	[dbo].[rptPartnerList]
AS
BEGIN

SELECT fe.FundraisingEntityId AS PartnerProgramId,
       DisplayName
FROM dbo.FundraisingEntities fe
WHERE fe.FundraisingCategoryId = 1
  AND Active = 1
ORDER BY 2;

END
