﻿/****** Object:  StoredProcedure [dbo].[spDeleteRadiothonRadioStation] ******/
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/11/2015
-- Description:	This procedure is used to delete an existing RadiothonRadioStation record.
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteRadiothonRadioStation]
	@RadiothonId int,
	@RadioStationId int,
	@PrimaryRadioStation bit
AS
BEGIN
	DELETE FROM [dbo].[RadiothonRadioStations]
	WHERE [RadiothonId] = @RadiothonId AND [RadioStationId] = @RadioStationId AND [PrimaryRadioStation] = @PrimaryRadioStation;
END


