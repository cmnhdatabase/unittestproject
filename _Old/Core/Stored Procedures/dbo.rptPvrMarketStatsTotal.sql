﻿








CREATE PROCEDURE [dbo].[rptPvrMarketStatsTotal]
(
@year INT
, @market INT
)
 AS

/****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market	INT = 61
/*****************/

IF OBJECT_ID('tempdb..#tempMarketStatTotal', 'U') IS NOT NULL
    DROP TABLE #tempMarketStatTotal

SELECT

(CY - CYMinus1) AS CYIncrease,
(CY - CYMinus1)/NULLIF(CYMinus1,0) AS CYPercentIncrease,
(CY/NULLIF(POPULATION,0)) AS PerCap,
RANK() OVER (ORDER BY (CY - CYMinus1) DESC) AS CYIncreaseRank,
RANK() OVER (ORDER BY (CYMinus1 - CYMinus2) DESC) AS CYMinus1IncreaseRank,
RANK() OVER (ORDER BY ((CY - CYMinus1)/NULLIF(CYMinus1,0))DESC) AS CYPercentIncreaseRank,
RANK() OVER (ORDER BY ((CYMinus1 - CYMinus2)/NULLIF(CYMinus2,0))DESC) AS CYMinus1PercentIncreaseRank,
RANK() OVER (ORDER BY ((CY/NULLIF(POPULATION,0)))DESC) AS CYPerCapRank,
RANK() OVER (ORDER BY ((CYMinus1/NULLIF(POPULATION,0)))DESC) AS CYMinus1PerCapRank,
(b.CyTotalCosts/a.CY) AS CRD,
RANK() OVER (ORDER BY((CYTotalCosts/CY))) AS CRDCYRank,
RANK() OVER (ORDER BY((CYMinus1TotalCosts/CYMinus1))) AS CRDCYMinus1Rank,
((cy-CyTotalCosts)/CYTotalCosts) AS ROI,
RANK() OVER (ORDER BY ((cy-CYTotalCosts)/CYTotalCosts)DESC) AS ROICYRank,
RANK() OVER (ORDER BY ((CYMinus1-CYMinus1TotalCosts)/CYminus1TotalCosts)DESC) AS ROICYMinus1Rank,
b.CYFTE,
MarketId,
MarketName
INTO #tempMarketStatTotal
FROM (
SELECT 
fd.MarketId AS MarketId,
fd.Marketname AS MarketName,
SUM(CASE WHEN fd.FundraisingYear = @year - 2 THEN Amount ELSE 0 END) AS CYMinus2,
SUM(CASE WHEN FundraisingYear = @year -1 THEN Amount ELSE 0 END) AS CYMinus1,
SUM(CASE WHEN FundraisingYear = @year THEN Amount ELSE 0 END) AS CY,
MAX(PopulationEstimate) AS Population
FROM dbo.vwFundraisingData fd
INNER JOIN dbo.vwMarketPopulationRank mp ON mp.MarketId = fd.MarketId
WHERE FundraisingYear BETWEEN @year - 2 AND @year
GROUP BY fd.MarketId, fd.Marketname
) AS a

INNER JOIN 
(SELECT MarketId AS ID,
(FixedFees + VariableFees + OtherFees + (OverHead/COLIRate)) AS CYTotalCosts, FTE AS CyFTE
FROM core.dbo.PVRFees
WHERE year = @year) AS b ON b.Id = a.MarketId

INNER JOIN 
(SELECT MarketId AS ID1,
(FixedFees + VariableFees + OtherFees + (OverHead/COLIRate)) AS CYMinus1TotalCosts, FTE AS Cyminus1FTE
FROM core.dbo.PVRFees
WHERE year = @year - 1) AS c ON c.Id1 = a.MarketId


SELECT *
FROM #tempMarketStatTotal
WHERE MarketId = @market













