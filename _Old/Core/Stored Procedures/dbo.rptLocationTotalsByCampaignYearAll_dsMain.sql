﻿




CREATE PROCEDURE [dbo].[rptLocationTotalsByCampaignYearAll_dsMain]
( @yr			INT,
  @MarketId		NVARCHAR(MAX),
  @prtnr		NVARCHAR(MAX), 
  @IncludeSubMarkets BIT
)
AS

BEGIN
	SET NOCOUNT ON

	/***********	TEST SECTION	***********/

		--DECLARE @yr			INT = 2015
		--DECLARE @MarketId	NVARCHAR(MAX) = '95'
		--DECLARE @prtnr		NVARCHAR(MAX) = '20, 26, 28'
		--DECLARE @IncludeSubMarkets	BIT = 1

		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/
	
DECLARE @Markets TABLE 
	 (	MarketID	INT
	  , MarketName	NVARCHAR(100)
	  , RegionId	INT
	 )

	   IF @IncludeSubMarkets = 1
		INSERT INTO @Markets
		SELECT mkt.MarketId
			 , mkt.MarketName
			 , mkt.RegionId
		  FROM [Core].[dbo].[ContractHolders] AS ch 
		    INNER JOIN [Core].[dbo].[Markets] AS mkt ON ch.SubMarketId = mkt.MarketId
		  WHERE ch.PrimaryMarketId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@MarketId, ','))  
	  ELSE
		INSERT INTO @Markets
		SELECT mkt.MarketId
			 , mkt.MarketName
			 , mkt.RegionId
		  FROM [Core].[dbo].[Markets] AS mkt 
		  WHERE mkt.MarketId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@MarketId, ','))  

SELECT DISTINCT
YEAR(GETDATE()) as CurrentYear,
vld.LocationName as FacilityName,
vld.LocationNumber AS LocationNumber,
vld.LocationId AS LocationId,
vld.Address1 AS Address1,
vld.Address2 AS Address2,
vld.City AS City,
p.Abbreviation AS Abbreviation,
vld.PostalCode AS PostalCode,
case when vld.Active = 1 THEN 'Open' ELSE 'Closed' END AS StoreStat,
vld.MarketName AS MarketName,
fe.Name AS PartnerName,
d.CY AS CY,
d.CY1 AS [CY-1],
d.CY2 AS [CY-2],
d.CY3 AS [CY-3],
c.lastname + ',' + c.firstname AS FacilityManager,
c.Phone AS Phone
FROM
dbo.vwLocationDetails vld
LEFT JOIN dbo.Locations l ON vld.LocationId = l.LocationId
LEFT JOIN dbo.provinces p ON p.ProvinceId = l.ProvinceId
LEFT JOIN dbo.FundraisingEntities fe ON fe.FundraisingEntityId = l.FundraisingEntityId
LEFT JOIN dbo.LocationAreas la ON la.LocationId = l.LocationId
LEFT JOIN dbo.Areas a ON a.AreaId = la.AreaId
LEFT JOIN @Markets m ON m.MarketID = vld.MarketId
LEFT JOIN contacts.dbo.contacts c ON c.ContactId = a.ContactId
LEFT JOIN (
    SELECT
    d.FundraisingEntityId,
    d.LocationId,
    SUM(CASE WHEN d.FundraisingYear = @yr THEN d.Amount END) as 'CY',
    SUM(CASE WHEN d.FundraisingYear = @yr-1 THEN d.Amount END) as 'CY1',
    SUM(CASE WHEN d.FundraisingYear = @yr-2 THEN d.Amount END) as 'CY2',
    SUM(CASE WHEN d.FundraisingYear = @yr-3 THEN d.Amount END) as 'CY3'
    FROM
    dbo.disbursements d
    WHERE
    d.FundraisingYear >= @yr-3
    GROUP BY
    d.FundraisingEntityId,
    d.LocationId
) d ON d.LocationId = l.LocationId
WHERE
fe.FundraisingEntityId = @prtnr
AND vld.MarketId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@marketid, ','))

END



