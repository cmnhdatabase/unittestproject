﻿
CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryByMarket_dsCampaigns]
(
@year INT,
@partnerid NVARCHAR(MAX),
@marketid NVARCHAR(MAX)
)
AS
Begin

DECLARE @results TABLE( CampaignType VARCHAR(255),
                        cyminus3     MONEY,
                        cyminus2     MONEY,
                        cyminus1     MONEY,
                        cy           MONEY );
INSERT INTO @results
       SELECT TOP 4 CampaignType,
                    ISNULL(SUM(CASE
                                   WHEN FundraisingYear = @year - 3
                                   THEN Amount
                               END), 0) AS cyminus3,
                    ISNULL(SUM(CASE
                                   WHEN FundraisingYear = @year - 2
                                   THEN Amount
                               END), 0) AS cyminus2,
                    ISNULL(SUM(CASE
                                   WHEN FundraisingYear = @year - 1
                                   THEN Amount
                               END), 0) AS cyminus1,
                    ISNULL(SUM(CASE
                                   WHEN FundraisingYear = @year
                                   THEN Amount
                               END), 0) AS cy
       FROM vwFundraisingData
       WHERE FundraisingCategoryId = 1
         AND FundraisingYear >= @year - 3
         AND FundraisingEntityId = @partnerid
         AND MarketId IN( 
                          SELECT Item
                          FROM dbo.DelimitedSplitN4K(@marketid , ',' ))
       GROUP BY CampaignType
       ORDER BY 4 DESC;
SELECT 1,
       *
FROM @results
UNION
SELECT 2,
       'Remaining Fundraising',
       ISNULL(SUM(CASE
                      WHEN FundraisingYear = @year - 3
                      THEN Amount
                  END), 0) AS cyminus3,
       ISNULL(SUM(CASE
                      WHEN FundraisingYear = @year - 2
                      THEN Amount
                  END), 0) AS cyminus2,
       ISNULL(SUM(CASE
                      WHEN FundraisingYear = @year - 1
                      THEN Amount
                  END), 0) AS cyminus1,
       ISNULL(SUM(CASE
                      WHEN FundraisingYear = @year
                      THEN Amount
                  END), 0) AS cy
FROM vwFundraisingData
WHERE FundraisingEntityId = 1
  AND FundraisingYear >= @year - 3
  AND FundraisingEntityId = @partnerid
  AND MarketId IN( 
			   SELECT Item
		        FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
  AND CampaignType NOT IN( 
                           SELECT CampaignType
                           FROM @results )
ORDER BY 1,
         4 DESC;
END
