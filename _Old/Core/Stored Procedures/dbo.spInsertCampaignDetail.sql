﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new CampaignDetail record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertCampaignDetail]
	@CampaignId INT,
	@CampaignDetailName varchar(300),
	@ShortDescription varchar(300),
	@LongDescription varchar(500),
	@CampaignYear INT,
	@LegacyEventId varchar(50),
	@StartDate DATETIME,
	@EndDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[CampaignDetails] ( 
		CampaignId ,
	    CampaignDetailName ,
	    ShortDescription ,
	    LongDescription ,
	    CampaignYear ,
	    LegacyEventId ,
	    StartDate ,
	    EndDate
	)
	VALUES ( 
		@CampaignId ,
	    @CampaignDetailName ,
	    @ShortDescription ,
	    @LongDescription ,
	    @CampaignYear ,
	    @LegacyEventId ,
	    @StartDate ,
	    @EndDate
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END


