﻿
-- =============================================
-- Author:           <Author,,Name>
-- Create date: <Create Date,,>
-- Description:      <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateFundraisingEntityInfo] 
       -- Add the parameters for the stored procedure here
       @FundraisingEntityInfoId int, @Info varchar(max) 

AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[FundraisingEntityInfo]
       SET [Info] = @info
       WHERE [FundraisingEntityInfoId] = @FundraisingEntityInfoId
END

