﻿


CREATE PROCEDURE [dbo].[rptNationalCorporatePartnersOverallResultsByMarket] 
(	@Year     INT 
  , @MarketID INT 
  , @PercentilePartnerCount INT 
  , @PercentileProgramCount INT 
)
AS

BEGIN
	SET NOCOUNT ON;

	/***********	TEST SECTION	***********/

		--DECLARE @Year		INT = 2014, 
		--		@MarketID	INT = 10


		--declare @PercentilePartnerCount INT = 3
		--declare @PercentileProgramCount INT = 3
				 
		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	
		--IF OBJECT_ID('tempdb..#Test', 'U') IS NOT NULL DROP TABLE #Test

	/*********** END TEST SECTION	***********/

	DECLARE @prmYear     INT = @Year
	DECLARE @prmMarketID INT = @MarketID

	DECLARE @prmPercentilePartnerCount INT = @PercentilePartnerCount
	DECLARE @prmPercentileProgramCount INT = @PercentileProgramCount
		

	;WITH cteTemp AS (	------SMS DS
	SELECT MarketId
		 , Marketname
		 , t.FundraisingCategoryId
		 , t2.FundraisingCategory
		 , CategoryTotal
		 , PartnerId
		 , DisplayName
		 , CYminus3
		 , CYminus2
		 , CYminus1
		 , CY
		 --, PartnerProgramId
		 , LocCount
		 , NatAvg
		 , Mrktavg
		 , pctChangeY3toY2
		 , pctChangeY2toY1
		 , CASE t.Fundraisingcategoryid WHEN 1 THEN ROW_NUMBER() OVER (ORDER BY (CASE t.Fundraisingcategoryid WHEN 1 THEN CYminus1 ELSE -1 END) DESC) ELSE 9999999 END AS PartnerRanking
		 , CASE t.Fundraisingcategoryid WHEN 2 THEN ROW_NUMBER() OVER (ORDER BY (CASE t.Fundraisingcategoryid WHEN 2 THEN CYminus1 ELSE -1 END) DESC) ELSE 9999999 END AS ProgramEventRanking
--	  into #Temp
	  FROM   (SELECT *, 
				   cyminus1 / loccount AS MrktAvg, 
				   CASE WHEN cyminus3 = 0 THEN 0 ELSE ( ( cyminus2 - cyminus3 ) / cyminus3 ) * 100 END                 AS pctChangeY3toY2, 
				   CASE WHEN cyminus2 = 0 THEN 0 ELSE ( ( cyminus1 - cyminus2 ) / cyminus2 ) * 100 END                 AS pctChangeY2toY1 
				FROM   (SELECT d.MarketId,
							   d.Marketname,
							   d.Fundraisingcategoryid, 
							   d.FundraisingEntityId, 
							   d.displayname, 
							   SUM(CASE WHEN fundraisingyear = @prmYear - 3 THEN amount ELSE 0 END)	AS CYminus3, 
							   SUM(CASE WHEN fundraisingyear = @prmYear - 2 THEN amount ELSE 0 END)	AS CYminus2, 
							   SUM(CASE WHEN fundraisingyear = @prmYear - 1 THEN amount ELSE 0 END)	AS CYminus1, 
							   SUM(CASE WHEN fundraisingyear = @prmYear THEN amount ELSE 0 END)		AS CY 
						FROM   [dbo].[vwfundraisingdata] AS d 
						WHERE  fundraisingyear >= @prmYear - 3 
							   AND d.marketid = @prmMarketID 
							   --and d.PartnerId not in (56,20) 
							   AND d.Fundraisingcategoryid != 2 
						GROUP  BY d.marketid, 
								  d.marketname, 
								  d.Fundraisingcategoryid, 
								  d.FundraisingEntityId, 
								  d.displayname) AS t 
					   LEFT JOIN (SELECT l.FundraisingEntityId AS partnerid, 
										 COUNT(locationid) LocCount 
									FROM   locations AS l 
									   INNER JOIN postalcodes AS pc ON l.postalcode = pc.postalcode 
									   INNER JOIN markets AS m ON pc.marketid = m.marketid 
									WHERE  m.marketid = @prmMarketID 
									   AND l.active = 1 
									GROUP  BY FundraisingEntityId
								  ) AS l ON t.FundraisingEntityId = l.partnerid 
					   LEFT JOIN (SELECT t.FundraisingEntityId AS PartnerId2, 
										 total / loccount AS NatAvg 
									FROM   (SELECT d.FundraisingEntityId, 
												   Sum(amount) AS total 
											FROM disbursements AS d 
											WHERE d.fundraisingyear = @prmYear - 1 
											  AND recordtypeid = 1 
											GROUP BY d.FundraisingEntityId) AS t 
											   INNER JOIN 
												(	SELECT FundraisingEntityId,
														   Count(locationid) AS LocCount 
													FROM   locations AS l 
													WHERE  l.active = 1 
													GROUP  BY FundraisingEntityId
												) AS l ON t.FundraisingEntityId = l.FundraisingEntityId 
										UNION 
										SELECT 56, 
											   Sum(amount) / Count(DISTINCT marketid) 
										FROM   disbursements AS d 
										WHERE  d.fundraisingyear = @prmYear 
											   AND recordtypeid = 1 
											   AND d.FundraisingEntityId = 56
								  ) AS n ON t.FundraisingEntityId = n.partnerid2 
			UNION ALL 
			SELECT *, 
				   cyminus1 / loccount AS MrktAvg, 
				   CASE WHEN cyminus3 = 0 THEN 0 ELSE ( ( cyminus2 - cyminus3 ) / cyminus3 ) * 100 END                 AS pctChangeY3toY2, 
				   CASE WHEN cyminus2 = 0 THEN 0 ELSE ( ( cyminus1 - cyminus2 ) / cyminus2 ) * 100 END                 AS pctChangeY2toY1 
			FROM 
			  (	SELECT d.marketid, 
					   d.marketname, 
					   d.FundraisingCategoryId, 
					   0																	AS PartnerId, 
					   d.programdisplayname, 
					   Sum(CASE WHEN fundraisingyear = @prmYear - 3 THEN amount ELSE 0 END)	AS CYminus3, 
					   Sum(CASE WHEN fundraisingyear = @prmYear - 2 THEN amount ELSE 0 END)	AS CYminus2, 
					   Sum(CASE WHEN fundraisingyear = @prmYear - 1 THEN amount ELSE 0 END)	AS CYminus1, 
					   Sum(CASE WHEN fundraisingyear = @prmYear THEN amount ELSE 0 END)		AS CY 
				  FROM [Core].[dbo].[vwfundraisingdata] AS d 
				  WHERE fundraisingyear >= @prmYear - 3 
					AND d.marketid = @prmMarketID 
						   --and d.PartnerId not in (56,20) 
					AND d.FundraisingCategoryId = 2 
				  GROUP BY d.marketid, 
						   d.marketname, 
						   d.FundraisingCategoryId, 
						   d.programdisplayname
			  ) AS t 
				LEFT JOIN (	SELECT FundraisingEntityId, 
								   Count(locationid) AS LocCount 
							  FROM locations AS l 
								INNER JOIN postalcodes AS pc ON l.postalcode = pc.postalcode 
								INNER JOIN markets AS m ON pc.marketid = m.marketid 
							  WHERE m.marketid = @prmMarketID 
								AND l.active = 1 
							  GROUP BY FundraisingEntityId
							 ) AS l ON t.partnerid = l.FundraisingEntityId 
				LEFT JOIN (	SELECT t.FundraisingEntityId      AS PartnerId2, 
								   total / loccount AS natavg 
							  FROM
								  (	SELECT d.FundraisingEntityId, 
										   Sum(amount) AS total 
									  FROM disbursements AS d 
									  WHERE d.fundraisingyear = @prmYear - 1 
										AND recordtypeid = 1 
									  GROUP BY d.FundraisingEntityId
								  ) AS t 
								INNER JOIN 
								  (	SELECT FundraisingEntityId, 
										   Count(locationid) AS LocCount 
									  FROM locations AS l 
									  WHERE l.active = 1 
									  GROUP BY FundraisingEntityId
								  ) AS l ON t.FundraisingEntityId = l.FundraisingEntityId 
							UNION 
									SELECT 56, 
										   Sum(amount) / Count(DISTINCT marketid) 
									FROM   disbursements AS d 
									WHERE  d.fundraisingyear = @prmYear 
										   AND recordtypeid = 1 
										   AND d.FundraisingEntityId = 56) AS n 
								ON t.partnerid = n.partnerid2) AS t 
		   INNER JOIN (SELECT Fundraisingcategoryid as CategoryIDx, 
							  Fundraisingcategory, 
							  Sum(amount) AS CategoryTotal 
					   FROM   [dbo].[vwfundraisingdata] 
					   WHERE  fundraisingyear = @prmYear 
							  AND marketid = @prmMarketID 
					   GROUP  BY Fundraisingcategoryid, 
								 Fundraisingcategory
					  ) AS t2 ON t.Fundraisingcategoryid = t2.CategoryIDx 
	)
	SELECT --DISTINCT
		   cte.MarketId
		 , cte.Marketname
		 , cte.Fundraisingcategoryid
		 , cte.Fundraisingcategory
		 , cte.CategoryTotal
		 , cte.PartnerId
		 , cte.DisplayName
		 --, cte.PartnerProgramId
		 , cte.CYminus3
		 , cte.CYminus2
		 , cte.CYminus1
		 , cte.CY
	 --, cte.LocCount AS LocationMarketCount
	 --, cte.NatAvg AS NatAvg
		 , (CASE WHEN cte.Fundraisingcategoryid = 2 THEN PandENatAvgs.MarketCount
				 WHEN cte.Fundraisingcategoryid = 2 THEN LocalNatAvgs.MarketCount
				 ELSE cte.LocCount 
			END)															AS LocationMarketCount
		 , (CASE WHEN cte.Fundraisingcategoryid = 2 THEN PandENatAvgs.NatAvg
				 WHEN cte.Fundraisingcategoryid = 2 THEN LocalNatAvgs.NatAvg
				 ELSE cte.NatAvg 
			END)															AS NatAvg
		 , cte.MrktAvg
		 , cte.pctChangeY3toY2
		 , cte.pctChangeY2toY1
		 , cte.PartnerRanking
		 , cte.ProgramEventRanking
		 , rt.RankedTotal
		 , pert.PERankedTotal
		 , rt.RankedTotal / cte.CategoryTotal AS RankedPartnersPct
		 , (CASE cte.Fundraisingcategoryid WHEN 2 THEN pert.PERankedTotal / cte.CategoryTotal ELSE 0 END) AS RankedProgramsPct
	  --into #Test
	  FROM cteTemp AS cte
		OUTER APPLY
		  (	SELECT SUM( CASE WHEN PartnerRanking <= @prmPercentilePartnerCount THEN CY ELSE 0 END) AS RankedTotal FROM cteTemp WHERE Fundraisingcategoryid = 1) AS rt
		OUTER APPLY
		  (	SELECT SUM( CASE WHEN ProgramEventRanking <= @prmPercentileProgramCount THEN CY ELSE 0 END) AS PERankedTotal FROM cteTemp WHERE Fundraisingcategoryid = 2) AS pert
		OUTER APPLY
		  (	SELECT SUM(vfd.Amount) / COUNT(DISTINCT vfd.MarketId)	AS NatAvg
				 , COUNT(DISTINCT vfd.MarketId)						AS MarketCount
				 , vfd.Fundraisingcategoryid
				--, vfd.Category
				, vfd.DisplayName
			  FROM vwFundraisingData AS vfd
			  WHERE vfd.Fundraisingcategoryid = 2
				AND vfd.Fundraisingcategoryid = cte.Fundraisingcategoryid
				AND vfd.DisplayName = cte.DisplayName
				AND vfd.fundraisingyear = @prmYear - 1 
			  GROUP BY vfd.Fundraisingcategoryid
					 , vfd.DisplayName
		  ) AS PandENatAvgs
		OUTER APPLY
		  (	SELECT SUM(vfd.Amount) / COUNT(DISTINCT vfd.MarketId)	AS NatAvg
				 , COUNT(DISTINCT vfd.MarketId)						AS MarketCount
			  FROM vwFundraisingData AS vfd
			  WHERE vfd.Fundraisingcategoryid = 3
				AND vfd.Fundraisingcategoryid = cte.Fundraisingcategoryid
				AND vfd.FundraisingEntityId = cte.PartnerId
				AND vfd.fundraisingyear = @prmYear - 1 
			  GROUP BY vfd.Fundraisingcategoryid
		  ) AS LocalNatAvgs
	  ORDER BY cte.Fundraisingcategory
			 , cte.CYMinus1 DESC
			 , cte.DisplayName

END



