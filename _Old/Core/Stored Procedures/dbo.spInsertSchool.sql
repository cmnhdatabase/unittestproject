﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/12/2015
-- Description:	This procedure is used to add a new School record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertSchool]
	@HospitalId int,
	@SchoolName varchar(255),
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId int,
	@PostalCode varchar(10),
	@CountryId int,
	@InauguralEvent int,
	@SchoolTypeId int,
	@Active bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Schools] (
		[HospitalId],
		[SchoolName],
		[Address1],
		[Address2],
		[City],
		[ProvinceId],
		[PostalCode],
		[CountryId],
		[InauguralEvent],
		[SchoolTypeId],
		[Active]
	)
	VALUES (
		@HospitalId,
		@SchoolName,
		@Address1,
		@Address2,
		@City,
		@ProvinceId,
		@PostalCode,
		@CountryId,
		@InauguralEvent,
		@SchoolTypeId,
		@Active
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END

