﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 12/16/2014
-- Description:	This will merge all donor info information from our core database to the old Core database.
-- =============================================
CREATE PROCEDURE [dbo].[spMergeDonorInfoInformation]
AS
--BEGIN
/*
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	SET IDENTITY_INSERT Core.dbo.DonorInfo ON

	MERGE Core.dbo.DonorInfo AS t
	USING core.dbo.DonorInfo AS s
	ON (
		t.DonorInfoId = s.DonorInfoId
	)
	WHEN NOT MATCHED
		THEN INSERT([DonorInfoId],[DisbursementId],[Amount],[DonationDate],[FirstName],[MiddleName],[LastName],[Address1],[Address2],[City],[ProvinceId],[PostalCode],[CountryId],[Email],[Receiptable],[CompanyName],[CurrencyTypeId],[FundTypeId],[AssociateId],[TransactionID]) 
		VALUES(s.[DonorInfoId],s.[DisbursementId],s.[Amount],s.[DonationDate],s.[FirstName],s.[MiddleName],s.[LastName],s.[Address1],s.[Address2],s.[City],s.[ProvinceId],s.[PostalCode],s.[CountryId],s.[Email],s.[Receiptable],s.[CompanyName],s.[CurrencyTypeId],s.[FundTypeId],s.[AssociateId],s.[TransactionID])

	WHEN MATCHED AND t.[DisbursementId] != s.DisbursementId OR t.[Amount] != s.Amount OR t.[DonationDate] != s.DonationDate OR t.[FirstName] != s.FirstName OR t.[MiddleName] != s.MiddleName OR t.[LastName] != s.LastName
					OR t.[Address1] != s.Address1 OR t.[Address2] != s.Address2 OR t.[City] != s.City OR t.[ProvinceId] != s.ProvinceId OR t.[PostalCode] != s.PostalCode OR t.[CountryId] != s.CountryId
					OR t.[Email] != s.Email OR t.[Receiptable] != s.Receiptable OR t.[CompanyName] != s.CompanyName OR t.[CurrencyTypeId] != s.CurrencyTypeId
					OR t.[FundTypeId] != s.FundTypeId OR t.[AssociateId] != s.AssociateId OR t.[TransactionID] != s.TransactionID
		THEN UPDATE SET t.[DisbursementId] = s.DisbursementId, t.[Amount] = s.Amount, t.[DonationDate] = s.DonationDate, t.[FirstName] = s.FirstName, t.[MiddleName] = s.MiddleName, t.[LastName] = s.LastName,
					    t.[Address1] = s.Address1, t.[Address2] = s.Address2, t.[City] = s.City, t.[ProvinceId] = s.ProvinceId, t.[PostalCode] = s.PostalCode, t.[CountryId] = s.CountryId,
					    t.[Email] = s.Email, t.[Receiptable] = s.Receiptable, t.[CompanyName] = s.CompanyName, t.[CurrencyTypeId] = s.CurrencyTypeId,
					    t.[FundTypeId] = s.FundTypeId, t.[AssociateId] = s.AssociateId, t.[TransactionID] = s.TransactionID
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.DonorInfo OFF	

	--SET IDENTITY_INSERT Core.dbo.DonorInfoPhoneNumbers ON

	MERGE Core.dbo.DonorInfoPhoneNumbers AS t
	USING core.dbo.DonorInfoPhoneNumbers AS s
	ON (
		t.DonorInfoId = s.DonorInfoId
	)
	WHEN NOT MATCHED
		THEN INSERT(DonorInfoId, PhoneId) 
		VALUES(s.DonorInfoId,s.PhoneId)
	--WHEN MATCHED AND t.PhoneId != s.PhoneId
	--	THEN UPDATE SET t.PhoneId = s.PhoneId
	OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.DonorInfoPhoneNumbers OFF

	--SET IDENTITY_INSERT Core.dbo.DonorInfoCheckNumbers ON

	MERGE Core.dbo.DonorInfoCheckNumbers AS t
	USING core.dbo.DonorInfoCheckNumbers AS s
	ON (
		t.DonorInfoId = s.DonorInfoId
	)
	WHEN NOT MATCHED
		THEN INSERT(DonorInfoId, CheckNumberId) 
		VALUES(s.DonorInfoId,s.CheckNumberId)
	WHEN MATCHED AND t.CheckNumberId != s.CheckNumberId
		THEN UPDATE SET t.CheckNumberId = s.CheckNumberId
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.DonorInfoCheckNumbers OFF

	----Does this need to be done?  Not really using this field anywhere.
	--UPDATE Core.dbo.DonorInfo
	--SET ProgramEventId = s.FundraisingEntityId
	--FROM Core.dbo.DonorInfo INNER JOIN
	--		DonorInfooftCredits AS s ON Core.dbo.DonorInfo.DonorInfoId = s.DonorInfoId

	END TRY

			BEGIN CATCH
				SELECT 
					 ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;

				IF @@TRANCOUNT > 0
					BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Rollback'
					END
			END CATCH;

			IF @@TRANCOUNT > 0
				BEGIN
				COMMIT TRANSACTION;
				PRINT 'Commit'
			END
*/
--END


