﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spInsertFundraisingEntityInfo 
	-- Add the parameters for the stored procedure here
	@FundraisingEntityId int, @FundraisingEntityInfoTypeId int, @Info varchar(max) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO [dbo].[FundraisingEntityInfo]
           ([FundraisingEntityId]
           ,[FundraisingEntityInfoTypeId]
           ,[Info])
     VALUES
           (@FundraisingEntityId
           ,@FundraisingEntityInfoTypeId
           ,@Info)
END
