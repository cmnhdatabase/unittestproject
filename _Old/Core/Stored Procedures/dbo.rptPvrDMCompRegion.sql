﻿

CREATE PROCEDURE [dbo].[rptPvrDMCompRegion] ( @year INT, @market INT )
AS
 /****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market INT = 39
/*****************/

    IF OBJECT_ID('tempdb..#tempDMCompRegion', 'U') IS NOT NULL
        DROP TABLE #tempDMCompRegion

    SELECT  
            SUM(b.CYRegTotal) / NULLIF(SUM(b.DMCount), 0) AS RegAverage ,
            SUM(b.CYRegTotal) - SUM(b.CYMinus1RegTotal)/NULLIF(SUM(b.DMCount), 0) AS RegIncrease ,
            ( SUM(b.CYRegTotal) - SUM(b.CYMinus1RegTotal) )
            / NULLIF(SUM(b.CYRegTotal), 0) AS RegPctChange ,
            SUM(b.CYRegTotal) / SUM(b.Pop) AS RegPerCap 
    INTO    #tempDMCompRegion
    FROM    ( 
			SELECT    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                                        THEN d.Amount
                                                   END), 0) AS CYRegTotal ,
                                        ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                                             - 1 THEN d.Amount
                                                   END), 0) AS CYMinus1RegTotal ,
                                        '' AS DMCount ,
                                        '' AS pop ,
                                        MarketId
                              FROM      dbo.vwFundraisingData d
                              WHERE     ( d.FundraisingYear >= @year - 1 )
                                        AND d.CampaignTypeId = 10
                                        AND RegionId IN (
                                        SELECT DISTINCT
                                                RegionId
                                        FROM    dbo.vwFundraisingData
                                        WHERE   MarketId = @market )
                              GROUP BY  MarketId
                              UNION ALL
                              SELECT    '' ,
                                        '' ,
                                        COUNT(DISTINCT DanceMarathonId) AS RegDMCount ,
                                        '' ,
                                        ''
                              FROM      dbo.vwDanceMarathons dm
                                        INNER JOIN dbo.vwCampaignDetails cd ON cd.CampaignDetailsId = dm.CampaignDetailsId
                                        INNER JOIN dbo.vwFundraisingData fd ON fd.CampaignId = cd.CampaignId
                              WHERE     cd.CampaignYear = @year
                                        AND RegionId IN (
                                        SELECT DISTINCT
                                                RegionId
                                        FROM    dbo.vwFundraisingData
                                        WHERE   MarketId = @market )
                              UNION ALL
                              SELECT    '' ,
                                        '' ,
                                        '' ,
                                        SUM(PopulationEstimate) AS PopEstReg ,
                                        ''
                              FROM      dbo.vwMarketPopulationRank
                              WHERE     MarketId IN (
                                        SELECT DISTINCT
                                                marketId
                                        FROM    dbo.vwFundraisingData
                                        WHERE   RegionId = ( SELECT  DISTINCT
                                                              RegionId
                                                             FROM
                                                              dbo.vwFundraisingData
                                                             WHERE
                                                              MarketId = @market
                                                           ) )
                            ) AS b 
			

    SELECT  *
    FROM    #tempDMCompRegion
