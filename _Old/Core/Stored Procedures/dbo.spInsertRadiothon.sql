﻿

-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/10/2015
-- Description:	This procedure is used to add a new Radiothon.
-- Updates    : 08/12/2015 - Remove HospitalContactId, RadiothonContactId
--
-- =============================================
CREATE PROCEDURE [dbo].[spInsertRadiothon]
	@CampaignDetailsId int,
	@City varchar(100),
	@OnAirHours smallint,
	@OnAirMinutes smallint,
	@LanguageId int,
	@PhoneBank varchar(100),
	@Note varchar(MAX),
	@SubmittedBy int,
	@Active bit,
	@EventId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Radiothons] (

		[CampaignDetailsId],
		[City],
		[OnAirHours],
		[OnAirMinutes],
		[LanguageId],
		[PhoneBank],
		[Note],
		[SubmittedBy],
		[SubmittedDate],
		[ModifiedBy],
		[ModifiedDate],
		[Active],
		[EventId],
		[RadiothonContactEmailSent]
	)
	VALUES (

		@CampaignDetailsId,
		@City,
		@OnAirHours,
		@OnAirMinutes,
		@LanguageId,
		@PhoneBank,
		@Note,
		@SubmittedBy,
		GETDATE(),
		@SubmittedBy,
		GETDATE(),
		@Active,
		@EventId,
		0
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END

