﻿


CREATE PROCEDURE [dbo].[rptMarketsParameterDataset]
( @RegionId	NVARCHAR(MAX) = NULL)
AS

BEGIN
	SET NOCOUNT ON

	/***********	TEST SECTION	***********/

		--DECLARE @RegionId	NVARCHAR(MAX) = '1'
		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

	/*********** END TEST SECTION	***********/

	SELECT DISTINCT mkt.MarketId
		 , mkt.MarketName
		 , mkt.RegionId
		 , CountryId
	  FROM Markets AS mkt INNER JOIN ContractHolders AS c
		ON mkt.MarketId = c.PrimaryMarketId
	  WHERE @RegionId IS NULL
		OR mkt.RegionId IN (SELECT Item FROM dbo.DelimitedSplitN4K(@RegionId, ',') AS DelimitedSplitN4K_1)
	ORDER BY CountryId, mkt.MarketName

END


