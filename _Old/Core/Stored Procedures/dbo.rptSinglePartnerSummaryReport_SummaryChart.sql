﻿


CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryReport_SummaryChart]
  (	  @year     INT 
	, @partnerid INT 	
  )

AS
/****************TEST***********************/
--DECLARE @year INT = 2014,
--		@partnerid INT = 123
/******************************************/

IF OBJECT_ID('tempdb..#tempMarket', 'U') IS NOT NULL DROP TABLE #tempMarket

SELECT
CASE 
	WHEN DirectToHospital = 0 
		THEN 'Thru CMN Hospitals' 
		ELSE 'Direct to Market' 
		END AS Cateogry
, SUM(CASE WHEN FundraisingYear = @year - 3 THEN Amount END) AS cyminus3
, SUM(CASE WHEN FundraisingYear = @year - 2 THEN Amount END) AS cyminus2
, SUM(CASE WHEN FundraisingYear = @year - 1 THEN Amount END) AS cyminus1
, SUM(CASE WHEN FundraisingYear = @year THEN Amount END) AS cy
, FundraisingYear
INTO #tempMarket
FROM vwFundraisingData vfd
WHERE (vfd.FundraisingCategoryId = 1) 
	AND (FundraisingYear >= @year - 3) 
	AND (vfd.FundraisingEntityId = @partnerid)
GROUP BY DirectToHospital, FundraisingYear

DECLARE @Totalcy MONEY = (SELECT SUM(cy) FROM #tempMarket)
DECLARE @Totalcy3 MONEY = (SELECT SUM(cyminus3) FROM #tempMarket)
DECLARE @Totalcy2 MONEY = (SELECT SUM(cyminus2) FROM #tempMarket)
DECLARE @Totalcy1 MONEY = (SELECT SUM(cyminus1) FROM #tempMarket)

SELECT
	Cateogry
	, cyminus3 / NULLIF(@Totalcy3, 0) AS Totalcy3
	, cyminus2 / NULLIF(@Totalcy2, 0) AS Totalcy2
	, cyminus1 / NULLIF(@Totalcy1, 0) AS Totalcy1
	, cy / NULLIF(@Totalcy, 0) AS Totalcy
	, FundraisingYear
FROM #tempMarket

