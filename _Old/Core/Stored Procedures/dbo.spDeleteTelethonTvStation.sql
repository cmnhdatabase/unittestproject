﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to delete an existing TelethonTvStation record.
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteTelethonTvStation]
	@TelethonId int,
	@TvStationId int,
	@PrimaryStation bit
AS
BEGIN
	DELETE FROM [dbo].[TelethonTvStations]
	WHERE [TelethonId] = @TelethonId AND [TvStationId] = @TvStationId AND [PrimaryStation] = @PrimaryStation;
END

-------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[spInsertRadiothonHospital] ******/
SET ANSI_NULLS ON
