﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/10/2015
-- Description:	This procedure is used to add a new RadiothonRadioStation record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertRadiothonRadioStation]
	@RadiothonId int,
	@RadioStationId int,
	@PrimaryRadioStation bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[RadiothonRadioStations] (
		[RadiothonId],
		[RadioStationId],
		[PrimaryRadioStation]
	)
	VALUES (
		@RadiothonId,
		@RadioStationId,
		@PrimaryRadioStation
	)
END


