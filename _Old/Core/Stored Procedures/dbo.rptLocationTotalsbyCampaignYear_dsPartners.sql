﻿


CREATE PROCEDURE [dbo].[rptLocationTotalsbyCampaignYear_dsPartners]
(	@MarketId	NVARCHAR(MAX)	)
AS

BEGIN
	SET NOCOUNT ON

	/**********************	 TEST SECTION	**********************/
		--declare @MarketID NVARCHAR(MAX) = '10, 12, 6'

		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	
	/********************** END TEST SECTION **********************/

	DECLARE @Markets TABLE ( MarketId	INT )
	INSERT INTO @Markets
	  SELECT Item FROM [dbo].[DelimitedSplitN4K] (@MarketID, ',')

	SELECT DISTINCT 
		   pp.fundraisingEntityId
		 , pp.DisplayName + '-' + co.Abbreviation AS DisplayName
	  FROM dbo.Disbursements AS db
		INNER JOIN @Markets AS mkt ON db.SubMarketId = mkt.MarketId
		INNER JOIN dbo.fundraisingEntities AS pp ON db.fundraisingEntityId = pp.fundraisingEntityId
		INNER JOIN dbo.Countries AS co ON pp.CountryId = co.CountryId
	  WHERE 
	  pp.FundraisingCategoryId = 1 AND 
	  Active = 1
	  ORDER BY DisplayName

END


