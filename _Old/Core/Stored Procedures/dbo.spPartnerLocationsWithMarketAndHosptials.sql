﻿


-- =============================================
-- Author:		Travis Poll	
-- Create date: 05/01/2014
-- Description:	Returns a list of all partner locations with thier marketname and hospital names.
-- =============================================
CREATE PROCEDURE [dbo].[spPartnerLocationsWithMarketAndHosptials]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @tempmarketdata TABLE
	  ( 
		 marketid      INT, 
		 hospitalnames VARCHAR(2000) 
	  ) 

	INSERT INTO @tempmarketdata 
	SELECT t.marketid, 
		   t.hospitalnames 
	FROM   (SELECT marketid, 
				   dbo.Gethospitalnamesbymarketid(marketid) AS hospitalNames 
			FROM   Core..markets) t 

	--Locations with market and hospital names 
	SELECT DISTINCT l.[locationnumber], 
					l.[FundraisingEntityId], 
					l.[postalcode], 
					CASE WHEN rl.[RedirectedToPostalCode] IS NULL THEN m.[marketname] ELSE sm.[MarketName] END AS MarketName, 
					CASE WHEN rl.[RedirectedToPostalCode] IS NULL THEN t.hospitalnames ELSE st.hospitalnames END AS Hospitalnames 
	FROM   [Core].[dbo].locations l 
			--Redirected Locations
		   LEFT JOIN [RedirectedLocations] rl ON rl.[LocationId] = l.[LocationId]
		   LEFT JOIN postalcodes spc ON spc.postalcode = rl.[RedirectedToPostalCode]
		   LEFT JOIN @tempmarketdata st ON st.[marketid] = spc.[MarketId]
		   LEFT JOIN [Markets] sm ON sm.[MarketId] = st.[marketid]
		   --non-redirected locations
		   LEFT JOIN [PostalCodes] pc ON pc.[PostalCode] = l.[PostalCode] 
		   LEFT JOIN @tempmarketdata t ON pc.[MarketId] = t.[marketid] 
		   LEFT JOIN [Markets] m ON m.[marketid] = t.[marketid] 
			 WHERE l.PostalCode != '00000'
END





