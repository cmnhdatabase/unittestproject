﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 08/11/2016
-- Description:	This sp will grab all hospitals in salesforce and update the information in the core database.
-- =============================================
CREATE PROCEDURE [sf].[spUpdateHospitalFromSFDC]
	-- Add the parameters for the stored procedure here
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        UPDATE  h
        SET     h.HospitalName = a.Name ,
                h.FriendlyHospitalName = a.Name ,
                h.Active = CASE WHEN ( a.Hospital_Membership_Status__c = 'Active' )
                                THEN 1
                                ELSE 0
                           END ,
                h.LongDescription = a.Description ,
                h.ShortDescription = a.Hospital_Short_Description__c ,
                h.Address1 = a.billingstreet ,
				--We appeneded address2 to address1 in our system to accomodate salesforce.
				--h.Address2 = LTRIM(REPLACE(a.billingstreet, ISNULL(h.Address1,''), '')),
                h.City = a.BillingCity ,
                h.ProvinceId = p.ProvinceId ,
                h.PostalCode = a.BillingPostalCode ,
                h.CountryId = p.CountryId ,
                h.Website = a.Website ,
                h.SalesForceId = a.Id
        FROM    Hospitals AS h
                INNER JOIN SALESFORCE...Account AS a ON h.HospitalId = a.CMN_Hospital_ID__c
                INNER JOIN SALESFORCE...RecordType AS rt ON rt.Name = 'Hospital'
                                                            AND a.RecordTypeId = rt.Id
                INNER JOIN Provinces AS p ON a.BillingState = p.Province;
    END;

