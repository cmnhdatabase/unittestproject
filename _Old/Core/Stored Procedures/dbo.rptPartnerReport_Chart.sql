﻿










CREATE PROCEDURE [dbo].[rptPartnerReport_Chart]
(
@year INT  
, @prtnr INT 
, @foundation INT
)
AS

BEGIN
	/***********	TEST SECTION	***********/
--	DECLARE
--@year INT  = 2014
--, @prtnr INT = 123
------, @mrkt INT = 1



	/*********** END TEST SECTION	***********/
CREATE TABLE #tempLocation
(
    LocationId INT,
    District VARCHAR(MAX),
    Zone VARCHAR(MAX),
	Division VARCHAR(MAX),
	Facility VARCHAR(MAX),
	Region VARCHAR(MAX),

)

INSERT INTO #tempLocation
        ( LocationId ,
          District ,
          Zone ,
          Division ,
		Facility ,
          Region
        )
EXEC dbo.rptLocationAreas

SELECT v.FundraisingYear, 
MONTH(v.DonationDate) AS MonthInt, 
CONVERT(CHAR(3), 
v.DonationDate, 0) AS MonthName, 
SUM(v.Amount) AS Amount, 
ct.CampaignType, 
ct.CampaignTypeId,
t.Region,
t.Division,
CAST (CASE WHEN b.Foundation = '1' THEN 1 ELSE 0 END AS INT) AS 'Foundationbit',
r.RegionId,
m.MarketId,
p.PropertyTypeId

FROM #tempLocation t
	INNER JOIN dbo.Disbursements v ON t.LocationId = v.locationid 
	INNER JOIN dbo.Locations l ON v.locationid = l.LocationId
	INNER JOIN dbo.PropertyTypes p ON l.PropertyTypeId = p.PropertyTypeId
	INNER JOIN dbo.FundraisingEntities pp ON v.FundraisingEntityID = pp.FundraisingEntityID
	INNER JOIN dbo.CampaignDetails cd ON v.CampaignDetailsId = cd.CampaignDetailsId
	INNER JOIN dbo.Campaigns c ON cd.CampaignId = c.CampaignId
	INNER JOIN dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
	INNER JOIN dbo.Markets m ON v.MarketId = m.MarketId
	INNER JOIN dbo.Regions r ON r.RegionId = m.RegionId
	INNER JOIN dbo.Batches b ON v.BatchId = b.BatchId
WHERE (v.FundraisingEntityID = @prtnr) AND (v.FundraisingYear >= @year - 1) AND (v.RecordTypeId = 1) 
--AND (m.MarketId = @mrkt) 
--AND (b.Foundation = 1) 
AND (v.DonationDate <= DATEADD(YEAR, - 1, GETDATE())) AND 
(v.FundraisingYear = @year - 1) OR
(v.FundraisingEntityID = @prtnr) AND (v.FundraisingYear >= @year - 1) AND (v.RecordTypeId = 1) 
--AND (m.MarketId = @mrkt) 
AND (b.Foundation = @foundation) AND (v.FundraisingYear = @year)
GROUP BY v.FundraisingYear, MONTH(v.DonationDate), CONVERT(CHAR(3), v.DonationDate, 0), ct.CampaignType, ct.CampaignTypeId, t.Region,
t.Division,
b.Foundation,
r.RegionId,
m.MarketId,
p.PropertyTypeId
END

--DROP TABLE #tempLocation










