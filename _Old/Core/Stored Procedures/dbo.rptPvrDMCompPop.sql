﻿

CREATE PROCEDURE [dbo].[rptPvrDMCompPop] ( @year INT, @market INT )
AS
 /****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market INT = 95
/*****************/

    IF OBJECT_ID('tempdb..#tempDMCompPop', 'U') IS NOT NULL
        DROP TABLE #tempDMCompPop

    SELECT  

            SUM(c.CYPopTotal) / NULLIF(SUM(c.PopDMCount), 0) AS PopAverage ,
            (SUM(c.CYPopTotal) - SUM(c.CYMinus1PopTotal))/NULLIF(SUM(c.PopDMCount), 0) AS PopIncrease ,
            ( SUM(c.CYPopTotal) - SUM(c.CYMinus1PopTotal) )
            / NULLIF(SUM(c.CYPopTotal), 0) AS PopPctChange ,
            SUM(c.CYPopTotal) / SUM(c.Pop) AS PopPerCap
    INTO    #tempDMCompPop
    FROM    ( 
	
			SELECT    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                                        THEN d.Amount
                                                   END), 0) AS CYPopTotal ,
                                        ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                                             - 1 THEN d.Amount
                                                   END), 0) AS CYMinus1PopTotal ,
                                        '' AS PopDMCount ,
                                        '' AS Pop ,
                                        MarketId
                              FROM      dbo.vwFundraisingData d
                              WHERE     ( d.FundraisingYear >= @year - 1 )
                                        AND d.CampaignTypeId = 10
                                        AND MarketId IN (
                                        SELECT DISTINCT
                                                MarketId
                                        FROM    dbo.vwMarketPopulationRank
                                        WHERE   PopulationRank = ( SELECT DISTINCT
                                                              PopulationRank
                                                              FROM
                                                              dbo.vwMarketPopulationRank
                                                              WHERE
                                                              MarketId = @market
                                                              ) )
                              GROUP BY  MarketId
                              UNION ALL
                              SELECT    '' ,
                                        '' ,
                                        COUNT(DISTINCT DanceMarathonId) AS PopDMCount ,
                                        '' ,
                                        ''
                              FROM      dbo.vwDanceMarathons dm
                                        INNER JOIN dbo.vwCampaignDetails cd ON cd.CampaignDetailsId = dm.CampaignDetailsId
                                        INNER JOIN dbo.vwFundraisingData fd ON fd.CampaignId = cd.CampaignId
                              WHERE     cd.CampaignYear = @year
                                        AND MarketId IN (
                                        SELECT DISTINCT
                                                MarketId
                                        FROM    dbo.vwMarketPopulationRank
                                        WHERE   PopulationRank = ( SELECT DISTINCT
                                                              PopulationRank
                                                              FROM
                                                              dbo.vwMarketPopulationRank
                                                              WHERE
                                                              MarketId = @market
                                                              ) )
                              UNION ALL
                              SELECT    '' ,
                                        '' ,
                                        '' ,
                                        SUM(PopulationEstimate) AS PopEstPop ,
                                        ''
                              FROM      dbo.vwMarketPopulationRank
                              WHERE     MarketId IN (
                                        SELECT DISTINCT
                                                MarketId
                                        FROM    dbo.vwMarketPopulationRank
                                        WHERE   PopulationRank = ( SELECT DISTINCT
                                                              PopulationRank
                                                              FROM
                                                              dbo.vwMarketPopulationRank
                                                              WHERE
                                                              MarketId = @market
                                                              ) )
                            ) AS c 
			

    SELECT  *
    FROM    #tempDMCompPop
