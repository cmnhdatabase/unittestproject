﻿
CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryByMarket_GeographicalComparisons]
(
@year INT,
@partnerid NVARCHAR(MAX),
@marketid NVARCHAR(MAX)
)
AS
BEGIN
SELECT r.RegionName AS Geography,
       SUM(CASE
               WHEN f.fundraisingyear = @year - 3
               THEN f.amount
           END) AS 'CYminus3',
       SUM(CASE
               WHEN f.fundraisingyear = @year - 2
               THEN f.amount
           END) AS 'CYminus2',
       SUM(CASE
               WHEN f.fundraisingyear = @year - 1
               THEN f.amount
           END) AS 'CYminus1',
       COALESCE(SUM(CASE
                        WHEN f.fundraisingyear = @year
                        THEN f.amount
                    END), 0) AS 'CY',
       d.markcount AS 'NumberofBenefittingMarkets',
       d2.loccount AS 'NumberofFundraisingLocations',
       ( CASE
             WHEN d2.loccount = 0
             THEN 0
             ELSE SUM(CASE
                          WHEN f.fundraisingyear = @year - 1
                          THEN f.amount
                      END) / d2.loccount
         END ) AS 'RegionLocAverageCYminus1',
       d3.loccount AS 'TotalNumberofLocations',
       COALESCE(( CASE
                      WHEN d3.loccount = 0
                      THEN 0
                      ELSE SUM(CASE
                                   WHEN f.fundraisingyear = @year
                                   THEN f.amount
                               END) / d3.loccount
                  END ), 0) AS 'RegionLocAverageCY'
FROM vwFundraisingData AS f
     INNER JOIN Regions AS r ON r.RegionId = f.RegionId
     INNER JOIN( 
                 SELECT RegionId,
                        COUNT(DISTINCT MarketId) AS markcount
                 FROM vwFundraisingData
                 WHERE( FundraisingYear = @year - 1 )
                  AND ( FundraisingEntityId = @partnerid )
                  AND MarketId IN(
                          SELECT Item
                          FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
                 GROUP BY RegionId ) AS d ON d.RegionId = r.RegionId
     INNER JOIN( 
                 SELECT r.RegionId,
                        COUNT(DISTINCT di.LocationId) AS loccount
                 FROM Disbursements AS di
                      INNER JOIN Markets AS m ON m.MarketId = di.MarketId
                      INNER JOIN Regions AS r ON r.RegionId = m.RegionId
                 WHERE( di.FundraisingYear = @year - 1 )
                  AND ( di.FundraisingEntityId = @partnerid )
                 GROUP BY r.RegionId ) AS d2 ON f.RegionId = d2.RegionId
     INNER JOIN( 
                 SELECT r.RegionId,
                        CONVERT( INT, SUM(cnt)) AS loccount
                 FROM(
                          SELECT l.PostalCode,
                                 COUNT(DISTINCT LocationId) cnt
                          FROM dbo.Locations l
                          WHERE FundraisingEntityId = @partnerid
                            AND l.Active = 1
                            AND CAST(LocationId AS VARCHAR(100)) NOT IN(
                                   SELECT splitfrom
                                   FROM dbo.Splits)
                          GROUP BY l.PostalCode
                          UNION ALL
                          SELECT PostalCode,
                                 SUM(SplitPercent)
                          FROM dbo.Splits
                          WHERE FundraisingEntityId = @partnerid
                          GROUP BY PostalCode ) t
                     INNER JOIN dbo.PostalCodes pc ON t.PostalCode = pc.PostalCode
                     INNER JOIN dbo.Markets m ON m.MarketId = pc.MarketId
                     INNER JOIN dbo.Regions r ON r.RegionId = m.RegionId
                 GROUP BY r.RegionId ) AS d3 ON f.RegionID = d3.regionId
WHERE( f.FundraisingYear >= @year - 3 )
 AND ( f.FundraisingEntityId = @partnerid )
 AND f.FundraisingCategoryId = 1
 AND MarketId IN( 
                  SELECT Item
                  FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
GROUP BY r.RegionId,
         r.RegionName,
         d.markcount,
         d2.loccount,
         d3.loccount,
         d3.RegionId;
END
