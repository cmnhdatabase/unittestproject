﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new Disbursement
-- =============================================
CREATE PROCEDURE [dbo].[spInsertDisbursement]
	@RecordTypeId INT,
	@FundraisingEntityId INT,
	@MarketId INT,
	@SubMarketId INT,
	@DirectToHospital BIT,
	@FundraisingYear INT,
	@Amount MONEY,
	@CurrencyTypeId INT,
	@CampaignDetailsId INT,
	@LocationId INT,
	@DateReceived DATE,
	@DateRecorded DATE,
	@DonationDate DATE,
	@FundTypeId INT,
	@BatchId INT,
	@CampaignPeriod INT,
	@UploadId varchar(20),
	@Comment varchar(300),
	@DisbursementPeriodId INT,
	@DisbursementDateId INT,
	@FundraisingCategoryId INT,
	@CreatedBy INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.Disbursements
	        ( RecordTypeId ,
	          FundraisingEntityId ,
	          MarketId ,
	          SubMarketId ,
	          DirectToHospital ,
	          FundraisingYear ,
	          Amount ,
	          CurrencyTypeId ,
	          CampaignDetailsId ,
	          LocationId ,
	          DateReceived ,
	          DateRecorded ,
	          DonationDate ,
	          FundTypeId ,
	          BatchId ,
	          CampaignPeriod ,
	          UploadId ,
	          Comment ,
	          DisbursementPeriodId ,
	          DisbursementDateId ,
	          FundraisingCategoryId ,
	          CreatedBy
	        )
	VALUES  ( @RecordTypeId ,
	          @FundraisingEntityId ,
	          @MarketId ,
	          @SubMarketId ,
	          @DirectToHospital ,
	          @FundraisingYear ,
	          @Amount ,
	          @CurrencyTypeId ,
	          @CampaignDetailsId ,
	          @LocationId ,
	          @DateReceived ,
	          @DateRecorded ,
	          @DonationDate ,
	          @FundTypeId ,
	          @BatchId ,
	          @CampaignPeriod ,
	          @UploadId ,
	          @Comment ,
	          @DisbursementPeriodId ,
	          @DisbursementDateId ,
	          @FundraisingCategoryId ,
	          @CreatedBy
	        )
	SELECT CAST(SCOPE_IDENTITY() AS INT)
END



