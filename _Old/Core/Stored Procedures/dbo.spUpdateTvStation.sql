﻿
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 026/12/2015
-- Description:	This procedure is used to update an existing TV record.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateTvStation]
	@TvStationId int,
	@CallLetters varchar(10),
	@Affiliation varchar(10),
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId int,
	@PostalCode varchar(10),
	@CountryId int,
	@Website varchar(100),
	@MarketId int,
	@StationOwner varchar(100),
	@MarketTag varchar(100),
	@Director int,
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[TV] SET
		[CallLetters] = @CallLetters,
		[Affiliation] = @Affiliation,
		[Address1] = @Address1,
		[Address2] = @Address2,
		[City] = @City,
		[ProvinceId] = @ProvinceId,
		[PostalCode] = @PostalCode,
		[CountryId] = @CountryId,
		[Website] = @Website,
		[MarketId] = @MarketId,
		[StationOwner] = @StationOwner,
		[MarketTag] = @MarketTag,
		[Director] = @Director,
		[Note] = @Note,
		[ModifiedBy] = @ModifiedBy,
		[ModifiedDate] = GETDATE(),
		[Active] = @Active
	WHERE [TvStationId] = @TvStationId;
END

