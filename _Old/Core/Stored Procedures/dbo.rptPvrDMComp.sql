﻿
CREATE PROCEDURE [dbo].[rptPvrDMComp]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 175
/*****************/

IF OBJECT_ID('tempdb..#temp', 'U') IS NOT NULL
    DROP TABLE #temp

SELECT   
        SUM(CYNatTotal) / NULLIF(SUM(a.DmCount), 0) AS NatAverage ,
        SUM(a.CYNatTotal) - SUM(a.CYMinus1NatTotal) AS Increase ,
        ( SUM(CYNatTotal) - SUM(CYMinus1NatTotal) ) / NULLIF(SUM(CYNatTotal),
                                                             0) AS PctChange ,
        SUM(CYNatTotal)/ SUM(a.pop) AS PerCap,
		
		SUM(b.CYRegTotal) / NULLIF(SUM(b.DMCount), 0) AS RegAverage ,
        SUM(b.CYRegTotal) - SUM(b.CYMinus1RegTotal) AS RegIncrease ,
        (SUM(b.CYRegTotal) - SUM(b.CYMinus1RegTotal) ) / NULLIF(SUM(b.CYRegTotal),
                                                             0) AS RegPctChange ,
        SUM(b.CYRegTotal)/ SUM(b.Pop) AS RegPerCap,
		
		SUM(c.CYPopTotal) / NULLIF(SUM(c.PopDMCount), 0) AS PopAverage ,
        SUM(c.CYPopTotal) - SUM(c.CYMinus1PopTotal) AS PopIncrease ,
        (SUM(c.CYPopTotal) - SUM(c.CYMinus1PopTotal) ) / NULLIF(SUM(c.CYPopTotal),
                                                             0) AS PopPctChange ,
        SUM(c.CYPopTotal)/ SUM(c.Pop) AS PopPerCap
		

INTO    #temp
FROM    
( SELECT    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                    THEN d.Amount
                               END), 0) AS CYNatTotal ,
                    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year - 1
                                    THEN d.Amount
                               END), 0) AS CYMinus1NatTotal ,
                    '' AS DMCount ,
                    '' AS pop,
					MarketId
          FROM      dbo.vwFundraisingData d
          WHERE     ( d.FundraisingYear >= @year - 1 )
                    AND d.CampaignTypeId = 10
			GROUP BY MarketId
          UNION ALL
          SELECT    '' ,
                    '' ,
                    COUNT(DISTINCT DanceMarathonId) AS DMCount ,
                    '',
					''
          FROM      dbo.vwDanceMarathons dm
                    INNER JOIN dbo.vwCampaignDetails cd ON cd.CampaignDetailsId = dm.CampaignDetailsId
          WHERE     cd.CampaignYear = @year
          UNION ALL
          SELECT    '', '', '', PopulationEstimate, ''
          FROM      dbo.vwMarketPopulationRank
        ) AS a 
LEFT OUTER JOIN 
		

		( SELECT    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                    THEN d.Amount
                               END), 0) AS CYRegTotal ,
                    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year - 1
                                    THEN d.Amount
                               END), 0) AS CYMinus1RegTotal ,
                    '' AS DMCount ,
                    '' AS pop,
					MarketId
          FROM      dbo.vwFundraisingData d
          WHERE     ( d.FundraisingYear >= @year - 1 )
                    AND d.CampaignTypeId = 10
					AND RegionId in (SELECT DISTINCT RegionId FROM dbo.vwFundraisingData WHERE MarketId = @market)
			GROUP BY MarketId
          UNION ALL
          SELECT    '' ,
                    '' ,
                    COUNT(DISTINCT DanceMarathonId) AS RegDMCount ,
                    '',
					''
          FROM      dbo.vwDanceMarathons dm
                    INNER JOIN dbo.vwCampaignDetails cd ON cd.CampaignDetailsId = dm.CampaignDetailsId
					INNER JOIN dbo.vwFundraisingData fd ON fd.CampaignId = cd.CampaignId
          WHERE     cd.CampaignYear = @year
		  AND RegionId IN (SELECT DISTINCT RegionId FROM dbo.vwFundraisingData WHERE MarketId = @market)
          UNION ALL
          SELECT
		  '', '', '', SUM(PopulationEstimate) AS PopEstReg, ''
          FROM dbo.vwMarketPopulationRank
		  WHERE MarketId IN (
SELECT DISTINCT marketId FROM dbo.vwFundraisingData WHERE RegionId = (SELECT  DISTINCT RegionId
FROM dbo.vwFundraisingData
WHERE   MarketId = @market))) 
AS b 
ON b.MarketId = a.MarketId
LEFT OUTER JOIN  
(SELECT    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year
                                    THEN d.Amount
                               END), 0) AS CYPopTotal ,
                    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year - 1
                                    THEN d.Amount
                               END), 0) AS CYMinus1PopTotal ,
                    '' AS PopDMCount ,
                    '' AS Pop,
					MarketId
          FROM      dbo.vwFundraisingData d
          WHERE     ( d.FundraisingYear >= @year - 1 )
                    AND d.CampaignTypeId = 10
					AND MarketId IN (SELECT DISTINCT MarketId FROM dbo.vwMarketPopulationRank WHERE PopulationRank = (SELECT DISTINCT PopulationRank FROM dbo.vwMarketPopulationRank WHERE MarketId = @market))
			GROUP BY MarketId
          UNION ALL
          SELECT    '' ,
                    '' ,
                    COUNT(DISTINCT DanceMarathonId) AS PopDMCount ,
                    '',
					''
          FROM      dbo.vwDanceMarathons dm
                    INNER JOIN dbo.vwCampaignDetails cd ON cd.CampaignDetailsId = dm.CampaignDetailsId
					INNER JOIN dbo.vwFundraisingData fd ON fd.CampaignId = cd.CampaignId
          WHERE     cd.CampaignYear = @year
		  AND MarketId IN (SELECT DISTINCT MarketId FROM dbo.vwMarketPopulationRank WHERE PopulationRank = (SELECT DISTINCT PopulationRank FROM dbo.vwMarketPopulationRank WHERE MarketId = @market))
          UNION ALL
          SELECT
		  '', '', '', SUM(PopulationEstimate) AS PopEstPop, ''
          FROM dbo.vwMarketPopulationRank
		  WHERE MarketId IN (SELECT DISTINCT MarketId FROM dbo.vwMarketPopulationRank WHERE PopulationRank = (SELECT DISTINCT PopulationRank FROM dbo.vwMarketPopulationRank WHERE MarketId = @market))
) AS c 
ON c.MarketId = a.MarketId
			

SELECT  *
FROM    #temp





