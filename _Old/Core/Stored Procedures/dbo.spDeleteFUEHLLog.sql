﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure will remove the FUEHLLog containing the LogId passed to the procedure.
-- =============================================
Create PROCEDURE [dbo].[spDeleteFUEHLLog]
@LogId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.FUEHLLog
      WHERE LogId = @LogId
END



