﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This will change the reconciled status of the given batch.  It will also update the date to current date or null if unreconciling.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateBatchReconciledStatus]
	@BatchNumber VARCHAR(25), @Reconcile bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Batches
	SET Reconciled = @Reconcile, 
		DateReconciled =  CASE @Reconcile WHEN 0 THEN NULL ELSE GETDATE() END
	WHERE BatchNumber = @BatchNumber
END


