﻿
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/12/2015
-- Description:	This procedure is used to update an existing DanceMarathon record.
-- Updates    : 08/12/2015 - Remove HospitalContactId
--            : 10/27/2015 - Changed ManagerId to INT
--            : 12/10/2015 - Added RegistrationUrl and DonationUrl
--            : 06/21/2016 - Add HasMiniMarathons, MiniMarathonCount, MiniMarathonContribution, DanceMarathonTypeId and Nickname
--
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateDanceMarathon]
	@DanceMarathonId int,
	@CampaignDetailsId int,
	@SchoolId int,
	@TotalHours int,
	@Shifts int,
	@AnticipatedParticipants int,
	@TotalParticipants int,
	@ContactFirstName varchar(50),
	@ContactLastName varchar(50),
	@ContactEmail varchar(100),
	@GrossTotal money,
	@ToteBoardImagePath varchar(500),
	@EventId int,
	@Note varchar(MAX),
	@ModifiedBy int,
	@Active bit,
	@ManagerId int,
	@RegistrationUrl varchar(1000),
	@DonationUrl varchar(1000),
	@HasMiniMarathons bit,
	@MiniMarathonCount INT,
	@MiniMarathonContribution money,
	@DanceMarathonTypeId int,
	@Nickname varchar(300)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[DanceMarathons] SET
		[CampaignDetailsId] = @CampaignDetailsId,
		[SchoolId] = @SchoolId,
		[TotalHours] = @TotalHours,
		[Shifts] = @Shifts,
		[AnticipatedParticipants] = @AnticipatedParticipants,
		[TotalParticipants] = @TotalParticipants,
		[ContactFirstName] = @ContactFirstName,
		[ContactLastName] = @ContactLastName,
		[ContactEmail] = @ContactEmail,
		[GrossTotal] = @GrossTotal,
		[ToteBoardImagePath] = @ToteBoardImagePath,
		[EventId] = @EventId,
		[Note] = @Note,
		[ModifiedBy] = @ModifiedBy,
		[ModifiedDate] = GETDATE(),
		[Active] = @Active,
		[ManagerId] = @ManagerId,
		[RegistrationUrl] = @RegistrationUrl,
		[DonationUrl] = @DonationUrl,
		[HasMiniMarathons] = @HasMiniMarathons,
		[MiniMarathonCount] = @MiniMarathonCount,
		[MiniMarathonContribution] = @MiniMarathonContribution,
		[DanceMarathonTypeId] = @DanceMarathonTypeId,
		[Nickname] = @Nickname
	WHERE [DanceMarathonId] = @DanceMarathonId;
END
