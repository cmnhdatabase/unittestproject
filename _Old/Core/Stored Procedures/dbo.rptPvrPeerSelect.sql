﻿


CREATE PROCEDURE [dbo].[rptPvrPeerSelect]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market INT = 178
/*****************/

IF OBJECT_ID('tempdb..#tempPeerSelect', 'U') IS NOT NULL
    DROP TABLE #tempPeerSelect

Declare @peer1 INT = NULLIF((SELECT peer1 FROM core.dbo.PVRFees WHERE MarketId = @market AND YEAR = @year),'NULL')
Declare @peer2 INT = NULLIF((SELECT peer2 FROM core.dbo.PVRFees WHERE MarketId = @market AND YEAR = @year),'NULL')

SELECT  SUBSTRING(MarketName ,1, 19) AS MarketName,
        PopulationEstimate ,
        RegionName ,
        CYMinus2 ,
        CYMinus1 ,
        CY ,
        PopRank ,
        a.MarketId ,
        ( ( b.CYMinus1 - b.CYMinus2 ) / NULLIF(b.CYMinus1, 0) ) AS CY2VsCY1 ,
        ( ( b.CY - b.CYMinus1 ) / NULLIF(b.CYMinus1, 0) ) AS CY1VsCY ,
        ( CYMinus1 / NULLIF(PopulationEstimate, 0) ) AS CYMinus1PerCap ,
        ( CY / NULLIF(PopulationEstimate, 0) ) AS CyPerCap,
		CASE WHEN a.MarketId = @peer1 THEN 1 
		WHEN a.MarketId = @market THEN 2
		ELSE 3 END AS Orders
		
INTO    #tempPeerSelect
FROM    ( SELECT    mpr.ShortName AS MarketName ,
                    PopulationEstimate ,
                    mpr.MarketId ,
                    r.RegionName ,
                    RANK() OVER ( ORDER BY populationEstimate DESC ) AS PopRank
          FROM      dbo.vwMarketPopulationRank mpr
                    INNER JOIN dbo.Markets m ON m.MarketId = mpr.MarketId
                    INNER JOIN dbo.Regions r ON r.RegionId = m.RegionId
        ) AS a
        INNER JOIN ( SELECT ISNULL(SUM(CASE WHEN FundraisingYear = @year - 2
                                            THEN Amount
                                       END), 0) AS 'CYMinus2' ,
                            ISNULL(SUM(CASE WHEN FundraisingYear = @year - 1
                                            THEN Amount
                                       END), 0) AS 'CYMinus1' ,
                            ISNULL(SUM(CASE WHEN FundraisingYear = @year
                                            THEN Amount
                                       END), 0) AS 'CY' ,
                            MarketId
                     FROM   dbo.vwFundraisingData
                     GROUP BY MarketId
                   ) AS b ON b.MarketId = a.MarketId



SELECT  tps.*, FTE, (CY/FTE) AS PerFte
FROM    #tempPeerSelect tps
INNER JOIN dbo.PVRFees pf ON pf.MarketId = tps.MarketId 
WHERE  (tps.MarketId = @peer1
OR tps.MarketId = @peer2
OR tps.MarketId = @market)
AND pf.Year = @year
ORDER BY Orders
