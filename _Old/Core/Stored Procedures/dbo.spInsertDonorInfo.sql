﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new DonorInfo record, if the disbursement record doesnt exist it will error.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertDonorInfo]
	@DisbursementId INT,
	@Amount MONEY,
	@DonationDate DATE,
	@FirstName varchar(100),
	@MiddleName varchar(100),
	@LastName varchar(100),
	@Address1 varchar(100),
	@Address2 varchar(100),
	@City varchar(100),
	@ProvinceId INT,
	@PostalCode varchar(15),
	@CountryId INT,
	@Email varchar(100),
	@Receiptable BIT,
	@CompanyName varchar(150),
	@CurrencyTypeId INT,
	@FundTypeId INT,
	@AssociateId INT,
	@TransactionID nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @err_message VARCHAR(50)
	--IF NOT EXISTS (select * from dbo.Disbursements WHERE DisbursementId = @DisbursementId)
	--	BEGIN
		INSERT INTO dbo.DonorInfo
		        ( DisbursementId ,
		          Amount ,
		          DonationDate ,
		          FirstName ,
		          MiddleName ,
		          LastName ,
		          Address1 ,
		          Address2 ,
		          City ,
		          ProvinceId ,
		          PostalCode ,
		          CountryId ,
		          Email ,
		          Receiptable ,
		          CompanyName ,
		          CurrencyTypeId ,
		          FundTypeId ,
		          AssociateId ,
		          TransactionID
		        )
		VALUES  ( @DisbursementId ,
		          @Amount ,
		          @DonationDate ,
		          @FirstName ,
		          @MiddleName ,
		          @LastName ,
		          @Address1 ,
		          @Address2 ,
		          @City ,
		          @ProvinceId ,
		          @PostalCode ,
		          @CountryId ,
		          @Email ,
		          @Receiptable ,
		          @CompanyName ,
		          @CurrencyTypeId ,
		          @FundTypeId ,
		          @AssociateId ,
		          @TransactionID
		        )
			SELECT CAST(SCOPE_IDENTITY() AS INT)
	--	END
	--ELSE
	--	BEGIN
	--	SET @err_message = @DisbursementId +  ' does not exist in the Disbursements table.'
 --   		RAISERROR (@err_message,10, 1) 
	--	END
END


