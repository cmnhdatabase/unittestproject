﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new CheckNumber, if the check number already exists it will raise and error.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertCheckNumber]
	@CheckNumber VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @err_message VARCHAR(50)
	IF NOT EXISTS (select * from dbo.CheckNumbers WHERE CheckNumber = @CheckNumber)
		BEGIN
		INSERT INTO dbo.CheckNumbers
	        ( CheckNumber )
			VALUES  ( @CheckNumber )
		END
	ELSE
		BEGIN
		SET @err_message = @CheckNumber +  ' already exists.'
    		RAISERROR (@err_message,10, 1) 
		END

	
END


