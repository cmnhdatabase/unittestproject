﻿


-- =============================================
-- Author:		Travis Poll
-- Create date: 12/16/2014
-- Description:	This will merge all partnerprogram information from our core database to the old Core database.
-- =============================================
CREATE PROCEDURE [dbo].[spMergePartnerPrograms]
AS
 --   BEGIN
/*	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        BEGIN TRANSACTION
        BEGIN TRY

            SET IDENTITY_INSERT Core.[dbo].[Champions] ON
-----------------------------------------------------------------hosp info merge
            MERGE INTO Core.[dbo].[hospitals] AS Target
            USING
                ( SELECT    ch.HospitalId ,
                            h.HospitalName ,
                            h.FriendlyHospitalName ,
                            h.MarketId ,
                            h.Address1 ,
                            h.Address2 ,
                            h.City ,
                            h.ProvinceId ,
                            h.PostalCode ,
                            h.CountryId ,
                            h.LongDescription ,
                            h.ShortDescription ,
                            h.Website ,
                            h.HospitalId AS SyncId ,
                            h.Latitude ,
                            h.Longitude ,
                            h.Active
                  FROM      core.dbo.Hospitals h
                            INNER JOIN Core.dbo.Hospitals ch ON h.HospitalId = ch.SynchId
                ) AS Source ( [HospitalId], [HospitalName],
                              [FriendlyHospitalName], [MarketId], [Address1],
                              [Address2], [City], [ProvinceId], [PostalCode],
                              [CountryId], [LongDescription],
                              [ShortDescription], [Website], [SynchId],
                              [Latitude], [Longitude], [Active] )
            ON ( Target.HospitalId = Source.HospitalId )
            WHEN MATCHED AND ( NULLIF(Source.[HospitalName],
                                      Target.[HospitalName]) IS NOT NULL
                               OR NULLIF(Target.[HospitalName],
                                         Source.[HospitalName]) IS NOT NULL
                               OR NULLIF(Source.[FriendlyHospitalName],
                                         Target.[FriendlyHospitalName]) IS NOT NULL
                               OR NULLIF(Target.[FriendlyHospitalName],
                                         Source.[FriendlyHospitalName]) IS NOT NULL
                               OR NULLIF(Source.[MarketId], Target.[MarketId]) IS NOT NULL
                               OR NULLIF(Target.[MarketId], Source.[MarketId]) IS NOT NULL
                               OR NULLIF(Source.[Address1], Target.[Address1]) IS NOT NULL
                               OR NULLIF(Target.[Address1], Source.[Address1]) IS NOT NULL
                               OR NULLIF(Source.[Address2], Target.[Address2]) IS NOT NULL
                               OR NULLIF(Target.[Address2], Source.[Address2]) IS NOT NULL
                               OR NULLIF(Source.[City], Target.[City]) IS NOT NULL
                               OR NULLIF(Target.[City], Source.[City]) IS NOT NULL
                               OR NULLIF(Source.[ProvinceId],
                                         Target.[ProvinceId]) IS NOT NULL
                               OR NULLIF(Target.[ProvinceId],
                                         Source.[ProvinceId]) IS NOT NULL
                               OR NULLIF(Source.[PostalCode],
                                         Target.[PostalCode]) IS NOT NULL
                               OR NULLIF(Target.[PostalCode],
                                         Source.[PostalCode]) IS NOT NULL
                               OR NULLIF(Source.[CountryId],
                                         Target.[CountryId]) IS NOT NULL
                               OR NULLIF(Target.[CountryId],
                                         Source.[CountryId]) IS NOT NULL
                               OR NULLIF(Source.[LongDescription],
                                         Target.[LongDescription]) IS NOT NULL
                               OR NULLIF(Target.[LongDescription],
                                         Source.[LongDescription]) IS NOT NULL
                               OR NULLIF(Source.[ShortDescription],
                                         Target.[ShortDescription]) IS NOT NULL
                               OR NULLIF(Target.[ShortDescription],
                                         Source.[ShortDescription]) IS NOT NULL
                               OR NULLIF(Source.[Website], Target.[Website]) IS NOT NULL
                               OR NULLIF(Target.[Website], Source.[Website]) IS NOT NULL
                               OR NULLIF(Source.[SynchId], Target.[SynchId]) IS NOT NULL
                               OR NULLIF(Target.[SynchId], Source.[SynchId]) IS NOT NULL
                               OR NULLIF(Source.[Latitude], Target.[Latitude]) IS NOT NULL
                               OR NULLIF(Target.[Latitude], Source.[Latitude]) IS NOT NULL
                               OR NULLIF(Source.[Longitude],
                                         Target.[Longitude]) IS NOT NULL
                               OR NULLIF(Target.[Longitude],
                                         Source.[Longitude]) IS NOT NULL
                               OR NULLIF(Source.[Active], Target.[Active]) IS NOT NULL
                               OR NULLIF(Target.[Active], Source.[Active]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [HospitalName] = Source.[HospitalName] ,
                         [FriendlyHospitalName] = Source.[FriendlyHospitalName] ,
                         [MarketId] = Source.[MarketId] ,
                         [Address1] = Source.[Address1] ,
                         [Address2] = Source.[Address2] ,
                         [City] = Source.[City] ,
                         [ProvinceId] = Source.[ProvinceId] ,
                         [PostalCode] = Source.[PostalCode] ,
                         [CountryId] = Source.[CountryId] ,
                         [LongDescription] = Source.[LongDescription] ,
                         [ShortDescription] = Source.[ShortDescription] ,
                         [Website] = Source.[Website] ,
                         [SynchId] = Source.[SynchId] ,
                         [Latitude] = Source.[Latitude] ,
                         [Longitude] = Source.[Longitude] ,
                         [Active] = Source.[Active]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [HospitalId] ,
                         [HospitalName] ,
                         [FriendlyHospitalName] ,
                         [MarketId] ,
                         [Address1] ,
                         [Address2] ,
                         [City] ,
                         [ProvinceId] ,
                         [PostalCode] ,
                         [CountryId] ,
                         [LongDescription] ,
                         [ShortDescription] ,
                         [Website] ,
                         [SynchId] ,
                         [Latitude] ,
                         [Longitude] ,
                         [Active]
                       )
                VALUES ( Source.[HospitalId] ,
                         Source.[HospitalName] ,
                         Source.[FriendlyHospitalName] ,
                         Source.[MarketId] ,
                         Source.[Address1] ,
                         Source.[Address2] ,
                         Source.[City] ,
                         Source.[ProvinceId] ,
                         Source.[PostalCode] ,
                         Source.[CountryId] ,
                         Source.[LongDescription] ,
                         Source.[ShortDescription] ,
                         Source.[Website] ,
                         Source.[SynchId] ,
                         Source.[Latitude] ,
                         Source.[Longitude] ,
                         Source.[Active]
                       )
	   --WHEN NOT MATCHED BY SOURCE THEN 
	    --DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.[dbo].[Champions] ON	 

            MERGE INTO Core.[dbo].[Champions] AS Target
            USING
                ( SELECT    ChampionId ,
                            DisplayProvince ,
                            FirstName ,
                            LastName ,
                            Age ,
                            Illness ,
                            Year ,
                            ISNULL(h.HospitalId, NULL) ,
                            c.CountryId ,
                            BlogUri
                  FROM      core.dbo.Champions c
                            LEFT OUTER JOIN Core.dbo.Hospitals h ON c.HospitalId = h.SynchId
                ) AS Source ( [ChampionId], [DisplayProvince], [FirstName],
                              [LastName], [Age], [Illness], [Year],
                              [HospitalId], [CountryId], [BlogUri] )
            ON ( Target.[ChampionId] = Source.[ChampionId] )
            WHEN MATCHED AND ( source.ChampionId NOT IN ( 32, 65, 78, 84, 145,
                                                          162, 205, 235, 238 ) )
                AND ( NULLIF(Source.[DisplayProvince],
                             Target.[DisplayProvince]) IS NOT NULL
                      OR NULLIF(Target.[DisplayProvince],
                                Source.[DisplayProvince]) IS NOT NULL
                      OR NULLIF(Source.[FirstName], Target.[FirstName]) IS NOT NULL
                      OR NULLIF(Target.[FirstName], Source.[FirstName]) IS NOT NULL
                      OR NULLIF(Source.[LastName], Target.[LastName]) IS NOT NULL
                      OR NULLIF(Target.[LastName], Source.[LastName]) IS NOT NULL
                      OR NULLIF(Source.[Age], Target.[Age]) IS NOT NULL
                      OR NULLIF(Target.[Age], Source.[Age]) IS NOT NULL
                      OR NULLIF(Source.[Illness], Target.[Illness]) IS NOT NULL
                      OR NULLIF(Target.[Illness], Source.[Illness]) IS NOT NULL
                      OR NULLIF(Source.[Year], Target.[Year]) IS NOT NULL
                      OR NULLIF(Target.[Year], Source.[Year]) IS NOT NULL
                      OR NULLIF(Source.[HospitalId], Target.[HospitalId]) IS NOT NULL
                      OR NULLIF(Target.[HospitalId], Source.[HospitalId]) IS NOT NULL
                      OR NULLIF(Source.[CountryId], Target.[CountryId]) IS NOT NULL
                      OR NULLIF(Target.[CountryId], Source.[CountryId]) IS NOT NULL
                      OR NULLIF(Source.[BlogUri], Target.[BlogUri]) IS NOT NULL
                      OR NULLIF(Target.[BlogUri], Source.[BlogUri]) IS NOT NULL
                    ) THEN
                UPDATE SET
                         [DisplayProvince] = Source.[DisplayProvince] ,
                         [FirstName] = Source.[FirstName] ,
                         [LastName] = Source.[LastName] ,
                         [Age] = Source.[Age] ,
                         [Illness] = Source.[Illness] ,
                         [Year] = Source.[Year] ,
                         [HospitalId] = Source.[HospitalId] ,
                         [CountryId] = Source.[CountryId] ,
                         [BlogUri] = Source.[BlogUri]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [ChampionId] ,
                         [DisplayProvince] ,
                         [FirstName] ,
                         [LastName] ,
                         [Age] ,
                         [Illness] ,
                         [Year] ,
                         [HospitalId] ,
                         [CountryId] ,
                         [BlogUri]
                       )
                VALUES ( Source.[ChampionId] ,
                         Source.[DisplayProvince] ,
                         Source.[FirstName] ,
                         Source.[LastName] ,
                         Source.[Age] ,
                         Source.[Illness] ,
                         Source.[Year] ,
                         Source.[HospitalId] ,
                         Source.[CountryId] ,
                         Source.[BlogUri]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;


            SET IDENTITY_INSERT Core.[dbo].[Champions] OFF

            MERGE INTO Core.[dbo].[ChampionStories] AS Target
            USING
                ( SELECT    ct.ChampionId ,
                            ct.LanguageId ,
                            ct.Story
                  FROM      core.dbo.ChampionStories ct
                ) AS Source ( [ChampionId], [LanguageId], [Story] )
            ON ( Target.[ChampionId] = Source.[ChampionId]
                 AND Target.[LanguageId] = Source.[LanguageId]
               )
            WHEN MATCHED AND ( NULLIF(Source.[Story], Target.[Story]) IS NOT NULL
                               OR NULLIF(Target.[Story], Source.[Story]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [Story] = Source.[Story]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [ChampionId] ,
                         [LanguageId] ,
                         [Story]
                       )
                VALUES ( Source.[ChampionId] ,
                         Source.[LanguageId] ,
                         Source.[Story]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;


            SET IDENTITY_INSERT Core.[dbo].[MiracleChildren] ON

            MERGE INTO Core.[dbo].[MiracleChildren] AS Target
            USING
                ( SELECT    MiracleChildId ,
                            h.HospitalId ,
                            FirstName ,
                            LastName ,
                            mc.ShortDescription ,
                            Year ,
                            mc.CountryId
                  FROM      core.dbo.MiracleChildren mc
                            LEFT OUTER JOIN Core.dbo.Hospitals h ON mc.HospitalId = h.SynchId
                ) AS Source ( [MiracleChildId], [HospitalId], [FirstName],
                              [LastName], [ShortDescription], [Year],
                              [CountryId] )
            ON ( Target.[MiracleChildId] = Source.[MiracleChildId] )
            WHEN MATCHED AND ( NULLIF(Source.[HospitalId], Target.[HospitalId]) IS NOT NULL
                               OR NULLIF(Target.[HospitalId],
                                         Source.[HospitalId]) IS NOT NULL
                               OR NULLIF(Source.[FirstName],
                                         Target.[FirstName]) IS NOT NULL
                               OR NULLIF(Target.[FirstName],
                                         Source.[FirstName]) IS NOT NULL
                               OR NULLIF(Source.[LastName], Target.[LastName]) IS NOT NULL
                               OR NULLIF(Target.[LastName], Source.[LastName]) IS NOT NULL
                               OR NULLIF(Source.[ShortDescription],
                                         Target.[ShortDescription]) IS NOT NULL
                               OR NULLIF(Target.[ShortDescription],
                                         Source.[ShortDescription]) IS NOT NULL
                               OR NULLIF(Source.[Year], Target.[Year]) IS NOT NULL
                               OR NULLIF(Target.[Year], Source.[Year]) IS NOT NULL
                               OR NULLIF(Source.[CountryId],
                                         Target.[CountryId]) IS NOT NULL
                               OR NULLIF(Target.[CountryId],
                                         Source.[CountryId]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [HospitalId] = Source.[HospitalId] ,
                         [FirstName] = Source.[FirstName] ,
                         [LastName] = Source.[LastName] ,
                         [ShortDescription] = Source.[ShortDescription] ,
                         [Year] = Source.[Year] ,
                         [CountryId] = Source.[CountryId]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [MiracleChildId] ,
                         [HospitalId] ,
                         [FirstName] ,
                         [LastName] ,
                         [ShortDescription] ,
                         [Year] ,
                         [CountryId]
                       )
                VALUES ( Source.[MiracleChildId] ,
                         Source.[HospitalId] ,
                         Source.[FirstName] ,
                         Source.[LastName] ,
                         Source.[ShortDescription] ,
                         Source.[Year] ,
                         Source.[CountryId]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.[dbo].[MiracleChildren] OFF

            MERGE INTO Core.[dbo].[MiracleStories] AS Target
            USING
                ( SELECT    *
                  FROM      core.dbo.MiracleStories
                ) AS Source ( [MiracleChildId], [LanguageId], [Story] )
            ON ( Target.[LanguageId] = Source.[LanguageId]
                 AND Target.[MiracleChildId] = Source.[MiracleChildId]
               )
            WHEN MATCHED AND ( NULLIF(Source.[Story], Target.[Story]) IS NOT NULL
                               OR NULLIF(Target.[Story], Source.[Story]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [Story] = Source.[Story]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [MiracleChildId] ,
                         [LanguageId] ,
                         [Story]
                       )
                VALUES ( Source.[MiracleChildId] ,
                         Source.[LanguageId] ,
                         Source.[Story]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;


            MERGE INTO Core.[dbo].[RadiothonRadioStations] AS Target
            USING
                ( SELECT    RadiothonId ,
                            RadioStationId ,
                            PrimaryRadioStation
                  FROM      core.dbo.RadiothonRadioStations
                ) AS Source ( [RadiothonId], [RadioStationId],
                              [PrimaryRadioStation] )
            ON ( Target.[RadioStationId] = Source.[RadioStationId]
                 AND Target.[RadiothonId] = Source.[RadiothonId]
               )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.[dbo].[Radiothons] ON

            MERGE INTO Core.[dbo].[Radiothons] AS Target
            USING
                ( SELECT    r.RadiothonId ,
                            r.CampaignDetailsId ,
                            r.City ,
                            r.OnAirHours ,
                            r.OnAirMinutes ,
                            r.LanguageId ,
                            r.PhoneBank ,
                            r.Note ,
                            r.SubmittedBy ,
                            r.SubmittedDate ,
                            r.ModifiedBy ,
                            r.ModifiedDate ,
                            r.Active ,
                            r.EventId ,
                            r.RadiothonContactEmailSent
                  FROM      core.dbo.Radiothons r
                            LEFT OUTER JOIN dbo.RadiothonHospitals rh ON rh.RadiothonId = r.RadiothonId
                            LEFT OUTER JOIN Core.dbo.Hospitals h ON h.SynchId = rh.HospitalId
                  GROUP BY  r.RadiothonId ,
                            r.CampaignDetailsId ,
                            r.City ,
                            r.OnAirHours ,
                            r.OnAirMinutes ,
                            r.LanguageId ,
                            r.PhoneBank ,
                            r.Note ,
                            r.SubmittedBy ,
                            r.SubmittedDate ,
                            r.ModifiedBy ,
                            r.ModifiedDate ,
                            r.Active ,
                            r.EventId ,
                            r.RadiothonContactEmailSent
                ) AS Source ( [RadiothonId], [CampaignDetailsId], [City],
                              [OnAirHours], [OnAirMinutes], [LanguageId],
                              [PhoneBank], [Note], [SubmittedBy],
                              [SubmittedDate], [ModifiedBy], [ModifiedDate],
                              [Active], [EventId], [RadiothonContactEmailSent] )
            ON ( Target.[RadiothonId] = Source.[RadiothonId] )
            WHEN MATCHED AND ( NULLIF(Source.[CampaignDetailsId],
                                      Target.[CampaignDetailsId]) IS NOT NULL
                               OR NULLIF(Target.[CampaignDetailsId],
                                         Source.[CampaignDetailsId]) IS NOT NULL
                               OR NULLIF(Source.[City], Target.[City]) IS NOT NULL
                               OR NULLIF(Target.[City], Source.[City]) IS NOT NULL
                               OR NULLIF(Source.[OnAirHours],
                                         Target.[OnAirHours]) IS NOT NULL
                               OR NULLIF(Target.[OnAirHours],
                                         Source.[OnAirHours]) IS NOT NULL
                               OR NULLIF(Source.[OnAirMinutes],
                                         Target.[OnAirMinutes]) IS NOT NULL
                               OR NULLIF(Target.[OnAirMinutes],
                                         Source.[OnAirMinutes]) IS NOT NULL
                               OR NULLIF(Source.[LanguageId],
                                         Target.[LanguageId]) IS NOT NULL
                               OR NULLIF(Target.[LanguageId],
                                         Source.[LanguageId]) IS NOT NULL
                               OR NULLIF(Source.[PhoneBank],
                                         Target.[PhoneBank]) IS NOT NULL
                               OR NULLIF(Target.[PhoneBank],
                                         Source.[PhoneBank]) IS NOT NULL
                               OR NULLIF(Source.[Note], Target.[Note]) IS NOT NULL
                               OR NULLIF(Target.[Note], Source.[Note]) IS NOT NULL
                               OR NULLIF(Source.[SubmittedBy],
                                         Target.[SubmittedBy]) IS NOT NULL
                               OR NULLIF(Target.[SubmittedBy],
                                         Source.[SubmittedBy]) IS NOT NULL
                               OR NULLIF(Source.[SubmittedDate],
                                         Target.[SubmittedDate]) IS NOT NULL
                               OR NULLIF(Target.[SubmittedDate],
                                         Source.[SubmittedDate]) IS NOT NULL
                               OR NULLIF(Source.[ModifiedBy],
                                         Target.[ModifiedBy]) IS NOT NULL
                               OR NULLIF(Target.[ModifiedBy],
                                         Source.[ModifiedBy]) IS NOT NULL
                               OR NULLIF(Source.[ModifiedDate],
                                         Target.[ModifiedDate]) IS NOT NULL
                               OR NULLIF(Target.[ModifiedDate],
                                         Source.[ModifiedDate]) IS NOT NULL
                               OR NULLIF(Source.[Active], Target.[Active]) IS NOT NULL
                               OR NULLIF(Target.[Active], Source.[Active]) IS NOT NULL
                               OR NULLIF(Source.[EventId], Target.[EventId]) IS NOT NULL
                               OR NULLIF(Target.[EventId], Source.[EventId]) IS NOT NULL
                               OR NULLIF(Source.[RadiothonContactEmailSent],
                                         Target.[RadiothonContactEmailSent]) IS NOT NULL
                               OR NULLIF(Target.[RadiothonContactEmailSent],
                                         Source.[RadiothonContactEmailSent]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [CampaignDetailsId] = Source.[CampaignDetailsId] ,
                         [City] = Source.[City] ,
                         [OnAirHours] = Source.[OnAirHours] ,
                         [OnAirMinutes] = Source.[OnAirMinutes] ,
                         [LanguageId] = Source.[LanguageId] ,
                         [PhoneBank] = Source.[PhoneBank] ,
                         [Note] = Source.[Note] ,
                         [SubmittedBy] = Source.[SubmittedBy] ,
                         [SubmittedDate] = Source.[SubmittedDate] ,
                         [ModifiedBy] = Source.[ModifiedBy] ,
                         [ModifiedDate] = Source.[ModifiedDate] ,
                         [Active] = Source.[Active] ,
                         [EventId] = Source.[EventId] ,
                         [RadiothonContactEmailSent] = Source.[RadiothonContactEmailSent]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [RadiothonId] ,
                         [CampaignDetailsId] ,
                         [City] ,
                         [OnAirHours] ,
                         [OnAirMinutes] ,
                         [LanguageId] ,
                         [PhoneBank] ,
                         [Note] ,
                         [SubmittedBy] ,
                         [SubmittedDate] ,
                         [ModifiedBy] ,
                         [ModifiedDate] ,
                         [Active] ,
                         [RadiothonContactId] ,
                         [EventId] ,
                         [RadiothonContactEmailSent]
                       )
                VALUES ( Source.[RadiothonId] ,
                         Source.[CampaignDetailsId] ,
                         Source.[City] ,
                         Source.[OnAirHours] ,
                         Source.[OnAirMinutes] ,
                         Source.[LanguageId] ,
                         Source.[PhoneBank] ,
                         Source.[Note] ,
                         Source.[SubmittedBy] ,
                         Source.[SubmittedDate] ,
                         Source.[ModifiedBy] ,
                         Source.[ModifiedDate] ,
                         Source.[Active] ,
                         NULL ,
                         Source.[EventId] ,
                         Source.[RadiothonContactEmailSent]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;
 
            SET IDENTITY_INSERT Core.[dbo].[Radiothons] OFF  

            MERGE INTO Core.[dbo].[RadiothonRadioStations] AS Target
            USING
                ( SELECT    RadiothonId ,
                            RadioStationId ,
                            PrimaryRadioStation
                  FROM      core.dbo.RadiothonRadioStations
                ) AS Source ( [RadiothonId], [RadioStationId],
                              [PrimaryRadioStation] )
            ON ( Target.[RadioStationId] = Source.[RadioStationId]
                 AND Target.[RadiothonId] = Source.[RadiothonId]
               )
            WHEN MATCHED AND ( NULLIF(Source.[PrimaryRadioStation],
                                      Target.[PrimaryRadioStation]) IS NOT NULL
                               OR NULLIF(Target.[PrimaryRadioStation],
                                         Source.[PrimaryRadioStation]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [PrimaryRadioStation] = Source.[PrimaryRadioStation]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [RadiothonId] ,
                         [RadioStationId] ,
                         [PrimaryRadioStation]
                       )
                VALUES ( Source.[RadiothonId] ,
                         Source.[RadioStationId] ,
                         Source.[PrimaryRadioStation]
                       )
	   --WHEN NOT MATCHED BY SOURCE THEN 
	   -- DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;


            MERGE INTO Core.[dbo].[RadiothonHospitals] AS Target
            USING
                ( SELECT    rh.RadiothonId,
							rh.HospitalId
                  FROM      core.dbo.RadiothonHospitals rh
                ) AS Source ( [RadiothonId], [HospitalId] )
            ON ( Target.[RadiothonId] = Source.[RadiothonId] AND Target.HospitalId = Source.HospitalId)
            WHEN MATCHED AND ( NULLIF(Source.[RadiothonId],
                                      Target.[RadiothonId]) IS NOT NULL
                               OR NULLIF(Target.[HospitalId],
                                         Source.[HospitalId]) IS NOT NULL
                              ) THEN
                UPDATE SET
                         [RadiothonId] = Source.[RadiothonId] ,
                         [HospitalId] = Source.[HospitalId]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [RadiothonId] ,
                         [HospitalId]
                       )
                VALUES ( Source.[RadiothonId] ,
                         Source.[HospitalId]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;
 


            SET IDENTITY_INSERT Core.[dbo].[schooltypes] ON

            MERGE INTO Core.[dbo].[schooltypes] AS Target
            USING
                ( SELECT    SchoolTypeId ,
                            SchoolType
                  FROM      core.dbo.SchoolTypes
                ) AS Source ( [SchoolTypeId], [SchoolType] )
            ON ( Target.[SchoolTypeId] = Source.[SchoolTypeId] )
            WHEN MATCHED AND ( NULLIF(Source.[SchoolType], Target.[SchoolType]) IS NOT NULL
                               OR NULLIF(Target.[SchoolType],
                                         Source.[SchoolType]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [SchoolType] = Source.[SchoolType]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [SchoolTypeId] ,
                         [SchoolType]
                       )
                VALUES ( Source.[SchoolTypeId] ,
                         Source.[SchoolType]
                       )
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.[dbo].[schooltypes] OFF

            SET IDENTITY_INSERT Core.[dbo].[Schools] ON

            MERGE INTO Core.[dbo].[Schools] AS Target
            USING
                ( SELECT    s.SchoolId ,
                            h.HospitalId ,
                            s.SchoolName ,
                            s.Address1 ,
                            s.Address2 ,
                            s.City ,
                            s.ProvinceId ,
                            s.PostalCode ,
                            s.CountryId ,
                            s.InauguralEvent ,
                            s.SchoolTypeId ,
                            s.Active
                  FROM      core.dbo.Schools s
                            LEFT OUTER JOIN Core.dbo.Hospitals h ON s.HospitalId = h.SynchId
                ) AS Source ( [SchoolId], [HospitalId], [SchoolName],
                              [Address1], [Address2], [City], [ProvinceId],
                              [PostalCode], [CountryId], [InauguralEvent],
                              [SchoolTypeId], [Active] )
            ON ( Target.[SchoolId] = Source.[SchoolId] )
            WHEN MATCHED AND ( NULLIF(Source.[HospitalId], Target.[HospitalId]) IS NOT NULL
                               OR NULLIF(Target.[HospitalId],
                                         Source.[HospitalId]) IS NOT NULL
                               OR NULLIF(Source.[SchoolName],
                                         Target.[SchoolName]) IS NOT NULL
                               OR NULLIF(Target.[SchoolName],
                                         Source.[SchoolName]) IS NOT NULL
                               OR NULLIF(Source.[Address1], Target.[Address1]) IS NOT NULL
                               OR NULLIF(Target.[Address1], Source.[Address1]) IS NOT NULL
                               OR NULLIF(Source.[Address2], Target.[Address2]) IS NOT NULL
                               OR NULLIF(Target.[Address2], Source.[Address2]) IS NOT NULL
                               OR NULLIF(Source.[City], Target.[City]) IS NOT NULL
                               OR NULLIF(Target.[City], Source.[City]) IS NOT NULL
                               OR NULLIF(Source.[ProvinceId],
                                         Target.[ProvinceId]) IS NOT NULL
                               OR NULLIF(Target.[ProvinceId],
                                         Source.[ProvinceId]) IS NOT NULL
                               OR NULLIF(Source.[PostalCode],
                                         Target.[PostalCode]) IS NOT NULL
                               OR NULLIF(Target.[PostalCode],
                                         Source.[PostalCode]) IS NOT NULL
                               OR NULLIF(Source.[CountryId],
                                         Target.[CountryId]) IS NOT NULL
                               OR NULLIF(Target.[CountryId],
                                         Source.[CountryId]) IS NOT NULL
                               OR NULLIF(Source.[InauguralEvent],
                                         Target.[InauguralEvent]) IS NOT NULL
                               OR NULLIF(Target.[InauguralEvent],
                                         Source.[InauguralEvent]) IS NOT NULL
                               OR NULLIF(Source.[SchoolTypeId],
                                         Target.[SchoolTypeId]) IS NOT NULL
                               OR NULLIF(Target.[SchoolTypeId],
                                         Source.[SchoolTypeId]) IS NOT NULL
                               OR NULLIF(Source.[Active], Target.[Active]) IS NOT NULL
                               OR NULLIF(Target.[Active], Source.[Active]) IS NOT NULL
                             ) THEN
                UPDATE SET
                         [HospitalId] = Source.[HospitalId] ,
                         [SchoolName] = Source.[SchoolName] ,
                         [Address1] = Source.[Address1] ,
                         [Address2] = Source.[Address2] ,
                         [City] = Source.[City] ,
                         [ProvinceId] = Source.[ProvinceId] ,
                         [PostalCode] = Source.[PostalCode] ,
                         [CountryId] = Source.[CountryId] ,
                         [InauguralEvent] = Source.[InauguralEvent] ,
                         [SchoolTypeId] = Source.[SchoolTypeId] ,
                         [Active] = Source.[Active]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [SchoolId] ,
                         [HospitalId] ,
                         [SchoolName] ,
                         [Address1] ,
                         [Address2] ,
                         [City] ,
                         [ProvinceId] ,
                         [PostalCode] ,
                         [CountryId] ,
                         [InauguralEvent] ,
                         [SchoolTypeId] ,
                         [Active]
                       )
                VALUES ( Source.[SchoolId] ,
                         Source.[HospitalId] ,
                         Source.[SchoolName] ,
                         Source.[Address1] ,
                         Source.[Address2] ,
                         Source.[City] ,
                         Source.[ProvinceId] ,
                         Source.[PostalCode] ,
                         Source.[CountryId] ,
                         Source.[InauguralEvent] ,
                         Source.[SchoolTypeId] ,
                         Source.[Active]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.[dbo].[Schools] OFF  

            MERGE INTO Core.[dbo].[SubSchools] AS Target
            USING
                ( SELECT    SchoolId ,
                            SubSchoolId
                  FROM      core.dbo.SubSchools
                ) AS Source ( [SchoolId], [SubSchoolId] )
            ON ( Target.[SchoolId] = Source.[SchoolId]
                 AND Target.[SubSchoolId] = Source.[SubSchoolId]
               )
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ( [SchoolId] ,
                         [SubSchoolId]
                       )
                VALUES ( Source.[SchoolId] ,
                         Source.[SubSchoolId]
                       )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            MERGE INTO Core.[dbo].[schooltypes] AS Target
            USING
                ( SELECT    SchoolTypeId ,
                            SchoolType
                  FROM      core.dbo.SchoolTypes
                ) AS Source ( [SchoolTypeId], [SchoolType] )
            ON ( Target.[SchoolTypeId] = Source.[SchoolTypeId] )
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;
-----------------------------------------------------------------hosp info merge
            SET IDENTITY_INSERT Core.dbo.CampaignTypePartnerPrograms ON

            MERGE Core.dbo.CampaignTypePartnerPrograms AS t
            USING core.dbo.CampaignTypeFundraisingEntities AS s
            ON ( t.CampaignTypePartnerProgramId = s.CampaignTypeFundraisingEntityId )
            WHEN NOT MATCHED THEN
                INSERT ( CampaignTypePartnerProgramId ,
                         PartnerProgramId ,
                         CampaignTypeId
                       )
                VALUES ( s.CampaignTypeFundraisingEntityId ,
                         s.FundraisingEntityId ,
                         s.CampaignTypeId
                       )
            WHEN MATCHED AND t.PartnerProgramId != s.FundraisingEntityId
                OR t.CampaignTypeId != s.CampaignTypeId THEN
                UPDATE SET
                         t.PartnerProgramId = s.FundraisingEntityId ,
                         t.CampaignTypeId = s.CampaignTypeId
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.dbo.CampaignTypePartnerPrograms OFF
	

            SET IDENTITY_INSERT Core.dbo.PartnerPrograms ON

            MERGE Core.dbo.PartnerPrograms AS t
            USING core.dbo.FundraisingEntities AS s
            ON ( t.PartnerProgramId = s.FundraisingEntityId )
            WHEN NOT MATCHED THEN
                INSERT ( [PartnerProgramId] ,
                         [Name] ,
                         [FriendlyName] ,
                         [DisplayName] ,
                         [YearStarted] ,
                         [PartnerProgramTypeId] ,
                         [CategoryId] ,
                         [Website] ,
                         [Active] ,
                         [SponsorId] ,
                         [CountryId]
                       )
                VALUES ( s.FundraisingEntityId ,
                         s.Name ,
                         s.FriendlyName ,
                         s.DisplayName ,
                         s.YearStarted ,
                         CASE WHEN s.FundraisingCategoryId > 2 THEN 3
                              ELSE s.FundraisingCategoryId
                         END ,
                         0 ,
                         s.Website ,
                         s.Active ,
                         s.SponsorId ,
                         s.CountryId
                       )
            WHEN MATCHED AND t.Name != s.Name
                OR t.DisplayName != s.DisplayName
                OR t.FriendlyName != s.FriendlyName
                OR t.SponsorId != s.SponsorId
                OR t.YearStarted != s.YearStarted
                OR t.Website != s.Website
                OR t.CountryId != s.CountryId
                OR t.Active != s.Active THEN
                UPDATE SET
                         t.Name = s.Name ,
                         t.DisplayName = s.DisplayName ,
                         t.YearStarted = s.YearStarted ,
                         t.Website = s.Website ,
                         t.CountryId = s.CountryId ,
                         t.FriendlyName = s.FriendlyName ,
                         t.SponsorId = s.SponsorId ,
                         t.Active = s.Active
  --   WHEN NOT MATCHED BY SOURCE
		--THEN DELETE 
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

            SET IDENTITY_INSERT Core.dbo.PartnerPrograms OFF

	--SET IDENTITY_INSERT Core.dbo.PartnerAccountExecutives ON

            MERGE Core.dbo.PartnerAccountExecutives AS t
            USING core.dbo.PartnerAccountExecutives AS s
            ON ( t.PartnerProgramId = s.FundraisingEntityId
                 AND t.PartnerAccountStaffTypeId = s.PartnerAccountStaffTypeId
               )
            WHEN NOT MATCHED THEN
                INSERT ( PartnerProgramId ,
                         ContactId ,
                         PartnerAccountStaffTypeId
                       )
                VALUES ( s.FundraisingEntityId ,
                         s.ContactId ,
                         s.PartnerAccountStaffTypeId
                       )
            WHEN MATCHED AND t.ContactId != s.ContactId THEN
                UPDATE SET
                         t.ContactId = s.ContactId
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

	--SET IDENTITY_INSERT Core.dbo.PartnerAccountExecutives OFF

	--SET IDENTITY_INSERT Core.dbo.PartnerMarketLocationTotalsByYear ON

            MERGE Core.dbo.PartnerMarketLocationTotalsByYear AS t
            USING core.dbo.PartnerMarketLocationTotalsByYear AS s
            ON ( t.MarketId = s.MarketId
                 AND t.PartnerProgramId = s.FundraisingEntityId
                 AND t.CampaignYear = s.CampaignYear
               )
            WHEN NOT MATCHED THEN
                INSERT ( MarketId ,
                         PartnerProgramId ,
                         CampaignYear ,
                         Locations
                       )
                VALUES ( s.MarketId ,
                         s.FundraisingEntityId ,
                         s.CampaignYear ,
                         s.Locations
                       )
            WHEN MATCHED AND t.Locations != s.Locations THEN
                UPDATE SET
                         t.Locations = s.Locations
            WHEN NOT MATCHED BY SOURCE THEN
                DELETE
            OUTPUT
                $action ,
                inserted.* ,
                deleted.*;

	--SET IDENTITY_INSERT Core.dbo.PartnerMarketLocationTotalsByYear OFF

        END TRY

        BEGIN CATCH
            SELECT  ERROR_NUMBER() AS ErrorNumber ,
                    ERROR_SEVERITY() AS ErrorSeverity ,
                    ERROR_STATE() AS ErrorState ,
                    ERROR_PROCEDURE() AS ErrorProcedure ,
                    ERROR_LINE() AS ErrorLine ,
                    ERROR_MESSAGE() AS ErrorMessage;

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION;
                    PRINT 'Rollback'
                END
        END CATCH;

        IF @@TRANCOUNT > 0
            BEGIN
                COMMIT TRANSACTION;
                PRINT 'Commit'
            END*/
--    END


