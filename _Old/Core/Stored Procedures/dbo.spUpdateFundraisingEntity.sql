﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 01/26/2015
-- Description:	This procedure is used to update an existing Fundraising Entity.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateFundraisingEntity]
	@FundraisingEntityId INT,
	@Name VARCHAR(100),
	@FriendlyName VARCHAR(100),
	@DisplayName VARCHAR(100),
	@YearStarted INT,
	@FundraisingCategoryId INT,
	@Website VARCHAR(255),
	@Active BIT,
	@CountryId INT
AS
BEGIN
	UPDATE [dbo].[FundraisingEntities] SET
		[Name] = @Name,
      	[FriendlyName] = @FriendlyName,
      	[DisplayName] = @DisplayName,
      	[YearStarted] = @YearStarted,
      	[FundraisingCategoryId] = @FundraisingCategoryId,
      	[Website] = @Website,
      	[Active] = @Active,
      	[CountryId] = @CountryId
    WHERE [FundraisingEntityId] = @FundraisingEntityId;
END


