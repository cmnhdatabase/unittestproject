﻿

---- =============================================
---- Author:        Braden Kern
---- Create date: 3/19/2015
---- Description:   counts records between core and Core
---- =============================================
CREATE PROCEDURE [dbo].[spCountTableRecords]
 
AS
--BEGIN
/*  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
 
 
 
BEGIN TRY
 
 
--Areas
IF(SELECT COUNT(1) FROM Core.dbo.Areas) != (SELECT COUNT(1) FROM core.dbo.Areas)
Begin
RAISERROR (N'Areas row counts do not match.', 16, -1);
END
 
--AreaTypes
IF(SELECT COUNT(1) FROM Core.dbo.AreaTypes) != (SELECT COUNT(1) FROM core.dbo.AreaTypes)
Begin
RAISERROR (N'AreaTypes row counts do not match.', 16, -1);
END
 
--Batches
IF(SELECT COUNT(1) FROM Core.dbo.Batches) != (SELECT COUNT(1) FROM core.dbo.Batches)
Begin
RAISERROR (N'Batches row counts do not match.', 16, -1);
END
 
--BatchTypes
IF(SELECT COUNT(1) FROM Core.dbo.BatchTypes) != (SELECT COUNT(1) FROM core.dbo.BatchTypes)
Begin
RAISERROR (N'BatchTypes row counts do not match.', 16, -1);
END
 
--CampaignDetails
IF(SELECT COUNT(1) FROM Core.dbo.CampaignDetails) != (SELECT COUNT(1) FROM core.dbo.CampaignDetails)
Begin
RAISERROR (N'CampaignDetails row counts do not match.', 16, -1);
END
 
--Campaigns
IF(SELECT COUNT(1) FROM Core.dbo.Campaigns) != (SELECT COUNT(1) FROM core.dbo.Campaigns)
Begin
RAISERROR (N'Campaigns row counts do not match.', 16, -1);
END
 
--CampaignTypes
IF(SELECT COUNT(1) FROM Core.dbo.CampaignTypes) != (SELECT COUNT(1) FROM core.dbo.CampaignTypes) + 1
Begin
RAISERROR (N'CampaignTypes row counts do not match.', 16, -1);
END
 
--Celebrities
IF(SELECT COUNT(1) FROM Core.dbo.Celebrities) != (SELECT COUNT(1) FROM core.dbo.Celebrities)
Begin
RAISERROR (N'Celebrities row counts do not match.', 16, -1);
END
 
--Champions
IF(SELECT COUNT(1) FROM Core.dbo.Champions) != (SELECT COUNT(1) FROM core.dbo.Champions)
Begin
RAISERROR (N'Champions row counts do not match.', 16, -1);
END
 
--ChampionStories
IF(SELECT COUNT(1) FROM Core.dbo.ChampionStories) != (SELECT COUNT(1) FROM core.dbo.ChampionStories)
Begin
RAISERROR (N'ChampionStories row counts do not match.', 16, -1);
END
 
--CheckNumbers
IF(SELECT COUNT(1) FROM Core.dbo.CheckNumbers) != (SELECT COUNT(1) FROM core.dbo.CheckNumbers)
Begin
RAISERROR (N'CheckNumbers row counts do not match.', 16, -1);
END
 
--ContractHolders
IF(SELECT COUNT(1) FROM Core.dbo.ContractHolders) != (SELECT COUNT(1) FROM core.dbo.ContractHolders)
Begin
RAISERROR (N'ContractHolders row counts do not match.', 16, -1);
END
 
--Countries
IF(SELECT COUNT(1) FROM Core.dbo.Countries) != (SELECT COUNT(1) FROM core.dbo.Countries)
Begin
RAISERROR (N'Countries row counts do not match.', 16, -1);
END
 
--Currencies
IF(SELECT COUNT(1) FROM Core.dbo.Currencies) != (SELECT COUNT(1) FROM core.dbo.Currencies)
Begin
RAISERROR (N'Currencies row counts do not match.', 16, -1);
END
 
--DanceMarathons
IF(SELECT COUNT(1) FROM Core.dbo.DanceMarathons) != (SELECT COUNT(1) FROM core.dbo.DanceMarathons)
Begin
RAISERROR (N'DanceMarathons row counts do not match.', 16, -1);
END
 
--DanceMarathonSubSchools
IF(SELECT COUNT(1) FROM Core.dbo.DanceMarathonSubSchools) != (SELECT COUNT(1) FROM core.dbo.DanceMarathonSubSchools)
Begin
RAISERROR (N'DanceMarathonSubSchools row counts do not match.', 16, -1);
END
 
--DisbursementCheckNumbers
IF(SELECT COUNT(1) FROM Core.dbo.DisbursementCheckNumbers) != (SELECT COUNT(1) FROM core.dbo.DisbursementCheckNumbers)
Begin
RAISERROR (N'DisbursementCheckNumbers row counts do not match.', 16, -1);
END
 
--DisbursementDates
IF(SELECT COUNT(1) FROM Core.dbo.DisbursementDates) != (SELECT COUNT(1) FROM core.dbo.DisbursementDates)
Begin
RAISERROR (N'DisbursementDates row counts do not match.', 16, -1);
END
 
--DisbursementPeriods
IF(SELECT COUNT(1) FROM Core.dbo.DisbursementPeriods) != (SELECT COUNT(1) FROM core.dbo.DisbursementPeriods)
Begin
RAISERROR (N'DisbursementPeriods row counts do not match.', 16, -1);
END
 
--Disbursements
IF(SELECT COUNT(1) FROM Core.dbo.Disbursements) != (SELECT COUNT(1) FROM core.dbo.Disbursements)
Begin
RAISERROR (N'Disbursements row counts do not match.', 16, -1);
END
 
--Donations
IF(SELECT COUNT(1) FROM Core.dbo.Donations) != (SELECT COUNT(1) FROM core.dbo.Donations)
Begin
RAISERROR (N'Donations row counts do not match.', 16, -1);
END
 
--DonorInfo
IF(SELECT COUNT(1) FROM Core.dbo.DonorInfo) != (SELECT COUNT(1) FROM core.dbo.DonorInfo)
Begin
RAISERROR (N'DonorInfo row counts do not match.', 16, -1);
END
 
--DonorInfoCheckNumbers
IF(SELECT COUNT(1) FROM Core.dbo.DonorInfoCheckNumbers) != (SELECT COUNT(1) FROM core.dbo.DonorInfoCheckNumbers)
Begin
RAISERROR (N'DonorInfoCheckNumbers row counts do not match.', 16, -1);
END
 
--DonorInfoPhoneNumbers
IF(SELECT COUNT(1) FROM Core.dbo.DonorInfoPhoneNumbers) != (SELECT COUNT(1) FROM core.dbo.DonorInfoPhoneNumbers)
Begin
RAISERROR (N'DonorInfoPhoneNumbers row counts do not match.', 16, -1);
END
 
--FUEHLLog
IF(SELECT COUNT(1) FROM Core.dbo.FUEHLLog) != (SELECT COUNT(1) FROM core.dbo.FUEHLLog)
Begin
RAISERROR (N'FUEHLLog row counts do not match.', 16, -1);
END
 
--FundTypes
IF(SELECT COUNT(1) FROM Core.dbo.FundTypes) != (SELECT COUNT(1) FROM core.dbo.FundTypes)
Begin
RAISERROR (N'FundTypes row counts do not match.', 16, -1);
END
 
--HospitalPhoneNumbers
IF(SELECT COUNT(1) FROM Core.dbo.HospitalPhoneNumbers) != (SELECT COUNT(1) FROM core.dbo.HospitalPhoneNumbers)
Begin
RAISERROR (N'HospitalPhoneNumbers row counts do not match.', 16, -1);
END
 
--Hospitals
IF(SELECT COUNT(1) FROM Core.dbo.Hospitals) != (SELECT COUNT(1) FROM core.dbo.Hospitals)
Begin
RAISERROR (N'Hospitals row counts do not match.', 16, -1);
END
 
--ImageFormats
IF(SELECT COUNT(1) FROM Core.dbo.ImageFormats) != (SELECT COUNT(1) FROM core.dbo.ImageFormats)
Begin
RAISERROR (N'ImageFormats row counts do not match.', 16, -1);
END
 
--Images
IF(SELECT COUNT(1) FROM Core.dbo.Images) != (SELECT COUNT(1) FROM core.dbo.Images)
Begin
RAISERROR (N'Images row counts do not match.', 16, -1);
END
 
--ImageTypes
IF(SELECT COUNT(1) FROM Core.dbo.ImageTypes) != (SELECT COUNT(1) FROM core.dbo.ImageTypes)
Begin
RAISERROR (N'ImageTypes row counts do not match.', 16, -1);
END
 
--Languages
IF(SELECT COUNT(1) FROM Core.dbo.Languages) != (SELECT COUNT(1) FROM core.dbo.Languages)
Begin
RAISERROR (N'Languages row counts do not match.', 16, -1);
END
 
--LocationAreas
IF(SELECT COUNT(1) FROM Core.dbo.LocationAreas) != (SELECT COUNT(1) FROM core.dbo.LocationAreas)
Begin
RAISERROR (N'LocationAreas row counts do not match.', 16, -1);
END
 
--LocationPhoneNumbers
IF(SELECT COUNT(1) FROM Core.dbo.LocationPhoneNumbers) != (SELECT COUNT(1) FROM core.dbo.LocationPhoneNumbers)
Begin
RAISERROR (N'LocationPhoneNumbers row counts do not match.', 16, -1);
END
 
--Locations
IF(SELECT COUNT(1) FROM Core.dbo.Locations) != (SELECT COUNT(1) FROM core.dbo.Locations)
Begin
RAISERROR (N'Locations row counts do not match.', 16, -1);
END
 
--Markets
IF(SELECT COUNT(1) FROM Core.dbo.Markets) != (SELECT COUNT(1) FROM core.dbo.Markets)
Begin
RAISERROR (N'Markets row counts do not match.', 16, -1);
END
 
--MiracleChildren
IF(SELECT COUNT(1) FROM Core.dbo.MiracleChildren) != (SELECT COUNT(1) FROM core.dbo.MiracleChildren)
Begin
RAISERROR (N'MiracleChildren row counts do not match.', 16, -1);
END
 
--MiracleStories
IF(SELECT COUNT(1) FROM Core.dbo.MiracleStories) != (SELECT COUNT(1) FROM core.dbo.MiracleStories)
Begin
RAISERROR (N'MiracleStories row counts do not match.', 16, -1);
END
 
--PartnerAccountExecutives
IF(SELECT COUNT(1) FROM Core.dbo.PartnerAccountExecutives) != (SELECT COUNT(1) FROM core.dbo.PartnerAccountExecutives)
Begin
RAISERROR (N'PartnerAccountExecutives row counts do not match.', 16, -1);
END
 
--PartnerAccountStaffTypes
IF(SELECT COUNT(1) FROM Core.dbo.PartnerAccountStaffTypes) != (SELECT COUNT(1) FROM core.dbo.PartnerAccountStaffTypes)
Begin
RAISERROR (N'PartnerAccountStaffTypes row counts do not match.', 16, -1);
END
 
--PartnerMarketLocationTotalsByYear
IF(SELECT COUNT(1) FROM Core.dbo.PartnerMarketLocationTotalsByYear) != (SELECT COUNT(1) FROM core.dbo.PartnerMarketLocationTotalsByYear)
Begin
RAISERROR (N'PartnerMarketLocationTotalsByYear row counts do not match.', 16, -1);
END
 
--FundraisingEntities
IF(SELECT COUNT(1) FROM Core.dbo.PartnerPrograms) != (SELECT COUNT(1) FROM core.dbo.FundraisingEntities) + 1
Begin
RAISERROR (N'PartnerPrograms/FundraisingEntities row counts do not match.', 16, -1);
END
 
 
 
--PhoneNumbers
IF(SELECT COUNT(1) FROM Core.dbo.PhoneNumbers) != (SELECT COUNT(1) FROM core.dbo.PhoneNumbers)
Begin
RAISERROR (N'PhoneNumbers row counts do not match.', 16, -1);
END
 
--PhoneTypes
IF(SELECT COUNT(1) FROM Core.dbo.PhoneTypes) != (SELECT COUNT(1) FROM core.dbo.PhoneTypes)
Begin
RAISERROR (N'PhoneTypes row counts do not match.', 16, -1);
END
 
--PledgeData
IF(SELECT COUNT(1) FROM Core.dbo.PledgeData) != (SELECT COUNT(1) FROM core.dbo.PledgeData)
Begin
RAISERROR (N'PledgeData row counts do not match.', 16, -1);
END
 
--PledgeTypes
IF(SELECT COUNT(1) FROM Core.dbo.PledgeTypes) != (SELECT COUNT(1) FROM core.dbo.PledgeTypes)
Begin
RAISERROR (N'PledgeTypes row counts do not match.', 16, -1);
END
 
--PopulationByMarket
IF(SELECT COUNT(1) FROM Core.dbo.PopulationByMarket) != (SELECT COUNT(1) FROM core.dbo.PopulationByMarket)
Begin
RAISERROR (N'PopulationByMarket row counts do not match.', 16, -1);
END
 
--PostalCodeArchive
IF(SELECT COUNT(1) FROM Core.dbo.PostalCodeArchive) != (SELECT COUNT(1) FROM core.dbo.PostalCodeArchive)
Begin
RAISERROR (N'PostalCodeArchive row counts do not match.', 16, -1);
END
 
--PostalCodeDemographics
IF(SELECT COUNT(1) FROM Core.dbo.PostalCodeDemographics) != (SELECT COUNT(1) FROM core.dbo.PostalCodeDemographics)
Begin
RAISERROR (N'PostalCodeDemographics row counts do not match.', 16, -1);
END
 
 
--PostalCodes
IF(SELECT COUNT(1) FROM Core.dbo.PostalCodes) != (SELECT COUNT(1) FROM core.dbo.PostalCodes)
Begin
RAISERROR (N'PostalCodes row counts do not match.', 16, -1);
END
 
--PropertyTypes
IF(SELECT COUNT(1) FROM Core.dbo.PropertyTypes) != (SELECT COUNT(1) FROM core.dbo.PropertyTypes)
Begin
RAISERROR (N'PropertyTypes row counts do not match.', 16, -1);
END
 
--Provinces
IF(SELECT COUNT(1) FROM Core.dbo.Provinces) != (SELECT COUNT(1) FROM core.dbo.Provinces)
Begin
RAISERROR (N'Provinces row counts do not match.', 16, -1);
END
 
--Radio
IF(SELECT COUNT(1) FROM Core.dbo.Radio) != (SELECT COUNT(1) FROM core.dbo.Radio)
Begin
RAISERROR (N'Radio row counts do not match.', 16, -1);
END
 
--RadioPhoneNumbers
IF(SELECT COUNT(1) FROM Core.dbo.RadioPhoneNumbers) != (SELECT COUNT(1) FROM core.dbo.RadioPhoneNumbers)
Begin
RAISERROR (N'RadioPhoneNumbers row counts do not match.', 16, -1);
END
 
--RadioStations
IF(SELECT COUNT(1) FROM Core.dbo.RadioStations) != (SELECT COUNT(1) FROM core.dbo.RadioStations)
Begin
RAISERROR (N'RadioStations row counts do not match.', 16, -1);
END
 
--RadiothonRadioStations
IF(SELECT COUNT(1) FROM Core.dbo.RadiothonRadioStations) != (SELECT COUNT(1) FROM core.dbo.RadiothonRadioStations)
Begin
RAISERROR (N'RadiothonRadioStations row counts do not match.', 16, -1);
END
 
--Radiothons
IF(SELECT COUNT(1) FROM Core.dbo.Radiothons) != (SELECT COUNT(1) FROM core.dbo.Radiothons)
Begin
RAISERROR (N'Radiothons row counts do not match.', 16, -1);
END
 
--RecordTypes
IF(SELECT COUNT(1) FROM Core.dbo.RecordTypes) != (SELECT COUNT(1) FROM core.dbo.RecordTypes)
Begin
RAISERROR (N'RecordTypes row counts do not match.', 16, -1);
END
 
--RegionDirectors
IF(SELECT COUNT(1) FROM Core.dbo.RegionDirectors) != (SELECT COUNT(1) FROM core.dbo.RegionDirectors)
Begin
RAISERROR (N'RegionDirectors row counts do not match.', 16, -1);
END
 
--Regions
IF(SELECT COUNT(1) FROM Core.dbo.Regions) != (SELECT COUNT(1) FROM core.dbo.Regions)
Begin
RAISERROR (N'Regions row counts do not match.', 16, -1);
END
 
--ReportSettings
IF(SELECT COUNT(1) FROM Core.dbo.ReportSettings) != (SELECT COUNT(1) FROM core.dbo.ReportSettings)
Begin
RAISERROR (N'ReportSettings row counts do not match.', 16, -1);
END
 
--Schools
IF(SELECT COUNT(1) FROM Core.dbo.Schools) != (SELECT COUNT(1) FROM core.dbo.Schools)
Begin
RAISERROR (N'Schools row counts do not match.', 16, -1);
END
 
--SchoolTypes
IF(SELECT COUNT(1) FROM Core.dbo.SchoolTypes) != (SELECT COUNT(1) FROM core.dbo.SchoolTypes)
Begin
RAISERROR (N'SchoolTypes row counts do not match.', 16, -1);
END
 

 
--TV
IF(SELECT COUNT(1) FROM Core.dbo.TV) != (SELECT COUNT(1) FROM core.dbo.TV)
Begin
RAISERROR (N'TV row counts do not match.', 16, -1);
END
 
--TvPhones
IF(SELECT COUNT(1) FROM Core.dbo.TvPhones) != (SELECT COUNT(1) FROM core.dbo.TvPhones)
Begin
RAISERROR (N'TvPhones row counts do not match.', 16, -1);
END
 
-------------------------CHECK SUMS
 
 
--Check disbursement sums
IF(SELECT sum(Amount) FROM Core.dbo.Disbursements) != (SELECT sum(Amount) FROM core.dbo.Disbursements)
Begin
RAISERROR (N'Disbursement sums do not match.', 16, -1);
END
 
--Check PledgeData sums
IF(SELECT sum(Amount) FROM Core.dbo.PledgeData) != (SELECT sum(Amount) FROM core.dbo.PledgeData)
Begin
RAISERROR (N'PledgeData sums do not match.', 16, -1);
END
 
--Check DonorInfo sums
IF(SELECT sum(Amount) FROM Core.dbo.DonorInfo) != (SELECT sum(Amount) FROM core.dbo.DonorInfo)
Begin
RAISERROR (N'DonorInfo sums do not match.', 16, -1);
END
 
--Check Donation sums
IF(SELECT sum(Amount) FROM Core.dbo.donations) != (SELECT sum(Amount) FROM core.dbo.donations)
Begin
RAISERROR (N'donations sums do not match.', 16, -1);
END
 
 
---------------------------
 
IF EXISTS
(
SELECT * 
FROM (
SELECT d.FundraisingYear, d.MarketId, SUM(d.Amount) Amount, c.Amount2
FROM core.dbo.Disbursements d INNER JOIN ( 
SELECT FundraisingYear, MarketId, SUM(Amount) Amount2
FROM Core.dbo.Disbursements
GROUP BY FundraisingYear, MarketId
) AS c ON d.FundraisingYear = c.FundraisingYear AND d.MarketId = c.MarketId
GROUP BY d.FundraisingYear, d.MarketId, c.Amount2
) AS t
WHERE t.Amount != t.Amount2
)
Begin
RAISERROR (N'Market Year Disbursement sums do not match.', 16, -1);
END
 
 
 
 
IF EXISTS
(
SELECT * 
FROM (
SELECT d.FundraisingYear, d.FundraisingEntityId, SUM(d.Amount) Amount, c.Amount2
FROM core.dbo.Disbursements d INNER JOIN ( 
SELECT FundraisingYear, PartnerId, SUM(Amount) Amount2
FROM Core.dbo.Disbursements
GROUP BY FundraisingYear, PartnerId
) AS c ON d.FundraisingYear = c.FundraisingYear AND d.FundraisingEntityId = c.PartnerId
GROUP BY d.FundraisingYear, d.FundraisingEntityId, c.Amount2
) AS t
WHERE t.Amount != t.Amount2
 
)
BEGIN
RAISERROR (N'Partner Year Disbursement sums do not match.', 16, -1);
END
 
 
IF EXISTS
(
SELECT * 
FROM (
SELECT p.FundraisingYear, p.FundraisingEntityId, SUM(p.Amount) Amount, c.Amount2
FROM core.dbo.PledgeData p INNER JOIN ( 
SELECT FundraisingYear,PartnerProgramId, SUM(Amount) Amount2
FROM Core.dbo.PledgeData
GROUP BY FundraisingYear, PartnerProgramId
) AS c ON p.FundraisingYear = c.FundraisingYear AND p.FundraisingEntityId = c.PartnerProgramId
GROUP BY p.FundraisingYear, p.FundraisingEntityId, c.Amount2
) AS t
WHERE t.Amount != t.Amount2
 
)
BEGIN
RAISERROR (N'Pledge Year Pledge sums do not match.', 16, -1);
END
 
 
 
 
 
 
 
 
 
 
 
 
 
END TRY
BEGIN CATCH
    -- Execute error retrieval routine.
    RAISERROR (N'Core and Core do not match.', 16, -1);
   PRINT ERROR_MESSAGE();
   
END CATCH;
 
 
  */
--END


