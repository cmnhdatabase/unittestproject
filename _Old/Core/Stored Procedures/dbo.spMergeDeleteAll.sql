﻿

-- =============================================
-- Author:		Travis Poll
-- Create date: 12/16/2014
-- Description:	This will delete all disb, donor info, pledge, location, campaign information from Core that has been removed in core.
-- =============================================
CREATE PROCEDURE [dbo].[spMergeDeleteAll]
AS
--BEGIN
/*	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY
	
	MERGE Core.dbo.DonorInfoCheckNumbers AS t
	USING core.dbo.DonorInfoCheckNumbers AS s
	ON (
		t.DonorInfoId = s.DonorInfoId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.DonorInfo AS t
	USING core.dbo.DonorInfo AS s
	ON (
		t.DonorInfoId = s.DonorInfoId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.Disbursements AS t
	USING core.dbo.Disbursements AS s
	ON (
		t.DisbursementId = s.DisbursementId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.DisbursementDates AS t
	USING core.dbo.DisbursementDates AS s
	ON (
		t.DisbursementDateId = s.DisbursementDateId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.DisbursementPeriods AS t
	USING core.dbo.DisbursementPeriods AS s
	ON (
		t.DisbursementPeriodId = s.DisbursementPeriodId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.DisbursementCheckNumbers AS t
	USING core.dbo.DisbursementCheckNumbers AS s
	ON (
		t.DisbursementId = s.DisbursementId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.Batches AS t
	USING core.dbo.Batches AS s
	ON (
		t.BatchId = s.BatchId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.BatchTypes AS t
      USING core.dbo.BatchTypes AS s
      ON (
            t.BatchTypeId = s. BatchTypeId
      )
      WHEN NOT MATCHED BY SOURCE
            THEN DELETE
      OUTPUT $action, inserted.*, deleted.*;


	MERGE Core.dbo.CampaignDetails AS t
	USING core.dbo.CampaignDetails AS s
	ON (
		t.CampaignDetailsId = s.CampaignDetailsId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.Campaigns AS t
	USING core.dbo.Campaigns AS s
	ON (
		t.CampaignId = s.CampaignId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.LocationAreas AS t
	USING core.dbo.LocationAreas AS s
	ON (
		t.LocationAreasId = s.LocationAreasId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.Locations AS t
	USING core.dbo.Locations AS s
	ON (
		t.LocationId = s.LocationId
	)	
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	MERGE Core.dbo.Areas AS t
	USING core.dbo.Areas AS s
	ON (
		t.AreaId = s.AreaId
	)
	WHEN NOT MATCHED BY SOURCE
		THEN DELETE
	OUTPUT $action, inserted.*, deleted.*;

	END TRY

			BEGIN CATCH
				SELECT 
					 ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;

				IF @@TRANCOUNT > 0
					BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Rollback'
					END
			END CATCH;

			IF @@TRANCOUNT > 0
				BEGIN
				COMMIT TRANSACTION;
				PRINT 'Commit'
			END*/
--END


