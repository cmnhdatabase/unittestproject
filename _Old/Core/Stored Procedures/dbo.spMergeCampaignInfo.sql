﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 12/11/2014
-- Description:	This will merge the 3 campaign tables to the old Core database.  
--					CampaignType
--					Campaigns
--					CampaignDetails
-- =============================================
CREATE PROCEDURE [dbo].[spMergeCampaignInfo]
	-- Add the parameters for the stored procedure here
AS
--BEGIN
/*	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	SET IDENTITY_INSERT Core.dbo.CampaignTypes ON

	MERGE Core.dbo.CampaignTypes AS t
	USING core.dbo.CampaignTypes AS s
	ON (
		t.CampaignTypeId = s.CampaignTypeId
	)
	WHEN NOT MATCHED
		THEN INSERT(CampaignTypeId, CampaignType) 
		VALUES(s.CampaignTypeId,s.CampaignType)
	--CampaignTypes need to stay the same in old db
	--WHEN MATCHED AND t.CampaignType != s.CampaignType
	--	THEN UPDATE SET t.CampaignType = s.CampaignType
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.CampaignTypes OFF


	SET IDENTITY_INSERT Core.dbo.Campaigns ON

	MERGE Core.dbo.Campaigns AS t
	USING core.dbo.Campaigns AS s
	ON (
		t.CampaignId = s.CampaignId
	)
	WHEN NOT MATCHED
		THEN INSERT([CampaignId],[CampaignName],[ShortDescription],[LongDescription],[CampaignTypeId]) 
		VALUES(s.CampaignId,s.CampaignName,s.ShortDescription,s.LongDescription,s.CampaignTypeId)
	WHEN MATCHED AND t.CampaignName != s.CampaignName OR t.ShortDescription != s.ShortDescription OR 
						t.LongDescription != s.LongDescription OR t.CampaignTypeId != s.CampaignTypeId
		THEN UPDATE SET t.CampaignName = s.CampaignName, t.ShortDescription = s.ShortDescription, 
						t.LongDescription = s.LongDescription, t.CampaignTypeId = s.CampaignTypeId
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.Campaigns OFF

	SET IDENTITY_INSERT Core.dbo.CampaignDetails ON

	MERGE Core.dbo.CampaignDetails AS t
	USING core.dbo.CampaignDetails AS s
	ON (
		t.CampaignDetailsId = s.CampaignDetailsId
	)
	WHEN NOT MATCHED
		THEN INSERT(CampaignDetailsId, CampaignId,CampaignDetailName,ShortDescription,LongDescription,CampaignYear,LegacyEventId,StartDate, EndDate) 
		VALUES(s.CampaignDetailsId, s.CampaignId,s.CampaignDetailName,s.ShortDescription,s.LongDescription,s.CampaignYear,s.LegacyEventId,s.StartDate, s.EndDate)
	WHEN MATCHED AND t.CampaignId != s.CampaignId OR t.CampaignDetailName != s.CampaignDetailName OR t.ShortDescription != s.ShortDescription OR 
						t.LongDescription != s.LongDescription OR t.CampaignYear != s.CampaignYear OR t.LegacyEventId != s.LegacyEventId OR
						t.StartDate != s.StartDate OR t.EndDate != s.EndDate
		THEN UPDATE SET t.CampaignId = s.CampaignId, t.CampaignDetailName = s.CampaignDetailName, t.ShortDescription = s.ShortDescription, 
						t.LongDescription = s.LongDescription, t.CampaignYear = s.CampaignYear, t.LegacyEventId = s.LegacyEventId,
						t.StartDate = s.StartDate, t.EndDate = s.EndDate
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.CampaignDetails OFF

	END TRY

			BEGIN CATCH
				SELECT 
					 ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;

				IF @@TRANCOUNT > 0
					BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Rollback'
					END
			END CATCH;

			IF @@TRANCOUNT > 0
				BEGIN
				COMMIT TRANSACTION;
				PRINT 'Commit'
			END*/
--END


