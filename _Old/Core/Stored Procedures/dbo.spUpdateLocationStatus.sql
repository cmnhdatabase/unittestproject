﻿

-- =============================================
-- Author:		Brad Harvey
-- Create date: 01/26/2015
-- Description:	This procedure is used to change the active status of a location.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateLocationStatus]
	@LocationId int,
	@Status bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	Update [dbo].[Locations] 
	set active = @Status
	where LocationId = @LocationId
	

	SELECT active from [dbo].[Locations] where LocationId = @LocationId;
END




