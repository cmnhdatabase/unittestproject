﻿-- =============================================
-- Author: Travis Poll
-- Create date: 06/2/2015
-- Description: This will update all c records for a partner in the given year to d records and assign a batchid.
-- =============================================
CREATE PROCEDURE spUpdateCRecordsToDRecords
	-- Add the parameters for the stored procedure here
	@batchid int, 
	@partnerid INT, 
	@year INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE dbo.Disbursements 
    SET batchid = @batchid, recordtypeid = 1
    WHERE FundraisingEntityId = @partnerid
    AND RecordTypeId = 4
    AND FundraisingYear = @year
END
