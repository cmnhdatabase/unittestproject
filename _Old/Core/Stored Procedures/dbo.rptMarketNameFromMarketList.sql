﻿
CREATE PROCEDURE [dbo].[rptMarketNameFromMarketList]
(
@marketid nvarchar(MAX)
)
AS
BEGIN

Select
MarketName,
count(m.MarketId) AS cnt
From
Markets m
JOIN dbo.DelimitedSplitN4K(@marketid,',') ds ON ds.item = m.MarketId
GROUP BY
MarketName
	
END

