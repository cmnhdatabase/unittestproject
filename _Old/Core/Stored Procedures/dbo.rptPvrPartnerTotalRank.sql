﻿


CREATE PROCEDURE [dbo].[rptPvrPartnerTotalRank]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 175
/******************/
IF OBJECT_ID('tempdb..#tempPartnerRank', 'U') IS NOT NULL
    DROP TABLE #tempPartnerRank

SELECT MarketId, PartnerCYTotal - PartnerCYminus1Total AS CyIncrease,
(PartnerCYTotal - PartnerCYminus1Total)/NULLIF(PartnerCYminus1Total,0) AS CYPercentIncrease,
(PartnerCYTotal/NULLIF(pop,0)) AS PerCap,
RANK() OVER (ORDER BY(PartnerCYTotal - PartnerCYminus1Total) DESC) AS CYIncreaseRank,
RANK() OVER (ORDER BY(PartnerCYminus1Total - PartnerCYminus2Total) DESC) AS CYminus1IncreaseRank,
RANK() OVER (ORDER BY((PartnerCYTotal - PartnerCYminus1Total)/NULLIF(PartnerCYminus1Total,0))DESC) AS CYPercentIncreaseRank,
RANK() OVER (ORDER BY((PartnerCYminus1Total - PartnerCYminus2Total)/NULLIF(PartnerCYminus2Total,0))DESC) AS CYMinus1PercentIncreaseRank,
RANK() OVER (ORDER BY(PartnerCYTotal/NULLIF(pop,0))DESC) AS CYPerCapRank,
RANK() OVER (ORDER BY(PartnerCYminus1Total/NULLIF(pop,0))DESC) AS CYMinus1PerCapRank
INTO #tempPartnerRank
FROM (SELECT
fd.MarketId,
SUM(CASE WHEN FundraisingYear = @year THEN amount ELSE 0 END) AS PartnerCYTotal,
SUM(CASE WHEN FundraisingYear = @year -1 THEN Amount END) AS PartnerCYminus1Total,
SUM(CASE WHEN FundraisingYear = @year -2 THEN amount END) AS PartnerCYminus2Total,
MAX(mpr.PopulationEstimate) AS Pop
FROM dbo.vwFundraisingData fd
INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = fd.MarketId
WHERE FundraisingYear >= @year - 2
AND FundraisingCategoryId = 1
GROUP BY fd.MarketId, FundraisingCategoryId) AS a

SELECT *
FROM #tempPartnerRank
WHERE MarketId = @market






