﻿
-- =============================================
-- Author:		Travis Poll
-- Create date: 12/16/2014
-- Description:	This will merge all pledgedata information from our core database to the old Core database.
-- =============================================
CREATE PROCEDURE [dbo].[spMergePledgeData]
AS
--BEGIN
	/*-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRANSACTION
	BEGIN TRY

	--EXEC [dbo].[spMergeCampaignInfo]
	--EXEC [dbo].[spMergePartnerPrograms]
	--EXEC [dbo].[spMergeLocationInformation]

	SET IDENTITY_INSERT Core.dbo.PledgeTypes ON

	MERGE Core.dbo.PledgeTypes AS t
	USING core.dbo.PledgeTypes AS s
	ON (
		t.PledgeTypeId = s.PledgeTypeId
	)
	WHEN NOT MATCHED
		THEN INSERT(PledgeTypeId, PledgeType) 
		VALUES(s.PledgeTypeId,s.PledgeType)
	WHEN MATCHED AND t.PledgeType != s.PledgeType
		THEN UPDATE SET t.PledgeType = s.PledgeType
	OUTPUT $action, inserted.*, deleted.*;

	SET IDENTITY_INSERT Core.dbo.PledgeTypes OFF

	--was
	--SET IDENTITY_INSERT Core.dbo.PledgeData ON

	--MERGE Core.dbo.PledgeData AS t
	--USING core.dbo.PledgeData AS s
	--ON (
	--	t.PledgeId = s.PledgeId
	--)
	--WHEN NOT MATCHED
	--	THEN INSERT(PledgeId, PartnerProgramId,[MarketId],[FundraisingYear],[CampaignDetailsId],[CurrencyTypeId],[Amount],[PledgeTypeId],[Quarter],[PledgeDate],[DirectToHospital]) 
	--	VALUES(s.PledgeId, s.FundraisingEntityId, s.MarketId, s.FundraisingYear, s.CampaignDetailsId, s.CurrencyTypeId, s.Amount, s.PledgeTypeId, s.Quarter, s.PledgeDate, s.DirectToHospital)
	--WHEN MATCHED AND t.PartnerProgramId != s.FundraisingEntityId OR t.MarketId != s.MarketId
	--			OR t.FundraisingYear != s.FundraisingYear OR t.CampaignDetailsId != s.CampaignDetailsId OR t.CurrencyTypeId != s.CurrencyTypeId OR t.Amount != s.Amount
	--			OR t.PledgeTypeId != s.PledgeTypeId OR t.DirectToHospital != s.DirectToHospital
	--	THEN UPDATE SET t.PartnerProgramId = s.FundraisingEntityId, t.MarketId = s.MarketId,
	--			 t.FundraisingYear = s.FundraisingYear, t.CampaignDetailsId = s.CampaignDetailsId, t.CurrencyTypeId = s.CurrencyTypeId, t.Amount = s.Amount,
	--			 t.PledgeTypeId = s.PledgeTypeId, t.DirectToHospital = s.DirectToHospital
	--WHEN NOT MATCHED BY SOURCE
	--	THEN DELETE
	--OUTPUT $action, inserted.*, deleted.*;

	--SET IDENTITY_INSERT Core.dbo.PledgeData OFF

	SET IDENTITY_INSERT Core.dbo.PledgeData ON

      MERGE Core.dbo.PledgeData AS t
      USING core.dbo.PledgeData AS s
      ON (
            t.PledgeId = s.PledgeId
      )
      WHEN NOT MATCHED
            THEN INSERT(PledgeId, PartnerProgramId,[MarketId],[FundraisingYear],[CampaignDetailsId],[CurrencyTypeId],[Amount],[PledgeTypeId],[Quarter],[PledgeDate],[DirectToHospital]) 
            VALUES(s.PledgeId, s.FundraisingEntityId, s.MarketId, s.FundraisingYear, s.CampaignDetailsId, s.CurrencyTypeId, s.Amount, s.PledgeTypeId, s.Quarter, s.PledgeDate, s.DirectToHospital)
      WHEN MATCHED AND t.PartnerProgramId != s.FundraisingEntityId OR t.MarketId != s.MarketId OR t.PledgeDate != s.PledgeDate
                        OR t.FundraisingYear != s.FundraisingYear OR t.CampaignDetailsId != s.CampaignDetailsId OR t.CurrencyTypeId != s.CurrencyTypeId OR t.Amount != s.Amount OR t.Quarter != s.Quarter
                        OR t.PledgeTypeId != s.PledgeTypeId OR t.DirectToHospital != s.DirectToHospital
            THEN UPDATE SET t.PartnerProgramId = s.FundraisingEntityId, t.MarketId = s.MarketId,
                        t.FundraisingYear = s.FundraisingYear, t.CampaignDetailsId = s.CampaignDetailsId, t.CurrencyTypeId = s.CurrencyTypeId, t.Amount = s.Amount, t.Quarter = s.Quarter, t.PledgeDate = s.PledgeDate,
                        t.PledgeTypeId = s.PledgeTypeId, t.DirectToHospital = s.DirectToHospital
      WHEN NOT MATCHED BY SOURCE
            THEN DELETE
      OUTPUT $action, inserted.*, deleted.*;

      SET IDENTITY_INSERT Core.dbo.PledgeData OFF     

	 MERGE Core.dbo.PledgeTypes AS t
   USING core.dbo. PledgeTypes AS s
   ON (
            t.PledgeTypeId = s.PledgeTypeId
      )
      WHEN NOT MATCHED BY SOURCE
            THEN DELETE
      OUTPUT $action, inserted.*, deleted.*;


	END TRY

			BEGIN CATCH
				SELECT 
					 ERROR_NUMBER() AS ErrorNumber
					,ERROR_SEVERITY() AS ErrorSeverity
					,ERROR_STATE() AS ErrorState
					,ERROR_PROCEDURE() AS ErrorProcedure
					,ERROR_LINE() AS ErrorLine
					,ERROR_MESSAGE() AS ErrorMessage;

				IF @@TRANCOUNT > 0
					BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Rollback'
					END
			END CATCH;

			IF @@TRANCOUNT > 0
				BEGIN
				COMMIT TRANSACTION;
				PRINT 'Commit'
			END*/
--END


