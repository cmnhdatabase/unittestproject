﻿
CREATE PROCEDURE [dbo].[spGetInvoiceDetails]
	-- Add the parameters for the stored procedure here
	@yr INT ,
	@qrtr INT ,
	@mrktid INT = NULL
AS
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;

    -- Insert statements for procedure here
		IF OBJECT_ID('tempdb..#tempInvoices') IS NOT NULL
			BEGIN
				DROP TABLE #tempInvoices;
			END;

		DECLARE	@invid INT = (
							   SELECT DISTINCT
										InvoiceId
							   FROM		Invoices iv
							   WHERE	iv.InvoiceYear = @yr
										AND iv.InvoiceQuarter = @qrtr
							 ); 

--;WITH cte AS (
		SELECT	MarketId ,
				MarketName ,
				FundraisingYear ,
				Category ,
				invoiceid ,
				SUM(CASE WHEN DirectToHospital = 1
							  AND CategoryId != 3 THEN Amount
						 ELSE 0
					END) AS dth ,
				SUM(CASE WHEN DirectToHospital = 0
							  AND CategoryId != 3 THEN Amount
						 ELSE 0
					END) AS nodth ,
				SUM(CASE WHEN CategoryId = 3 THEN Amount
						 ELSE 0
					END) AS notinvoiced
		INTO	#tempInvoices
		FROM	(
				  SELECT	@invid AS invoiceid ,
							3 AS CategoryId ,
							'Local' AS Category ,
							FundraisingYear ,
							0 disbursementquarter ,
							0 dispursementyear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName AS ProgramDisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							CAST(CASE WHEN DirectToHospital = 0 THEN 0
									  ELSE 1
								 END AS INT) AS DirectToHospital ,
							m.RegionId ,
							l.LocationId
				  FROM		Disbursements d
				  INNER JOIN /*dbo.DisbursementPeriods dp ON d .DisbursementPeriodId = dp.DisbursementPeriodId INNER JOIN*/ Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  WHERE		RecordTypeId = 1
							AND d.FundraisingEntityId = 56
							AND d.FundraisingYear = @yr
							AND DATEPART(QUARTER , d.DonationDate) = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							1 ,
							'National Corporate Partners' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND d.FundraisingEntityId NOT IN ( 56 , 20 , 21 ) /*---------------------update below to use disbursementdate*/
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							1 ,
							'National Corporate Partners' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND ( p.FundraisingEntityId IN ( 20 , 21 )
								  AND ( cp.CampaignTypeFundraisingEntityId IS NULL
										OR ( ps.EndDate IS NOT NULL
											 AND YEAR(ps.EndDate) <= d.FundraisingYear ) ) )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							2 ,
							'National Programs and Events' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							CASE WHEN pp.DisplayName LIKE '%Radiothon' THEN 'Radiothon Announced'
								 ELSE pp.DisplayName
							END AS programDisplayName ,
							CurrencyTypeId ,
							Amount ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
								  AND ( ps.EndDate IS NULL
										OR
                                                        /*y(ear(ps.EndDate) >= d .FundraisingYear AND disbursementdate != '1900-01-01')))*/ ( YEAR(ps.EndDate) - 1 >= d.FundraisingYear/*AND disbursementdate != '1900-01-01'*/ ) ) )
							AND NOT ( c.CampaignTypeId IN ( 27 , 32 )
									  AND d.FundraisingYear >= 2013 )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
                               /*-pledge data for ntl programs*/
				  UNION ALL
				  SELECT	i.InvoiceId ,
							4 ,
							'Overlap' AS Category ,
							FundraisingYear ,
							dp.DisbursementQuarter ,
							dp.DisbursementYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							pp.DisplayName ,
							CurrencyTypeId ,
							Amount * -1 ,
							FundTypeId ,
							RecordTypeId ,
							NULL AS PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							l.LocationId
				  FROM		Disbursements d
				  INNER JOIN dbo.DisbursementPeriods dp ON d.DisbursementPeriodId = dp.DisbursementPeriodId
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  INNER JOIN DisbursementDates dd ON d.DisbursementDateId = dd.DisbursementDateId
				  LEFT OUTER JOIN dbo.Locations l ON d.LocationId = l.LocationId
													 AND d.FundraisingEntityId = l.FundraisingEntityId
				  LEFT OUTER JOIN [dbo].[InvoicedDisbursements] i ON d.DisbursementId = i.DisbursementId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		RecordTypeId = 1
							AND d.FundraisingEntityId NOT IN ( 56 , 20 , 21 ) /*---------------------update below to use disbursementdate*/
							AND ( cp.CampaignTypeFundraisingEntityId IS NOT NULL
								  AND ( ps.EndDate IS NULL
										OR ( YEAR(ps.EndDate) - 1 >= d.FundraisingYear
											 AND DisbursementDate != '1900-01-01' ) ) )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				  UNION ALL
				  SELECT	i.InvoiceId ,
							2 ,
							'National Programs and Events' AS Category ,
							FundraisingYear ,
							d.Quarter ,
							YEAR(d.PledgeDate) AS DisbYear ,
							d.FundraisingEntityId ,
							p.DisplayName ,
							m.MarketId ,
							m.MarketName ,
							d.CampaignDetailsId ,
							cd.CampaignDetailName ,
							c.CampaignName ,
							ct.CampaignTypeId ,
							ct.CampaignType ,
							CASE WHEN pp.DisplayName LIKE '%Radiothon' THEN 'Radiothon Announced'
								 ELSE pp.DisplayName
							END AS DisplayName ,
							CurrencyTypeId ,
							Amount ,
							NULL ,
							NULL ,
							PledgeTypeId ,
							DirectToHospital ,
							m.RegionId ,
							NULL
				  FROM		PledgeData d
				  INNER JOIN Markets m ON d.MarketId = m.MarketId
				  INNER JOIN FundraisingEntities p ON d.FundraisingEntityId = p.FundraisingEntityId
				  INNER JOIN CampaignDetails cd ON d.CampaignDetailsId = cd.CampaignDetailsId
				  INNER JOIN Campaigns c ON cd.CampaignId = c.CampaignId
				  INNER JOIN CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
				  LEFT OUTER JOIN CampaignTypeFundraisingEntities cp ON ct.CampaignTypeId = cp.CampaignTypeId
				  LEFT OUTER JOIN FundraisingEntities pp ON cp.FundraisingEntityId = pp.FundraisingEntityId
				  LEFT OUTER JOIN FundraisingEntityNationalProgramStatus ps ON pp.FundraisingEntityId = ps.FundraisingEntityId
				  LEFT OUTER JOIN InvoicedPledges i ON d.PledgeId = i.PledgeId
				  INNER JOIN dbo.Invoices iv ON i.InvoiceId = iv.InvoiceId
				  WHERE		PledgeTypeId IN ( 1 , 2 )
							AND iv.InvoiceYear = @yr
							AND iv.InvoiceQuarter = @qrtr
				) t
		WHERE	t.MarketId = @mrktid
				OR @mrktid IS NULL
		GROUP BY t.MarketId ,
				MarketName ,
				t.FundraisingYear ,
				Category ,
				invoiceid;

		IF @mrktid IS NOT NULL
			BEGIN 
				DECLARE	@market VARCHAR(100) = (
												 SELECT TOP 1
														MarketName
												 FROM	#tempInvoices
											   );
				SELECT	t.MarketId ,
						t.MarketName ,
						t.FundraisingYear ,
						t.Category ,
						t.invoiceid ,
						c.CountryId ,
						c.CountryName ,
						CASE WHEN c.CountryId = 1 THEN .055
							 WHEN c.CountryId = 2
								  AND t.FundraisingYear = 2015 THEN .055
							 WHEN c.CountryId = 2
								  AND t.FundraisingYear = 2016 THEN .060
							 WHEN c.CountryId = 2
								  AND t.FundraisingYear = 2017 THEN .070
							 ELSE .060
						END AS InvoiceRate ,
						SUM(t.dth) DirectToHospital ,
						SUM(t.nodth) ThroughCMN ,
						SUM(t.notinvoiced) NotInvoiced
				FROM	(
						  SELECT	*
						  FROM		#tempInvoices
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr ,
									'National Corporate Partners' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr ,
									'Local' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr ,
									'National Programs and Events' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr ,
									'Overlap' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr - 1 ,
									'National Corporate Partners' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr - 1 ,
									'Local' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr - 1 ,
									'National Programs and Events' ,
									@invid ,
									0 ,
									0 ,
									0
						  UNION ALL
						  SELECT	@mrktid ,
									@market ,
									@yr - 1 ,
									'Overlap' ,
									@invid ,
									0 ,
									0 ,
									0
						) AS t
				JOIN	dbo.Markets m ON m.MarketId = t.MarketId
				JOIN	dbo.Countries c ON c.CountryId = m.CountryId
				GROUP BY t.MarketId ,
						t.MarketName ,
						t.FundraisingYear ,
						t.Category ,
						t.invoiceid ,
						c.CountryId ,
						c.CountryName;
			END; 
		ELSE
			SELECT	i.MarketId ,
					i.MarketName ,
					i.FundraisingYear ,
					i.Category ,
					i.invoiceid ,
					c.CountryId ,
					c.CountryName ,
					CASE WHEN c.CountryId = 1 THEN .055
						 WHEN c.CountryId = 2
							  AND i.FundraisingYear = 2015 THEN .055
						 WHEN c.CountryId = 2
							  AND i.FundraisingYear = 2016 THEN .060
						 WHEN c.CountryId = 2
							  AND i.FundraisingYear = 2017 THEN .070
						 ELSE .060
					END AS InvoiceRate ,
					i.dth AS DirectToHospital ,
					i.nodth AS ThroughCMN ,
					i.notinvoiced AS NotInvoiced
			FROM	#tempInvoices i
			JOIN	dbo.Markets m ON m.MarketId = #tempInvoices.MarketId
			JOIN	dbo.Countries c ON c.CountryId = m.CountryId;
	END;

