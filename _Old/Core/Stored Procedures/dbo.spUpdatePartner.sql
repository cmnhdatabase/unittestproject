﻿/****** Object:  StoredProcedure [dbo].[spUpdatePartner] ******/
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 01/26/2015
-- Description:	This procedure is used to update an existing Partner (Fundraising Entity).
-- =============================================
CREATE PROCEDURE [dbo].[spUpdatePartner]
	@FundraisingEntityId INT,
	@Name VARCHAR(100),
	@FriendlyName VARCHAR(100),
	@DisplayName VARCHAR(100),
	@YearStarted INT,
	@Website VARCHAR(255),
	@CountryId INT
AS
BEGIN
	UPDATE [dbo].[FundraisingEntities] SET
		[Name] = @Name,
      	[FriendlyName] = @FriendlyName,
      	[DisplayName] = @DisplayName,
      	[YearStarted] = @YearStarted,
      	[FundraisingCategoryId] = 1,
      	[Website] = @Website,
      	[Active] = 1,
      	[CountryId] = @CountryId
    WHERE [FundraisingEntityId] = @FundraisingEntityId;
END


