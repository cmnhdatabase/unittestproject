﻿




CREATE PROCEDURE [dbo].[rptPvrMarketRegionStatTotal]
(
@year INT
, @market INT
)
 AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market	INT = 175
/*****************/

IF OBJECT_ID('tempdb..#tempMarketRegionStatTotal', 'U') IS NOT NULL
    DROP TABLE #tempMarketRegionStatTotal

SELECT

(SUM(cy) - SUM(CYMinus1)) / NULLIF(SUM(counts),0) AS CYIncrease,
((SUM(cy) - SUM(CYMinus1)) / NULLIF(SUM(counts),0))/ (SUM(CY) / NULLIF(SUM(counts),0)) AS CYPercentIncrease,
(SUM(CY)/SUM(Counts))/(NULLIF(SUM(Population),0)/NULLIF(SUM(counts),0)) AS PerCap,
(SUM(TotalCosts)/NULLIF(SUM(counts),0))/(NULLIF(SUM(cy),0)/NULLIF(SUM(counts),0)) AS CRD,
((SUM(cy) - SUM(TotalCosts))/NULLIF(SUM(TotalCosts),0)) AS ROI,
(SUM(CYFTE))/NULLIF(SUM(counts),0) AS CYFET
INTO #tempMarketRegionStatTotal
FROM (
SELECT 
fd.MarketId AS MarketId,
COUNT(DISTINCT fd.MarketId) AS counts,
SUM(CASE WHEN FundraisingYear = @year -1 THEN Amount ELSE 0 END) AS CYMinus1,
SUM(CASE WHEN FundraisingYear = @year THEN Amount ELSE 0 END) AS CY,
MAX(PopulationEstimate) AS Population
FROM dbo.vwFundraisingData fd
INNER JOIN dbo.vwMarketPopulationRank mp ON mp.MarketId = fd.MarketId
WHERE FundraisingYear BETWEEN @year - 1 AND @year
AND RegionId = (SELECT MAX(RegionId) FROM dbo.vwFundraisingData WHERE MarketId = @market)
GROUP BY fd.MarketId
) AS a
INNER JOIN 
(
SELECT MarketId AS ID,
(FixedFees + VariableFees + OtherFees + Overhead) AS TotalCosts,
 FTE AS CYFTE
FROM Core.dbo.PVRFees tpd
) AS b ON b.Id = a.MarketId



SELECT *
FROM #tempMarketRegionStatTotal






