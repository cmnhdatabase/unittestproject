﻿-- =============================================
-- Author:		Braden Kern
-- Create date: 6/30/2015
-- Description:	checks for dupe Campaigns and CampaignDetails
-- =============================================
CREATE PROCEDURE [dbo].[spCampaignDupCheck]

	-- Add the parameters for the stored procedure here
	
--SELECT
--    campaigndetailsId,y.CampaigndetailName,y.CampaignId,y.StartDate
--    FROM Campaigndetails y
--        INNER JOIN (SELECT
--                        CampaigndetailName,CampaignId,StartDate, COUNT(*) AS Cnt
--                        FROM Campaigndetails
--                        WHERE CampaignYear < 2015
--                        GROUP BY CampaigndetailName,CampaignId,StartDate
--                       HAVING COUNT(*)>1
--                    ) dt ON y.CampaigndetailName=dt.CampaigndetailName and y.CampaignId=dt.CampaignId AND y.StartDate =dt.StartDate
                    
                    



--SELECT
--    campaignId,y.CampaignName,y.CampaignTypeId
--    FROM Campaigns y
--        INNER JOIN (SELECT
--                        CampaignName,CampaignTypeId, COUNT(*) AS Cnt
--                        FROM Campaigns
--                        GROUP BY CampaignName,CampaignTypeId
--                        HAVING COUNT(*)>1
--                    ) dt ON y.CampaignName=dt.CampaignName and y.CampaignTypeId=dt.CampaignTypeId
                    

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRY
    
    
    
    --Checks for CD dupes
    IF(SELECT COUNT(*) FROM
						(
                        SELECT CampaigndetailName,CampaignId,StartDate, COUNT(*) AS Cnt
                        FROM Campaigndetails
                        WHERE CampaignYear < 2015
                        GROUP BY CampaigndetailName,CampaignId,StartDate
                       HAVING COUNT(*)>1
                       ) t2) > 0
                       
                       Begin
RAISERROR (N'CD Duplicates found.', 16, -1);
END

    --Checks for Campaign dupes
    IF(SELECT COUNT(*)
						FROM
						(
                        SELECT CampaignName,CampaignTypeId, COUNT(*) AS Cnt
                        FROM Campaigns
                        GROUP BY CampaignName,CampaignTypeId
                        HAVING COUNT(*)>1
                        ) t1
                       ) > 0
                       
                       Begin
RAISERROR (N'Campaign Duplicates found.', 16, -1);
END








END TRY
BEGIN CATCH
    -- Execute error retrieval routine.
    RAISERROR (N'Duplicates found.', 16, -1);
   PRINT ERROR_MESSAGE();
   
END CATCH;
 
    
    
	
END
