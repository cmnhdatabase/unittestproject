﻿







CREATE PROCEDURE [dbo].[rptPartnerReport_Foundation]

AS

BEGIN
	/***********	TEST SECTION	***********/



	/*********** END TEST SECTION	***********/

SELECT DISTINCT b.Foundation
	, CASE WHEN b.Foundation = 1 THEN 'Yes' ELSE 'No' END AS Value
	, CAST(CASE WHEN b.Foundation = '1' THEN 1 ELSE 0 END AS bit) AS bit
FROM Disbursements AS d INNER JOIN
     Batches AS b ON b.BatchId = d.BatchId
ORDER BY b.Foundation DESC
END








