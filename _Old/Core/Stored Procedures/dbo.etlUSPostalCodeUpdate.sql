﻿
CREATE PROCEDURE [dbo].[etlUSPostalCodeUpdate]
(	@EchoResults BIT = 0	)
AS

BEGIN
	SET NOCOUNT ON;

	/***********	TEST SECTION	***********/

		--DECLARE @EchoResults BIT = 1
		--IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	
		----select top 5 * 
		----  from [dbo].[stagingPostalCodesRawUS]

		----select ZipCode
		----	 , count(distinct State) as 'MultiStateCount'
		----  from [dbo].[stagingPostalCodesRawUS]
		----  group by ZipCode having count(distinct State) > 1

		--delete dbo.testPostalCodes

		--insert into dbo.testPostalCodes
		--select * from dbo.PostalCodes
		--  where PostalCode like '%25%'

		--delete dbo.testPostalCodeDemographics
		-- ----select * from dbo.testPostalCodeDemographics
		--insert into dbo.testPostalCodeDemographics
		--  select * from dbo.PostalCodeDemographics

	/*********** END TEST SECTION	***********/

	IF OBJECT_ID('tempdb..#Zips', 'U') IS NOT NULL DROP TABLE #Zips	
	IF OBJECT_ID('tempdb..#PostalCodesChanges', 'U') IS NOT NULL DROP TABLE #PostalCodesChanges	

	IF @EchoResults = 1
	  BEGIN
			SELECT ZIPCount, DemoCount FROM 
			  (	SELECT COUNT(*) AS ZIPcount, 1 AS FakeJoin
				  FROM dbo.PostalCodes) AS pc
				  --FROM dbo.testPostalCodes) AS pc
				INNER JOIN 
				(SELECT COUNT(*) AS DemoCount, 1 AS FakeJoin
				  FROM dbo.PostalCodeDemographics) AS Q ON pc.FakeJoin = Q.FakeJoin
				  --FROM dbo.testPostalCodeDemographics) AS Q ON pc.FakeJoin = Q.FakeJoin
	  END

	BEGIN TRANSACTION
		BEGIN TRY

		CREATE TABLE #PostalCodesChanges
		(
			[ProcessStep]				NVARCHAR(20) NOT NULL,
			[OldPostalCode] 			VARCHAR(10) NULL,
			[OldMarketId] 				INT NULL,
			[OldSubMarketId] 			INT NULL,
			[OldProvinceId] 			INT NULL,
			[OldCountryId] 				INT NULL,
			[OldCity] 					VARCHAR(100) NULL,
			[OldCounty] 				VARCHAR(50) NULL,
			[OldPopulationEstimate]		INT NULL,
			[OldLatitude]				FLOAT NULL,
			[OldLongitude] 				FLOAT NULL,
			[OldLastModified]			DATETIME NULL,
			[action]					NVARCHAR(10) NOT NULL,
			[NewPostalCode] 			VARCHAR(10) NOT NULL,
			[NewMarketId] 				INT NOT NULL,
			[NewSubMarketId] 			INT NOT NULL,
			[NewProvinceId] 			INT NOT NULL,
			[NewCountryId] 				INT NOT NULL,
			[NewCity] 					VARCHAR(100) NOT NULL,
			[NewCounty] 				VARCHAR(50) NULL,
			[NewPopulationEstimate]		INT NOT NULL,
			[NewLatitude]				FLOAT NULL,
			[NewLongitude] 				FLOAT NULL,
			[NewLastModified]			DATETIME NULL
		)

		SELECT DISTINCT 
			   qq.* 
			 , (CASE WHEN qq.CountryId = 180 THEN 180 ELSE ISNULL(mkt.MarketIdX, 0) END)	AS MarketId
		  INTO #Zips
		  FROM 
		  (
			SELECT
				   (CASE WHEN LEN(CAST(zips.[ZipCode] AS varchar(50))) < 5 AND ISNUMERIC(zips.[ZipCode]) = 1 THEN LEFT(LEFT('00000', 5 - LEN(CAST(zips.[ZipCode] AS varchar(50)))) + CAST(zips.[ZipCode] AS varchar(50)), 15)
						 ELSE CAST(zips.[ZipCode] AS nvarchar(10))
					 END)										AS PostalCode
				 , State										AS State
				 , prv.ProvinceID								AS ProvinceId
				 , (CASE WHEN State = 'PR' THEN 180 ELSE 1 END) AS CountryId
				 , City
				 , County
				 , Population									AS PopulationEstimate
				 , Latitude
				 , Longitude
				 , Population
				 , HouseholdsPerZipcode
				 , AverageHouseValue
				 , IncomePerHousehold
				 , PersonsPerHousehold
				 , AsianPopulation
				 , BlackPopulation
				 , FemalePopulation
				 , HawaiianPopulation
				 , HispanicPopulation
				 , IndianPopulation
				 , MalePopulation
				 , OtherPopulation
				 , WhitePopulation
				 , MedianAge
				 , MedianAgeMale
				 , MedianAgeFemale
				 , BusinessAnnualPayroll						AS AnnualPayroll
				 , BusinessFirstQuarterPayroll 					AS FirstQuarterPayroll
				 , NumberOfEmployees							AS Employment
				 , NumberOfBusinesses 							AS Establishments	  
			  ----FROM [dbo].[US ZIP Codes raw] AS zips
			  FROM TemporaryData.[dbo].[stagingPostalCodesRawUS] AS zips
				INNER JOIN dbo.Provinces AS prv ON zips.State = prv.Abbreviation AND (CASE WHEN State = 'PR' THEN 180 ELSE 1 END) = prv.CountryId
			  WHERE zips.PrimaryRecord = 'P'
			  ) qq
			OUTER APPLY 
			  (	SELECT DISTINCT tcm.County, tcm.MarketId as MarketIdX, CountyCnt
				  FROM dbo.PostalCodes AS tcm
					INNER JOIN 
					  (	SELECT ProvinceId, County, count(DISTINCT MarketId) AS CountyCnt 
						  FROM dbo.PostalCodes 
						  WHERE ProvinceId = qq.ProvinceId AND County = qq.County AND MarketId <> 0 
						  GROUP BY ProvinceId, County 
						  HAVING COUNT(DISTINCT MarketId)  = 1 
					  ) AS tcs ON tcm.ProvinceId = tcs.ProvinceId AND tcm.County = tcs.County
			  ) AS mkt

		  ORDER BY PostalCode

		MERGE dbo.PostalCodes AS target
		----MERGE dbo.testPostalCodes AS target
		USING (	SELECT DISTINCT 
					   zp.PostalCode
					 , zp.MarketId
					 , zp.ProvinceId
					 , zp.CountryId
					 , zp.City
					 , zp.County
					 , zp.PopulationEstimate
					 , zp.Latitude
					 , zp.Longitude 
				  FROM #Zips AS zp
			  ) AS source (PostalCode, MarketId, ProvinceId, CountryId, City, County, PopulationEstimate, Latitude, Longitude )
			  ON target.PostalCode = source.PostalCode AND target.CountryId = source.CountryId

		WHEN MATCHED AND ((target.ProvinceId				<> source.ProvinceId)
							or (target.CountryId			<> source.CountryId)
							or (target.City					<> source.City)
							or (target.County 				<> source.County)
							or (target.PopulationEstimate	<> source.PopulationEstimate)
							or (target.Latitude				<> source.Latitude)
							or (target.Longitude			<> source.Longitude)
						 ) THEN 
		  UPDATE
			SET ProvinceId			= source.ProvinceId
			  , CountryId			= source.CountryId
			  , City				= source.City
			  , County 				= source.County
			  , PopulationEstimate	= source.PopulationEstimate
			  , Latitude			= source.Latitude
			  , Longitude			= source.Longitude
			  , LastModified		= GETDATE()

		WHEN NOT MATCHED BY TARGET THEN
		  INSERT (PostalCode, MarketId, SubMarketId, ProvinceId, CountryId, City, County, PopulationEstimate, Latitude, Longitude)
		  VALUES (source.PostalCode, source.MarketId, source.MarketId, source.ProvinceId, source.CountryId, source.City
					, source.County, source.PopulationEstimate, source.Latitude, source.Longitude)


--		OUTPUT deleted.*, $action, inserted.*
		OUTPUT 'PostalCodeChanges', deleted.*, $action AS 'Action', inserted.*
			INTO #PostalCodesChanges
		;

		MERGE dbo.PostalCodeDemographics AS target
		----MERGE dbo.testPostalCodeDemographics AS target
		USING (	SELECT PostalCode, Population, HouseholdsPerZipcode, AverageHouseValue, IncomePerHousehold, PersonsPerHousehold
					 , AsianPopulation, BlackPopulation, FemalePopulation, HawaiianPopulation, HispanicPopulation, IndianPopulation
					 , MalePopulation, OtherPopulation, WhitePopulation, MedianAge, MedianAgeMale, MedianAgeFemale
					 , AnnualPayroll, FirstQuarterPayroll, Employment, Establishments
				  FROM #Zips
			  ) AS source (PostalCode, Population, HouseholdsPerZipcode, AverageHouseValue, IncomePerHousehold, PersonsPerHousehold
							 , AsianPopulation, BlackPopulation, FemalePopulation, HawaiianPopulation, HispanicPopulation, IndianPopulation
							 , MalePopulation, OtherPopulation, WhitePopulation, MedianAge, MedianAgeMale, MedianAgeFemale
							 , AnnualPayroll, FirstQuarterPayroll, Employment, Establishments)
				ON target.PostalCode = source.PostalCode

		WHEN MATCHED THEN
		  UPDATE
			SET Population			= source.Population
			  , HouseholdsPerZipcode	= source.HouseholdsPerZipcode
			  , AverageHouseValue	= source.AverageHouseValue
			  ,	IncomePerHousehold	= source.IncomePerHousehold
			  ,	PersonsPerHousehold	= source.PersonsPerHousehold
			  , AsianPopulation		= source.AsianPopulation
			  ,	BlackPopulation		= source.BlackPopulation
			  ,	FemalePopulation	= source.FemalePopulation
			  ,	HawaiianPopulation	= source.HawaiianPopulation
			  , HispanicPopulation	= source.HispanicPopulation
			  , IndianPopulation	= source.IndianPopulation
			  , MalePopulation 		= source.MalePopulation
			  , OtherPopulation		= source.OtherPopulation
			  , WhitePopulation		= source.WhitePopulation
			  , MedianAge			= source.MedianAge
			  , MedianAgeMale		= source.MedianAgeMale
			  , MedianAgeFemale		= source.MedianAgeFemale
			  , AnnualPayroll		= source.AnnualPayroll	
			  , FirstQuarterPayroll	= source.FirstQuarterPayroll
			  , Employment			= source.Employment	
			  , Establishments		= source.Establishments

		WHEN NOT MATCHED BY TARGET THEN
		  INSERT (PostalCode, Population, HouseholdsPerZipcode, AverageHouseValue, IncomePerHousehold, PersonsPerHousehold
							, AsianPopulation, BlackPopulation, FemalePopulation, HawaiianPopulation, HispanicPopulation, IndianPopulation
							, MalePopulation, OtherPopulation, WhitePopulation, MedianAge, MedianAgeMale, MedianAgeFemale
							, AnnualPayroll, FirstQuarterPayroll, Employment, Establishments
				 )
		  VALUES (source.PostalCode, source.Population, source.HouseholdsPerZipcode, source.AverageHouseValue, source.IncomePerHousehold, source.PersonsPerHousehold
						, source.AsianPopulation, source.BlackPopulation, source.FemalePopulation, source.HawaiianPopulation, source.HispanicPopulation, source.IndianPopulation
						, source.MalePopulation, source.OtherPopulation, source.WhitePopulation, source.MedianAge, source.MedianAgeMale, source.MedianAgeFemale
						, source.AnnualPayroll, source.FirstQuarterPayroll, source.Employment, source.Establishments)

		OUTPUT $Action, inserted.*
		;

		END TRY

		BEGIN CATCH
			SELECT 
				 ERROR_NUMBER() AS ErrorNumber
				,ERROR_SEVERITY() AS ErrorSeverity
				,ERROR_STATE() AS ErrorState
				,ERROR_PROCEDURE() AS ErrorProcedure
				,ERROR_LINE() AS ErrorLine
				,ERROR_MESSAGE() AS ErrorMessage;

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

		IF @EchoResults = 1
		  BEGIN

			SELECT ZIPCount, DemoCount FROM 
			  (	SELECT COUNT(*) AS ZIPcount, 1 AS FakeJoin
				  FROM dbo.PostalCodes) AS pc
				  ----FROM dbo.testPostalCodes) AS pc
				INNER JOIN 
				(SELECT COUNT(*) AS DemoCount, 1 AS FakeJoin
				  FROM dbo.PostalCodeDemographics) AS Q ON pc.FakeJoin = Q.FakeJoin
				  ----FROM dbo.testPostalCodeDemographics) AS Q ON pc.FakeJoin = Q.FakeJoin

			  /* change counts */
			SELECT ProcessStep, Action, COUNT(*) AS RecordsAffected
			  FROM #PostalCodesChanges
			  GROUP BY ProcessStep, Action

			SELECT 'NewRecords' = (SELECT COUNT(*) FROM #PostalCodesChanges WHERE Action = 'INSERT')
				 , 'PopulationEstimate' =  (SELECT COUNT(*) 
											  FROM #PostalCodesChanges WHERE Action = 'UPDATE' 
																			 AND (OldPopulationEstimate <> NewPopulationEstimate)
								)
				 , 'CityStateCountry' =  (SELECT COUNT(*) 
											  FROM #PostalCodesChanges WHERE Action = 'UPDATE' 
																			 AND (OldCity <> NewCity OR OldProvinceId <> NewProvinceId
																				  OR LEFT(OldCountryId,2) <> LEFT(NewCountryId,2))
											)
				 , 'LatLong' = (SELECT COUNT(*) 
								  FROM #PostalCodesChanges WHERE Action = 'UPDATE' 
																 AND (OldLatitude <> NewLatitude OR OldLongitude <> NewLongitude) 
								)

		  END
END









