﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to delete an existing RadiothonHospital record.
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteRadiothonHospital]
	@RadiothonId int,
	@HospitalId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM [dbo].[RadiothonHospitals] 
	WHERE [RadiothonId] = @RadiothonId AND [HospitalId] = @HospitalId;
END
