﻿







CREATE PROCEDURE [dbo].[rptPvrCorpPartner]
(
@year INT
, @market INT
)
 AS

--DECLARE @year INT = 2014
--DECLARE @market INT = 39

IF OBJECT_ID('tempdb..#tempLocationInfo', 'U') IS NOT NULL
    DROP TABLE #tempLocationInfo

SELECT  d.FundraisingEntityId ,
        fe.Name AS DisplayName,
        ISNULL(SUM(CASE WHEN FundraisingYear = @year - 1 THEN Amount
                   END), 0) AS 'CYMinus1Total' ,
        ISNULL(SUM(CASE WHEN FundraisingYear = @year THEN Amount
                   END), 0) AS 'CYTotal' ,
        a.loccount AS Active ,
        b.CYlocCount AS CYCount,
		b3.CYMinus1locCount AS CYMinus1Count
INTO    #tempLocationInfo
FROM    dbo.vwFundraisingData d
        INNER JOIN dbo.vwFundraisingEntities fe ON fe.FundraisingEntityId = d.FundraisingEntityId
        LEFT OUTER JOIN ( SELECT FundraisingEntityId ,
                            COUNT(DISTINCT LocationId) AS loccount
                     FROM   dbo.vwLocations l
                            INNER JOIN dbo.vwPostalCodes pc ON pc.PostalCode = l.PostalCode
                     WHERE  MarketId = @market
                            AND Active = 1
                     GROUP BY FundraisingEntityId
                   ) AS a ON a.FundraisingEntityId = d.FundraisingEntityId
        LEFT OUTER JOIN ( SELECT d2.FundraisingEntityId ,
                            COUNT(DISTINCT LocationId) AS CYlocCount
                     FROM   dbo.vwFundraisingData d2
                     WHERE  FundraisingYear = @year
                            AND MarketId = @market
                     GROUP BY FundraisingEntityId
                   ) AS b ON b.FundraisingEntityId = fe.FundraisingEntityId
        LEFT OUTER JOIN ( SELECT d3.FundraisingEntityId ,
                            COUNT(DISTINCT LocationId) AS CYMinus1locCount
                     FROM   dbo.vwFundraisingData d3
                     WHERE  FundraisingYear = @year -1
                            AND MarketId = @market
                     GROUP BY FundraisingEntityId
                   ) AS b3 ON b3.FundraisingEntityId = fe.FundraisingEntityId
WHERE   FundraisingYear BETWEEN @year - 1 AND @year
        AND MarketId = @market
		AND d.FundraisingCategoryId = 1
GROUP BY d.FundraisingEntityId ,
        fe.Name ,
        b.CYlocCount ,
		b3.CYMinus1locCount,
        a.loccount
ORDER BY d.FundraisingEntityId 



SELECT  * ,
        ( CYTotal - CYMinus1Total ) / NULLIF(CYMinus1Total, 0) AS PerChange ,
        CYTotal / NULLIF(CYCount, 0) AS CYAvg,
		CYMinus1Total / NULLIF(CYMinus1Count, 0) AS CYMinus1Avg,
		CASE WHEN (Active - CYCount) * (CYTotal / NULLIF(CYCount, 0)) < 0 THEN 0 ELSE (Active - CYCount) * (CYTotal / NULLIF(CYCount, 0)) END AS Additional
FROM    #tempLocationInfo


