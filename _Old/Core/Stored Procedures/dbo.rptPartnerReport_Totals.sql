﻿












CREATE PROCEDURE [dbo].[rptPartnerReport_Totals]
(
@year INT
, @prtnr INT
, @foundation INT
)
AS

BEGIN
	/***********	TEST SECTION	***********/
--	DECLARE
--@year INT  = 2014
--, @prtnr INT = 71
--, @foundation INT = 0
----, @mrkt INT = 1


--IF OBJECT_ID('tempdb..#tempLocation', 'U') IS NOT NULL DROP TABLE #tempLocation
	/*********** END TEST SECTION	***********/

CREATE TABLE #tempLocation
(
    LocationId INT,
    District VARCHAR(MAX),
    Zone VARCHAR(MAX),
	Division VARCHAR(MAX),
	Facility VARCHAR(MAX),
	Region VARCHAR(MAX),

)

INSERT INTO #tempLocation
        ( LocationId ,
          District ,
          Zone ,
          Division ,
		Facility ,
          Region
        )
EXEC dbo.rptLocationAreas

SELECT 
 l.FundraisingEntityID
, r.RegionId
, r.RegionName
, CASE   
	WHEN t.Region IS NULL THEN 'Unknown' 
	ELSE t.Region
END AS Region
, CASE 
	WHEN t.Division  IS NULL THEN 'Unknown'
	ELSE t.Division
  END AS Division
, m.MarketId
, p.PropertyTypeId
, CAST (CASE WHEN b.Foundation = '1' THEN 1 ELSE 0 END AS INT) AS 'Foundationbit'
, SUM(CASE WHEN FundraisingYear = @year - 1 THEN Amount ELSE 0 END) AS Yprev
, SUM(CASE WHEN FundraisingYear = @year THEN Amount ELSE 0 END) AS Ycur
, SUM(v.Amount) AS Amount
FROM 
#tempLocation t
	INNER JOIN dbo.Disbursements v ON t.LocationId = v.locationid 
	INNER JOIN dbo.Locations l ON v.locationid = l.LocationId
	INNER JOIN dbo.PropertyTypes p ON l.PropertyTypeId = p.PropertyTypeId
	INNER JOIN dbo.FundraisingEntities pp ON v.FundraisingEntityID = pp.FundraisingEntityID
	INNER JOIN dbo.CampaignDetails cd ON v.CampaignDetailsId = cd.CampaignDetailsId
	INNER JOIN dbo.Campaigns c ON cd.CampaignId = c.CampaignId
	INNER JOIN dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
	INNER JOIN dbo.Markets m ON v.MarketId = m.MarketId
	INNER JOIN dbo.Regions r ON r.RegionId = m.RegionId
	INNER JOIN dbo.Batches b ON v.BatchId = b.BatchId
WHERE        (v.FundraisingEntityID = @prtnr) AND (v.FundraisingYear >= @year - 1) AND (v.RecordTypeId = 1) 
--AND (m.MarketId = @mrkt)
 AND (b.Foundation = @foundation) AND (v.DonationDate <= DATEADD(YEAR, - 1, GETDATE())) AND 
                         (v.FundraisingYear = @year - 1) OR
                         (v.FundraisingEntityID = @prtnr) AND (v.FundraisingYear >= @year - 1) AND (v.RecordTypeId = 1) 
--AND (m.MarketId = @mrkt) 
AND (b.Foundation = @foundation) AND (v.FundraisingYear = @year)



GROUP BY
 l.FundraisingEntityID
, m.MarketId
, r.RegionId
, r.RegionName
, t.Region
, t.Division
, p.PropertyTypeId
, b.Foundation

END











