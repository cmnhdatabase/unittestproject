﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This will set the specified disbursements disbursementperiodid field.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateDisbursementPeriodId]
	@DisbursementId INT,
	@DisbursementPeriodId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Disbursements
	SET DisbursementPeriodId = @DisbursementPeriodId
	WHERE DisbursementId = @DisbursementId
END


