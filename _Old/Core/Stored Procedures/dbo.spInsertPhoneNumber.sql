﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new PhoneNumber.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertPhoneNumber]
	@Phone VARCHAR(15), @PhoneTypeId INT, @CountryId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		INSERT INTO dbo.PhoneNumbers
		        ( Phone, PhoneTypeId, CountryId )
		VALUES  ( @Phone, @PhoneTypeId, @CountryId)	
END


