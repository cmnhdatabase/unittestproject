﻿
CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryReport_AllLocations](
       @year             INT,
       @partnerId        INT,
       @topLocationCount INT = 20 )
AS
    BEGIN
	
        /***********	TEST SECTION	***********/

        --DECLARE @partnerId INT = 123, 
        --		@year      INT = 2014
        --IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL DROP TABLE #Temp	

        /*********** END TEST SECTION	***********/

        IF OBJECT_ID('tempdb..#tmpLocationInfo', 'U') IS NOT NULL
        DROP TABLE #tmpLocationInfo;
        DECLARE @prmYear      INT = @year,
                @prmPartnerID INT = @partnerid;
        SELECT l.LocationId AS LocationID,
               l.LocationNumber,
               m.MarketName AS 'Benefitting Market',
               SUM(CASE
                       WHEN d.FundraisingYear = @prmYear - 3
                       THEN Amount
                       ELSE 0
                   END) AS CYminus3,
               SUM(CASE
                       WHEN d.FundraisingYear = @prmYear - 2
                       THEN Amount
                       ELSE 0
                   END) AS CYminus2,
               SUM(CASE
                       WHEN d.FundraisingYear = @prmYear - 1
                       THEN Amount
                       ELSE 0
                   END) AS CYminus1,
               SUM(CASE
                       WHEN d.FundraisingYear = @prmYear
                       THEN Amount
                       ELSE 0
                   END) AS CY,
               l.LocationName AS 'Location Name',
               l.city AS 'Shipping City',
               p.Province AS 'State'
        FROM Locations l
             INNER JOIN Disbursements d ON l.LocationId = d.LocationId
             INNER JOIN Provinces p ON l.ProvinceId = p.ProvinceId
             INNER JOIN Markets m ON d.MarketId = m.MarketId
        WHERE d.fundraisingyear >= @prmYear - 3
          AND d.FundraisingEntityId = @prmPartnerID
        GROUP BY l.LocationId,
                 l.LocationName,
                 l.LocationNumber,
                 l.City,
                 p.Province,
                 m.MarketName
        ORDER BY CYminus1 DESC;
    END;

