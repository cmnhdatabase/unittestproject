﻿




CREATE PROCEDURE [dbo].[spMergeOfficeGroupImport]
AS
	BEGIN

		BEGIN TRANSACTION;
		MERGE RemaxMiracleSystem.dbo.RemaxOfficeGroups t
		USING
			(
			  SELECT DISTINCT
						ISNULL(imp.MainOfficeID , imp.OfficeID) AS MainOfficeID
			  FROM		TemporaryData.dbo.ReMaxOfficesImportStaging imp
			  JOIN		Core.dbo.Locations l ON l.LocationNumber = imp.OfficeID
												AND l.FundraisingEntityId IN ( 96 , 97 )
												AND l.CountryId = 1
			) s
		ON ( t.Name = s.MainOfficeID )
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ( Name ,
					 CreatedDate ,
					 LastModifiedDate )
			VALUES ( s.MainOfficeID ,
					 GETDATE() ,
					 GETDATE() )
		OUTPUT
			'Inserted --->' ,
			Inserted.* ,
			'Deleted --->' ,
			Deleted.*;
		COMMIT;

		BEGIN TRANSACTION;
		MERGE RemaxMiracleSystem.dbo.RemaxOffices t
		USING
			(
			  SELECT DISTINCT
						imp.OfficeID ,
						l.LocationId AS RemaxOfficeID ,
						rog.Id
			  FROM		TemporaryData.dbo.ReMaxOfficesImportStaging imp
			  JOIN		RemaxMiracleSystem.dbo.RemaxOfficeGroups rog ON ISNULL(imp.MainOfficeID, imp.OfficeID) = rog.Name
			  JOIN		Core.dbo.Locations l ON l.LocationNumber = imp.OfficeID
												AND l.FundraisingEntityId IN ( 96 , 97 )
												AND l.CountryId = 1
			  WHERE		l.LocationId IS NOT NULL
			) s
		ON t.RemaxOfficeId = s.RemaxOfficeID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT ( RemaxOfficeId ,
					 RemaxOfficeGroupId ,
					 ResidentialHonorCardMinimum ,
					 CommercialHonorCardMinimum ,
					 DirectContributionsOnly ,
					 HonorCardPaymentDayOfMonth ,
					 HonorCardBalance ,
					 HonorCardBalanceCalculatedDate ,
					 HonorCardBalanceDue ,
					 HonorCardBalance30DayPastDue ,
					 HonorCardBalance60DayPastDue ,
					 HonorCardBalance90DayPastDue ,
					 CreatedByUser ,
					 CreatedDate ,
					 LastModifiedByUser ,
					 LastModifiedDate )
			VALUES ( s.RemaxOfficeID ,
					 s.Id ,
					 25.00 ,
					 25.00 ,
					 0 ,
					 NULL ,
					 0.00 ,
					 GETDATE() ,
					 0.00 ,
					 0.00 ,
					 0.00 ,
					 0.00 ,
					 4 ,
					 GETDATE() ,
					 4 ,
					 GETDATE() )
		WHEN MATCHED THEN
			UPDATE SET
					t.RemaxOfficeGroupId = s.Id
		OUTPUT
			'Inserted --->' ,
			Inserted.* ,
			'Deleted --->' ,
			Deleted.*;
		COMMIT;

	END;



