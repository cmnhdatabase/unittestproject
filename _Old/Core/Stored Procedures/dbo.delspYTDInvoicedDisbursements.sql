﻿
CREATE PROCEDURE [dbo].[delspYTDInvoicedDisbursements] 
	-- Add the parameters for the stored procedure here
	@year INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	;WITH inv AS
(SELECT m.MarketId, m.MarketName, SUM(d.Amount) Invoiced
FROM dbo.InvoicedDisbursements id INNER JOIN dbo.Disbursements d ON d.DisbursementId = id.DisbursementId
	INNER JOIN dbo.DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
	INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
WHERE dp.DisbursementYear = @year
--AND dp.DisbursementQuarter = 1
GROUP BY m.MarketId, m.MarketName),
disb AS (
SELECT   m.MarketId ,
                        m.MarketName ,
                        SUM(CASE WHEN d.RecordTypeId = 1 THEN d.Amount
                                 ELSE 0
                            END) Amount ,
                        SUM(CASE WHEN d.RecordTypeId = 2 THEN d.Amount
                                 ELSE 0
                            END) Expense
               FROM     dbo.Disbursements d
                        INNER JOIN dbo.DisbursementPeriods dp ON dp.DisbursementPeriodId = d.DisbursementPeriodId
                        INNER JOIN dbo.Markets m ON m.MarketId = d.MarketId
                        INNER JOIN dbo.Batches b ON b.BatchId = d.BatchId
               WHERE    FundraisingYear = @year
                        AND dp.DisbursementQuarter != 0
                        AND b.BatchTypeId = 1
               GROUP BY m.MarketId ,
                        m.MarketName
)
SELECT disb.MarketId, disb.MarketName, inv.Invoiced, disb.Amount AS Disbursed, disb.Expense 
FROM inv INNER JOIN disb ON disb.MarketId = inv.MarketId
ORDER BY 2
END

