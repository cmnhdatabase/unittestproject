﻿
CREATE PROC [dbo].[spMergeAssociateImport]
AS
	BEGIN
		SET NOCOUNT ON;	

		IF OBJECT_ID('tempdb..#RemaxAssociateChanges' , 'U') IS NOT NULL
			DROP TABLE #RemaxAssociateChanges;
		IF OBJECT_ID('tempdb..#StagingTable' , 'U') IS NOT NULL
			DROP TABLE #StagingTable;	
		IF OBJECT_ID('tempdb..#Counts' , 'U') IS NOT NULL
			DROP TABLE #Counts;		
		IF OBJECT_ID('tempdb..#BadOffices' , 'U') IS NOT NULL
			DROP TABLE #BadOffices;

		CREATE TABLE #StagingTable
		(
		  RemaxOfficeId INT NULL ,
		  RemaxTitleId INT NULL ,
		  EmployeeNumber VARCHAR(100) NULL ,
		  FirstName VARCHAR(100) NULL ,
		  LastName VARCHAR(100) NULL ,
		  Email VARCHAR(100) NULL ,
		  IsActive BIT NULL ,
		  HonorCardBalanceDue MONEY NULL ,
		  CreatedDate DATETIMEOFFSET(7) NULL ,
		  LastModifiedDate DATETIMEOFFSET(7) NULL ,
		  Type INT NULL
		); 

		CREATE TABLE #Counts
		(
		  ProcessStage VARCHAR(12) ,
		  isactive BIT ,
		  RecordCount INT ,
		  SortOrder TINYINT
		);

		INSERT	INTO #Counts
		SELECT	'Pre Load' ,
				q1.* ,
				1
		FROM	(
				  SELECT	re.IsActive AS isactive ,
							COUNT(*) AS AssociateCount
				  FROM		RemaxMiracleSystem.dbo.RemaxEmployees re
				  GROUP BY	re.IsActive
				) q1
		ORDER BY isactive;

		BEGIN TRANSACTION;
		BEGIN TRY
			BEGIN
				INSERT	INTO #Counts
				SELECT	'NewImports' ,
						1 AS isactive ,
						COUNT(*) ,
						2
				FROM	TemporaryData.dbo.RemaxAssociateImportStaging;

				INSERT	INTO #StagingTable
				SELECT DISTINCT
						ofc.RemaxOfficeId AS RemaxOfficeId ,
						ISNULL(rt.Id , 5) AS RemaxTitleId ,
						taz.AssociateID AS EmployeeNumber ,
						taz.FirstName AS FirstName ,
						ISNULL(taz.LastName , '') AS LastName ,
						CASE WHEN ofc.OfficeNumber = 'R0490319' THEN ''
							 ELSE ISNULL(taz.EMail , 'n/a')
						END AS Email ,
						CASE WHEN taz.CustomerStatus = 'ACTIVE' THEN 1
							 ELSE 0
						END AS IsActive ,
						0.00 AS HonorCardBalanceDue ,
						GETDATE() AS CreatedDate ,
						GETDATE() AS LastModifiedDate ,
						0 AS [Type]
				FROM	TemporaryData.dbo.RemaxAssociateImportStaging AS taz
				INNER JOIN RemaxMiracleSystem.dbo.vwRemaxOfficeDetails AS ofc ON taz.OfficeID = ofc.OfficeNumber
				LEFT JOIN RemaxMiracleSystem.dbo.RemaxTitles rt ON rt.Code = taz.TitleCode;
			END;

			SELECT	'Bad Office' AS 'Header' ,
					*
			INTO	#BadOffices
			FROM	TemporaryData.dbo.RemaxAssociateImportStaging AS taz
			WHERE	taz.OfficeID NOT IN ( SELECT	OfficeNumber
										  FROM		RemaxMiracleSystem.dbo.vwRemaxOfficeDetails );

			SELECT	*
			FROM	#BadOffices;

			IF (
				 SELECT	COUNT(*) AS booger
				 FROM	#StagingTable
			   ) IS NOT NULL
				BEGIN
					MERGE RemaxMiracleSystem.dbo.RemaxEmployees AS target
					USING
						(
						  SELECT DISTINCT
									RemaxOfficeId ,
									RemaxTitleId ,
									EmployeeNumber ,
									FirstName ,
									LastName ,
									Email ,
									IsActive ,
									HonorCardBalanceDue ,
									CreatedDate ,
									LastModifiedDate ,
									Type
						  FROM		#StagingTable
						) AS source
					ON ( target.EmployeeNumber = source.EmployeeNumber )
					WHEN MATCHED THEN
						UPDATE SET target.RemaxOfficeId = source.RemaxOfficeId ,
								   target.RemaxTitleId = source.RemaxTitleId ,
								   target.EmployeeNumber = source.EmployeeNumber ,
								   target.FirstName = source.FirstName ,
								   target.LastName = source.LastName ,
								   target.Email = source.Email ,
								   target.IsActive = source.IsActive ,
								   target.LastModifiedDate = source.LastModifiedDate ,
								   target.[Type] = source.[Type]
					WHEN NOT MATCHED BY TARGET THEN
						INSERT ( RemaxOfficeId ,
								 RemaxTitleId ,
								 EmployeeNumber ,
								 FirstName ,
								 LastName ,
								 Email ,
								 IsActive ,
								 HonorCardBalanceDue ,
								 CreatedDate ,
								 LastModifiedDate ,
								 [Type] )
						VALUES ( source.RemaxOfficeId ,
								 source.RemaxTitleId ,
								 source.EmployeeNumber ,
								 source.FirstName ,
								 source.LastName ,
								 source.Email ,
								 source.IsActive ,
								 source.HonorCardBalanceDue ,
								 source.CreatedDate ,
								 source.LastModifiedDate ,
								 source.Type )
					WHEN NOT MATCHED BY SOURCE THEN
						UPDATE SET target.IsActive = 0;
				END;    
		END TRY

		BEGIN CATCH
			SELECT	ERROR_NUMBER() AS ErrorNumber ,
					ERROR_SEVERITY() AS ErrorSeverity ,
					ERROR_STATE() AS ErrorState ,
					ERROR_PROCEDURE() AS ErrorProcedure ,
					ERROR_LINE() AS ErrorLine ,
					ERROR_MESSAGE() AS ErrorMessage;

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

		UPDATE	RemaxMiracleSystem..RemaxEmployees
		SET		IsActive = 1
		WHERE	RemaxOfficeId IN ( 675200 , 675201 , 675202 );

		UPDATE	RemaxMiracleSystem..RemaxEmployees
		SET		LegacyRemaxEmployeeId = EmployeeNumber
		WHERE	LegacyRemaxEmployeeId IS NULL;

	END;

