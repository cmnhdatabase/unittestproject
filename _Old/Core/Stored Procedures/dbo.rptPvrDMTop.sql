﻿


CREATE PROCEDURE [dbo].[rptPvrDMTop]
(
@year INT
, @market INT
, @region INT
, @pop INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 175
--DECLARE @region INT = (SELECT DISTINCT RegionID FROM dbo.vwFundraisingData WHERE MarketId = @market)
--DECLARE @pop INT = (SELECT DISTINCT PopulationRank FROM dbo.vwMarketPopulationRank WHERE MarketId = @market)
/*****************/

IF OBJECT_ID('tempdb..#tempDMrank', 'U') IS NOT NULL
    DROP TABLE #tempDMrank

SELECT  b.MarketId ,
b.RegionId ,
        b.bCampaignTypeId ,
        b.CYTotal ,
        b.CYMinus1 ,
		( ( CYTotal - CYminus1 ) / NULLIF(CYminus1, 0) )AS PctChange,
		CYTotal / NULLIF(Pop, 0) AS PerCap ,
		CYTotal - CYminus1 AS AmountIncrease ,
		RANK() OVER (PARTITION BY RegionId ORDER BY CYTotal DESC) AS RegionCYTotalRank,
		
		RANK() OVER (PARTITION BY bCampaignTypeId ORDER BY CYTotal DESC) AS NationalCYTotalRank,
        b.NationalRank ,
		b.PopulationRank,
        --a.RegionRank ,
      b.PopRank
INTO    #tempDMrank
FROM    ( SELECT    d.MarketId ,
					d.RegionId,
					mpr.PopulationRank,
                    ISNULL(SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END) , 0) AS CYTotal ,
					ISNULL(SUM(CASE WHEN d.FundraisingYear = @year -1 THEN d.Amount END) , 0) AS CYMinus1,
                    d.CampaignTypeId AS bCampaignTypeId ,
                    RANK() OVER ( PARTITION BY d.CampaignTypeId ORDER BY SUM(Amount) DESC ) AS NationalRank,
					RANK() OVER (PARTITION BY mpr.PopulationRank ORDER BY SUM(Amount) DESC) AS PopRank

          FROM      dbo.vwFundraisingData d
		  INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
          WHERE     ( d.FundraisingYear >= @year - 1 )
                    AND d.CampaignTypeId = 10
          GROUP BY  d.MarketId ,
                    d.CampaignTypeId,
					RegionId,
					PopulationRank
        ) AS b
        INNER JOIN ( SELECT PopulationEstimate AS pop ,
                            MarketId
                     FROM   dbo.vwMarketPopulationRank
                   ) AS c ON b.MarketId = c.MarketId

SELECT  d.MarketId ,
		m.MarketName,
		r.RegionName,
		d.RegionId,
		CYTotal,
		AmountIncrease,
		ISNULL(PctChange, 0) AS PctChange,
		PerCap, 
		RegionCYTotalRank,
		NationalCYTotalRank,
		PopRank,
		PopulationRank
FROM    #tempDMrank d
INNER JOIN dbo.vwMarkets m ON m.MarketId = d.MarketId
INNER JOIN dbo.vwRegions r ON r.RegionId = d.RegionId
WHERE NationalCYTotalRank = 1
OR (RegionCYTotalRank = 1 AND d.RegionId = @region)
OR (PopRank = 1 AND PopulationRank = @pop)



