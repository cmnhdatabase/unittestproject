﻿


create PROCEDURE [dbo].[rptPvrDOI]
(
@year INT
, @market INT
)
AS
/*********Test***********/
--DECLARE @year INT = 2014
--DECLARE @market INT = 1
/***********************/

IF OBJECT_ID('tempdb..#tempDOI', 'U') IS NOT NULL
    DROP TABLE #tempDOI

SELECT FixedFees, VariableFees, OtherFees, Overhead, TotalCost, TotalCost/CyTotal AS CostToRaise, (CyTotal - TotalCost)/TotalCost AS ROI, CyTotal
INTO #tempDOI
FROM (

SELECT MarketId, FixedFees, VariableFees, OtherFees, (OverHead/COLIRate) AS Overhead, (FixedFees + VariableFees + OtherFees + (OverHead/COLIRate)) AS TotalCost
FROM PVRFees
WHERE YEAR = @year
AND MarketId = @market
) AS a
INNER JOIN (
SELECT MarketId AS MarketId2, SUM(amount) AS CyTotal
FROM dbo.vwFundraisingData
WHERE FundraisingYear = @year
AND MarketId = @market
GROUP BY MarketId
) AS b ON a.marketId = b.MarketId2
 

SELECT * FROM #tempDOI