﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 01/26/2015
-- Description:	This procedure is used to add a new Image.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertImage]
	@TypeId int,
	@Value varchar(255),
	@URI varchar(255),
	@Path varchar(255),
	@Width int,
	@Height int,
	@ImageFormatId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Images] (
      [TypeId],
      [Value],
      [URI],
      [Path],
      [Width],
      [Height],
      [ImageFormatId],
      [ModifiedDate]
	)
	VALUES (
		@TypeId,
		@Value,
		@URI,
		@Path,
		@Width,
		@Height,
		@ImageFormatId,
		GETDATE()
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END


