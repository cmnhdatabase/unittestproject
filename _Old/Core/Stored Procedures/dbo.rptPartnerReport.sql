﻿












CREATE PROCEDURE [dbo].[rptPartnerReport]
(	@year  INT 
  , @prtnr INT 
  --, @mrkt INT
)
AS

BEGIN
	/***********	TEST SECTION	***********/
--DECLARE
--@year INT  = 2014
--, @prtnr INT = 123
--, @mrkt INT = 1
--, @proptype INT = 94

--IF OBJECT_ID('tempdb..#tempLocation', 'U') IS NOT NULL DROP TABLE #tempLocation


	/*********** END TEST SECTION	***********/

CREATE TABLE #tempLocation
(
    LocationId INT,
    District VARCHAR(MAX),
    Zone VARCHAR(MAX),
	Division VARCHAR(MAX),
	Facility VARCHAR(MAX),
	Region VARCHAR(MAX),

)

INSERT INTO #tempLocation
        ( LocationId ,
          District ,
          Zone ,
          Division ,
		Facility ,
          Region
        )
EXEC dbo.rptLocationAreas

SELECT
m.MarketId
, l.LocationId
, m.MarketName
, pp.FundraisingEntityID
, pp.Name
, FundraisingYear
, ct.CampaignType
, c.CampaignName
, cd.CampaignDetailName
, l.LocationNumber
, l.LocationName
, p.PropertyType
, p.PropertyTypeId
, v.DonationDate
, v.Amount
, b.Foundation
, r.RegionId
, r.RegionName
, t.Region
, t.Division
, t.District
, CAST (CASE WHEN b.Foundation = '1' THEN 1 ELSE 0 END AS INT) AS 'Foundationbit'
FROM #tempLocation t
	INNER JOIN dbo.Disbursements v ON t.LocationId = v.locationid 
	INNER JOIN dbo.Locations l ON v.locationid = l.LocationId
	INNER JOIN	dbo.PropertyTypes p ON l.PropertyTypeId = p.PropertyTypeId
	INNER JOIN dbo.FundraisingEntities pp ON v.FundraisingEntityID = pp.FundraisingEntityID
	INNER JOIN dbo.CampaignDetails cd ON v.CampaignDetailsId = cd.CampaignDetailsId
	INNER JOIN dbo.Campaigns c ON cd.CampaignId = c.CampaignId
	INNER JOIN dbo.CampaignTypes ct ON c.CampaignTypeId = ct.CampaignTypeId
	INNER JOIN dbo.Markets m ON v.MarketId = m.MarketId
	INNER JOIN dbo.Regions r ON r.RegionId = m.RegionId
	INNER JOIN dbo.Batches b ON v.BatchId = b.BatchId
	WHERE v.FundraisingEntityID = @prtnr 
	AND FundraisingYear >= @year 
	AND RecordTypeId = 1
	--AND M.marketid = @mrkt
END













