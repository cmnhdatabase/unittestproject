﻿



CREATE PROCEDURE [dbo].[rptPvrTopLocalCA]
(
@year INT
, @market INT
)
 AS

/****** Test ******/
--DECLARE @year INT = 2015
--DECLARE @market	INT = 175
/*****************/

IF OBJECT_ID('tempdb..#tempLocalCA', 'U') IS NOT NULL
    DROP TABLE #tempLocalCA

SELECT *

INTO    #tempLocalCA
FROM    ( SELECT    d.MarketId AS TotalID ,
					d.ShortName AS TotalName,
					m.CountryId AS TotalCountry,
					ISNULL(SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END) , 0) AS CyTotal,
					RANK() OVER (PARTITION BY m.CountryId ORDER BY SUM(Amount) DESC) AS TotalRank
          FROM      dbo.vwFundraisingData d
		  INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
		  INNER JOIN dbo.vwMarkets m ON m.MarketId = mpr.MarketId
          WHERE     ( d.FundraisingYear >= @year - 1 )
					AND CountryId = 2
					AND FundraisingCategoryId = 5
          GROUP BY  m.CountryId,
		  d.MarketId ,
					d.ShortName
					
        ) AS a
		INNER JOIN ( SELECT    d.MarketId AS ChangeID,
					d.ShortName AS ChangeName,
					m.CountryId AS ChangeCountry,
					((SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END) - SUM(CASE WHEN d.FundraisingYear = @year - 1 THEN d.Amount END)) / NULLIF((SUM(CASE WHEN d.FundraisingYear = @year - 1 THEN d.Amount END)),0)) AS ChangePer,
					RANK() OVER (PARTITION BY m.CountryId ORDER BY (((SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END)) - (SUM(CASE WHEN d.FundraisingYear = @year - 1 THEN d.Amount END))) / NULLIF(SUM(CASE WHEN d.FundraisingYear = @year - 1 THEN d.Amount END),0))DESC) AS ChangeRank
          FROM      dbo.vwFundraisingData d
		  INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
		  INNER JOIN dbo.vwMarkets m ON m.MarketId = mpr.MarketId
          WHERE     ( d.FundraisingYear >= @year - 1 )
					AND CountryId = 2
					AND FundraisingCategoryId = 5
          GROUP BY  m.CountryId,
		  d.MarketId ,
					d.ShortName) AS b ON b.ChangeCountry = a.TotalCountry
				
				INNER JOIN ( SELECT    d.MarketId AS PerCapID,
					d.ShortName AS PerCapName,
					m.CountryId AS PerCapCountry,
					SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END) / NULLIF(mpr.PopulationEstimate,0) AS PerCap,
					RANK() OVER (PARTITION BY m.CountryId ORDER BY (SUM(CASE WHEN d.FundraisingYear = @year THEN d.Amount END) / NULLIF(mpr.PopulationEstimate,0))DESC) AS PerCapRank
          FROM      dbo.vwFundraisingData d
		  INNER JOIN dbo.vwMarketPopulationRank mpr ON mpr.MarketId = d.MarketId
		  INNER JOIN dbo.vwMarkets m ON m.MarketId = mpr.MarketId
          WHERE     ( d.FundraisingYear = @year)
					AND CountryId = 2
					AND FundraisingCategoryId = 5
          GROUP BY  m.CountryId,
		  d.MarketId ,
		  PopulationEstimate,
					d.ShortName) AS c ON c.PerCapCountry = a.TotalCountry
WHERE TotalRank = 1
AND ChangeRank = 1
AND PerCapRank = 1

SELECT 	TotalID ,
            TotalRank ,
            TotalName AS TotalName ,
            CyTotal ,
            ChangeID ,
            ChangeRank ,
            ChangeName AS ChangeName ,
            ChangePer ,
            PerCapID ,
            PerCapRank ,
            PerCapName AS PerCapName ,
            PerCap
FROM    #tempLocalCA d





