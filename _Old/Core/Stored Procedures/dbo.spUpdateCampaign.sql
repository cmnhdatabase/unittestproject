﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 02/20/2015
-- Description:	This procedure is used to update an existing Campaign.
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCampaign]
	@CampaignId int,
	@CampaignName varchar(300),
	@ShortDescription varchar(300),
	@LongDescription varchar(500),
	@CampaignTypeId int
AS
BEGIN
	UPDATE [dbo].[Campaigns] SET
		[CampaignName] = @CampaignName,
	    [ShortDescription] = @ShortDescription,
	    [LongDescription] = @LongDescription,
	    [CampaignTypeId] = @CampaignTypeId
	WHERE [CampaignId] = @CampaignId;
END
