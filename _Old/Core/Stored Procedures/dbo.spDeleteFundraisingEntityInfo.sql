﻿
-- =============================================
-- Author:           <Author,,Name>
-- Create date: <Create Date,,>
-- Description:      <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteFundraisingEntityInfo] 
       -- Add the parameters for the stored procedure here
       @FundraisingEntityInfoId int

AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

    -- Insert statements for procedure here
    DELETE FROM [dbo].[FundraisingEntityInfo]
       WHERE [FundraisingEntityInfoId] = @FundraisingEntityInfoId
END

