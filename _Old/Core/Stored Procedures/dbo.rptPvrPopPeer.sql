﻿



CREATE PROCEDURE [dbo].[rptPvrPopPeer]
(
@year INT
, @market INT
)
AS

/****** Test ******/
--DECLARE @year INT = 2014
--DECLARE @market INT = 210
/*****************/

IF OBJECT_ID('tempdb..#tempPopPeers', 'U') IS NOT NULL
    DROP TABLE #tempPopPeers

SELECT  SUBSTRING(MarketName ,1, 19) AS MarketName,
        PopulationEstimate ,
        RegionName ,
        CYMinus2 ,
        CYMinus1 ,
        CY ,
        PopRank ,
        a.MarketId ,
        ( ( b.CYMinus1 - b.CYMinus2 ) / NULLIF(b.CYMinus1, 0) ) AS CY2VsCY1 ,
        ( ( b.CY - b.CYMinus1 ) / NULLIF(b.CYMinus1, 0) ) AS CY1VsCY ,
        ( CYMinus1 / NULLIF(PopulationEstimate, 0) ) AS CYMinus1PerCap ,
        ( CY / NULLIF(PopulationEstimate, 0) ) AS CyPerCap
INTO    #tempPopPeers
FROM    ( SELECT    mpr.ShortName AS MarketName ,
                    PopulationEstimate ,
                    mpr.MarketId ,
                    r.RegionName ,
                    RANK() OVER ( ORDER BY populationEstimate DESC ) AS PopRank
          FROM      dbo.vwMarketPopulationRank mpr
                    INNER JOIN dbo.Markets m ON m.MarketId = mpr.MarketId
                    INNER JOIN dbo.Regions r ON r.RegionId = m.RegionId
        ) AS a
        INNER JOIN ( SELECT ISNULL(SUM(CASE WHEN FundraisingYear = @year - 2
                                            THEN Amount
                                       END), 0) AS 'CYMinus2' ,
                            ISNULL(SUM(CASE WHEN FundraisingYear = @year - 1
                                            THEN Amount
                                       END), 0) AS 'CYMinus1' ,
                            ISNULL(SUM(CASE WHEN FundraisingYear = @year
                                            THEN Amount
                                       END), 0) AS 'CY' ,
                            MarketId
                     FROM   dbo.vwFundraisingData
                     GROUP BY MarketId
                   ) AS b ON b.MarketId = a.MarketId



SELECT tpp.* , FTE, (Cy/FTE) AS PerFte
FROM    #tempPopPeers tpp
LEFT OUTER JOIN dbo.PVRFees pf ON pf.MarketId = tpp.MarketId
WHERE   PopRank BETWEEN ( SELECT    PopRank
                          FROM      #tempPopPeers
                          WHERE     MarketId = @market
                        ) - 2
                AND     ( SELECT    PopRank
                          FROM      #tempPopPeers
                          WHERE     MarketId = @market
                        ) + 2
AND pf.Year = @year
ORDER BY tpp.PopulationEstimate DESC

