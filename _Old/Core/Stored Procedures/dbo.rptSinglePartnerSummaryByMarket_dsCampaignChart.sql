﻿
CREATE PROCEDURE [dbo].[rptSinglePartnerSummaryByMarket_dsCampaignChart]
(
@year INT,
@partnerid NVARCHAR(MAX),
@marketid NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @results TABLE( CampaignType    VARCHAR(255),
                        FundraisingYear INT,
                        Amount          MONEY );
INSERT INTO @results
       SELECT CampaignType,
              FundraisingYear,
              SUM(Amount) Amount
       FROM vwFundraisingData
       WHERE FundraisingCategoryId = 1
         AND FundraisingYear >= @year - 3
         AND FundraisingEntityId = @partnerid
         AND MarketId IN( 
                          SELECT Item
                          FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
         AND CampaignType IN( 
                              SELECT CampaignType
                              FROM(
                                       SELECT TOP 4 CampaignType,
                                                    SUM(Amount) Amount
                                       FROM vwFundraisingData
                                       WHERE FundraisingCategoryId = 1
                                         AND FundraisingYear >= @year - 1
                                         AND FundraisingEntityId = @partnerid
                                         AND MarketId IN(
                                                SELECT Item
                                                FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
                                       GROUP BY CampaignType
                                       ORDER BY 2 DESC ) t )
       GROUP BY FundraisingYear,
                CampaignType;
SELECT 1,
       *
FROM @results
UNION
SELECT 2,
       'Remaining Fundraising',
       FundraisingYear,
       SUM(Amount) Amount
FROM vwFundraisingData
WHERE FundraisingCategoryId = 1
  AND FundraisingYear >= @year - 3
  AND FundraisingEntityId = @partnerid
  AND MarketId IN( 
                   SELECT Item
                   FROM dbo.DelimitedSplitN4K( @marketid, ',' ))
  AND CampaignType NOT IN( 
                           SELECT CampaignType
                           FROM @results )
GROUP BY FundraisingYear
ORDER BY 1,
         3 DESC;
END
