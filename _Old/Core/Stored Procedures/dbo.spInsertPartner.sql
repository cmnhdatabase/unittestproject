﻿/****** Object:  StoredProcedure [dbo].[spInsertPartner] ******/
-- =============================================
-- Author:		Ethan Tipton
-- Create date: 01/16/2015
-- Description:	This procedure is used to add a new Partner (Fundraising Entity).
-- =============================================
CREATE PROCEDURE [dbo].[spInsertPartner]
	@Name VARCHAR(100),
	@FriendlyName VARCHAR(100),
	@DisplayName VARCHAR(100),
	@YearStarted INT,
	@Website VARCHAR(255),
	@CountryId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[FundraisingEntities] (
		[Name],
      	[FriendlyName],
      	[DisplayName],
      	[YearStarted],
      	[FundraisingCategoryId],
      	[Website],
      	[Active],
      	[SponsorId],
      	[CountryId] 
	)
	VALUES (
		@Name,
		@FriendlyName,
		@DisplayName,
		@YearStarted,
		1,
		@Website,
		1,
		UPPER(@FriendlyName),
		@CountryId
	)

	SELECT CAST(SCOPE_IDENTITY() AS INT);
END


