﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to add a new TelethonTvStation record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertTelethonTvStation]
	@TelethonId int,
	@TvStationId int,
	@PrimaryStation bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[TelethonTvStations] (
		[TelethonId],
		[TvStationId],
		[PrimaryStation]
	)
	VALUES (
		@TelethonId,
		@TvStationId,
		@PrimaryStation
	)
END
