﻿



CREATE PROCEDURE [dbo].[rptPvrLocalList]
(
@year INT
, @market INT
)
 AS

--/******TEST********/
--DECLARE @year INT = 2015
--DECLARE @market INT = 231
--/******************/

;
WITH    cte
          AS ( SELECT   * ,
                        ROW_NUMBER() OVER ( ORDER BY CampaignName desc) Row
               FROM     ( 
			   SELECT    CampaignName ,
                                    SUM(CASE WHEN FundraisingYear = @year - 2
                                             THEN Amount
                                             ELSE 0
                                        END) AS CYminus2 ,
                                    SUM(CASE WHEN FundraisingYear = @year - 1
                                             THEN Amount
                                             ELSE 0
                                        END) AS CYminus1 ,
                                    SUM(CASE WHEN FundraisingYear = @year
                                             THEN Amount
                                             ELSE 0
                                        END) AS CY ,
                                    1 AS temp
                          FROM      dbo.vwFundraisingData
                          WHERE     FundraisingCategoryId = 5
                                    AND FundraisingYear >= @year - 2
                                    AND MarketId = @market
                          GROUP BY  CampaignName
                          UNION ALL
                          SELECT    NULL ,
                                    '' ,
                                    '' ,
                                    '' ,
                                    1
                        ) AS t
             )
    SELECT  cte.CampaignName ,
            cte.CYminus2 ,
            cte.CYminus1 ,
            cte.CY ,
            cte.temp ,
			cte.Row,
            ISNULL(( CY - CYminus1 ) / NULLIF(CYminus1, 0), 0) AS Increase
    FROM    cte
    WHERE NOT (cte.CampaignName IS NULL AND cte.Row != 1)
	ORDER BY CY DESC
	;
