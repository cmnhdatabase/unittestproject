﻿

CREATE PROCEDURE [dbo].[etlCanadianPostalCodeUpdate]
(	@EchoResults	BIT = 0 )
AS

BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#CanuckZIP', 'U') IS NOT NULL DROP TABLE #CanuckZIP	
	IF OBJECT_ID('tempdb..#DupeCities', 'U') IS NOT NULL DROP TABLE #DupeCities
	IF OBJECT_ID('tempdb..#PostalCodesChanges', 'U') IS NOT NULL DROP TABLE #PostalCodesChanges

	CREATE TABLE #PostalCodesChanges
	(
		[ProcessStep]				NVARCHAR(16) NOT NULL,
		[OldPostalCode] 			VARCHAR(10) NULL,
		[OldMarketId] 				INT NULL,
		[OldSubMarketId] 			INT NULL,
		[OldProvinceId] 			INT NULL,
		[OldCountryId] 				INT NULL,
		[OldCity] 					VARCHAR(100) NULL,
		[OldCounty] 				VARCHAR(50) NULL,
		[OldPopulationEstimate]		INT NULL,
		[OldLatitude]				FLOAT NULL,
		[OldLongitude] 				FLOAT NULL,
		[OldLastModified]			DATETIME NULL,
		[action]					NVARCHAR(10) NOT NULL,
		[NewPostalCode] 			VARCHAR(10) NOT NULL,
		[NewMarketId] 				INT NOT NULL,
		[NewSubMarketId] 			INT NOT NULL,
		[NewProvinceId] 			INT NOT NULL,
		[NewCountryId] 				INT NOT NULL,
		[NewCity] 					VARCHAR(100) NOT NULL,
		[NewCounty] 				VARCHAR(50) NULL,
		[NewPopulationEstimate]		INT NOT NULL,
		[NewLatitude]				FLOAT NULL,
		[NewLongitude] 				FLOAT NULL,
		[NewLastModified]			DATETIME NULL
	)

	; WITH cteChunks AS
	(	SELECT DISTINCT 
				LEFT(pc.PostalCode,3) AS PCchunk
				, COUNT(DISTINCT pc.MarketId) AS mktcount
			FROM Core.dbo.PostalCodes AS pc
			WHERE pc.CountryId = 2
			AND pc.MarketId <> 0
			GROUP BY LEFT(pc.PostalCode,3)
	)
	SELECT DISTINCT 
		   rca.[PostalCode]
		 , ISNULL(mkt.MarketIdX,0)	AS MarketId
		 , prv.ProvinceId
		 , 2						AS CountryId
		 , CASE WHEN RIGHT(rca.[CityMixedCase],1) = '.' THEN REPLACE(rca.[CityMixedCase], '.', '') ELSE rca.[CityMixedCase] END	AS City
		 , rca.[Population]			AS PopulationEstimate
		 , rca.[Latitude]
		 , rca.[Longitude]
		 , ROW_NUMBER() OVER (PARTITION BY rca.PostalCode ORDER BY rca.postalcode) AS RowNumber
	  INTO #CanuckZIP
	  FROM [TemporaryData].[dbo].[stagingPostalCodesRawCA] AS rca
		INNER JOIN Core.[dbo].[Provinces] AS prv ON rca.Province = prv.Abbreviation AND prv.CountryId = 2
		OUTER APPLY 
		  (	SELECT DISTINCT tcm.MarketId as MarketIdX
			  FROM Core.dbo.PostalCodes AS tcm
				INNER JOIN 
				  (	SELECT PCchunk, mktcount
					  FROM cteChunks AS pc
					  WHERE PCchunk = LEFT(rca.PostalCode,3) 
					    AND mktcount = 1
				  ) AS tcs ON LEFT(tcm.PostalCode,3) = tcs.PCchunk 
		  ) AS mkt
	  ORDER BY PostalCode

	CREATE TABLE #DupeCities 
	(  PostalCode			NVARCHAR(10)
	 , MarketId				INT
	 , City					NVARCHAR(100)
	 , ProvinceId			INT
	 , PopulationEstimate	INT
	 , Latitude				FLOAT
	 , Longitude			FLOAT
	)
	INSERT INTO #DupeCities
		SELECT cz.PostalCode
			 , MarketId
			 , cz.City 
			 , ProvinceId
			 , PopulationEstimate
			 , Latitude
			 , Longitude
		  FROM #CanuckZIP AS cz
		  WHERE cz.RowNumber > 1
		  ORDER BY PostalCode
	
	BEGIN TRANSACTION
	  BEGIN TRY
		MERGE [dbo].[PostalCodes] AS target
		  USING 
		  (	
			SELECT PostalCode ,
				   MarketId ,
				   ProvinceId ,
				   CountryId ,
				   City ,
				   PopulationEstimate ,
				   Latitude ,
				   Longitude
			  FROM #CanuckZIP
			  WHERE RowNumber = 1
		  ) AS source (PostalCode, MarketId, ProvinceId, CountryId, City, PopulationEstimate, Latitude, Longitude)
			ON (target.PostalCode = source.PostalCode)

			WHEN MATCHED AND ((target.ProvinceId				<> source.ProvinceId)
								or (target.CountryId			<> source.CountryId)
								or (target.City					<> source.City)
								or (target.PopulationEstimate	<> source.PopulationEstimate)
								or (target.Latitude				<> source.Latitude)
								or (target.Longitude			<> source.Longitude)
							 ) THEN 
			  UPDATE
				SET ProvinceId			= SOURCE.ProvinceId
				  , CountryId			= SOURCE.CountryId
				  , City				= SOURCE.City
				  , PopulationEstimate	= SOURCE.PopulationEstimate
				  , Latitude			= SOURCE.Latitude
				  , Longitude			= SOURCE.Longitude
				  , target.LastModified = GETDATE()

			WHEN NOT MATCHED BY TARGET THEN
			  INSERT (PostalCode, MarketId, SubMarketId, ProvinceId, CountryId, City, PopulationEstimate, Latitude, Longitude)
			  VALUES (source.PostalCode, source.MarketId, source.MarketId, source.ProvinceId, source.CountryId, source.City
						, source.PopulationEstimate, source.Latitude, source.Longitude)


			OUTPUT 'Singles', deleted.*, $action AS 'Action', inserted.*
			INTO #PostalCodesChanges
			;
	END TRY

	BEGIN CATCH
		SELECT 'Error Updating Canadian Postal Codes' AS ErrorWhere 
			, ERROR_NUMBER() AS ErrorNumber
			, ERROR_SEVERITY() AS ErrorSeverity
			, ERROR_STATE() AS ErrorState
			, ERROR_PROCEDURE() AS ErrorProcedure
			, ERROR_LINE() AS ErrorLine
			, ERROR_MESSAGE() AS ErrorMessage;
 
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;
	IF @EchoResults = 1 
	BEGIN

		SELECT ZIPCount, DemoCount FROM 
			(	SELECT COUNT(*) AS ZIPcount, 1 AS FakeJoin
				FROM Core.dbo.PostalCodes) AS pc
			INNER JOIN 
			(SELECT COUNT(*) AS DemoCount, 1 AS FakeJoin
				  FROM Core.dbo.PostalCodeDemographics) AS Q ON pc.FakeJoin = Q.FakeJoin
		SELECT ProcessStep, Action, COUNT(*) AS RecordsAffected
		  FROM #PostalCodesChanges
		  GROUP BY ProcessStep, Action

		SELECT 'NewRecords' = (SELECT COUNT(*) FROM #PostalCodesChanges WHERE Action = 'INSERT')
			 , 'PopulationEstimate' =  (SELECT COUNT(*) 
										  FROM #PostalCodesChanges WHERE Action = 'UPDATE' 
																		 AND (OldPopulationEstimate <> NewPopulationEstimate)
							)
			 , 'CityStateCountry' =  (SELECT COUNT(*) 
										  FROM #PostalCodesChanges WHERE Action = 'UPDATE' 
																		 AND (OldCity <> NewCity OR OldProvinceId <> NewProvinceId
																			  OR LEFT(OldCountryId,2) <> LEFT(NewCountryId,2))
										)
			 , 'LatLong' = (SELECT COUNT(*) 
							  FROM #PostalCodesChanges WHERE Action = 'UPDATE' 
															 AND (OldLatitude <> NewLatitude OR OldLongitude <> NewLongitude) 
							)

		SELECT * FROM 
		(
			SELECT OldPostalCode, OldCity, OldProvinceId, OldCountryId, OldPopulationEstimate, NewCity, NewProvinceId, NewCountryId, NewPopulationEstimate FROM #PostalCodesChanges
			  WHERE Action = 'UPDATE' 
				AND (OldCity <> NewCity)
				AND NOT (OldCity = 'St Albert' AND NewCity = 'St. Albert')
				AND NOT (OldCity = 'Fort St John' AND NewCity = 'Fort St. John')
				AND NOT (OldCity = 'Sault Ste Marie' AND NewCity = 'Sault Ste. Marie')
		) q1 WHERE OldCity LIKE '%.%' OR NewCity LIKE '%.%'
	END
END

