﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure will remove the Batch containing the BatchId passed to the procedure.
-- =============================================
Create PROCEDURE [dbo].[spDeleteBatch]
@BatchId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.Batches
	WHERE BatchId = @BatchId
END



