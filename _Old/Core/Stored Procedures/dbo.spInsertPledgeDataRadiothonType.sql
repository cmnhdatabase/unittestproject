﻿-- =============================================
-- Author:		Ethan Tipton
-- Create date: 06/12/2015
-- Description:	This procedure is used to add a new PledgeDataRadiothonType record.
-- =============================================
CREATE PROCEDURE [dbo].[spInsertPledgeDataRadiothonType]
	@PledgeId int,
	@RadiothonTypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[PledgeDataRadiothonType] (
		[PledgeId],
		[RadiothonTypeId]
	)
	VALUES (
		@PledgeId,
		@RadiothonTypeId
	)
END
