﻿
-- ==========================================================================================
-- Author:		Scott Lance
-- Create date: 7/16/2015
-- Description:	Rolls back a filed EZPMR Worksheet to just before it was filed
-- ==========================================================================================
CREATE PROCEDURE [dbo].[spRollbackFiledWorksheet]
	@MarketId AS INT,
	@Year AS INT,
	@Quarter AS INT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @WorksheetId INT
			DECLARE @FiledDate DATE

			SELECT @WorksheetId = w.[WorksheetId], @FiledDate = w.[FiledDate]
			FROM [EZPMR].[dbo].[MarketWorksheets] w
			WHERE w.[MarketId] = @MarketId
			AND w.[Year] = @Year
			AND w.[Quarter] = @Quarter
			AND w.[Filed] = 1
	
			SELECT [DisbursementId]
			INTO #TempPartnerFundDisbursementIds
			FROM
			(
				SELECT [DisbursementId]
				FROM [EZPMR].[dbo].[PartnerFunds] pf
				INNER JOIN [EZPMR].[dbo].[PartnerEntries] pe ON pe.[PartnerEntryId] = pf.[PartnerEntryId]
				INNER JOIN [EZPMR].[dbo].[Entries] e ON e.[EntryId] = pe.[EntryId]
				INNER JOIN [EZPMR].[dbo].[Selections] s ON s.[SelectionId] = e.[SelectionId]
				INNER JOIN [EZPMR].[dbo].[MarketWorksheets] w ON w.[WorksheetId] = s.[WorksheetId]
				WHERE w.[WorksheetId] = @WorksheetId
			) AS TempPartnerFundDisbursementIds

			SELECT [DisbursementId]
			INTO #TempMiscFundDisbursementIds
			FROM
			(
				SELECT [DisbursementId]
				FROM [EZPMR].[dbo].[MiscFunds] mf
				INNER JOIN [EZPMR].[dbo].[Entries] e ON e.[EntryId] = mf.[EntryId]
				INNER JOIN [EZPMR].[dbo].[Selections] s ON s.[SelectionId] = e.[SelectionId]
				INNER JOIN [EZPMR].[dbo].[MarketWorksheets] w ON w.[WorksheetId] = s.[WorksheetId]
				WHERE w.[WorksheetId] = @WorksheetId
			) AS TempMiscFundDisbursementIds

			-- Delete the existing Disbursement records that were generated from PartnerFunds and MiscFunds
			DELETE FROM [Core].[dbo].[Disbursements]
			WHERE [DisbursementId] IN
			(
				SELECT [DisbursementId] 
				FROM  #TempPartnerFundDisbursementIds

				UNION

				SELECT [DisbursementId] 
				FROM  #TempMiscFundDisbursementIds
			)
	
			-- Delete the existing PledgeData records that were generated from ExtenedEntries
			DELETE FROM [Core].[dbo].[PledgeData]
			WHERE [PledgeId] IN
			(
				SELECT pd.[PledgeId]
				FROM [Core].[dbo].[PledgeData] pd
				INNER JOIN [Core].[dbo].[CampaignDetails] cd ON cd.[CampaignDetailsId] = pd.[CampaignDetailsId]
				WHERE cd.CampaignDetailName IN 
				(
					SELECT e.[EventName]
					FROM [EZPMR].[dbo].[ExtendedEntries] ee
					INNER JOIN [EZPMR].[dbo].[Entries] e ON e.EntryId = ee.EntryId
					INNER JOIN [EZPMR].[dbo].[Selections] s ON s.[SelectionId] = e.[SelectionId]
					INNER JOIN [EZPMR].[dbo].[MarketWorksheets] w ON w.[WorksheetId] = s.[WorksheetId]
					WHERE w.[WorksheetId] = @WorksheetId
					AND [Type] = 'Announced_Total'
				)
				AND (pd.[PledgeTypeId] = 1 
				OR pd.[PledgeTypeId] = 2
				OR pd.[PledgeTypeId] = 5)
				AND pd.[DateRecorded] = @FiledDate
			)

			-- Update all PartnerFunds DisbursementIds to null
			UPDATE [EZPMR].[dbo].[PartnerFunds]
			SET [DisbursementId] = null
			WHERE [DisbursementId] IN
			(
				SELECT [DisbursementId]
				FROM #TempPartnerFundDisbursementIds
			)

			-- Update all MiscFunds DisbursementIds to null
			UPDATE [EZPMR].[dbo].[MiscFunds]
			SET [DisbursementId] = null
			WHERE [DisbursementId] IN
			(
				SELECT [DisbursementId]
				FROM #TempMiscFundDisbursementIds
			)

			-- Reset the MarketWorksheet to unfiled
			UPDATE [EZPMR].[dbo].[MarketWorksheets]
			SET [Filed] = 0,
			[FiledDate] = null,
			[Enabled] = 1
			WHERE [WorksheetId] = @WorksheetId

			-- Drop the #TempPartnerFundDisbursementIds and #TempMiscFundDisbursementIds tables
			DROP TABLE #TempPartnerFundDisbursementIds
			DROP TABLE #TempMiscFundDisbursementIds
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION
                                                
        DECLARE @ErrorMessage VARCHAR(4000),
                @ErrorNumber INT,
                @ErrorSeverity INT,
                @ErrorState INT,
                @ErrorLine INT,
                @ErrorProcedure VARCHAR(200),
                @CRLF CHAR(2)
                                
        SELECT @ErrorMessage = ERROR_MESSAGE(), 
               @ErrorNumber = ERROR_NUMBER(), 
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE(), 
               @ErrorLine = ERROR_LINE(),
               @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
               @CRLF = CHAR(13) + CHAR(10);

        -- Raise the error that occured
        RAISERROR(@ErrorMessage, @ErrorSeverity, 1)
                
        DECLARE @rec VARCHAR(100),
                @sub VARCHAR(255),
                @msg VARCHAR(MAX)
                                
        SET @rec  = 'slance@Coreospitals.org;etipton@Coreospitals.org'
        SET @sub  = 'EZPMR Filed Worksheet Rollback for Market: ' + @MarketId + ' failed.'
        SET @msg  = 'Error in procedure ' + @ErrorProcedure + '.' + @CRLF +
                    'Error Number: ' + CAST(@ErrorNumber AS VARCHAR(50)) + @CRLF + 
                    'Error State: ' + CAST(@ErrorState AS VARCHAR(50)) + @CRLF + 
                    'Severity: ' + CAST(@ErrorSeverity AS VARCHAR(50)) + @CRLF + 
                    'Line Number: ' + CAST(@ErrorLine AS VARCHAR(50)) + @CRLF + 
                    'Message: ' + @ErrorMessage
                                
        -- Email support that the STORED PROC failed
        EXEC [msdb].[dbo].[sp_send_dbmail]
			@recipients = @rec,
			@subject = @sub,
			@body = @msg
	END CATCH
END

