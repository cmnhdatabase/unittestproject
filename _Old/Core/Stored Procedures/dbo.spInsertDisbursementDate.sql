﻿-- =============================================
-- Author:		Travis Poll
-- Create date: 1/9/2015
-- Description:	This procedure is used to add a new DisbursementDate
-- =============================================
Create PROCEDURE [dbo].[spInsertDisbursementDate]
@DisbursementDate DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.DisbursementDates
	        ( DisbursementDate )
	VALUES  ( @DisbursementDate )
END


