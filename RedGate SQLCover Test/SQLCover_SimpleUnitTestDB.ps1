﻿# If running your PowerShell script from a build tool, pass in these variables as parameters
$server='sqldev01\darrin'
$database='SimpleUnitTestDB'
 
Add-Type -Path ".\SQLCover.dll"
$connectionString = "server=$server;initial catalog=$database;integrated security=sspi"
$coverage = new-object SQLCover.CodeCoverage($connectionString, $database, $true, $false)
 
# This starts SQLCover. It uses XEvents to monitor activity on the database.
$coverage.Start()
 
# Between the Start and Stop, the tSQLt tests are run. 
# Any testing process that exercises the code (eg. NUnit, Selenium) can be used instead.
$testResults = $connectionString | Invoke-DlmDatabaseTests 
 
# We stop SQLCover now that we've run our tests.
$coverageResults = $coverage.Stop()
 
# Generate a basic single-page code coverage report
$coverageResults.Html() | Out-File (".\SQLCoverResults.html")